package servlets;

import iaau.BaseDb;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author focus
 */
public class OutOfList extends HttpServlet {

    private PreparedStatement statement = null;
    private ResultSet result = null;
    private Connection conn = null;
    private BaseDb base = null;
    ResultSetMetaData rsmt = null;
    private String query;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Out Of Student List</title>");
        out.println("<meta http-equiv=Content-Type content=text/html; charset=UTF-8>");
        out.println("<link rel=stylesheet href=../style/sis_style.css>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        out.println("<center><table width=80% border=1 align=center class=textBold");
        out.println("<thead class=label><tr height=30>" +
                "<th width=5% align=center bgcolor=#eeeeee valign=middle class=textBold" +
                "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;" +
                "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;" +
                "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>" +
                "#</th>");
        out.println("<th width=15% align=center bgcolor=#eeeeee valign=middle class=textBold" +
                "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;" +
                "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;" +
                "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>" +
                "Name</th>");
        out.println("<th width=25% align=center bgcolor=#eeeeee valign=middle class=textBold" +
                "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;" +
                "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;" +
                "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>" +
                "Surname</th>");
        out.println("<th width=10% align=center bgcolor=#eeeeee valign=middle class=textBold" +
                "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;" +
                "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;" +
                "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>" +
                "Group</th>");
        out.println("<th width=15% align=center bgcolor=#eeeeee valign=middle class=textBold" +
                "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;" +
                "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;" +
                "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>" +
                "Entering Order</th>");
        out.println("<th width=15% align=center bgcolor=#eeeeee valign=middle class=textBold" +
                "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;" +
                "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;" +
                "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>" +
                "Academic Order</th>");
        out.println("<th width=15% align=center bgcolor=#eeeeee valign=middle class=textBold" +
                "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;" +
                "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;" +
                "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>" +
                "Expulsion Order</th>");
        out.println("</tr></thead><tbody class=reportBody>");

        try {
            base = new BaseDb();
            conn = base.getConnection();

            query = "select student.name as name, student.surname as surname, sinif.name as gr, student.edu_order_number1 as prikaz1, " +
                    "student.edu_order_number2 as prikaz2, student.edu_order_number3 as prikaz3" +
                    " from student left join sinif on student.group_id=sinif.id" +
                    " where student.edu_status_id=4 order by gr, name, surname ";
            statement = conn.prepareStatement(query);
            result = statement.executeQuery();

            int colCount = result.getMetaData().getColumnCount();
            int j = 1;

            while (result.next()) {
                out.println("<tr><td align=center>"+j+"</td>");

                for (int i = 1; i <= colCount; i++) {
                    out.println("<td align=center>" + result.getString(i) + "</td>");
                }
                out.println("</tr>");
                j++;
            }
            out.println("</tbody></table></center></body></html>");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (base != null) {
                try {
                    base.close();
                } catch (SQLException e) {
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}