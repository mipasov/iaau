package servlets;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import iaau.DbStudent;
import iaau.Student;
import java.awt.Color;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author focus
 */
public class studentList extends HttpServlet {

    DbStudent st = null;
    Image img;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String group = request.getParameter("group");
        SimpleDateFormat df = new SimpleDateFormat("dd.MMMMMMMMMM.yyyy");
        st = new DbStudent();

        try {
            st.connect();
            st.execSQL_GR_Active(group);
            ArrayList<Student> stList = st.getArray();

            Document document = new Document(PageSize.A4, 10, 10, 10, 10);

            PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
            response.setContentType("application/pdf");
            document.open();

            PdfContentByte punder = writer.getDirectContentUnder();
            img = Image.getInstance("/usr/local/images/iaauLogoT.png");
            img.setAbsolutePosition(document.getPageSize().getWidth() / 4, document.getPageSize().getHeight() / 3);
            img.scaleAbsolute(300, 300);

            punder.addImage(img);

            Font big_font = new Font(Font.COURIER, 19, Font.BOLD);
            big_font.setColor(new Color(0x92, 0x90, 0x83));
            Font title_font = new Font(Font.COURIER, 13, Font.BOLD);
            title_font.setColor(new Color(0x92, 0x90, 0x83));
            Font warning = new Font(Font.COURIER, 10, Font.BOLD);
            warning.setColor(new Color(0xFF, 0x00, 0x00));
            Font in_font = new Font(Font.COURIER, 12, Font.BOLD);
            Font text_font = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);

            Paragraph iaau = new Paragraph("INTERNATIONAL ATATURK ALATOO UNIVERSITY", title_font);
            iaau.setAlignment(Element.ALIGN_CENTER);
            Paragraph sif = new Paragraph("STUDENT LIST", big_font);
            sif.setAlignment(Element.ALIGN_CENTER);
            document.add(iaau);
            document.add(sif);
            document.add(new Paragraph(15, " "));

            if (!stList.isEmpty()) {
                float[] Thead_colsWidth = {0.8f, 1.5f, 0.5f, 1.5f};
                PdfPTable Thead = new PdfPTable(4);
                Thead.setWidthPercentage(90f);
                Thead.setWidths(Thead_colsWidth);
                Thead.getDefaultCell().setBorder(0);
                Thead.addCell(new Phrase("Department:", in_font));
                Thead.addCell(new Phrase(stList.get(0).getDepartName(), text_font));
                Thead.addCell(new Phrase("Group:", in_font));
                Thead.addCell(new Phrase(stList.get(0).getGroupName(), text_font));
                document.add(Thead);
                document.add(new Paragraph(15, " "));

                float[] Tbody_colsWidth = {0.2f, 1.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f};
                PdfPTable Tbody = new PdfPTable(7);
                Tbody.setWidthPercentage(90f);
                Tbody.setWidths(Tbody_colsWidth);
                Tbody.getDefaultCell().setFixedHeight(16);
                Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                Tbody.addCell(new Phrase("#", in_font));
                Tbody.addCell(new Phrase("Name Surname", in_font));
                Tbody.addCell(new Phrase("status", in_font));
                Tbody.addCell(new Phrase("1", in_font));
                Tbody.addCell(new Phrase("2", in_font));
                Tbody.addCell(new Phrase("3", in_font));
                Tbody.addCell(new Phrase("4", in_font));

                for (int i = 0; i < stList.size(); i++) {
                    Tbody.addCell(new Phrase(Integer.toString(i + 1), text_font));
                    Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    Tbody.addCell(new Phrase(stList.get(i).getName() + " " + stList.get(i).getSurname(), text_font));
                    Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    Tbody.addCell(new Phrase(stList.get(i).getEducation(), text_font));
                    Tbody.addCell(" ");
                    Tbody.addCell(" ");
                    Tbody.addCell(" ");
                    Tbody.addCell(" ");
                }
                document.add(Tbody);
                document.add(new Paragraph(15, " "));
            } else {
                document.add(new Phrase("No records found", warning));
            }
            document.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
