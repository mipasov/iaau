package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import iaau.DbStudent_Attendance;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;
import java.util.ArrayList;

public class SubjectAttEvaluation extends HttpServlet {

    Connection conn = null;
    ResultSet result = null;
    PreparedStatement statement = null;
    ResultSetMetaData rsm = null;
    BaseDb base = null;
    DbStudent_Attendance dbStAtt = null;
    ArrayList<String> nameSurname = null;
    ArrayList<Integer> studentID = null;
    String department = null;
    String subjectName = null;
    String year = null;
    String semester = null;
    int hours = 0;

    public int getStudents(String subj, String y, String sem) throws SQLException {
        nameSurname = new ArrayList<String>();
        studentID = new ArrayList<Integer>();
        String query = "select s.id, concat(s.name,' ',s.surname) as stname, sb.name, sb.hours, y.year, sm.semester, d.name "
                + "from student as s "
                + "left join less_stud as l on s.id=l.student_id "
                + "left join subjects as sb on sb.id=l.subject_id "
                + "left join year as y on l.year_id=y.id "
                + "left join semester as sm on sb.sem_id=sm.id "
                + "left join department as d on sb.dept_id=d.id "
                + "where s.edu_status_id=1 and l.subject_id=? and l.year_id=? and l.sem_id=?";

        statement = conn.prepareStatement(query);
        statement.setString(1, subj);
        statement.setString(2, y);
        statement.setString(3, sem);
        result = statement.executeQuery();
        rsm = result.getMetaData();
        int colCount = rsm.getColumnCount();

        while (result.next()) {
            studentID.add(result.getInt("s.id"));
            nameSurname.add(result.getString("stname"));
            subjectName = result.getString("sb.name");
            hours = result.getInt("sb.hours");
            year = result.getString("y.year");
            semester = result.getString("sm.semester");
            department = result.getString("d.name");
        }
        return colCount;
    }

    public int getAttendance(String subj, String year, String sem, int stID, int week) throws SQLException {
        int attendance = 0;
        /*
         * String query = "select sum(attendance) " + "from attendance " +
         * "where subject_id=? and year_id=? and semester_id=? and student_id=?
         * and weeks_id<?;";
         */
        
        
          String query = "select sum(attendance) from attendance "
                  + "where subject_id=? and year_id=? and semester_id=? and student_id=?;";
         
        
        statement = conn.prepareStatement(query);
        statement.setString(1, subj);
        statement.setString(2, year);
        statement.setString(3, sem);
        statement.setInt(4, stID);
        //statement.setInt(5, week);
        result = statement.executeQuery();

        while (result.next()) {
            attendance = result.getInt("sum(attendance)");
        }
        return attendance;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        base = new BaseDb();
        dbStAtt = new DbStudent_Attendance();

        String yearID = (String) request.getParameter("year");
        String semID = (String) request.getParameter("semester");
        String[] subjID = request.getParameterValues("subjID");

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Evalution Report</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        //out.println("<center><table width=80% height=78 border=1" +
        //        " cellpadding=0 cellspacing=0 class=tealtable ><tr>");
        //out.println("<td><img src=../images/AttEvaReport.bmp width=622 height=78 alt=headline/></td></tr></table>");
        try {
            conn = base.getConnection();
            dbStAtt.connect();
            int week = 17;
            int attend = 0;

            for (int i = 0; i < subjID.length; i++) {
                int count = getStudents(subjID[i], yearID, semID);
                if (count != 0) {
                    out.println("<p> </p>");
                    out.println("<center><table width=80% align=center cellpadding=1 cellspacing=1 class=tealtable >");
                    out.println("<tr><td><b>Department : </b><td>" + department + "</td>");
                    out.println("<td><b>Subject : </b><td>" + subjectName + "</td></tr>");
                    out.println("<tr><td><b>Year : </b><td>" + year + "</td>");
                    out.println("<td><b>Semester : </b><td>" + semester + "</td></tr>");
                    out.println("<p> </p>");
                    out.println("<center>"
                            + "<table  width=80% align=center border=1"
                            + " cellpadding=0 cellspacing=0 class=tealtable >");
                    out.println("<tr>");
                    out.println("<th> NO  </th> <th> Name Surname </th> "
                            + "<th>Attendance </th><th>Status</th>");
                    out.println("</tr>");

                    for (int st = 0; st < studentID.size(); st++) {
                        attend = getAttendance(subjID[i], yearID, semID, studentID.get(st), week);

                        if (attend > (hours * week * 0.18)) {
                            out.println("<tr ><td align = center>" + (st + 1) + "</td><td>" + nameSurname.get(st) + "</td> <td align = center>" + attend + ""
                                    + "</td><td>F2</td></tr>");
                            dbStAtt.changeAttStatus(studentID.get(st), Integer.parseInt(subjID[i]), Integer.parseInt(yearID), Integer.parseInt(semID));
                        }//if
                        else {
                            out.println("<tr ><td align=center>" + (st + 1) + "</td><td>" + nameSurname.get(st) + "</td><td align = center>" + attend + ""
                                    + " </td><td>OK</td></tr>");
                        }//else
                    }//for st

                }
                out.println("</table></p></p>");
            } //for i
            out.println("</center></body></html>");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (base != null) {
                try {
                    base.close();
                } catch (SQLException ex) {
                }
            }
            if (dbStAtt != null) {
                try {
                    dbStAtt.close();
                } catch (SQLException ex) {
                }
            }
        }
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
