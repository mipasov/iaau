package servlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.DbStudYears;

public class addYear extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Year Inserted Successfully . Thank you";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String newYear = request.getParameter("newYear");
        DbStudYears dbStY = new DbStudYears();
        boolean status = false;
        try {
            dbStY.connect();
            dbStY.addYear(newYear);
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        } finally {
            if (status) {
                response.sendRedirect("yearDetails.jsp");
            } else {
                request.getSession().setAttribute("Message", errorMesg);
                response.sendRedirect("info.jsp");
            }

            if (dbStY != null) {
                try {
                    dbStY.close();
                } catch (Exception ex) {
                }
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
