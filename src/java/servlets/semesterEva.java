package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;
import iaau.DbStud_Less;
import java.util.ArrayList;

public class semesterEva extends HttpServlet {

    public static final String errorMesg = "No entries to display";
    private ArrayList<String> subjectIDs;
    private ArrayList<String> studentIDs;
    private ArrayList<String> stName;
    private ArrayList<String> stStatus;
    private String department;
    private String subjectName;
    private String yearName;
    private String semester;
    private Connection conn;
    private ResultSet result;
    private PreparedStatement statement;
    private ResultSetMetaData rsm;
    private BaseDb base;
    private DbStud_Less dbStudLess;

    public void subjects(int stdyear, String sem, String dept) throws SQLException {
        String query = "select id from subjects where dept_id=? and sem_id=? and stdyear=? and status = 1;";

        subjectIDs = new ArrayList<String>();

        statement = conn.prepareStatement(query);
        statement.setString(1, dept);
        statement.setString(2, sem);
        statement.setInt(3, stdyear);
        result = statement.executeQuery();

        while (result.next()) {
            subjectIDs.add(result.getString("id"));
        }
    }

    public void getInfo(String subj, String year, String sem) throws SQLException {
        String query = "select t1.id,concat(t1.name,' ',t1.surname)as name,t5.name,t5.hours,"
                + "t2.year,t7.name,t3.semester,t6.status"
                + " from less_stud as t6"
                + " left join subjects as t5 on t6.subject_id=t5.id"
                + " left join year as t2 on t6.year_id=t2.id"
                + " left join semester as t3 on t6.sem_id=t3.id"
                + " left join student as t1 on t6.student_id=t1.id"
                + " left join department as t7 on t5.dept_id=t7.id"
                + " where t1.edu_status_id = 1 and t5.id =? and t2.id =? and t3.id=? order by name;";

        studentIDs = new ArrayList<String>();
        stName = new ArrayList<String>();
        stStatus = new ArrayList<String>();

        statement = conn.prepareStatement(query);
        statement.setString(1, subj);
        statement.setString(2, year);
        statement.setString(3, sem);
        result = statement.executeQuery();

        while (result.next()) {
            studentIDs.add(result.getString("t1.id"));
            stName.add(result.getString("name"));
            stStatus.add(result.getString("t6.status"));
            subjectName = result.getString("t5.name");
            yearName = result.getString("t2.year");
            semester = result.getString("t3.semester");
            department = result.getString("t7.name");
        }//while
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String yearID = (String) session.getAttribute("yearID");
        String semID = (String) session.getAttribute("semID");
        String[] dept = request.getParameterValues("department");

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Semester Evalution Report</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        out.println("<center><table width=80% height=78 border=1"
                + " cellpadding=0 cellspacing=0 class=tealtable ><tr>");
        out.println("<td align=center><img src=../images/SemEvaReport.bmp width=100% height=78 alt=headline/></td></tr></table>");

        base = new BaseDb();
        dbStudLess = new DbStud_Less();

        try {
            conn = base.getConnection();
            dbStudLess.connect();
            for (int d = 0; d < dept.length; d++) {
                if (dept[d].equals("12")) {
                    subjects(1, semID, dept[d]);
                    for (int s = 0; s < subjectIDs.size(); s++) {
                        getInfo(subjectIDs.get(s), yearID, semID);
                        out.println("<p> </p>");
                        out.println("<center><table width=80% align=center cellpadding=1 cellspacing=1 class=tealtable >");
                        out.println("<tr><td><b>Department : </b><td>" + department + "</td>");
                        out.println("<td><b>Subject : </b><td>" + subjectName + "</td></tr>");
                        out.println("<tr><td><b>Year : </b><td>" + yearName + "</td>");
                        out.println("<td><b>Semester : </b><td>" + semester + "</td></tr>");
                        out.println("<tr><td><b>Course : </b></td><td>1</td></tr></table>");
                        out.println("<center><table  width=80% align=center border=1 cellpadding=0 cellspacing=0 class=tealtable >");
                        out.println("<tr>");
                        out.println("<th> NO  </th> <th> Name Surname </th><th>Status</th><th> Comment </th></tr>");
                        String nextY = Integer.toString(Integer.parseInt(yearID) + 1);
                        for (int st = 0; st < studentIDs.size(); st++) {
                            out.println("<tr>");
                            int stat = Integer.parseInt(stStatus.get(st));
                            if (stat == 4) {
                                dbStudLess.execRegistration(studentIDs.get(st), subjectIDs.get(s), Integer.toString(2), nextY, semID);
                                out.println("<td align=center>" + (st + 1) + "</td><td>" + stName.get(st)
                                        + "</td><td align=center>" + stStatus.get(st) + "</td>");
                                out.println("<td align=center>(Attendance)</td>");
                            } else if (stat == 5) {
                                dbStudLess.execRegistration(studentIDs.get(st), subjectIDs.get(s), Integer.toString(3), nextY, semID);
                                out.println("<td align=center>" + (st + 1) + "</td><td>" + stName.get(st) + "</td><td align=center>" + stStatus.get(st) + "</td>");
                                out.println("<td align=center>(Exam)</td>");
                            } else {
                                out.println("<td align=center>" + (st + 1) + "</td><td>" + stName.get(st) + "</td><td align=center>" + stStatus.get(st) + "</td>");
                                out.println("<td align=center>&nbsp;</td>");
                            }
                            out.println("</tr>");
                        }
                        out.println("</table></p></p>");
                    } //s
                } else {
                    for (int stdy = 2; stdy <= 5; stdy++) {
                        subjects(stdy, semID, dept[d]);
                        for (int s = 0; s < subjectIDs.size(); s++) {
                            getInfo(subjectIDs.get(s), yearID, semID);
                            out.println("<p> </p>");
                            out.println("<center><table width=80% align=center cellpadding=1 cellspacing=1 class=tealtable >");
                            out.println("<tr><td><b>Department : </b><td>" + department + "</td>");
                            out.println("<td><b>Subject : </b><td>" + subjectName + "</td></tr>");
                            out.println("<tr><td><b>Year : </b><td>" + yearName + "</td>");
                            out.println("<td><b>Semester : </b><td>" + semester + "</td></tr>");
                            out.println("<tr><td><b>Course : </b></td><td>" + stdy + "</td></tr></table>");
                            out.println("<center><table  width=80% align=center border=1 cellpadding=0 cellspacing=0 class=tealtable >");
                            out.println("<tr>");
                            out.println("<th> NO  </th> <th> Name Surname </th><th>Status</th><th> Comment </th></tr>");
                            String nextY = Integer.toString(Integer.parseInt(yearID) + 1);
                            for (int st = 0; st < studentIDs.size(); st++) {
                                out.println("<tr>");
                                int stat = Integer.parseInt(stStatus.get(st));
                                if (stat == 4) {
                                    dbStudLess.execRegistration(studentIDs.get(st), subjectIDs.get(s), Integer.toString(2), nextY, semID);
                                    out.println("<td align=center>" + (st + 1) + "</td><td>" + stName.get(st) + "</td><td align=center>" + stStatus.get(st) + "</td>");
                                    out.println("<td align=center>(Attendance)Registered to next year</td>");
                                } else if (stat == 5) {
                                    dbStudLess.execRegistration(studentIDs.get(st), subjectIDs.get(s), Integer.toString(3), nextY, semID);
                                    out.println("<td align=center>" + (st + 1) + "</td><td>" + stName.get(st) + "</td><td align=center>" + stStatus.get(st) + "</td>");
                                    out.println("<td align=center>(Failed)Registered to next year</td>");
                                } else {
                                    out.println("<td align=center>" + (st + 1) + "</td><td>" + stName.get(st) + "</td><td align=center>" + stStatus.get(st) + "</td>");
                                    out.println("<td align=center>&nbsp;</td>");
                                }
                            }
                            out.println("</table></p></p>");
                        } //s
                    }//stdy
                }//else
            }//for
        } catch (Exception e) {
            throw new ServletException(e.getMessage());
        } finally {
            if (base != null) {
                try {
                    base.close();
                } catch (SQLException ex) {
                }
            }
            if (dbStudLess != null) {
                try {
                    dbStudLess.close();
                } catch (SQLException ex) {
                }
            }
        }
        out.println("</center></body></html>");
        // out.println("<h1>Servlet Search at " + request.getContextPath () + "</h1>");
        out.close();
    }

    public void getData(String query) {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
