package servlets;

import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.DbStudent;
import java.util.Calendar;
import java.util.Date;

public class deleteStudent extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Student deleted successfully. Thank you";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String user = request.getRemoteUser();
        Calendar c = Calendar.getInstance();
        Date d = new Date();
        c.setTime(d);
        String time = c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND);
        String data = c.get(Calendar.YEAR) + "/" + c.get(Calendar.MONTH) + "/" + c.get(Calendar.DAY_OF_MONTH);
        DbStudent dbSt = null;
        String[] ids = request.getParameterValues("studID");
        String[] stName = request.getParameterValues("stName");
        String[] stSurname = request.getParameterValues("stSurname");
        boolean status = false;
        //String path = "/home/focus/Documents/delete.txt";

        //FileOutputStream fos = new FileOutputStream(path,true);
        //DataOutputStream dos = new DataOutputStream(fos);
        //dos.writeBytes("\n"+data+" "+time+" user:"+user);

        try {
            dbSt = new DbStudent();
            dbSt.connect();

            if (ids != null) {
                for (int i = 0; i < ids.length; i++) {
                    dbSt.deleteStudent(ids[i]);
                    //dos.writeBytes(" delete student: "+stName[i]+" "+stSurname[i]);
                }
            }
            //dos.flush();
            //dos.close();

            status = true;
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        } finally {
            if (status) {
                request.getSession().setAttribute("Message", succMesg);
            } else {
                request.getSession().setAttribute("Message", errorMesg);
            }

            response.sendRedirect("info.jsp");

            if (dbSt != null) {
                try {
                    dbSt.close();
                } catch (SQLException e) {
                }
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
