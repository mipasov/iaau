package servlets;

import iaau.BaseDb;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * @author focus
 */
public class GPA extends HttpServlet {
    Connection conn = null;
    PreparedStatement statement = null;
    ResultSet result = null;
    ResultSetMetaData rsm = null;
    BaseDb base = null;

    private ArrayList<String> nameSurname;
    private ArrayList<String> department;
    private ArrayList<String> studentID;
    private ArrayList<String> group;
    private ArrayList<String> stud_less_id = new ArrayList<String>();
    private ArrayList<String> subjectID = new ArrayList<String>();
    private ArrayList<String> subjectName = new ArrayList<String>();
    private ArrayList<String> subjectCode = new ArrayList<String>();
    private ArrayList<String> semester = new ArrayList<String>();
    private ArrayList<String> year = new ArrayList<String>();
    private ArrayList<Integer> s_l_id;
    private ArrayList<Integer> credits;
    private int failed;

    public void findStudents(String group_id) throws SQLException{
        studentID = new ArrayList<String>();
        nameSurname = new ArrayList<String>();
        department = new ArrayList<String>();
        group = new ArrayList<String>();
        String query = "select distinct(t1.id) as id, concat(t1.name,' ',t1.surname) as name, t2.code, t3.name" +
                " from student as t1" +
                " left join department as t2 on t1.dept_id=t2.id" +
                " left join sinif as t3 on t1.group_id=t3.id" +
                " where t1.edu_status_id=1 and t1.acad_status_id=1 and t1.group_id=?" +
                " order by name;";

        statement = conn.prepareStatement(query);
        statement.setString(1,group_id);
        result = statement.executeQuery();

        while(result.next()){
            studentID.add(result.getString("id"));
            nameSurname.add(result.getString("name"));
            department.add(result.getString("t2.code"));
            group.add(result.getString("t3.name"));
        }
    }

    public double findSubjects(String student_id) throws SQLException{
        double totAvr = 0;
        double totCr = 0;
        double temp = 0;
        failed = 0;

        s_l_id = new ArrayList<Integer>();
        credits = new ArrayList<Integer>();
        String query = "select t1.id as id,t2.credit as credit " +
                "from less_stud as t1 " +
                "left join subjects as t2 on t1.subject_id=t2.id " +
                "where t1.status < 4 and  t1.student_id=?";
        statement = conn.prepareStatement(query);
        statement.setString(1,student_id);
        result = statement.executeQuery();

        while(result.next()){
           credits.add(result.getInt("credit"));
           s_l_id.add(result.getInt("id"));
        }

        for (int i = 0; i < s_l_id.size(); i++){
            temp = average(s_l_id.get(i));
            if (temp == 0){
                failed++;
            }
            totAvr += temp * credits.get(i);
            totCr += credits.get(i);
        }

        return totAvr/totCr;
    }

    public double average(int less_stud_id) throws SQLException{
        double average = 0.0;
        String query = "select t3.exam_name as exam,(t4.mark * t3.percentage/100) as average from less_stud " +
                " as t5 left join student as  t1 on t5.student_id=t1.id left join " +
                " subjects as t2 on t5.subject_id=t2.id left join sinif as t6 on " +
                " t1.group_id=t6.id  left join subj_exam as t4 on t5.id = t4.stud_less_id " +
                " left join exam as t3 on t4.exam_id=t3.exam_id " +
                " where t5.id=?;";
        statement = conn.prepareStatement(query);
        statement.setInt(1,less_stud_id);
        result = statement.executeQuery();

        double midterm = 0,fin = 0,mup=0.0;
        while (result.next()) {
            String exam = result.getString("exam");
            if (result.wasNull()){
                midterm = 0.0; fin = 0.0; mup=0.0;
            }
            else if(exam.equals("Midterm")){
                midterm=result.getDouble("average");
            }
            else if(exam.equals("Final")){
                fin=result.getDouble("average");
                if(fin<29.7)
                    fin=0.0;
            }else if (exam.equals("MakeUp")){
                mup=result.getDouble("average");
                if(mup<29.7)
                    mup=0.0;
            }
            if((midterm+fin)<49.5) {
                average = midterm + mup;
            } else{
                average = midterm + fin;
            }
        }//while
        if (average <49.5){
            average = 0.0;
        }
            return average;
    }
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        String group_id = (String)request.getParameter("group");

        out.println("<html>");
        out.println("<head>");
        out.println("<title>GPA List</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        out.println("<center><table width=622 height=78 border=1" +
                " cellpadding=0 cellspacing=0 class=tealtable>");
        out.println("<tr><td>#</td><td>Dept</td><td>Group</td><td>Name Surname</td><td>GPA</td><td>Failed</td></tr>");

        try {
            base = new BaseDb();
            conn = base.getConnection();

            findStudents(group_id);

            for (int i = 0; i < studentID.size(); i++){
            double gpa = findSubjects(studentID.get(i));

            out.println("<tr><td>"+(i+1)+"</td><td>"+department.get(i)+"</td><td>"+group.get(i)+"</td>" +
                    "<td>"+nameSurname.get(i)+"</td><td>"+gpa+"</td><td>"+failed+"</td></tr>");
            }//for i

            out.println("</table></center></body></html>");
        }catch (Exception e) {           
                e.printStackTrace();           
        }finally{
            if(base != null)
                try{
                    base.close();
                }catch (SQLException ex){}
            out.close();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
