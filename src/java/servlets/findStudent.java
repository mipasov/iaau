package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;

public class findStudent extends HttpServlet {

    Connection conn = null;
    PreparedStatement statement = null;
    ResultSet result = null;
    BaseDb base = null;
    ResultSetMetaData rsmt = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String name = (String) request.getParameter("name");
        String type = (String) request.getParameter("type");
        String query = null;

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Student Search</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp<center>");
        out.println("<table width=100% height=124 align=center border=1 "
                + "cellpadding=0 cellspacing=0 class=textBold>");
        try {
            base = new BaseDb();
            conn = base.getConnection();

            switch (Integer.parseInt(type)) {
                case 1: {
                    out.println("<tr><td class=textBold width=100% align=center>#</td>"
                            + "<td class=textBold width=110 align=center>ID</td>"
                            + "<td class=textBold width=110 align=center>Name</td>"
                            + "<td class=textBold width=110 align=center>Surname</td>"
                            + "<td class=textBold width=110 align=center>Department</td>"
                            + "<td class=textBold width=110 align=center>Group</td>"
                            + "<td class=textBold width=110 align=center>Acad status</td>"
                            + "<td class=textBold width=110 align=center>Edu status</td>"
                            + "<td class=textBold width=110 align=center>Remark</td>"
                            + "<td class=textBold width=110 align=center>GHS type</td>"
                            + "<td class=textBold width=110 align=center>Country</td>"
                            + "<td class=textBold width=110 align=center>Oblast</td>"
                            + "<td class=textBold width=110 align=center>Region</td>"
                            + "<td class=textBold width=110 align=center>Entry year</td>"
                            + "<td class=textBold width=110 align=center>Birthday</td>"
                            + "<td class=textBold width=110 align=center>Gender</td>"
                            + "<td class=textBold width=110 align=center>Nation</td>"
                            + "<td class=textBold width=110 align=center>Phone</td>"
                            + "<td class=textBold width=110 align=center>Grad school</td>"
                            + "<td class=textBold width=110 align=center>Password</td>"
                            + "<td class=textBold width=110 align=center>Grad Project</td></tr>");

                    query = "select t1.id,t1.rollnum,t1.name,t1.surname, t2.code,t3.name, "
                            + "t4.name, t15.name, t5.name, t6.name, t7.name, t8.name, t9.name, t10.year, t1.birth_date, t11.code, t12.nationality, "
                            + "t1.phone, t13.name, t14.user_pass, t1.grad_project  from student as t1 "
                            + "left join department as t2 on t1.dept_id=t2.id "
                            + "left join sinif as t3 on t1.group_id=t3.id "
                            + "left join academic as t4 on t1.acad_status_id=t4.id "
                            + "left join remark as t5 on t1.remark_id=t5.id "
                            + "left join grad_school_type as t6 on t1.grad_school_type_id=t6.id "
                            + "left join country as t7 on t1.country_id=t7.id "
                            + "left join oblast as t8 on t1.oblast_id=t8.id "
                            + "left join region as t9 on t1.region_id=t9.id "
                            + "left join year as t10 on t1.enter_year_id=t10.id "
                            + "left join gender as t11 on t1.gender_id=t11.id "
                            + "left join nationality as t12 on t1.nationality_id=t12.id "
                            + "left join school as t13 on t1.school_id=t13.id "
                            + "left join users as t14 on t1.rollnum=t14.user_name "
                            + "left join education as t15 on t1.edu_status_id=t15.id "
                            + "where t1.name like ? or surname like ? ;";
                    break;
                }
                case 2: {
                    out.println("<tr><td class=textBold width=110 align=center>#</td>"
                            + "<td class=textBold width=110 align=center>ID</td>"
                            + "<td class=textBold width=110 align=center>Name</td>"
                            + "<td class=textBold width=110 align=center>Surname</td>"
                            + "<td class=textBold width=110 align=center>Level</td>"
                            + "<td class=textBold width=110 align=center>Status</td>"
                            + "<td class=textBold width=110 align=center>Country</td>"
                            + "<td class=textBold width=110 align=center>Region</td>"
                            + "<td class=textBold width=110 align=center>Oblast</td>"
                            + "<td class=textBold width=110 align=center>Phone</td>"
                            + "<td class=textBold width=110 align=center>Adr Pr</td>"
                            + "<td class=textBold width=110 align=center>Adr Curr</td>"
                            + "<td class=textBold width=110 align=center>Brth Pl</td>"
                            + "<td class=textBold width=110 align=center>Blood</td>"
                            + "<td class=textBold width=110 align=center>Nation</td>"
                            + "<td class=textBold width=110 align=center>Gender</td>"
                            + "<td class=textBold width=110 align=center>DoB</td>"
                            + "<td class=textBold width=110 align=center>Passport</td>"
                            + "<td class=textBold width=110 align=center>Email</td>"
                            + "<td class=textBold width=110 align=center>Faculty</td>"
                            + "<td class=textBold width=110 align=center>Dept</td>"
                            + "<td class=textBold width=110 align=center>Group</td>");


                    query = "select i.id,i.rollnum,i.name,i.surname,l.name,st.name,c.name,r.name,"
                            + "o.name,i.phone,i.permadr,i.curradr,i.birthpl,bl.code,n.nationality,"
                            + "g.code,i.birth_date,i.passport,i.email,f.code,d.code,s.name "
                            + "from instructor as i "
                            + "left join level as l on i.level_id = l.id "
                            + "left join status as st on i.status_id = st.id "
                            + "left join country as c on i.country_id = c.id "
                            + "left join region as r on i.region_id = r.id "
                            + "left join oblast as o on i.oblast_id = o.id "
                            + "left join blood as bl on i.blood_id = bl.id "
                            + "left join nationality as n on i.nationality_id = n.id "
                            + "left join gender as g on i.gender_id = g.id "
                            + "left join faculty as f on i.faculty_id = f.id "
                            + "left join department as d on i.dept_id = d.id "
                            + "left join sinif as s on i.group_id = s.id "
                            + "where i.name like ? or i.surname like ? ;";
                    break;
                }
            }

            statement = conn.prepareStatement(query);
            statement.setString(1, name + "%");
            statement.setString(2, name + "%");
            result = statement.executeQuery();
            rsmt = result.getMetaData();
            int colCount = rsmt.getColumnCount();

            if (colCount < 1) {
                out.println("<tr><td class=textBold width=110 align=center>No Students</td>");
            } else {
                while (result.next()) {
                    out.println("<tr>");
                    for (int i = 1; i <= colCount; i++) {
                        out.println("<td align=center class=textBold width=110>" + result.getString(i) + "</td>");
                    }
                    out.println("</tr>");
                }
            }
            out.println("</table></center></body></html>");
        } catch (Exception e) {
            throw new ServletException(e.getMessage());
        } finally {
            try {
                if (base != null) {
                    base.close();
                }
                out.close();
            } catch (SQLException sqle) {
            }
        }
        // out.println("<h1>Servlet Search at " + request.getContextPath () + "</h1>");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
