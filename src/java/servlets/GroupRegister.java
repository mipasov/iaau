package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.ResultSet;
import iaau.BaseDb;

public class GroupRegister extends HttpServlet {
    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Group Registration was completed successfully. Thank you";
    PreparedStatement statement = null;
    Connection conn = null;
    ResultSet result = null;
    BaseDb base = null;
    private String[] studentID;
    private String[] subjectID;
    
    public void getStudents(String groupID) throws SQLException {
        int count = 0;
        int index = 0;
        String query = "select count(distinct id) as count from student where group_id=? ;";
        statement = conn.prepareStatement(query);
        statement.setString(1,groupID);
        result = statement.executeQuery();
        while (result.next()) {
            count = result.getInt("count");
        }
        
        studentID = new String[count];
        query = "select id from student where group_id=? ;";        
        statement = conn.prepareStatement(query);
        statement.setString(1,groupID);
        result = statement.executeQuery();        
        while (result.next()) {
            studentID[index] = result.getString("id");
            index++;
        }
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        boolean status = false;
        String st = (String)request.getParameter("status");
        String stdyear = (String) request.getParameter("year");
        String semester = (String) request.getParameter("sem");
        String group = (String) request.getParameter("group");
        subjectID = request.getParameterValues("subjID");       
        base = new BaseDb();
        
        try {
            conn = base.getConnection();
            getStudents(group);
            
            if (subjectID != null) {
                for (int i = 0; i < subjectID.length; i++) {
                    for (int j = 0; j < studentID.length; j++) {
                        String query = "insert ignore into less_stud values('',?,?,?,?,?)";
                        statement = conn.prepareStatement(query);
                        statement.setString(1,studentID[j]);
                        statement.setString(2,subjectID[i]);
                        statement.setInt(3, Integer.parseInt(st));
                        statement.setString(4,stdyear);
                        statement.setString(5,semester);
                        statement.executeUpdate();
                    }
                }
                status = true;
            }
        } catch (Exception e) {            
                e.printStackTrace();
                status = false;
        } finally{
            
            if (status)
                request.getSession().setAttribute("Message", succMesg);
            else
                request.getSession().setAttribute("Message", errorMesg);
            response.sendRedirect("info.jsp");
            
            if(base != null)
                try{
                    base.close();
                }catch(Exception ex){}
            
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
