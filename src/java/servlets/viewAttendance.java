package servlets;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;
import iaau.DbInstructor;
import iaau.Instructor;
import java.awt.Color;
import java.util.ArrayList;

public class viewAttendance extends HttpServlet {

    Connection conn = null;
    PreparedStatement statement = null;
    ResultSet result = null;
    ResultSetMetaData rsm = null;
    BaseDb base = null;
    DbInstructor inst = null;
    String subjectName = null;
    String year = null;
    String semester = null;
    String department = null;
    String userNS = null;
    int hours = 0;
    ArrayList<String> studentName;
    ArrayList<String> studentID;
    ArrayList<String> studentGroup;
    ArrayList<Integer> studentAtt;

    public void getInfo(String subjID, String yearID, String semID) throws SQLException {
        String sql = "select t1.name,t1.hours, t2.name, t3.semester, t4.year from subjects as t1 "
                + "left join department as t2 on t1.dept_id=t2.id "
                + "left join semester as t3 on t1.sem_id=t3.id, "
                + "year as t4 "
                + "where t1.sem_id=? and t4.id=? and t1.id=?;";
        statement = conn.prepareStatement(sql);
        statement.setString(1, semID);
        statement.setString(2, yearID);
        statement.setString(3, subjID);
        result = statement.executeQuery();
        while (result.next()) {
            subjectName = result.getString("t1.name");
            year = result.getString("t4.year");
            semester = result.getString("t3.semester");
            department = result.getString("t2.name");
            hours = Integer.parseInt(result.getString("t1.hours"));
        }
    }

    public void getStudents(String subjID, String yearID, String semID) throws SQLException {
        studentID = new ArrayList<String>();
        studentName = new ArrayList<String>();
        studentGroup = new ArrayList<String>();
        studentAtt = new ArrayList<Integer>();

        String sql1 = "select sum(a.attendance) as sum,concat(t1.name,' ',t1.surname) as name,t2.student_id, t3.name from less_stud as t2 "
                + "left join student as t1 on t2.student_id=t1.id "
                + "left join sinif as t3 on t1.group_id=t3.id "
                + "left join attendance as a on a.student_id=t1.id "
                + "where a.subject_id=t2.subject_id and a.year_id=t2.year_id and a.semester_id=t2.sem_id "
                + "and t2.subject_id=? and t2.sem_id=? and t2.year_id=? and t2.status < 3 and t1.edu_status_id=1  group by a.student_id order by name";
        statement = conn.prepareStatement(sql1);
        statement.setString(1, subjID);
        statement.setString(2, semID);
        statement.setString(3, yearID);
        result = statement.executeQuery();
        while (result.next()) {
            studentAtt.add(result.getInt("sum"));
            studentID.add(result.getString("t2.student_id"));
            studentName.add(result.getString("name"));
            studentGroup.add(result.getString("t3.name"));
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        Image img;

        String subjID = request.getParameter("subjID");
        String yearID = (String) session.getAttribute("yearID");
        String semID = (String) session.getAttribute("semID");
        String weekID = (String) session.getAttribute("weekID");
        String user = request.getRemoteUser();

        try {
            base = new BaseDb();
            conn = base.getConnection();
            inst = new DbInstructor();
            inst.connect();
            inst.execSQLRN(user);
            ArrayList<Instructor> instList = inst.getArray();
            getInfo(subjID, yearID, semID);
            getStudents(subjID, yearID, semID);

            if ((instList.get(0).getInstructorRole().equals("secretary"))
                    || (instList.get(0).getInstructorRole().equals("oidb"))
                    || (instList.get(0).getInstructorRole().equals("studSecO"))) {
                userNS = "";
            } else {
                userNS = instList.get(0).getInstructorName() + " " + instList.get(0).getInstructorSurname();
            }

            Document document = new Document(PageSize.A4, 10, 10, 10, 10);

            PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
            response.setContentType("application/pdf");
            document.open();

            PdfContentByte punder = writer.getDirectContentUnder();
            img = Image.getInstance("/usr/local/images/iaauLogoT.png");
            img.setAbsolutePosition(document.getPageSize().getWidth() / 4, document.getPageSize().getHeight() / 3);
            img.scaleAbsolute(300, 300);

            punder.addImage(img);

            Font big_font = new Font(Font.COURIER, 19, Font.BOLD);
            big_font.setColor(new Color(0x92, 0x90, 0x83));
            Font title_font = new Font(Font.COURIER, 13, Font.BOLD);
            title_font.setColor(new Color(0x92, 0x90, 0x83));
            Font warning = new Font(Font.COURIER, 10, Font.BOLD);
            warning.setColor(new Color(0xFF, 0x00, 0x00));
            Font in_font = new Font(Font.COURIER, 12, Font.BOLD);
            Font text_font = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);

            Paragraph iaau = new Paragraph("INTERNATIONAL ATATURK ALATOO UNIVERSITY", title_font);
            iaau.setAlignment(Element.ALIGN_CENTER);
            Paragraph sif = new Paragraph("ATTENDANCE REPORT", big_font);
            sif.setAlignment(Element.ALIGN_CENTER);
            document.add(iaau);
            document.add(sif);
            document.add(new Paragraph(15, " "));

            if (!studentID.isEmpty()) {
                float[] Thead_colsWidth = {1.2f, 1.5f, 0.8f, 1.5f};
                PdfPTable Thead = new PdfPTable(4);
                Thead.setWidthPercentage(90f);
                Thead.setWidths(Thead_colsWidth);
                Thead.getDefaultCell().setBorder(0);
                Thead.addCell(new Phrase("Department:", in_font));
                Thead.addCell(new Phrase(department, text_font));
                Thead.addCell(new Phrase("Subject:", in_font));
                Thead.addCell(new Phrase(subjectName, text_font));
                Thead.addCell(new Phrase("Academic Year:", in_font));
                Thead.addCell(new Phrase(year, text_font));
                Thead.addCell(new Phrase("Semester:", in_font));
                Thead.addCell(new Phrase(semester, text_font));
                Thead.addCell(new Phrase("Current month:", in_font));
                Thead.addCell(new Phrase(weekID, text_font));
                Thead.addCell(new Phrase(""));
                Thead.addCell(new Phrase(""));
                document.add(Thead);
                document.add(new Paragraph(15, " "));

                float[] Tbody_colsWidth = {0.2f, 1.5f, 0.5f, 0.5f, 0.5f, 0.5f};
                PdfPTable Tbody = new PdfPTable(6);
                Tbody.setWidthPercentage(90f);
                Tbody.setWidths(Tbody_colsWidth);
                Tbody.getDefaultCell().setFixedHeight(16);
                Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                Tbody.addCell(new Phrase("#", in_font));
                Tbody.addCell(new Phrase("Name Surname", in_font));
                Tbody.addCell(new Phrase("Group", in_font));
                Tbody.addCell(new Phrase("Absents", in_font));
                Tbody.addCell(new Phrase("Left", in_font));
                Tbody.addCell(new Phrase("Status", in_font));

                double patt = Math.round(hours * 17 * 0.18);
                for (int i = 0; i < studentID.size(); i++) {
                    if (studentAtt.get(i) > (hours * 17 * 0.18)) {
                        Tbody.addCell(new Phrase(Integer.toString(i + 1), text_font));
                        Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                        Tbody.addCell(new Phrase(studentName.get(i), text_font));
                        Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                        Tbody.addCell(new Phrase(studentGroup.get(i), text_font));
                        Tbody.addCell(new Phrase(Integer.toString(studentAtt.get(i)), text_font));
                        Tbody.addCell(new Phrase(Double.toString((patt - studentAtt.get(i))), text_font));
                        Tbody.addCell(new Phrase("F2", text_font));
                    } else {
                        Tbody.addCell(new Phrase(Integer.toString(i + 1), text_font));
                        Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                        Tbody.addCell(new Phrase(studentName.get(i), text_font));
                        Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                        Tbody.addCell(new Phrase(studentGroup.get(i), text_font));
                        Tbody.addCell(new Phrase(Integer.toString(studentAtt.get(i)), text_font));
                        Tbody.addCell(new Phrase(Double.toString((patt - studentAtt.get(i))), text_font));
                        Tbody.addCell(new Phrase("OK", text_font));
                    }
                }
                document.add(Tbody);
                document.add(new Paragraph(15, " "));

                float[] Tfoot_colsWidth = {1.5f, 1f};
                PdfPTable Tfoot = new PdfPTable(2);
                Tfoot.setWidthPercentage(90f);
                Tfoot.setWidths(Tfoot_colsWidth);
                Tfoot.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                Tfoot.addCell(new Phrase("Name Surname : " + userNS, in_font));
                Tfoot.addCell(new Phrase("Signature :", in_font));
                document.add(Tfoot);

            } else {
                document.add(new Phrase("no records found", warning));
            }
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (base != null) {
                    base.close();
                }
                inst.close();
            } catch (SQLException sqle) {
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
