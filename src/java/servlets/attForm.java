package servlets;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import iaau.DbInstructor;
import iaau.DbSemester;
import iaau.DbStud_Less;
import iaau.DbStudYears;
import iaau.Instructor;
import iaau.Semester;
import iaau.Stud_Less;
import iaau.studYears;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author focus
 */
public class attForm extends HttpServlet {

    DbStud_Less db = null;
    DbStudYears db2 = null;
    DbSemester db3 = null;
    DbInstructor db4 = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sess = request.getSession();
        String userNS = null;

        Image img;
        db = new DbStud_Less();
        db2 = new DbStudYears();
        db3 = new DbSemester();
        db4 = new DbInstructor();

        String uname = null;
        String user = request.getRemoteUser();
        String subject = (String) request.getParameter("subjID");
        String year = (String) sess.getAttribute("yearID");
        String sem = (String) sess.getAttribute("semID");

        try {
            db.connect();
            db2.connect();
            db3.connect();
            db4.connect();

            db.execSQL_Subject(subject, year, sem);
            db2.execSQL_currYear();
            db3.execSQL_currSem();
            db4.execSQLRN(user);

            ArrayList<Stud_Less> list = db.getArray();
            ArrayList<studYears> yearList = db2.getArray();
            ArrayList<Semester> semList = db3.getArray();
            ArrayList<Instructor> instList = db4.getArray();

            if ((instList.get(0).getInstructorRole().equals("secretary"))
                    || (instList.get(0).getInstructorRole().equals("oidb"))
                    || (instList.get(0).getInstructorRole().equals("studSecO"))) {
                userNS = "";
            } else {
                userNS = instList.get(0).getInstructorName() + " " + instList.get(0).getInstructorSurname();
            }

            Document document = new Document(PageSize.A4, 10, 10, 10, 10);
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            PdfWriter writer = PdfWriter.getInstance(document, buffer);
            response.setContentType("application/pdf");
            document.open();

            PdfContentByte punder = writer.getDirectContentUnder();
            img = Image.getInstance("/usr/local/images/iaauLogoT.png");
            img.setAbsolutePosition(document.getPageSize().getWidth() / 4, document.getPageSize().getHeight() / 3);
            img.scaleAbsolute(300, 300);

            punder.addImage(img);

            Font big_font = new Font(Font.COURIER, 19, Font.BOLD);
            big_font.setColor(new Color(0x92, 0x90, 0x83));
            Font title_font = new Font(Font.COURIER, 13, Font.BOLD);
            title_font.setColor(new Color(0x92, 0x90, 0x83));
            Font warning = new Font(Font.COURIER, 10, Font.BOLD);
            warning.setColor(new Color(0xFF, 0x00, 0x00));
            Font in_font = new Font(Font.COURIER, 12, Font.BOLD);
            Font text_font = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);

            PdfPTable cells = new PdfPTable(17);
            cells.getDefaultCell().setFixedHeight(10);
            cells.getDefaultCell().setBorderWidthBottom(0);
            cells.getDefaultCell().setBorderWidthLeft(0);
            cells.getDefaultCell().setBorderWidthRight(1);
            cells.getDefaultCell().setBorderWidthTop(0);
            for (int c = 0; c < 16; c++) {
                cells.addCell(" ");
            }
            cells.getDefaultCell().setFixedHeight(10);
            cells.getDefaultCell().setBorderWidthBottom(0);
            cells.getDefaultCell().setBorderWidthLeft(0);
            cells.getDefaultCell().setBorderWidthRight(0);
            cells.getDefaultCell().setBorderWidthTop(0);
            cells.addCell(" ");

            Paragraph iaau = new Paragraph("INTERNATIONAL ATATURK ALATOO UNIVERSITY", title_font);
            iaau.setAlignment(Element.ALIGN_CENTER);
            Paragraph sif = new Paragraph("ATTENDANCE FORM", big_font);
            sif.setAlignment(Element.ALIGN_CENTER);
            document.add(iaau);
            document.add(sif);
            document.add(new Paragraph(15, " "));

            if (!list.isEmpty()) {

                float[] Thead_colsWidth = {1.2f, 1.5f, 0.8f, 1.5f};
                PdfPTable Thead = new PdfPTable(4);
                Thead.setWidthPercentage(90f);
                Thead.setWidths(Thead_colsWidth);
                Thead.getDefaultCell().setBorder(0);
                Thead.addCell(new Phrase("Department:", in_font));
                Thead.addCell(new Phrase(list.get(0).getStudDepartment(), text_font));
                Thead.addCell(new Phrase("Subject:", in_font));
                Thead.addCell(new Phrase(list.get(0).getSubjName(), text_font));
                Thead.addCell(new Phrase("Academic Year:", in_font));
                Thead.addCell(new Phrase(yearList.get(0).getYear(), text_font));
                Thead.addCell(new Phrase("Semester:", in_font));
                Thead.addCell(new Phrase(semList.get(0).getSemester(), text_font));
                document.add(Thead);
                document.add(new Paragraph(15, " "));

                float[] Tbody_colsWidth = {0.2f, 1f, 0.5f, 3f};
                PdfPTable Tbody = new PdfPTable(4);
                Tbody.setWidthPercentage(90f);
                Tbody.setWidths(Tbody_colsWidth);
                Tbody.getDefaultCell().setFixedHeight(16);
                Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                Tbody.addCell(new Phrase("#", in_font));
                Tbody.addCell(new Phrase("Name Surname", in_font));
                Tbody.addCell(new Phrase("Group", in_font));
                Tbody.addCell(cells);

                for (int i = 0; i < list.size(); i++) {
                    Tbody.addCell(new Phrase(Integer.toString(i + 1), text_font));
                    Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    Tbody.addCell(new Phrase(list.get(i).getStudName() + " " + list.get(i).getStudSurname(), text_font));
                    Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    Tbody.addCell(new Phrase(list.get(i).getGrpName(), text_font));
                    Tbody.addCell(cells);
                    if (i % 2 == 0) {
                        Tbody.getDefaultCell().setBackgroundColor(Color.LIGHT_GRAY);
                    } else {
                        Tbody.getDefaultCell().setBackgroundColor(Color.WHITE);
                    }
                }
                document.add(Tbody);
                document.add(new Paragraph(15, " "));

                float[] Tat_colsWidth = {0.5f, 2f};
                PdfPTable Tat = new PdfPTable(2);
                Tat.setWidthPercentage(90f);
                Tat.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                Tat.setWidths(Tat_colsWidth);
                Tat.getDefaultCell().setBorder(0);
                Tat.addCell(new Phrase("Attention: ", warning));
                Tat.addCell(new Phrase("Absent students are marked as '-'(minus sign)", warning));
                Tat.addCell(new Phrase(" "));
                Tat.addCell(new Phrase("Present students are marked as '/'(back slash)", warning));
                document.add(Tat);
                document.add(new Paragraph(15, " "));

                float[] Tfoot_colsWidth = {1.5f, 1f};
                PdfPTable Tfoot = new PdfPTable(2);
                Tfoot.setWidthPercentage(90f);
                Tfoot.setWidths(Tfoot_colsWidth);
                Tfoot.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                Tfoot.addCell(new Phrase("Name Surname : " + userNS, in_font));
                Tfoot.addCell(new Phrase("Signature :", in_font));
                document.add(Tfoot);
            } else {
                document.add(new Phrase("No records found", warning));
            }
            document.close();
            DataOutput output = new DataOutputStream(response.getOutputStream());
            byte[] bytes = buffer.toByteArray();
            response.setContentLength(bytes.length);
            for (int i = 0; i < bytes.length; i++) {
                output.writeByte(bytes[i]);
            }

        } catch (Exception e) {
            throw new ServletException("Your query is not working", e);
        } finally {
            //out.close();
            db.close();
            db2.close();
            db3.close();
            db4.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(attForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(attForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
