package servlets;

import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.DbFaculty;

public class addFaculty extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Faculty Inserted Successfully . Thank you";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String facultyName = request.getParameter("facultyName");
        String facultyCode = request.getParameter("facultyCode");
        DbFaculty dbFaculty = null;
        boolean status = false;
        try {
            dbFaculty = new DbFaculty();
            dbFaculty.connect();
            dbFaculty.execAddFaculty(facultyCode, facultyName);
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        } finally {
            if (status) {
                request.getSession().setAttribute("Message", succMesg);
            } else {
                request.getSession().setAttribute("Message", errorMesg);
            }

            response.sendRedirect("info.jsp");

            if (dbFaculty != null) {
                try {
                    dbFaculty.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                } //catch
            }
        }//finaly
    }// processRequest

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
