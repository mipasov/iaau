package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.sql.DataSource;

public class Servlet extends HttpServlet {

    Connection conn=null;
    Statement stmt=null;
    ResultSet rs=null;
    ResultSetMetaData rsm=null;    
    DataSource pool;
    
    @Override
    public void init() throws ServletException{
        Context env=null;
        try{
            env = (Context) new InitialContext().lookup("java:comp/env");
            pool=(DataSource)env.lookup("jdbc/iaauDB");
            if(pool == null)
                throw new ServletException(
                        "'iaauDB' is unknown DataSource");
        }catch(NamingException ne){
            throw new ServletException(ne.getMessage());
        }
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
         try{
            conn= pool.getConnection();
            stmt=conn.createStatement();
        /* TODO output your page here*/
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet Servlet</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Servlet Servlet at " + request.getContextPath () + "</h1>");
        out.println("</body>");
        out.println("</html>");         
        out.close();
         }catch(Exception e){
            throw new ServletException(e.getMessage());
        }finally{
            try{
                if(stmt != null)
                    stmt.close();
                if (conn !=null)
                    conn.close();
            }catch(SQLException sqle){sqle.getErrorCode();}
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
