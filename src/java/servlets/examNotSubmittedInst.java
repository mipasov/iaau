package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;
import iaau.DbInstructor;

public class examNotSubmittedInst extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        String user = request.getRemoteUser();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        ResultSetMetaData rsm = null;
        BaseDb base = null;
        DbInstructor inst = null;
        String sql = null;
        String sem = (String) session.getAttribute("semID");
        String year = (String) session.getAttribute("yearID");
        String exam = (String) session.getAttribute("examID");

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Not entered attendance list</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        out.println("<center><table><tr><td>Exam Period: " + exam + "</td></tr></table>");
        out.println("<table width=90% border=1 cellspacing=1 cellpadding=2 class=tealtable >");
        out.println("<tr><td><b>Department</td><td><b>Subject Code</td><td><b>Subject Name</td><td><b>Students</td><td><b>Instructor</td></tr>");
        try {
            base = new BaseDb();
            conn = base.getConnection();
            inst = new DbInstructor();
            inst.connect();
            inst.execSQLRN(user);

            if ((inst.q.get(0).getInstructorRole().equals("rector")) || (inst.q.get(0).getInstructorRole().equals("studSecO")) || (inst.q.get(0).getInstructorRole().equals("oidb"))) {
                sql = "select t4.code,t1.id,t1.name,t1.code,count(t3.id),concat(t5.name,' ',t5.surname) as instructor" +
                        " from less_stud as t2" +
                        " left join subjects as t1 on t2.subject_id=t1.id" +
                        " left join student as t3 on t2.student_id=t3.id" +
                        " left join department as t4 on t1.dept_id=t4.id " +
                        " left join subj_instructor as t6 on t6.subj_id=t1.id " +
                        " left join instructor as t5 on t6.inst_id=t5.id " +
                        " where t2.id not in (select stud_less_id from subj_exam where exam_id=?)" +
                        " and t1.status=1 and t3.edu_status_id=1 and t2.year_id=? and t2.sem_id=? and t2.status<4" +
                        " and t6.year_id=? group by t2.subject_id order by t4.code";
                statement = conn.prepareStatement(sql);
                statement.setString(1, exam);
                statement.setString(2, year);
                statement.setString(3, sem);
                statement.setString(4, year);

            } else if (inst.q.get(0).getInstructorRole().equals("dean")) {
                sql = "select t4.code,t1.id,t1.name,t1.code,count(t3.id),concat(t5.name,' ',t5.surname) as instructor" +
                        " from less_stud as t2" +
                        " left join subjects as t1 on t2.subject_id=t1.id" +
                        " left join student as t3 on t2.student_id=t3.id" +
                        " left join department as t4 on t1.dept_id=t4.id " +
                        " left join subj_instructor as t6 on t6.subj_id=t1.id " +
                        " left join instructor as t5 on t6.inst_id=t5.id " +
                        " where t2.id not in (select stud_less_id from subj_exam where exam_id=?)" +
                        " and t1.status=1 and t3.edu_status_id=1 and t2.year_id=? " +
                        " and t2.sem_id=? and t4.faculty_id=? and t2.status<4 and t6.year_id=?" +
                        " group by t2.subject_id order by t4.code";
                statement = conn.prepareStatement(sql);
                statement.setString(1, exam);
                statement.setString(2, year);
                statement.setString(3, sem);
                statement.setInt(4, inst.q.get(0).getInstructorF_id());
                statement.setString(5, year);

            } else if (inst.q.get(0).getInstructorRole().equals("hod")) {
                sql = "select t4.code,t1.id,t1.name,t1.code,count(t3.id),concat(t5.name,' ',t5.surname) as instructor" +
                        " from less_stud as t2" +
                        " left join subjects as t1 on t2.subject_id=t1.id" +
                        " left join student as t3 on t2.student_id=t3.id" +
                        " left join department as t4 on t1.dept_id=t4.id " +
                        " left join subj_instructor as t6 on t6.subj_id=t1.id " +
                        " left join instructor as t5 on t6.inst_id=t5.id " +
                        " where t2.id not in (select stud_less_id from subj_exam where exam_id=?)" +
                        " and t1.status=1 and t3.edu_status_id=1 and t2.year_id=? and" +
                        " t2.sem_id=? and t4.id=? and t2.status<4 and t6.year_id=?" +
                        " group by t2.subject_id order by t4.code";
                statement = conn.prepareStatement(sql);
                statement.setString(1, exam);
                statement.setString(2, year);
                statement.setString(3, sem);
                statement.setInt(4, inst.q.get(0).getInstructorD_id());
                statement.setString(5, year);
            }
            result = statement.executeQuery();
            rsm = result.getMetaData();
            int colCount = rsm.getColumnCount();

            if (colCount > 0) {
                while (result.next()) {
                    out.println("<tr>");
                    out.println("</td><td>" + result.getString("t4.code") + "</td><td>" + result.getString("t1.code") + "</td>" +
                            "<td>" + result.getString("t1.name") + "</td>" + "<td>" + result.getString("count(t3.id)") + "</td>" +
                            "<td>" + result.getString("instructor") + "</td>");
                    out.println("</tr>");
                }//while
            }//if
            out.println("</table></center></body></html>");
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (base != null) {
                    base.close();
                    inst.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
