package servlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.DbSchool;
import java.sql.SQLException;

public class updateSchool extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "School Updated Successfully . Thank you";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        DbSchool dbSch = null;
        boolean status = false;

        String id = request.getParameter("schoolID");
        String school = request.getParameter("schoolName");

        try {
            dbSch = new DbSchool();
            dbSch.connect();
            dbSch.updateSchool(school, id);
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        } finally {
            if (status) {
                session.setAttribute("Message", succMesg);
                response.sendRedirect("schoolDetails.jsp");
            } else {
                session.setAttribute("Message", errorMesg);
                response.sendRedirect("info.jsp");
            }
            if (dbSch != null) {
                try {
                    dbSch.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
