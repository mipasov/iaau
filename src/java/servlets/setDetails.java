package servlets;

import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;
import java.sql.ResultSet;

public class setDetails extends HttpServlet {

    static final String FAILURE_MESSAGE = "Your account is disabled. Contact Student Service.";

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        BaseDb base = null;
        String yearID = null;
        String semID = null;
        String weekID = null;
        String examID = null;
        String facultyID = null;
        String deptID = null;
        String groupID = null;
        String status = null;
        String username = request.getRemoteUser();

        String query = "select y.id,s.id,w.id,e.exam_id,inst.faculty_id,inst.dept_id,inst.group_id, users.status " +
                "from year as y,semester as s,weeks as w,exam as e, user_roles as u, instructor as inst, users " +
                "where y.curr=1 and s.curr=1 and w.curr=1 and e.curr=1 and inst.rollnum=u.user_name and inst.rollnum=users.user_name and u.user_name=?;";
        try {
            base = new BaseDb();
            Connection conn = base.getConnection();
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, username);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                yearID = Integer.toString(result.getInt("y.id"));
                semID = Integer.toString(result.getInt("s.id"));
                weekID = Integer.toString(result.getInt("w.id"));
                examID = Integer.toString(result.getInt("e.exam_id"));
                facultyID = Integer.toString(result.getInt("inst.faculty_id"));
                deptID = Integer.toString(result.getInt("inst.dept_id"));
                groupID = Integer.toString(result.getInt("inst.group_id"));
                status = Integer.toString(result.getInt("users.status"));
            }
        } catch (Exception e) {
                e.printStackTrace();
        } finally {
            if (status.equals("1")) {
                session.setAttribute("yearID", yearID);
                session.setAttribute("semID", semID);
                session.setAttribute("weekID", weekID);
                session.setAttribute("examID", examID);
                session.setAttribute("facultyID", facultyID);
                session.setAttribute("deptID", deptID);
                session.setAttribute("groupID", groupID);
                //session.setAttribute("username",username);

                response.sendRedirect("studSection.jsp");
            } else if (status.equals("0")) {
                request.getSession().setAttribute("Message", FAILURE_MESSAGE);
                response.sendRedirect("info.jsp");
            }
        }
    }

    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {

        doGet(request, response);
    }
}