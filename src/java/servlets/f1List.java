package servlets;

import iaau.BaseDb;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class f1List extends HttpServlet {

    static final String errorMesg = "You have to perform Mark Evaluation first!";
    private ArrayList<String> subjectIDs = new ArrayList<String>();
    private ArrayList<String> studentIDs = new ArrayList<String>();
    private ArrayList<String> studentGroup = new ArrayList<String>();
    private ArrayList<String> stName = new ArrayList<String>();
    private ArrayList<String> stStatus = new ArrayList<String>();
    private ArrayList<String> department = new ArrayList<String>();
    private ArrayList<String> subjectName = new ArrayList<String>();
    private ArrayList<String> subjectCode = new ArrayList<String>();
    private Connection conn = null;
    private ResultSet result = null;
    private PreparedStatement statement = null;
    private ResultSetMetaData rsm = null;
    private BaseDb base = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String yearID = (String) session.getAttribute("yearID");
        String semID = (String) session.getAttribute("semID");

        out.println("<html>");
        out.println("<head>");
        out.println("<title>F1 List</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        out.println("<center><table width=80% height=78 border=1"
                + " cellpadding=0 cellspacing=0 class=tealtable ><tr>");
        out.println("<p> </p>");
        out.println("<center><table width=90% align=center cellpadding=1 cellspacing=1 class=tealtable >");
        out.println("<tr>");
        out.println("<th>Dept</th><th>Group</th><th>Subj Name</th>"
                + "<th>Student</th><th>Status</th>");
        out.println("</tr>");
        try {
            base = new BaseDb();
            conn = base.getConnection();

            String query = "select t1.code, t2.name, t3.code, t3.name, concat(t4.name,' ',t4.surname) as stName, t5.status"
                    + " from student as t4 "
                    + " left join less_stud as t5 on t4.id=t5.student_id"
                    + " left join department as t1 on t4.dept_id=t1.id"
                    + " left join sinif as t2 on t4.group_id=t2.id"
                    + " left join subjects as t3 on t5.subject_id=t3.id"
                    + " where t3.status=1 and t4.edu_status_id=1 and t5.year_id=? and t5.sem_id=? and t5.status=5 "
                    + " order by t1.code, t2.name, t3.code;";
            statement = conn.prepareStatement(query);
            statement.setString(1, yearID);
            statement.setString(2, semID);
            result = statement.executeQuery();

            while (result.next()) {
                department.add(result.getString("t1.code"));
                studentGroup.add(result.getString("t2.name"));
                subjectName.add(result.getString("t3.name"));
                stName.add(result.getString("stName"));
                stStatus.add(result.getString("t5.status"));
            }
            if (!stName.isEmpty()) {
                for (int i = 0; i < stName.size() - 1; i++) {
                    out.println("<tr><td>" + department.get(i) + "</td>");
                    out.println("<td>" + studentGroup.get(i) + "</td>");
                    out.println("<td>" + subjectName.get(i) + "</td>");
                    out.println("<td>" + stName.get(i) + "</td>");
                    out.println("<td>" + stStatus.get(i) + "</td></tr>");
                }
            } else {
                request.getSession().setAttribute("Message", errorMesg);
                response.sendRedirect("info.jsp");
            }

        } catch (Exception e) {
            throw new ServletException(e.getMessage());
        } finally {
            if (base != null) {
                try {
                    base.close();
                    stName.clear();
                    department.clear();
                    studentGroup.clear();
                    subjectName.clear();
                    stStatus.clear();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        out.println("</table></center></body></html>");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
