package servlets;

import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.DbSubjects;

public class addSubject extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Subject Inserted Successfully . Thank you";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DbSubjects dbSubj = null;
        String subjName = request.getParameter("subjName");
        String subjHrs = request.getParameter("subjHrs");
        String subjCode = request.getParameter("subjCode");
        String department = request.getParameter("department");
        String year = request.getParameter("year");
        String semester = request.getParameter("semester");
        String credit = request.getParameter("credit");
        int subjStatus = Integer.parseInt(request.getParameter("status"));
        boolean status = false;

        try {
            dbSubj = new DbSubjects();
            dbSubj.connect();
            dbSubj.execAddSubj(subjName, subjHrs, subjCode, department, year, semester, credit, subjStatus);
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        } finally {
            if (status) {
                request.getSession().setAttribute("Message", succMesg);
            } else {
                request.getSession().setAttribute("Message", errorMesg);
            }

            response.sendRedirect("info.jsp");

            if (dbSubj != null) {
                try {
                    dbSubj.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
