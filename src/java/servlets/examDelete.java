package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.DbSubjExam;

public class examDelete extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Records Deleted Successfully . Thank you";
    Connection conn = null;
    PreparedStatement statement = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DbSubjExam dbSubjEx = null;
        String subjectID = request.getParameter("subjID");
        String exam = request.getParameter("exam");
        String[] SubjLessIDs = request.getParameterValues("SubjLessID");
        boolean status = false;

        try {
            dbSubjEx = new DbSubjExam();
            dbSubjEx.connect();
            for (int i = 0; i < SubjLessIDs.length; i++) {
                dbSubjEx.deleteSQL(SubjLessIDs[i], exam);
            }
            status = true;
        } catch (Exception e) {
            status = false;
        } finally {
            if (status) {
                request.getSession().setAttribute("Message", succMesg);
            } else {
                request.getSession().setAttribute("Message", errorMesg);
            }

            response.sendRedirect("info.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
