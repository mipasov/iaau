package servlets;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import iaau.BaseDb;
import java.awt.Color;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author focus
 */
public class GrandFinalTurkish extends HttpServlet {

    private Connection conn = null;
    private PreparedStatement statment = null;
    private ResultSet result = null;
    private ResultSetMetaData rsm = null;
    private BaseDb base = null;
    private ArrayList<Integer> subjectID1;
    private ArrayList<Integer> subjectID2;
    private ArrayList<String> nameSurname = null;
    private ArrayList<String> studentID = null;
    private ArrayList<String> group = null;
    private int grf = 0;

    public void getSubjectIDs() throws SQLException {
        subjectID1 = new ArrayList<Integer>();
        subjectID2 = new ArrayList<Integer>();
        String sql = "select s1.id, s2.id from subjects as s1 "
                + "inner join subjects as s2 on s2.name=s1.name "
                + "where s1.sem_id=1 and s2.sem_id=2 and s1.name like 'Turkish%' "
                + "and s1.stdyear=s2.stdyear and s1.stdyear=1 and s1.code=s2.code order by s1.code";
        statment = conn.prepareStatement(sql);
        result = statment.executeQuery();
        while (result.next()) {
            subjectID1.add(result.getInt("s1.id"));
            subjectID2.add(result.getInt("s2.id"));
        }
    }

    public void findStudents(int subj_id, String yearID, int semID) throws SQLException {
        studentID = new ArrayList<String>();
        nameSurname = new ArrayList<String>();
        group = new ArrayList<String>();

        String query = "select t2.id,concat(t2.name,' ',t2.surname) as name,t3.name from student as t2 "
                + "left join less_stud as t1 on t2.id=t1.student_id "
                + "left join sinif as t3 on t2.group_id=t3.id "
                + "where t1.subject_id=? and t1.year_id=? and t1.sem_id=? order by t2.name;";

        statment = conn.prepareStatement(query);
        statment.setInt(1, subj_id);
        statment.setString(2, yearID);
        statment.setInt(3, semID);
        result = statment.executeQuery();

        while (result.next()) {
            studentID.add(result.getString("t2.id"));
            nameSurname.add(result.getString("name"));
            group.add(result.getString("t3.name"));
        }
    }

    public double calcAverage(String s_id, int sem, String year, int subj_id) throws SQLException {

        String query = "select t3.exam_name as exam,(t4.mark * t3.percentage/100) as average from less_stud "
                + "as t5 left join student as  t1 on t5.student_id=t1.id left join "
                + "subjects as t2 on t5.subject_id=t2.id left join sinif as t6 on "
                + "t1.group_id=t6.id  left join subj_exam as t4 on t5.id = t4.stud_less_id "
                + "left join exam as t3 on t4.exam_id=t3.exam_id "
                + "where  t2.id=? and t5.year_id=? and t5.sem_id=? and t5.student_id =?;";

        double average = 0;

        statment = conn.prepareStatement(query);
        statment.setInt(1, subj_id);
        statment.setString(2, year);
        statment.setInt(3, sem);
        statment.setString(4, s_id);
        result = statment.executeQuery();

        int k = 0;
        double midterm = 0, fin = 0, mup = 0.0;
        while (result.next()) {
            String exam = result.getString("exam");
            if (result.wasNull()) {
                average = 0;
            } else if (exam.equals("Midterm")) {
                midterm = result.getDouble("average");
            } else if (exam.equals("Final")) {
                fin = result.getDouble("average");
                //if (fin < 29.7) {
                //    fin = 0.0;
                //}
            } else if (exam.equals("MakeUp")) {
                mup = result.getDouble("average");
                //if (mup < 29.7) {
                //    mup = 0.0;
                //}
            } else {
                grf = result.getInt("average");
            }
            if ((midterm + fin) < 49.5) {
                average = midterm + mup;
            } else {
                average = midterm + fin;
            }
        }
        return average;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();

        String yearID = (String) session.getAttribute("yearID");
        String semID = (String) session.getAttribute("semID");
        Image img;

        try {
            base = new BaseDb();
            conn = base.getConnection();

            Document document = new Document(PageSize.A4, 10, 10, 10, 10);
            PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
            response.setContentType("application/pdf");
            document.open();
            PdfContentByte punder = writer.getDirectContentUnder();
            img = Image.getInstance("/usr/local/images/iaauLogoT.png");
            img.setAbsolutePosition(document.getPageSize().getWidth() / 4, document.getPageSize().getHeight() / 3);
            img.scaleAbsolute(300, 300);
            punder.addImage(img);
            Font big_font = new Font(Font.COURIER, 19, Font.BOLD);
            big_font.setColor(new Color(0x92, 0x90, 0x83));
            Font title_font = new Font(Font.COURIER, 13, Font.BOLD);
            title_font.setColor(new Color(0x92, 0x90, 0x83));
            Font warning = new Font(Font.COURIER, 10, Font.BOLD);
            warning.setColor(new Color(0xFF, 0x00, 0x00));
            Font in_font = new Font(Font.COURIER, 12, Font.BOLD);
            Font text_font = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
            Paragraph iaau = new Paragraph("INTERNATIONAL ATATURK ALATOO UNIVERSITY", title_font);
            iaau.setAlignment(Element.ALIGN_CENTER);
            Paragraph sif = new Paragraph("GRAND FINAL TURKISH", big_font);
            sif.setAlignment(Element.ALIGN_CENTER);
            document.add(iaau);
            document.add(sif);
            document.add(new Paragraph(10, " "));

            getSubjectIDs();

            if (!subjectID1.isEmpty()) {

                float[] Tbody_colsWidth = {0.1f, 0.8f, 0.3f, 0.4f, 0.4f, 0.5f, 0.4f, 0.4f};
                PdfPTable Tbody = new PdfPTable(8);
                Tbody.setWidthPercentage(90f);
                Tbody.setWidths(Tbody_colsWidth);
                Tbody.getDefaultCell().setFixedHeight(16);
                Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                Tbody.addCell(new Phrase("#", in_font));
                Tbody.addCell(new Phrase("Name Surname", in_font));
                Tbody.addCell(new Phrase("Group", in_font));
                Tbody.addCell(new Phrase("Fall", in_font));
                Tbody.addCell(new Phrase("Spring", in_font));
                Tbody.addCell(new Phrase("GrandFinal", in_font));
                Tbody.addCell(new Phrase("Average", in_font));
                Tbody.addCell(new Phrase("Status", in_font));

                double fav = 0.0, sav = 0.0, res = 0.0;
                for (int i = 0; i < subjectID1.size(); i++) {
                    findStudents(subjectID2.get(i), yearID, 2);
                    for (int j = 0; j < studentID.size(); j++) {
                        grf = 0;
                        fav = calcAverage(studentID.get(j), 1, yearID, subjectID1.get(i));
                        sav = calcAverage(studentID.get(j), 2, yearID, subjectID2.get(i));
                        if (grf < 49.5) {
                            res = 0;
                        } else {
                            res = Math.round(((fav + sav) / 2) * 0.4 + (grf * 0.6));
                        }

                        Tbody.addCell(new Phrase(Integer.toString(j + 1), text_font));
                        Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                        Tbody.addCell(new Phrase(nameSurname.get(j), text_font));
                        Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                        Tbody.addCell(new Phrase(group.get(j), text_font));
                        Tbody.addCell(new Phrase(Double.toString(Math.round(fav)), text_font));
                        Tbody.addCell(new Phrase(Double.toString(Math.round(sav)), text_font));
                        Tbody.addCell(new Phrase(Double.toString(grf), text_font));
                        Tbody.addCell(new Phrase(Double.toString(res), text_font));

                        if (((fav + sav) / 2) * 0.4 + (grf * 0.6) >= 59.5) {
                            if (grf > 49.5) {
                                Tbody.addCell(new Phrase("Passed", text_font));
                            } else {
                                Tbody.addCell(new Phrase("Make Up", text_font));
                            }
                        } else {
                            Tbody.addCell(new Phrase("Make Up", text_font));
                        }
                    }
                }
                document.add(Tbody);
                document.add(new Paragraph(10, " "));

            } else {
                document.add(new Phrase("no records found"));
            }
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (base != null) {
                    base.close();
                }
            } catch (SQLException sqle) {
                sqle.getErrorCode();
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
