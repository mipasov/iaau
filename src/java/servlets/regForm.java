package servlets;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import iaau.DbInstructor;
import iaau.DbStud_Less;
import iaau.Instructor;
import iaau.Stud_Less;
import java.awt.Color;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author focus
 */
public class regForm extends HttpServlet {

    DbStud_Less st_l = null;
    DbInstructor inst = null;
    DbInstructor hod = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        Image img;
        String uname = null;
        String user = request.getRemoteUser();
        String student = (String) request.getParameter("studID");
        String subject = (String) request.getParameter("subjID");
        String year = (String) session.getAttribute("yearID");
        String sem = (String) session.getAttribute("semID");
        String hofd = null;
        int totalHr = 0;
        int totalCr = 0;
        st_l = new DbStud_Less();
        inst = new DbInstructor();
        hod = new DbInstructor();

        SimpleDateFormat df = new SimpleDateFormat("dd.MMMMMMMMMMM yyyy");

        try {
            st_l.connect();
            inst.connect();
            hod.connect();

            Document document = new Document(PageSize.A4, 10, 10, 10, 10);

            PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
            response.setContentType("application/pdf");
            document.open();

            PdfContentByte punder = writer.getDirectContentUnder();
            img = Image.getInstance("/usr/local/images/iaauLogoT.png");
            img.setAbsolutePosition(document.getPageSize().getWidth() / 4, document.getPageSize().getHeight() / 3);
            img.scaleAbsolute(300, 300);

            punder.addImage(img);

            Font big_font = new Font(Font.COURIER, 19, Font.BOLD);
            big_font.setColor(new Color(0x92, 0x90, 0x83));
            Font title_font = new Font(Font.COURIER, 13, Font.BOLD);
            title_font.setColor(new Color(0x92, 0x90, 0x83));
            Font warning = new Font(Font.COURIER, 10, Font.BOLD);
            warning.setColor(new Color(0xFF, 0x00, 0x00));
            Font in_font = new Font(Font.COURIER, 12, Font.BOLD);
            Font text_font = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);

            Paragraph iaau = new Paragraph("INTERNATIONAL ATATURK ALATOO UNIVERSITY", title_font);
            iaau.setAlignment(Element.ALIGN_CENTER);
            Paragraph sif = new Paragraph("SUBJECT REGISTRATION FORM", big_font);
            sif.setAlignment(Element.ALIGN_CENTER);
            document.add(iaau);
            document.add(sif);
            document.add(new Paragraph(15, " "));


            st_l.execSQL(student, sem, year);
            inst.execSQLRN(user);

            ArrayList<Stud_Less> list = st_l.getArray();
            ArrayList<Instructor> instList = inst.getArray();

            if (!list.isEmpty()) {

                hod.execSQLRD("hod", list.get(0).getStudDepID());
                ArrayList<Instructor> hodList = hod.getArray();

                float[] Thead_colsWidth = {1.2f, 1.5f, 1.3f, 1.5f};
                PdfPTable Thead = new PdfPTable(4);
                Thead.setWidthPercentage(90f);
                Thead.setWidths(Thead_colsWidth);
                Thead.getDefaultCell().setBorder(0);
                Thead.addCell(new Phrase("Academic Year:", in_font));
                Thead.addCell(new Phrase(list.get(0).getStudYear(), text_font));
                Thead.addCell(new Phrase("Name:", in_font));
                Thead.addCell(new Phrase(list.get(0).getStudName(), text_font));
                Thead.addCell(new Phrase("Semester:", in_font));
                Thead.addCell(new Phrase(list.get(0).getSemester(), text_font));
                Thead.addCell(new Phrase("Surname:", in_font));
                Thead.addCell(new Phrase(list.get(0).getStudSurname(), text_font));
                Thead.addCell(new Phrase("Faculty:", in_font));
                Thead.addCell(new Phrase(list.get(0).getFacultyName(), text_font));
                Thead.addCell(new Phrase("Student`s Group:", in_font));
                Thead.addCell(new Phrase(list.get(0).getGrpName(), text_font));
                Thead.addCell(new Phrase("Department:", in_font));
                Thead.addCell(new Phrase(list.get(0).getStudDepartment(), text_font));
                Thead.addCell(new Phrase("Date:", in_font));
                Thead.addCell(new Phrase(df.format(new java.util.Date()), text_font));
                document.add(Thead);
                document.add(new Paragraph(15, " "));

                float[] Tbody_colsWidth = {0.2f, 0.6f, 2f, 0.5f, 0.6f, 0.6f, 0.5f, 0.7f};
                PdfPTable Tbody = new PdfPTable(8);
                Tbody.setWidthPercentage(90f);
                Tbody.setWidths(Tbody_colsWidth);
                Tbody.getDefaultCell().setFixedHeight(16);
                Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                Tbody.addCell(new Phrase("#", in_font));
                Tbody.addCell(new Phrase("Code", in_font));
                Tbody.addCell(new Phrase("Subject Name", in_font));
                Tbody.addCell(new Phrase("Hour", in_font));
                Tbody.addCell(new Phrase("Credit", in_font));
                Tbody.addCell(new Phrase("Status", in_font));
                Tbody.addCell(new Phrase("Year", in_font));
                Tbody.addCell(new Phrase("Semester", in_font));

                for (int i = 0; i < list.size(); i++) {
                    Tbody.addCell(new Phrase(Integer.toString(i + 1), text_font));
                    Tbody.addCell(new Phrase(list.get(i).getSubjCode(), text_font));
                    Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    Tbody.addCell(new Phrase(list.get(i).getSubjName(), text_font));
                    Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    Tbody.addCell(new Phrase(list.get(i).getSubHour(), text_font));
                    Tbody.addCell(new Phrase(list.get(i).getSubCredit(), text_font));
                    Tbody.addCell(new Phrase(Integer.toString(list.get(i).getStatus()), text_font));
                    Tbody.addCell(new Phrase(list.get(i).getEdYear(), text_font));
                    Tbody.addCell(new Phrase(list.get(i).getSemester(), text_font));

                    totalHr += Integer.parseInt(list.get(i).getSubHour());
                    totalCr += Integer.parseInt(list.get(i).getSubCredit());
                }
                Tbody.addCell(new Phrase(" "));
                Tbody.addCell(new Phrase(" "));
                Tbody.addCell(new Phrase(" "));
                Tbody.addCell(new Phrase(Integer.toString(totalHr), text_font));
                Tbody.addCell(new Phrase(Integer.toString(totalCr), text_font));
                Tbody.addCell(new Phrase(" "));
                Tbody.addCell(new Phrase(" "));
                Tbody.addCell(new Phrase(" "));
                document.add(Tbody);
                document.add(new Paragraph(15, " "));

                float[] Tfoot_colsWidth = {1f, 1.5f, 1.5f};
                PdfPTable Tfoot = new PdfPTable(3);
                Tfoot.setWidthPercentage(90f);
                Tfoot.setWidths(Tfoot_colsWidth);
                Tfoot.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                Tfoot.addCell(new Phrase(" "));
                Tfoot.addCell(new Phrase("Name Surname", in_font));
                Tfoot.addCell(new Phrase("Signature", in_font));
                Tfoot.addCell(new Phrase("Student", in_font));
                Tfoot.addCell(new Phrase(list.get(0).getStudName() + " " + list.get(0).getStudSurname(), text_font));
                Tfoot.addCell(new Phrase(" "));
                Tfoot.addCell(new Phrase("Supervisor", in_font));
                Tfoot.addCell(new Phrase(instList.get(0).getInstructorName() + " " + instList.get(0).getInstructorSurname(), text_font));
                Tfoot.addCell(new Phrase(" "));
                Tfoot.addCell(new Phrase("HOD", in_font));
                Tfoot.addCell(new Phrase(hodList.get(0).getInstructorName() + " " + hodList.get(0).getInstructorSurname(), text_font));
                Tfoot.addCell(new Phrase(" "));
                document.add(Tfoot);


            } else {
                document.add(new Phrase("No records found", warning));
            }

            document.close();

        } catch (Exception e) {
            throw new ServletException("Your query is not working", e);
        } finally {
            try {
                st_l.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
