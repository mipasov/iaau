package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;
import iaau.DbStudent_Attendance;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;

public class depMarkEva extends HttpServlet {

    private ArrayList<Integer> subjectIDs;
    private ArrayList<Integer> studentIDs;
    private ArrayList<String> stName;
    private ArrayList<String> stStatus;
    private ArrayList<String> department = null;
    private ArrayList<String> subjectName = null;
    private String yearName = null;
    private ArrayList<String> semester = null;
    private Connection conn = null;
    private ResultSet result = null;
    private PreparedStatement statement = null;
    private ResultSetMetaData rsm = null;
    private BaseDb base = null;
    private DbStudent_Attendance dbStAtt = null;

    public void subjCount(String dept, String sem, int group) throws SQLException {
        subjectIDs = new ArrayList<Integer>();
        subjectName = new ArrayList<String>();
        semester = new ArrayList<String>();
        department = new ArrayList<String>();
        String query = "select s.id, s.name, sm.semester, d.name "
                + "from subjects as s "
                + "left join semester as sm on s.sem_id=sm.id "
                + "left join department as d on s.dept_id=d.id "
                + "where s.status=1 and s.dept_id=? and s.sem_id=? and s.stdyear=? ;";
        statement = conn.prepareStatement(query);
        statement.setString(1, dept);
        statement.setString(2, sem);
        statement.setInt(3, group);
        result = statement.executeQuery();

        while (result.next()) {
            subjectIDs.add(result.getInt("s.id"));
            subjectName.add(result.getString("s.name"));
            semester.add(result.getString("sm.semester"));
            department.add(result.getString("d.name"));
        }
    }

    public void getInfo(int subj, String year, String sem) throws SQLException {
        studentIDs = new ArrayList<Integer>();
        stName = new ArrayList<String>();
        stStatus = new ArrayList<String>();

        String query = "select t1.id,concat(t1.name,' ',t1.surname)as name,t5.name,t5.hours,t2.year,t7.name,t3.semester,t6.status "
                + "from less_stud as t6"
                + " left join subjects as t5 on t6.subject_id=t5.id"
                + " left join year as t2 on t6.year_id=t2.id"
                + " left join semester as t3 on t6.sem_id=t3.id"
                + " left join student as t1 on t6.student_id=t1.id"
                + " left join department as t7 on t5.dept_id=t7.id"
                + " where t1.edu_status_id=1 and t5.id =? and t2.id =? and t3.id=? and t6.status < 4 order by name;";

        statement = conn.prepareStatement(query);
        statement.setInt(1, subj);
        statement.setString(2, year);
        statement.setString(3, sem);
        result = statement.executeQuery();

        while (result.next()) {
            studentIDs.add(result.getInt("t1.id"));
            stName.add(result.getString("name"));
            stStatus.add(result.getString("t6.status"));
            yearName = result.getString("t2.year");
        }//while
    }

    public double calcAverage(int s_id, String sem, String year, int subj_id) throws SQLException {

        String query = "select t3.exam_name as exam,(t4.mark * t3.percentage/100) as average from less_stud as t5 "
                + "left join student as  t1 on t5.student_id=t1.id "
                + "left join subjects as t2 on t5.subject_id=t2.id "
                + "left join sinif as t6 on t1.group_id=t6.id "
                + "left join subj_exam as t4 on t5.id = t4.stud_less_id "
                + "left join exam as t3 on t4.exam_id=t3.exam_id "
                + "where  t2.id=? and t5.year_id=? and t5.sem_id=? and t5.student_id =? and t5.status<4;";

        double average = 0;

        statement = conn.prepareStatement(query);
        statement.setInt(1, subj_id);
        statement.setString(2, year);
        statement.setString(3, sem);
        statement.setInt(4, s_id);
        result = statement.executeQuery();
        rsm = result.getMetaData();
        int colcount = 0;
        colcount = rsm.getColumnCount();
        int k = 0;
        double midterm = 0, fin = 0, mup = 0.0;
        while (result.next()) {
            String exam = result.getString("exam");
            if (result.wasNull()) {
                average = 0;
            } else if (exam.equals("Midterm")) {
                midterm = result.getDouble("average");
            } else if (exam.equals("Final")) {
                fin = result.getDouble("average");
                if (fin < 29.7) {
                    fin = 0.0;
                }
            } else if (exam.equals("MakeUp")) {
                mup = result.getDouble("average");
                if (mup < 29.7) {
                    mup = 0.0;
                }
            }
            if ((midterm + fin) < 49.5) {
                average = midterm + mup;
            } else {
                average = midterm + fin;
            }
        }
        return average;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String yearID = (String) session.getAttribute("yearID");
        String semID = (String) session.getAttribute("semID");
        String[] dept = request.getParameterValues("department");
        base = new BaseDb();
        dbStAtt = new DbStudent_Attendance();

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Evalution Report</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        //out.println("<center><table width=622 height=78 border=1" +
        //        " cellpadding=0 cellspacing=0 class=tealtable ><tr>");
        //out.println("<td><img src=../images/MarkEvaReport.bmp width=622 height=78 alt=headline/></td></tr></table>");
        try {
            conn = base.getConnection();
            dbStAtt.connect();
            for (int d = 0; d < dept.length; d++) {
                if (dept[d].equals("12")) {
                    subjCount(dept[d], semID, 1);
                    for (int j = 0; j < subjectIDs.size(); j++) {
                        getInfo(subjectIDs.get(j), yearID, semID);
                        out.println("<p> </p>");
                        out.println("<center><table width=80% align=center cellpadding=1 cellspacing=1 class=tealtable ><tr><td><b>Department : </b><td>" + department.get(j) + "</td>");
                        out.println("<td><b>Subject : </b><td>" + subjectName.get(j) + "</td></tr>");
                        out.println("<tr><td><b>Year : </b><td>" + yearName + "</td>");
                        out.println("<td><b>Semester : </b><td>" + semester.get(j) + "</td></tr>");
                        out.println("<td><b>Course : </b><td>1</td></tr>");
                        out.println("<p> </p>");
                        out.println("<center>"
                                + "<table  width=80% align=center border=1"
                                + " cellpadding=0 cellspacing=0 class=tealtable >");
                        out.println("<tr>");
                        out.println("<th> NO  </th> <th> Name Surname </th> "
                                + "<th>Average </th><th>Status</th>");
                        out.println("</tr>");

                        for (int l = 0; l < studentIDs.size(); l++) {
                            double avr = calcAverage(studentIDs.get(l), semID, yearID, subjectIDs.get(j));
                            out.println("<tr>");
                            if (avr < 49.5) {
                                out.println("<tr ><td align = center>" + (l + 1) + "</td><td>" + stName.get(l) + "</td> <td align = center>" + avr + ""
                                        + "</td><td align=center>F1(" + stStatus.get(l) + ")</td></tr>");
                                dbStAtt.changeMarkStatus(studentIDs.get(l), subjectIDs.get(j), Integer.parseInt(yearID), Integer.parseInt(semID));
                            }//if
                            else {
                                out.println("<tr ><td align=center>" + (l + 1) + "</td><td>" + stName.get(l) + "</td><td align = center>" + avr + ""
                                        + " </td><td align=center>OK(" + stStatus.get(l) + ")</td></tr>");
                            }//else
                            out.println("</tr>");
                        } //for l
                        out.println("</table></p></p>");
                    } //for j
                } else {
                    for (int i = 2; i <= 5; i++) {
                        subjCount(dept[d], semID, i);
                        for (int j = 0; j < subjectIDs.size(); j++) {
                            getInfo(subjectIDs.get(j), yearID, semID);
                            out.println("<p> </p>");
                            out.println("<center><table width=80% align=center cellpadding=1 cellspacing=1 class=tealtable ><tr><td><b>Department : </b><td>" + department.get(j) + "</td>");
                            out.println("<td><b>Subject : </b><td>" + subjectName.get(j) + "</td></tr>");
                            out.println("<tr><td><b>Year : </b><td>" + yearName + "</td>");
                            out.println("<td><b>Semester : </b><td>" + semester.get(j) + "</td></tr>");
                            out.println("<td><b>Course : </b><td>" + (i) + "</td></tr>");
                            out.println("<p> </p>");
                            out.println("<center>"
                                    + "<table  width=80% align=center border=1"
                                    + " cellpadding=0 cellspacing=0 class=tealtable >");
                            out.println("<tr>");
                            out.println("<th> NO  </th> <th> Name Surname </th> "
                                    + "<th>Average </th><th>Status</th>");
                            out.println("</tr>");

                            for (int l = 0; l < studentIDs.size(); l++) {
                                double avr = calcAverage(studentIDs.get(l), semID, yearID, subjectIDs.get(j));
                                out.println("<tr>");
                                if (avr < 49.5) {
                                    out.println("<tr ><td align = center>" + (l + 1) + "</td><td>" + stName.get(l) + "</td> <td align = center>" + avr + ""
                                            + "</td><td align=center>F1(" + stStatus.get(l) + ")</td></tr>");
                                    dbStAtt.changeMarkStatus(studentIDs.get(l), subjectIDs.get(j), Integer.parseInt(yearID), Integer.parseInt(semID));
                                }//if
                                else {
                                    out.println("<tr ><td align=center>" + (l + 1) + "</td><td>" + stName.get(l) + "</td><td align = center>" + avr + ""
                                            + " </td><td align=center>OK(" + stStatus.get(l) + ")</td></tr>");
                                }//else
                                out.println("</tr>");
                            } //for l
                            out.println("</table></p></p>");
                        } //for j
                    } //for i
                }//else
            } // for d
            out.println("</center></body></html>");
        } catch (Exception e) {
            throw new ServletException(e.getMessage());
        } finally {
            try {
                if (base != null) {
                    base.close();
                }
            } catch (SQLException sqle) {
            }
        }
        // out.println("<h1>Servlet Search at " + request.getContextPath () + "</h1>");
        out.close();
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
