package servlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.DbExam;

public class setExam extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Current Exam Changed . Thank you";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String currExamID = request.getParameter("exam");
        DbExam dbEx = null;
        boolean status = false;
        try {
            dbEx = new DbExam();
            dbEx.connect();
            dbEx.execSetCurr_Exam(currExamID);
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        } finally {
            if (status) {
                session.setAttribute("Message", succMesg);
            } else {
                session.setAttribute("Message", errorMesg);
            }

            response.sendRedirect("info.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
