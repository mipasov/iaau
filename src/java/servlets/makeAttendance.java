package servlets;

import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.DbStudent_Attendance;

public class makeAttendance extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Record Inserted Successfully . Thank you";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        DbStudent_Attendance dbStAtt = null;

        String subject_id = request.getParameter("subjID");
        String year_id = request.getParameter("yearID");
        String semester_id = request.getParameter("semID");
        String week_id = request.getParameter("weekID");
        String[] studIDs = request.getParameterValues("studID");
        String[] attends = request.getParameterValues("attend");
        String[] query = new String[studIDs.length];

        boolean status = false;

        try {
            dbStAtt = new DbStudent_Attendance();
            dbStAtt.connect();

            for (int i = 0; i < studIDs.length; i++) {
                String currentAttendance = attends[i];
                if (currentAttendance == null || currentAttendance.equals("")) {
                    currentAttendance = "0";
                }
                dbStAtt.insertAttendance(subject_id, year_id, semester_id, week_id, studIDs[i], currentAttendance);
            }
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        } finally {
            if (status) {
                request.getSession().setAttribute("Message", succMesg);
            } else {
                request.getSession().setAttribute("Message", errorMesg);
            }

            response.sendRedirect("info.jsp");

            if (dbStAtt != null) {
                try {
                    dbStAtt.close();
                } catch (SQLException e) {
                }
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
