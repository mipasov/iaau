package servlets;

import iaau.DbStudent_Attendance;
import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;

public class deleteAttendance extends HttpServlet {

    static final String SUCCESS_MESSAGE = "Records Deleted Successfully . Thank you";
    static final String FAILURE_MESSAGE = "Please check the details and try again";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        String[] attendanceIDs = request.getParameterValues("attendanceID");
        String message = null;
        DbStudent_Attendance dbStud_Attendance = null;
        try {
            dbStud_Attendance = new DbStudent_Attendance();
            dbStud_Attendance.connect();
            for (int i = 0; i < attendanceIDs.length; i++) {
                int id = Integer.parseInt(attendanceIDs[i]);
                dbStud_Attendance.deleteSQL(id);
            }
            message = SUCCESS_MESSAGE;
        } catch (Exception exc) {
            message = FAILURE_MESSAGE;
        } finally {
            request.getSession().setAttribute("Message", message);
            response.sendRedirect("info.jsp");
            if (dbStud_Attendance != null) {
                try {
                    dbStud_Attendance.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
