package servlets;

import iaau.BaseDb;
import iaau.DbInstructor;
import iaau.Instructor;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class stgroup extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        String user = (String) request.getRemoteUser();
        PrintWriter out = response.getWriter();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        ResultSetMetaData rsm = null;
        BaseDb base = null;
        int total = 0;
        DbInstructor inst = null;
        String sql = null;

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Number of Students by Group</title>");
        out.println("<meta http-equiv=Content-Type content=text/html; charset=UTF-8>");
        out.println("<link rel=stylesheet href=../style/sis_style.css>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        out.println("<center><table width=50% align=center class=textBold class=example table-stripeclass:alternate>");
        out.println("<thead class=label><tr height=30>"
                + "<th width=5% align=center bgcolor=#eeeeee valign=middle class=textBold"
                + "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;"
                + "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;"
                + "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>"
                + "#</th>");
        out.println("<th width=30% align=center bgcolor=#eeeeee valign=middle class=textBold"
                + "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;"
                + "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;"
                + "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>"
                + "Department</th>");
        out.println("<th width=35% align=center bgcolor=#eeeeee valign=middle class=textBold"
                + "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;"
                + "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;"
                + "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>"
                + "Group</th>");
        out.println("<th width=30% align=center bgcolor=#eeeeee valign=middle class=textBold"
                + "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;"
                + "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;"
                + "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>"
                + "Sum</th>");
        out.println("</tr></thead><tbody class=reportBody>");

        try {
            base = new BaseDb();
            conn = base.getConnection();
            inst = new DbInstructor();
            inst.connect();
            inst.execSQLRN(user);
            ArrayList<Instructor> instList = inst.getArray();
            if ((instList.get(0).getInstructorRole().equals("rector"))
                    || (instList.get(0).getInstructorRole().equals("studSecO"))
                    || (instList.get(0).getInstructorRole().equals("oidb"))) {
                sql = "select t1.code,t2.name,count(distinct t3.id) as sum "
                        + "from department as t1 "
                        + "inner join sinif t2 on t1.id=t2.dept_id "
                        + "inner join student as t3 on t2.id=t3.group_id "
                        + "where t3.edu_status_id=1 "
                        + "group by t3.group_id order by t1.code, t2.name";
                statement = conn.prepareStatement(sql);

            } else if (instList.get(0).getInstructorRole().equals("dean")) {
                sql = "select t1.code,t2.name,count(distinct t3.id) as sum "
                        + "from department as t1 "
                        + "inner join sinif t2 on t1.id=t2.dept_id "
                        + "inner join student as t3 on t2.id=t3.group_id "
                        + "where t1.faculty_id=? and t3.edu_status_id=1 "
                        + "group by t3.group_id order by t1.code, t2.name";
                statement = conn.prepareStatement(sql);
                statement.setInt(1, instList.get(0).getInstructorF_id());

            } else if (instList.get(0).getInstructorRole().equals("hod")) {
                sql = "select t1.code,t2.name,count(distinct t3.id) as sum "
                        + "from department as t1 "
                        + "inner join sinif t2 on t1.id=t2.dept_id "
                        + "inner join student as t3 on t2.id=t3.group_id "
                        + "where t3.dept_id=? and t3.edu_status_id=1 "
                        + "group by t3.group_id order by t1.code, t2.name";
                statement = conn.prepareStatement(sql);
                statement.setInt(1, instList.get(0).getInstructorD_id());

            }
            result = statement.executeQuery();
            int index = 1;
            while (result.next()) {
                out.println("<tr align=center height=30><td>" + index + "</td>");
                out.println("<td>" + result.getString("t1.code") + "</td>");
                out.println("<td>" + result.getString("t2.name") + "</td>");
                out.println("<td>" + result.getString("sum") + "</td></tr>");
                total += result.getInt("sum");
                index++;
            }
            out.println("<table class=big><tr><td>Total : </td><td>" + total + " Students</td></tr></table>");
            out.println("</tbody></table>");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.println("</center></body></html>");
            out.close();
            try {
                base.close();
                inst.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
