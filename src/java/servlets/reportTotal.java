package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;

public class reportTotal extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        ResultSetMetaData rsm = null;
        BaseDb base = null;

        String query = "select department.code as dept,count(distinct (student.id)) as number from student "
                + "left join department on student.dept_id=department.id group by dept_id";

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Number of Students by Department</title>");
        out.println("<meta http-equiv=Content-Type content=text/html; charset=UTF-8>");
        out.println("<link rel=stylesheet href=../style/sis_style.css>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        out.println("<center><table width=50% align=center class=textBold class=example table-stripeclass:alternate>");
        out.println("<thead class=label><tr height=30>"
                + "<th width=5% align=center bgcolor=#eeeeee valign=middle class=textBold"
                + "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;"
                + "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;"
                + "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>"
                + "#</th>");
        out.println("<th width=30% align=center bgcolor=#eeeeee valign=middle class=textBold"
                + "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;"
                + "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;"
                + "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>"
                + "Department</th>");
        out.println("<th width=30% align=center bgcolor=#eeeeee valign=middle class=textBold"
                + "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;"
                + "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;"
                + "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>"
                + "Sum</th>");
        out.println("</tr></thead><tbody class=reportBody>");
        try {
            int sum = 0;
            int index = 1;
            base = new BaseDb();
            conn = base.getConnection();
            statement = conn.prepareStatement(query);
            result = statement.executeQuery();
            while (result.next()) {
                out.println("<tr align=center height=30><td>" + index + "</td>");
                out.println("<td align=center> " + result.getString("dept") + " </td><td align=center> " + result.getString("number") + " </td>");
                sum += Integer.parseInt(result.getString("number"));
                out.println("</tr>");
                index++;
            }
            out.println("<table align=center class=big><tr><td>Total : " + sum + " Students</td></tr></table>");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.println("</center></body></html>");
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
