package servlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.DbSystem_Info;

public class sysInfo extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Information Inserted Successfully . Thank you";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String info = request.getParameter("info");
        String button = request.getParameter("button");
        String[] id = request.getParameterValues("infoID");
        DbSystem_Info sysIn = null;
        boolean status = false;

        try {
            sysIn = new DbSystem_Info();
            sysIn.connect();

            if (button.equals("Submit")) {
                sysIn.insertSQL(info);
            } else if (button.equals("Delete")) {
                for (int i = 0; i < id.length; i++) {
                    sysIn.deleteSQL(Integer.valueOf(id[i]).intValue());
                }
            } //            else if (button.equals("Update"))
            //             {
            //                for (int i = 0; i<id.length; i++)
            //               sysIn.updateSQL(info,Integer.valueOf(id[i]).intValue());
            //          }
            else if (button.equals("View")) {
                sysIn.setNullSQL();
                for (int i = 0; i < id.length; i++) {
                    sysIn.viewSQL(Integer.valueOf(id[i]).intValue());
                }
            }
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        } finally {
            if (status) {
                request.getSession().setAttribute("Message", succMesg);
            } else {
                request.getSession().setAttribute("Message", errorMesg);
            }

            response.sendRedirect("info.jsp");

            if (sysIn != null) {
                try {
                    sysIn.close();
                } catch (Exception ex) {
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
