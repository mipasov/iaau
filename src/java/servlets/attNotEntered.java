package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;
import iaau.DbInstructor;

public class attNotEntered extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        String user = request.getRemoteUser();
        PrintWriter out = response.getWriter();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        ResultSetMetaData rsm = null;
        BaseDb base = null;
        DbInstructor inst = null;
        String year = (String) session.getAttribute("yearID");
        String sem = (String) session.getAttribute("semID");
        String week = (String) session.getAttribute("weekID");
        String sql = null;

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Not entered attendance list</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");

        out.println("<center><table width=50% class=tealtable >");
        out.println("<tr><td><b>Attendance Not entered for the: " + week + "<b> Month</td></tr>");
        out.println("</table></center><br>");

        out.println("<center><table width=90% border=1 cellspacing=1 cellpadding=2 class=tealtable >");
        out.println("<tr><td><b>Department</td><td><b>Subject Code</td><td><b>Subject Name</td><td><b>Instructor</td></tr>");
        try {
            base = new BaseDb();
            conn = base.getConnection();
            inst = new DbInstructor();
            inst.connect();
            inst.execSQLRN(user);
            if ((inst.q.get(0).getInstructorRole().equals("rector"))
                    || (inst.q.get(0).getInstructorRole().equals("studSecO"))
                    || (inst.q.get(0).getInstructorRole().equals("oidb"))) {
                sql = "select t1.code,t2.id,t2.code,t2.id,t2.name,concat(t5.name,' ',t5.surname) as instructor"
                        + " from subjects as t2"
                        + " left join department as t1 on t2.dept_id=t1.id "
                        + " left join attendance as t3 on t1.id=t3.subject_id"
                        + " left join subj_instructor as t4 on t2.id=t4.subj_id"
                        + " left join instructor as t5 on t4.inst_id=t5.id"
                        + " where t2.id not in (select subject_id from attendance where year_id=? and semester_id=? and weeks_id=?) "
                        + " and t2.status=1 and t4.year_id=? and t2.sem_id=? group by t2.id order by t1.code;";
                statement = conn.prepareStatement(sql);
                statement.setString(1, year);
                statement.setString(2, sem);
                statement.setString(3, week);
                statement.setString(4, year);
                statement.setString(5, sem);

            } else if (inst.q.get(0).getInstructorRole().equals("dean")) {
                sql = "select t1.code,t2.id,t2.code,t2.id,t2.name,concat(t5.name,' ',t5.surname) as instructor"
                        + " from subjects as t2"
                        + " left join department as t1 on t2.dept_id=t1.id "
                        + " left join attendance as t3 on t1.id=t3.subject_id"
                        + " left join subj_instructor as t4 on t2.id=t4.subj_id"
                        + " left join instructor as t5 on t4.inst_id=t5.id"
                        + " left join faculty as f on t5.faculty_id=f.id"
                        + " where t2.id not in (select subject_id from attendance where year_id=? and semester_id=? and weeks_id=?) "
                        + " and t4.year_id=? and t1.faculty_id=? and t2.sem_id=? group by t2.id order by t1.code;";
                statement = conn.prepareStatement(sql);
                statement.setString(1, year);
                statement.setString(2, sem);
                statement.setString(3, week);
                statement.setString(4, year);
                statement.setInt(5, inst.q.get(0).getInstructorF_id());
                statement.setString(6, sem);

            } else if (inst.q.get(0).getInstructorRole().equals("hod")) {
                sql = "select t1.code,t2.id,t2.code,t2.id,t2.name,concat(t5.name,' ',t5.surname) as instructor from subjects as t2"
                        + " left join department as t1 on t2.dept_id=t1.id "
                        + " left join attendance as t3 on t1.id=t3.subject_id"
                        + " left join subj_instructor as t4 on t2.id=t4.subj_id"
                        + " left join instructor as t5 on t4.inst_id=t5.id"
                        + " where t2.id not in (select subject_id from attendance where year_id=? and semester_id=? and weeks_id=?) "
                        + " and t2.status=1 and t4.year_id=? and t1.id=? and t2.sem_id=? group by t2.id order by t1.code;";
                statement = conn.prepareStatement(sql);
                statement.setString(1, year);
                statement.setString(2, sem);
                statement.setString(3, week);
                statement.setString(4, year);
                statement.setInt(5, inst.q.get(0).getInstructorD_id());
                statement.setString(6, sem);
            }

            result = statement.executeQuery();
            rsm = result.getMetaData();
            //out.println("<tr>");
            int colCount = rsm.getColumnCount();
            if (colCount > 0) {
                while (result.next()) {
                    out.println("<tr>");
                    out.println("<td>" + result.getString("t1.code") + "</td>" + "<td>" + result.getString("t2.code") + "</td>"
                            + "<td>" + result.getString("t2.name") + "</td>" + "<td>" + result.getString("instructor") + "</td>");
                    out.println("</tr>");
                }//while
            }//if
            out.println("</table></center></body></html>");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (base != null) {
                try {
                    base.close();
                    inst.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
