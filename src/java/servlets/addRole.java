package servlets;

import iaau.BaseDb;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class addRole extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Record inserted successfully. Thank you";
    String message = errorMesg;
    Connection conn = null;
    PreparedStatement statement = null;
    ResultSet result = null;
    ResultSetMetaData rsm = null;
    BaseDb base = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String button = request.getParameter("button");
        String userRole[] = request.getParameterValues("ur");
        String user = request.getParameter("rollnum");
        String sql = null;

        try {
            base = new BaseDb();
            conn = base.getConnection();

            if (userRole.length != 0) {
                if (button.equals("Add Role")) {
                    for (int i = 0; i < userRole.length; i++) {
                        sql = "insert ignore into user_roles values(?,?)";
                        statement = conn.prepareStatement(sql);
                        statement.setString(1, user);
                        statement.setString(2, userRole[i]);
                        statement.executeUpdate();
                        message = succMesg;
                    }
                } else if (button.equals("Remove Role")) {
                    for (int i = 0; i < userRole.length; i++) {
                        sql = "delete from user_roles where user_name=? and role_name=?";
                        statement = conn.prepareStatement(sql);
                        statement.setString(1, user);
                        statement.setString(2, userRole[i]);
                        statement.executeUpdate();
                        message = succMesg;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (base != null) {
                try {
                    base.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            request.getSession().setAttribute("Message", message);
            response.sendRedirect("info.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
