package servlets;

import java.io.*;
import iaau.DbGroup;
import javax.servlet.*;
import javax.servlet.http.*;

public class updateGroup extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Group Updated Successfully . Thank you";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String grID = request.getParameter("grID");
        String grName = request.getParameter("name");
        String deptID = request.getParameter("department");
        boolean status = false;
        DbGroup dbGroup = null;

        try {
            dbGroup = new DbGroup();

            dbGroup.connect();

            dbGroup.updateGroup(grID, grName, deptID);

            status = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (status) {
                request.getSession().setAttribute("Message", succMesg);
            } else {
                request.getSession().setAttribute("Message", errorMesg);
            }

            response.sendRedirect("info.jsp");

            if (dbGroup != null) {
                try {
                    dbGroup.close();
                } catch (Exception ex) {
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
