package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import iaau.BaseDb;
import iaau.DbStudent_Attendance;

public class updateStatus extends HttpServlet {

    static final String SUCCESS_MESSAGE = "Statuses Updated Successfully.";
    static final String FAILURE_MESSAGE = "Please check the details and try again.";
    Connection conn = null;
    BaseDb base = null;
    DbStudent_Attendance dbStAtt = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String message = null;
        base = new BaseDb();
        dbStAtt = new DbStudent_Attendance();

        String year = (String) session.getAttribute("yearID");
        String sem = (String) session.getAttribute("semID");
        String[] studID = request.getParameterValues("studID");
        String[] status = request.getParameterValues("status");
        String subjID = request.getParameter("subjID");

        try {
            conn = base.getConnection();
            dbStAtt.connect();

            for (int i = 0; i < studID.length; i++) {
                dbStAtt.setStatus(Integer.parseInt(studID[i]), Integer.parseInt(subjID), Integer.parseInt(year), Integer.parseInt(sem), Integer.parseInt(status[i]));
            }//for
            message = SUCCESS_MESSAGE;
        } catch (Exception e) {
            message = FAILURE_MESSAGE;
            //throw new ServletException(e.getMessage());
        } finally {
            request.getSession().setAttribute("Message", message);
            response.sendRedirect("info.jsp");
            if (base != null) {
                try {
                    base.close();
                } catch (SQLException ex) {
                }
            }
            if (dbStAtt != null) {
                try {
                    dbStAtt.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
