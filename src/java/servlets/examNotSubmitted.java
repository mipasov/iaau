package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;

public class examNotSubmitted extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        ResultSetMetaData rsm = null;
        BaseDb base = null;
        String year = (String) session.getAttribute("yearID");
        String sem = (String) session.getAttribute("semID");
        String exam = (String) session.getAttribute("examID");

        String query = "select t4.code,t1.name,t1.code,count(t3.id) "
                + "from less_stud as t2 "
                + "left join subjects as t1 on t2.subject_id=t1.id "
                + "left join student as t3 on t2.student_id=t3.id "
                + "left join department as t4 on t1.dept_id=t4.id "
                + "where t2.id not in (select stud_less_id from subj_exam where exam_id=?) "
                + "and t1.status=1 and t3.edu_status_id=1 and t2.year_id=? and t2.sem_id=? "
                + "and t2.status<4 group by t2.subject_id order by t4.code;";

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Not entered attendance list</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        out.println("<center><table width=90% border=1 cellspacing=1 cellpadding=2 class=tealtable >");
        out.println("<tr><td><b>Department</td><td><b>Subject Code</td><td><b>Subject Name</td><td><b>Students</td></tr>");
        try {
            base = new BaseDb();
            conn = base.getConnection();
            statement = conn.prepareStatement(query);
            statement.setString(1, exam);
            statement.setString(2, year);
            statement.setString(3, sem);

            result = statement.executeQuery();
            rsm = result.getMetaData();
            int colCount = rsm.getColumnCount();

            if (colCount > 0) {
                while (result.next()) {
                    out.println("<tr>");
                    out.println("<td>" + result.getString("t4.code") + "</td><td>" + result.getString("t1.code") + "</td>"
                            + "<td>" + result.getString("t1.name") + "</td>" + "<td>" + result.getString("count(t3.id)") + "</td>");
                    out.println("</tr>");
                }//while
            }//if
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.println("</table></center></body></html>");
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
