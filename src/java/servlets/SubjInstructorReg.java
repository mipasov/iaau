package servlets;

import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import iaau.BaseDb;

public class SubjInstructorReg extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Registration coplited successfully. Thank you";
    private PreparedStatement statement = null;
    private ResultSet result = null;
    private Connection conn = null;
    private BaseDb base = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();

        String instID = (String) request.getParameter("instructorID");
        String[] subjID = request.getParameterValues("subjID");
        String year = (String) session.getAttribute("yearID");
        boolean status = false;
        try {
            base = new BaseDb();
            conn = base.getConnection();
            if (subjID != null) {
                for (int i = 0; i < subjID.length; i++) {
                    String sql = "insert ignore into subj_instructor(inst_id,subj_id,year_id) values(?,?,?);";

                    statement = conn.prepareStatement(sql);
                    statement.setString(1, instID);
                    statement.setString(2, subjID[i]);
                    statement.setString(3, year);
                    statement.executeUpdate();
                }
            }
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (status) {
                request.getSession().setAttribute("Message", succMesg);
            } else {
                request.getSession().setAttribute("Message", errorMesg);
            }

            response.sendRedirect("info.jsp");

            if (base != null) {
                try {
                    base.close();
                } catch (SQLException e) {
                }
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
