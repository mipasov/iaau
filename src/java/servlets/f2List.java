package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;
import java.util.ArrayList;

public class f2List extends HttpServlet {

    private ArrayList<String> subjectIDs;
    private ArrayList<String> studentIDs;
    private ArrayList<String> studentGroup;
    private ArrayList<String> stName;
    private ArrayList<String> department = null;
    private ArrayList<String> subjectName = null;
    private ArrayList<String> subjectCode = null;
    private ArrayList<Integer> hours;
    private Connection conn = null;
    private ResultSet result = null;
    private PreparedStatement statement = null;
    private ResultSetMetaData rsm = null;
    private BaseDb base = null;

    public void sebjects(String dept, String sem, int stdyear) throws SQLException {
        subjectIDs = new ArrayList<String>();

        String query = "select id from subjects where dept_id=? and sem_id=? and stdyear=? and status=1 ;";

        statement = conn.prepareStatement(query);
        statement.setString(1, dept);
        statement.setString(2, sem);
        statement.setInt(3, stdyear);
        result = statement.executeQuery();

        while (result.next()) {
            subjectIDs.add(result.getString("id"));
        }
    }

    public void getInfo(String subject, String year, String sem) throws SQLException {
        studentIDs = new ArrayList<String>();
        stName = new ArrayList<String>();
        hours = new ArrayList<Integer>();
        subjectName = new ArrayList<String>();
        subjectCode = new ArrayList<String>();
        department = new ArrayList<String>();
        studentGroup = new ArrayList<String>();

        String query = "select t1.id,concat(t1.name,' ',t1.surname)as name,t5.name,t5.code,t5.hours,t2.year,t7.code,t3.semester,t8.name "
                + " from less_stud as t6"
                + " left join subjects as t5 on t6.subject_id=t5.id"
                + " left join year as t2 on t6.year_id=t2.id"
                + " left join semester as t3 on t6.sem_id=t3.id"
                + " left join student as t1 on t6.student_id=t1.id"
                + " left join department as t7 on t5.dept_id=t7.id"
                + " left join sinif as t8 on t1.group_id=t8.id"
                + " where t1.edu_status_id=1 and t5.id =? and t2.id =? and t3.id=? and (t6.status <5 and t6.status!=3) order by name;";

        statement = conn.prepareStatement(query);
        statement.setString(1, subject);
        statement.setString(2, year);
        statement.setString(3, sem);
        result = statement.executeQuery();

        while (result.next()) {
            studentIDs.add(result.getString("t1.id"));
            stName.add(result.getString("name"));
            hours.add(result.getInt("t5.hours"));
            subjectName.add(result.getString("t5.name"));
            subjectCode.add(result.getString("t5.code"));
            department.add(result.getString("t7.code"));
            studentGroup.add(result.getString("t8.name"));
        }//while
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String yearID = (String) session.getAttribute("yearID");
        String semID = (String) session.getAttribute("semID");
        String exam = (String) session.getAttribute("examID");
        String[] dept = request.getParameterValues("department");
        String week = (String) session.getAttribute("weekID");
        base = new BaseDb();

        int stdy = 5;

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Evalution Report</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        out.println("<center><table width=80% height=78 border=1 cellpadding=0 cellspacing=0 class=tealtable ><tr>");
        out.println("<th>Dept Code</th><th>Subj Code</th><th>Subj Name</th>"
                + "<th>Student</th><th>Group</th><th>Att</th> "
                + "<th>Total Att </th><th>Status</th>");
        out.println("</tr>");
        try {
            conn = base.getConnection();
            for (int d = 0; d < dept.length; d++) {
                if (dept[d].equals("12")) {
                    sebjects(dept[d], semID, 1);
                    for (int j = 0; j < subjectIDs.size(); j++) {

                        int attend = 0;
                        getInfo(subjectIDs.get(j), yearID, semID);

                        for (int k = 0; k < studentIDs.size(); k++) {
                            String query = "select sum(attendance) from attendance where subject_id=? "
                                    + "and year_id=? and semester_id=? and student_id=? and weeks_id<=? group by student_id;";

                            statement = conn.prepareStatement(query);
                            statement.setString(1, subjectIDs.get(j));
                            statement.setString(2, yearID);
                            statement.setString(3, semID);
                            statement.setString(4, studentIDs.get(k));
                            statement.setString(5, week);
                            result = statement.executeQuery();

                            int index = 0;
                            while (result.next()) {
                                attend = result.getInt("sum(attendance)");
                                index++;
                            }//while
                            if (attend > (hours.get(k) * 17 * 0.18)) {
                                out.println("<tr ><td align=center>" + department.get(k) + "</td>"
                                        + "<td align=center>" + subjectCode.get(k) + "</td>"
                                        + "<td align=center>" + subjectName.get(k) + "</td>"
                                        + "<td>" + stName.get(k) + "</td> "
                                        + "<td align = center>" + studentGroup.get(k) + "</td> "
                                        + "<td align = center>" + attend + "</td>"
                                        + "<td align=center>" + Math.round(hours.get(k) * 17 * 0.18)
                                        + "</td><td>F2</td></tr>");
                            }//if
                            else {
                                out.println("<tr ><td align=center>" + department.get(k) + "</td>"
                                        + "<td align=center>" + subjectCode.get(k) + "</td>"
                                        + "<td align=center>" + subjectName.get(k) + "</td>"
                                        + "<td>" + stName.get(k) + "</td>"
                                        + "<td align = center>" + studentGroup.get(k) + "</td>"
                                        + "<td align = center>" + attend + "</td>"
                                        + "<td align=center>" + Math.round(hours.get(k) * 17 * 0.18)
                                        + " </td><td>OK</td></tr>");
                            }
                        }//for k
                    } //for j
                } else {
                    for (int i = 2; i <= stdy; i++) {
                        sebjects(dept[d], semID, i);
                        for (int j = 0; j < subjectIDs.size(); j++) {
                            int attend = 0;
                            getInfo(subjectIDs.get(j), yearID, semID);

                            for (int k = 0; k < studentIDs.size(); k++) {
                                String query = "select sum(attendance) from attendance where subject_id=? "
                                        + "and year_id=? and semester_id=? and student_id=? and weeks_id<=? group by student_id;";

                                statement = conn.prepareStatement(query);
                                statement.setString(1, subjectIDs.get(j));
                                statement.setString(2, yearID);
                                statement.setString(3, semID);
                                statement.setString(4, studentIDs.get(k));
                                statement.setString(5, week);
                                result = statement.executeQuery();

                                int index = 0;
                                while (result.next()) {
                                    attend = result.getInt("sum(attendance)");
                                    index++;
                                }//while
                                if (attend > (hours.get(k) * 17 * 0.18)) {
                                    out.println("<tr ><td align=center>" + department.get(k) + "</td>"
                                            + "<td align=center>" + subjectCode.get(k) + "</td>"
                                            + "<td align=center>" + subjectName.get(k) + "</td>"
                                            + "<td>" + stName.get(k) + "</td> "
                                            + "<td align = center>" + studentGroup.get(k) + "</td> "
                                            + "<td align = center>" + attend + "</td>"
                                            + "<td align=center>" + Math.round(hours.get(k) * 17 * 0.18)
                                            + "</td><td>F2</td></tr>");
                                }//if
                                else {
                                    out.println("<tr ><td align=center>" + department.get(k) + "</td>"
                                            + "<td align=center>" + subjectCode.get(k) + "</td>"
                                            + "<td align=center>" + subjectName.get(k) + "</td>"
                                            + "<td>" + stName.get(k) + "</td> "
                                            + "<td align = center>" + studentGroup.get(k) + "</td> "
                                            + "<td align = center>" + attend + "</td>"
                                            + "<td align=center>" + Math.round(hours.get(k) * 17 * 0.18)
                                            + "</td><td>OK</td></tr>");
                                }
                            }//for k
                        } //for j
                    }// for i
                } //for else
            } //for d
            out.println("</table></p></p>");
            out.println("</center></body></html>");
        } catch (Exception e) {
            throw new ServletException(e.getMessage());
        } finally {
            if (base != null) {
                try {
                    base.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            out.close();
        }
        // out.println("<h1>Servlet Search at " + request.getContextPath () + "</h1>");
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
