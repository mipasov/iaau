package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;
import java.sql.ResultSet;

public class updateInstructor extends HttpServlet {

    final static String errorMesg = "Please check the details and try again";
    final static String succMesg = "Instructor Modified Successfully . Thank you";
    Connection conn = null;
    BaseDb base = null;
    PreparedStatement statement = null;
    ResultSet result = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();

        boolean status = false;
        String name = request.getParameter("instName");
        String surname = request.getParameter("instSurname");
        String instID = request.getParameter("id");
        String rollnum = request.getParameter("rollnum");
        String country = request.getParameter("country");
        String oblast = request.getParameter("oblast");
        String region = request.getParameter("region");
        String birthPl = request.getParameter("birthplace");
        String dob = request.getParameter("birthdate");
        String gender = request.getParameter("gender");
        String passport = request.getParameter("passport");
        String permadr = request.getParameter("permadr");
        String curradr = request.getParameter("curradr");
        String email = request.getParameter("email");
        String nation = request.getParameter("nation");
        String phone = request.getParameter("phone");
        String blood = request.getParameter("blood");
        String password = request.getParameter("password");
        String sts = request.getParameter("status");
        String level = request.getParameter("level");
        String role = request.getParameter("role");
        String photo = request.getParameter("photo");
        String faculty = request.getParameter("faculty");
        String dept = request.getParameter("dept");
        String group = request.getParameter("group");
        String account = request.getParameter("account");


        String query = "update ignore instructor set name = ?, surname = ?, level_id = ?, status_id = ?, country_id = ?, "
                + "region_id = ?, oblast_id = ?, phone = ?, permadr = ?, curradr = ?, birthpl = ?, blood_id = ?, nationality_id = ?, "
                + "gender_id = ?, birth_date = ?, passport = ?, email = ?, photo = ?, faculty_id = ?, dept_id = ?, group_id = ? "
                + "where id = ?;";
        String query1 = "update ignore users set user_pass=?, status=? where user_name=?;";
        String query2 = "update ignore user_roles set role_name=? where user_name=?;";

        try {
            base = new BaseDb();
            conn = base.getConnection();
            statement = conn.prepareStatement(query);
            statement.setString(1, name);
            statement.setString(2, surname);
            statement.setString(3, level);
            statement.setString(4, sts);
            statement.setString(5, country);
            statement.setString(6, region);
            statement.setString(7, oblast);
            statement.setString(8, phone);
            statement.setString(9, permadr);
            statement.setString(10, curradr);
            statement.setString(11, birthPl);
            statement.setString(12, blood);
            statement.setString(13, nation);
            statement.setString(14, gender);
            statement.setString(15, dob);
            statement.setString(16, passport);
            statement.setString(17, email);
            statement.setString(18, photo);
            statement.setString(19, faculty);
            statement.setString(20, dept);
            statement.setString(21, group);
            statement.setString(22, instID);
            statement.executeUpdate();

            statement = conn.prepareStatement(query1);
            statement.setString(1, password);
            statement.setString(2, account);
            statement.setString(3, rollnum);
            statement.executeUpdate();

            statement = conn.prepareStatement(query2);
            statement.setString(1, role);
            statement.setString(2, rollnum);
            statement.executeUpdate();

            status = true;

        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        } finally {
            if (status) {
                session.setAttribute("Message", succMesg);
            } else {
                session.setAttribute("Message", errorMesg);
            }

            response.sendRedirect("info.jsp");

            if (base != null) {
                try {
                    base.close();
                } catch (SQLException e) {
                }
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
