package servlets;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import iaau.DbExam;
import iaau.DbInstructor;
import iaau.DbSemester;
import iaau.DbStudYears;
import iaau.DbSubjExam;
import iaau.DbSubjects;
import iaau.Exam;
import iaau.ExamAverage;
import iaau.Instructor;
import iaau.Semester;
import iaau.SubjExam;
import iaau.Subjects;
import iaau.studYears;
import java.awt.Color;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author focus
 */
public class midtermMarks extends HttpServlet {

    DbSubjExam dbase = null;
    DbStudYears db2 = null;
    DbSemester db3 = null;
    DbExam db1 = null;
    DbSubjects sub = null;
    ExamAverage aver = null;
    DbInstructor inst = null;
    String userNS = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            response.setContentType("text/html;charset=UTF-8");
            HttpSession sess = request.getSession();
            Image img;
            String user = request.getRemoteUser();
            String subject = (String) request.getParameter("subjID");
            String year = (String) sess.getAttribute("yearID");
            String sem = (String) sess.getAttribute("semID");
            String exam_id = (String) request.getParameter("exam");
            try {
                dbase = new DbSubjExam();
                db1 = new DbExam();
                db2 = new DbStudYears();
                db3 = new DbSemester();
                aver = new ExamAverage();
                sub = new DbSubjects();
                inst = new DbInstructor();
                dbase.connect();
                sub.connect();
                db1.connect();
                db2.connect();
                db3.connect();
                aver.connect();
                inst.connect();
                sub.execSQL_subj(subject);
                db1.execSQL_Exam(exam_id);
                db2.execSQL_currYear();
                db3.execSQL_currSem();
                dbase.execSQL(subject, year, sem, exam_id);
                inst.execSQLRN(user);
                ArrayList<Subjects> subjectList = sub.getArray();
                ArrayList<Exam> examList = db1.getArray();
                ArrayList<studYears> yearList = db2.getArray();
                ArrayList<Semester> semList = db3.getArray();
                ArrayList<SubjExam> list = dbase.getArray();
                ArrayList<Instructor> instList = inst.getArray();

                String role = instList.get(0).getInstructorRole();
                if ((role.equals("secretary")) || (role.equals("oidb")) || (role.equals("studSecO"))) {
                    userNS = "";
                } else {
                    userNS = instList.get(0).getInstructorName() + " " + instList.get(0).getInstructorSurname();
                }

                Document document = new Document(PageSize.A4, 10, 10, 10, 10);
                PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
                response.setContentType("application/pdf");
                document.open();
                PdfContentByte punder = writer.getDirectContentUnder();
                img = Image.getInstance("/usr/local/images/iaauLogoT.png");
                img.setAbsolutePosition(document.getPageSize().getWidth() / 4, document.getPageSize().getHeight() / 3);
                img.scaleAbsolute(300, 300);
                punder.addImage(img);
                Font big_font = new Font(Font.COURIER, 19, Font.BOLD);
                big_font.setColor(new Color(0x92, 0x90, 0x83));
                Font title_font = new Font(Font.COURIER, 13, Font.BOLD);
                title_font.setColor(new Color(0x92, 0x90, 0x83));
                Font warning = new Font(Font.COURIER, 10, Font.BOLD);
                warning.setColor(new Color(0xFF, 0x00, 0x00));
                Font in_font = new Font(Font.COURIER, 12, Font.BOLD);
                Font text_font = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
                Paragraph iaau = new Paragraph("INTERNATIONAL ATATURK ALATOO UNIVERSITY", title_font);
                iaau.setAlignment(Element.ALIGN_CENTER);
                Paragraph sif = new Paragraph("EXAMINATION RESULT LIST", big_font);
                sif.setAlignment(Element.ALIGN_CENTER);
                document.add(iaau);
                document.add(sif);
                document.add(new Paragraph(10, " "));
                if (!list.isEmpty()) {
                    float[] Thead_colsWidth = {1.2f, 1.5f, 0.8f, 1.5f};
                    PdfPTable Thead = new PdfPTable(4);
                    Thead.setWidthPercentage(90f);
                    Thead.setWidths(Thead_colsWidth);
                    Thead.getDefaultCell().setBorder(0);
                    Thead.addCell(new Phrase("Department:", in_font));
                    Thead.addCell(new Phrase(subjectList.get(0).getDeptName(), text_font));
                    Thead.addCell(new Phrase("Subject:", in_font));
                    Thead.addCell(new Phrase(subjectList.get(0).getSubjectName(), text_font));
                    Thead.addCell(new Phrase("Academic Year:", in_font));
                    Thead.addCell(new Phrase(yearList.get(0).getYear(), text_font));
                    Thead.addCell(new Phrase("Semester:", in_font));
                    Thead.addCell(new Phrase(semList.get(0).getSemester(), text_font));
                    Thead.addCell(new Phrase("Examination:", in_font));
                    Thead.addCell(new Phrase(examList.get(0).getExam(), text_font));
                    Thead.addCell(new Phrase("Date: ", in_font));
                    Thead.addCell(new Phrase(" "));
                    document.add(Thead);
                    document.add(new Paragraph(10, " "));
                    float[] Tbody_colsWidth = {0.1f, 1f, 0.4f, 0.4f};
                    PdfPTable Tbody = new PdfPTable(4);
                    Tbody.setWidthPercentage(90f);
                    Tbody.setWidths(Tbody_colsWidth);
                    Tbody.getDefaultCell().setFixedHeight(16);
                    Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    Tbody.addCell(new Phrase("#", in_font));
                    Tbody.addCell(new Phrase("Name Surname", in_font));
                    Tbody.addCell(new Phrase("Group", in_font));
                    Tbody.addCell(new Phrase("Mark", in_font));
                    for (int i = 0; i < list.size(); i++) {
                        Tbody.addCell(new Phrase(Integer.toString(i + 1), text_font));
                        Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                        Tbody.addCell(new Phrase(list.get(i).getStudentName() + " " + list.get(i).getStudentSurname(), text_font));
                        Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                        Tbody.addCell(new Phrase(list.get(i).getGroup(), text_font));
                        Tbody.addCell(new Phrase(Integer.toString(list.get(i).getExamMark()), text_font));
                    }
                    document.add(Tbody);
                    document.add(new Paragraph(10, " "));
                    float[] Tfoot_colsWidth = {1.5f, 1f};
                    PdfPTable Tfoot = new PdfPTable(2);
                    Tfoot.setWidthPercentage(90f);
                    Tfoot.setWidths(Tfoot_colsWidth);
                    Tfoot.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    Tfoot.addCell(new Phrase("Name Surname : " + userNS, in_font));
                    Tfoot.addCell(new Phrase("Signature :", in_font));
                    document.add(Tfoot);
                } else {
                    document.add(new Phrase("No records found", warning));
                }
                document.close();

            } catch (Exception e) {
                throw new ServletException("Your query is not working", e);
            }
            dbase.close();
            sub.close();
            db1.close();
            db2.close();
            db3.close();
            aver.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
