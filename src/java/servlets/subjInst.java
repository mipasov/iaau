package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;

public class subjInst extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        String year = (String) session.getAttribute("yearID");
        PrintWriter out = response.getWriter();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        ResultSetMetaData rsm = null;
        BaseDb base = null;

        String query = "select t2.code as department,concat(t4.name,' ',t4.surname) as instructor,t1.code,t1.name as subject,t1.hours,t1.credit,t3.semester "
                + "from subjects as t1 "
                + "left join department as t2 on t1.dept_id=t2.id "
                + "left join semester as t3 on t1.sem_id=t3.id "
                + "left join subj_instructor as t5 on t5.subj_id=t1.id "
                + "left join instructor as t4 on t4.id=t5.inst_id "
                + "where t1.id=t5.subj_id and t5.year_id=? and t1.status=1 order by t4.name asc";

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Not entered attendance list</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        out.println("<center><table width=90% border=1 cellspacing=1 cellpadding=2 class=tealtable >");
        out.println("<tr><td><b>Department</td><td><b>Instructor</td><td><b>Subject code</td><td><b>Subject Name</td></tr>");
        try {
            base = new BaseDb();
            conn = base.getConnection();
            statement = conn.prepareStatement(query);
            statement.setString(1, year);

            result = statement.executeQuery();
            rsm = result.getMetaData();
            int colCount = rsm.getColumnCount();

            if (colCount > 0) {
                while (result.next()) {
                    out.println("<tr>");
                    out.println("<td>" + result.getString("department") + "</td><td>" + result.getString("instructor") + "</td>"
                            + "<td>" + result.getString("t1.code") + "</td><td>" + result.getString("subject") + "</td>");
                    out.println("</tr>");
                }//while
            }//if
        } catch (Exception e) {
            while (e != null) {
                e.printStackTrace();
            }
        } finally {
            out.println("</table></center></body></html>");
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
