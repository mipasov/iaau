package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;
import iaau.DbStudent_Attendance;
import java.util.ArrayList;

public class depAttEva extends HttpServlet {

    private ArrayList<String> subjectIDs;
    private ArrayList<String> studentIDs;
    private ArrayList<String> stName;
    private ArrayList<Integer> hours;
    private ArrayList<String> department = null;
    private ArrayList<String> subjectName = null;
    private String yearName = null;
    private String semester = null;
    private Connection conn = null;
    private ResultSet result = null;
    private PreparedStatement statement = null;
    private ResultSetMetaData rsm = null;
    private BaseDb base = null;
    private DbStudent_Attendance dbStAtt = null;

    public void subjCount(String dept, String sem, int stdyear) throws SQLException {
        subjectIDs = new ArrayList<String>();
        subjectName = new ArrayList<String>();
        hours = new ArrayList<Integer>();
        department = new ArrayList<String>();

        String query = "select s.id, s.name, s.hours, d.name "
                + "from subjects as s "
                + "left join department as d on s.dept_id=d.id "
                + "where s.status=1 and s.dept_id=? and s.sem_id=? and s.stdyear=? order by s.name ";

        statement = conn.prepareStatement(query);
        statement.setString(1, dept);
        statement.setString(2, sem);
        statement.setInt(3, stdyear);
        result = statement.executeQuery();

        while (result.next()) {
            subjectIDs.add(result.getString("s.id"));
            subjectName.add(result.getString("s.name"));
            hours.add(result.getInt("s.hours"));
            department.add(result.getString("d.name"));
        }
    }

    public void getInfo(String subject, String year, String sem) throws SQLException {
        studentIDs = new ArrayList<String>();
        stName = new ArrayList<String>();

        String query = "select t1.id,concat(t1.name,' ',t1.surname)as name,t5.name,t5.hours,t2.year,t7.name,t3.semester "
                + "from less_stud as t6"
                + " left join subjects as t5 on t6.subject_id=t5.id"
                + " left join year as t2 on t6.year_id=t2.id"
                + " left join semester as t3 on t6.sem_id=t3.id"
                + " left join student as t1 on t6.student_id=t1.id"
                + " left join department as t7 on t5.dept_id=t7.id"
                + " where t1.edu_status_id=1 and t5.id =? and t2.id =? and t3.id=? and t6.status<3 order by name;";

        statement = conn.prepareStatement(query);
        statement.setString(1, subject);
        statement.setString(2, year);
        statement.setString(3, sem);
        result = statement.executeQuery();

        while (result.next()) {
            studentIDs.add(result.getString("t1.id"));
            stName.add(result.getString("name"));
            yearName = result.getString("t2.year");
            semester = result.getString("t3.semester");
        }//while
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String yearID = (String) session.getAttribute("yearID");
        String semID = (String) session.getAttribute("semID");
        String[] dept = request.getParameterValues("department");

        base = new BaseDb();
        dbStAtt = new DbStudent_Attendance();

        int week = 17;
        int stdy = 5;

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Evalution Report</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        //out.println("<center><table width=622 height=78 border=1" +
        //        " cellpadding=0 cellspacing=0 class=tealtable ><tr>");
        //out.println("<td><img src=../images/AttEvaReport.bmp width=622 height=78 alt=headline/></td></tr></table>");
        try {
            conn = base.getConnection();
            dbStAtt.connect();

            for (int d = 0; d < dept.length; d++) {
                if (dept[d].equals("12")) {
                    subjCount(dept[d], semID, 1);
                    for (int j = 0; j < subjectIDs.size(); j++) {

                        int attend = 0;
                        getInfo(subjectIDs.get(j), yearID, semID);

                        out.println("<p> </p>");
                        out.println("<center><table width=80% align=center cellpadding=1 cellspacing=1 class=tealtable ><tr><td><b>Department : </b><td>" + department.get(j) + "</td>");
                        out.println("<td><b>Subject : </b><td>" + subjectName.get(j) + "</td></tr>");
                        out.println("<tr><td><b>Year : </b><td>" + yearName + "</td>");
                        out.println("<td><b>Semester : </b><td>" + semester + "</td></tr>");
                        out.println("<td><b>Course : </b><td>1</td></tr>");

                        out.println("<p> </p>");
                        out.println("<center>"
                                + "<table  width=80% align=center border=1"
                                + " cellpadding=0 cellspacing=0 class=tealtable >");

                        out.println("<tr>");
                        out.println("<th> NO  </th> <th> Name Surname </th> "
                                + "<th> Attendance </th><th>Status</th>");
                        out.println("</tr>");

                        for (int k = 0; k < studentIDs.size(); k++) {
                            String query = "select sum(attendance) from attendance where subject_id=? "
                                    + "and year_id=? and semester_id=? and student_id=? group by student_id;";

                            statement = conn.prepareStatement(query);
                            statement.setString(1, subjectIDs.get(j));
                            statement.setString(2, yearID);
                            statement.setString(3, semID);
                            statement.setString(4, studentIDs.get(k));
                            result = statement.executeQuery();

                            while (result.next()) {
                                attend = result.getInt("sum(attendance)");
                            }//while
                            if (attend > (hours.get(j) * week * 0.18)) {
                                out.println("<tr ><td align = center>" + (k + 1) + "</td><td>" + stName.get(k) + "</td> <td align = center>" + attend + ""
                                        + "</td><td>F2</td></tr>");
                                dbStAtt.changeAttStatus(Integer.parseInt(studentIDs.get(k)), Integer.parseInt(subjectIDs.get(j)), Integer.parseInt(yearID), Integer.parseInt(semID));
                            }//if
                            else {
                                out.println("<tr ><td align=center>" + (k + 1) + "</td><td>" + stName.get(k) + "</td><td align = center>" + attend + ""
                                        + " </td><td>OK</td></tr>");
                            }
                        }//for k
                        out.println("</table></p></p>");
                    } //for j
                }
                for (int i = 2; i <= stdy; i++) {
                    subjCount(dept[d], semID, i);
                    for (int j = 0; j < subjectIDs.size(); j++) {

                        int attend = 0;
                        getInfo(subjectIDs.get(j), yearID, semID);

                        out.println("<p> </p>");
                        out.println("<center><table width=80% align=center cellpadding=1 cellspacing=1 class=tealtable ><tr><td><b>Department : </b><td>" + department.get(j) + "</td>");
                        out.println("<td><b>Subject : </b><td>" + subjectName.get(j) + "</td></tr>");
                        out.println("<tr><td><b>Year : </b><td>" + yearName + "</td>");
                        out.println("<td><b>Semester : </b><td>" + semester + "</td></tr>");
                        out.println("<td><b>Course : </b><td>" + (i) + "</td></tr>");

                        out.println("<p> </p>");
                        out.println("<center>"
                                + "<table  width=80% align=center border=1"
                                + " cellpadding=0 cellspacing=0 class=tealtable >");

                        out.println("<tr>");
                        out.println("<th> NO  </th> <th> Name Surname </th> "
                                + "<th>Attendance </th><th>Status</th>");
                        out.println("</tr>");

                        for (int k = 0; k < studentIDs.size(); k++) {
                            String query = "select sum(attendance) from attendance where subject_id=? "
                                    + "and year_id=? and semester_id=? and student_id=? group by student_id;";

                            statement = conn.prepareStatement(query);
                            statement.setString(1, subjectIDs.get(j));
                            statement.setString(2, yearID);
                            statement.setString(3, semID);
                            statement.setString(4, studentIDs.get(k));
                            result = statement.executeQuery();

                            while (result.next()) {
                                attend = result.getInt("sum(attendance)");
                            }//while
                            if (attend > (hours.get(j) * week * 0.18)) {
                                out.println("<tr ><td align = center>" + (k + 1) + "</td><td>" + stName.get(k) + "</td> <td align = center>" + attend + ""
                                        + "</td><td>F2</td></tr>");
                                dbStAtt.changeAttStatus(Integer.parseInt(studentIDs.get(k)), Integer.parseInt(subjectIDs.get(j)), Integer.parseInt(yearID), Integer.parseInt(semID));
                            }//if
                            else {
                                out.println("<tr ><td align=center>" + (k + 1) + "</td><td>" + stName.get(k) + "</td><td align = center>" + attend + ""
                                        + " </td><td>OK</td></tr>");
                            }
                        }//for k
                        out.println("</table></p></p>");
                    } //for j
                } //for i
            } //for d
            out.println("</center></body></html>");
        } catch (Exception e) {
            throw new ServletException(e.getMessage());
        } finally {
            if (base != null) {
                try {
                    base.close();
                } catch (SQLException ex) {
                }
            }
            if (dbStAtt != null) {
                try {
                    dbStAtt.close();
                } catch (SQLException ex) {
                }
            }
        }
        // out.println("<h1>Servlet Search at " + request.getContextPath () + "</h1>");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
