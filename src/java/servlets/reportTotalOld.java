package servlets;
/*
 * Register.java
 * Created on September 24, 2007, 1:08 PM
 */
import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;
/**
 * @author opensky
 * @version
 */
public class reportTotalOld extends HttpServlet {
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        ResultSetMetaData rsm = null;
        BaseDb base = null;
        
        String[] department={"COM","MAT","INT","ELL","ELT","MAN","FIN","TUR",
        "KGZ","ICO","SYT","DOK","KYD","YLS","PHD","ARL","CHN","IND"};
    
        String query = "select count(distinct (student.id)) as number from student left join " +
                "department on student.dept_id=department.id where department.code=? ;";
        
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Attendance Sheet</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        out.println("<center><table border=0 class=labelForm>");
        try {
            base = new BaseDb();
            conn = base.getConnection();
            statement = conn.prepareStatement(query);
            
            int sum=0;
            for(int i = 0; i < department.length; i++){
                statement.setString(1,department[i]);
                result = statement.executeQuery();
                rsm=result.getMetaData();
                out.println("<tr>");
                int colCount = rsm.getColumnCount();
                
                if(colCount <= 0){
                    out.println("<td>"+department[i]+"</td><td>"+0+"</td>");
                } else{
                    while(result.next()){
                        out.println("<td>"+department[i]+"</td><td>"+result.getString("number")+"</td>");
                        sum+=Integer.parseInt(result.getString("number"));
                    }//while
                }//else
                out.println("</tr>");
            }//for
            out.println("<tr><td>Total</td><td>"+sum+"</td></tr>");
        }catch (Exception e) {
            while (e !=null) {
                e.printStackTrace();
            }
        }finally{
            out.println("</table></center></body></html>");
            out.close();
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
