package servlets;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import iaau.BaseDb;
import java.awt.Color;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

public class transc extends HttpServlet {

    Connection conn = null;
    PreparedStatement statement = null;
    ResultSet result = null;
    ResultSetMetaData rsm = null;
    BaseDb base = null;
    private String nameSurname;
    private String department;
    private String faculty;
    private ArrayList<String> stud_less_id;
    private ArrayList<String> subjectID;
    private ArrayList<String> subjectName;
    private ArrayList<String> subjectCode;
    private ArrayList<String> semesterID;
    private ArrayList<String> semesterName;
    private ArrayList<String> yearID;
    private ArrayList<String> yearName;
    private ArrayList<String> year_name;
    private ArrayList<Integer> credit;
    private ArrayList<Integer> year_id;
    private double total;
    private double creditSum;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");        

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
        String sid = (String) request.getParameter("sid");        

        try {
            base = new BaseDb();
            conn = base.getConnection();
            subjects(sid);
            execute(sid);
            year_count(sid);


            Document document = new Document(PageSize.A4, 20, 20, 20, 20);            
            PdfWriter.getInstance(document, response.getOutputStream());
            response.setContentType("application/pdf");
            //response.setHeader("Content-Disposition"," attachment; filename=\"example.pdf\"");

            document.open();
            Font title_font = new Font(Font.COURIER, 10, Font.BOLD);
            title_font.setColor(new Color(0x92, 0x90, 0x83));
            Font text_font = new Font(Font.TIMES_ROMAN, 8, Font.NORMAL);
            Chunk chunk1 = new Chunk("INTERNATIONAL ATATURK ALATOO UNIVERSITY UNDERGRADUATE STUDENT`S TRANSCRIPT FORM", title_font);
            //Chunk chunk2 = new Chunk("UNDERGRADUATE STUDENT`S TRANSCRIPT FORM", title_font);
            Paragraph paragraph = new Paragraph();
            paragraph.add(chunk1);
            paragraph.add(Chunk.NEWLINE);
            // paragraph.add(chunk2);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);
            //document.add(new Paragraph(10, " "));

            Paragraph st = new Paragraph("Name Surname:" + nameSurname + "\t Faculty: " +
                    faculty + "\t Department:" + department + "\t Date:" + df.format(new java.util.Date()), title_font);
            st.setAlignment(Element.ALIGN_CENTER);
            document.add(st);
            document.add(new Paragraph(10, " "));

            float[] table_colsWidth = {2f, 2f};
            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100f);
            table.setWidths(table_colsWidth);

            total = 0;
            creditSum = 0;

            for(int i = 0; i < year_id.size()-1; i++){

                float[] head_table_colsWidth = {2f,3f,0.5f,0.5f};
                PdfPTable head_table1 = new PdfPTable(4);
                head_table1.setWidths(head_table_colsWidth);
                head_table1.getDefaultCell().setBorder(0);

                PdfPTable head_table2 = new PdfPTable(4);
                head_table2.setWidths(head_table_colsWidth);
                head_table2.getDefaultCell().setBorder(0);

                head_table1.addCell(new Phrase(year_name.get(i),title_font));
                head_table1.addCell(new Phrase("FALL",title_font));
                head_table1.addCell(new Phrase("C",title_font));
                head_table1.addCell(new Phrase("G",title_font));

                head_table2.addCell(new Phrase(year_name.get(i),title_font));
                head_table2.addCell(new Phrase("SPRING",title_font));
                head_table2.addCell(new Phrase("C",title_font));
                head_table2.addCell(new Phrase("G",title_font));

                float[] colsWidth = {1f,4f,0.5f,0.5f};
                PdfPTable tableIn1 = new PdfPTable(4);
                tableIn1.setWidths(colsWidth);
                tableIn1.getDefaultCell().setBorder(0);
                PdfPCell cell1 = new PdfPCell(head_table1);
                cell1.setColspan(4);
                tableIn1.addCell(cell1);

                PdfPTable tableIn2 = new PdfPTable(4);
                tableIn2.setWidths(colsWidth);
                tableIn2.getDefaultCell().setBorder(0);
                PdfPCell cell2 = new PdfPCell(head_table2);
                cell2.setColspan(4);
                tableIn2.addCell(cell2);

                for(int j = 0; j < subjectID.size()-1; j++){
                    if (yearID.get(j).equals(Integer.toString(year_id.get(i)))&&(semesterID.get(j).equals("1"))){
                        double av = calcAverage(stud_less_id.get(j),subjectID.get(j));
                        total+=av*credit.get(j);
                        creditSum+=credit.get(j);
                        tableIn1.addCell(new Phrase(subjectCode.get(j),text_font));
                        tableIn1.addCell(new Phrase(subjectName.get(j),text_font));
                        tableIn1.addCell(new Phrase(Integer.toString(credit.get(j)),text_font));
                        tableIn1.addCell(new Phrase(Long.toString(Math.round(av)),text_font));
                        //subjectID.remove(j);
                    }
                    if (yearID.get(j).equals(Integer.toString(year_id.get(i)))&&(semesterID.get(j).equals("2"))){
                        double av = calcAverage(stud_less_id.get(j),subjectID.get(j));
                        total+=av*credit.get(j);
                        creditSum+=credit.get(j);
                        tableIn2.addCell(new Phrase(subjectCode.get(j),text_font));
                        tableIn2.addCell(new Phrase(subjectName.get(j),text_font));
                        tableIn2.addCell(new Phrase(Integer.toString(credit.get(j)),text_font));
                        tableIn2.addCell(new Phrase(Long.toString(Math.round(av)),text_font));
                        //subjectID.remove(j);
                    }
                } //for j
                table.addCell(tableIn1);
                table.addCell(tableIn2);
            } // for i

            document.add(table);

            document.add(new Paragraph(20, " "));
            float[] foot_colsWidth = {2f, 1f};
            PdfPTable foot = new PdfPTable(2);
            foot.setWidthPercentage(100f);
            foot.setTotalWidth(foot_colsWidth);
            foot.getDefaultCell().setBorder(0);
            foot.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            foot.addCell(new Phrase("GRADUATE PROJECT:", text_font));
            foot.addCell(new Phrase("AVERAGE:" + Math.round(total / creditSum), text_font));
            document.add(foot);

            document.add(new Paragraph(30, " "));
            float[] footer_colsWidth = {2f, 1f};
            PdfPTable footer = new PdfPTable(2);
            footer.setWidthPercentage(100f);
            footer.setTotalWidth(footer_colsWidth);
            footer.getDefaultCell().setBorder(0);
            footer.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            footer.addCell(new Phrase("  Askarbek Kamchiev", text_font));
            footer.addCell(new Phrase("", text_font));
            footer.addCell(new Phrase("HEAD OF THE STUDENT SERVICE", text_font));
            footer.addCell(new Phrase("", text_font));
            document.add(footer);

            document.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (base != null) {
                try {
                    base.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void subjects(String sid) throws SQLException {

        stud_less_id = new ArrayList<String>();
        subjectID = new ArrayList<String>();
        yearID = new ArrayList<String>();
        semesterID = new ArrayList<String>();
        subjectCode = new ArrayList<String>();
        subjectName = new ArrayList<String>();
        credit = new ArrayList<Integer>();
        yearName = new ArrayList<String>();
        semesterName = new ArrayList<String>();

        String query = "select t1.id, t1.subject_id, t1.year_id, t1.sem_id, t2.code, t2.name, t2.credit," +
                " t5.year, t6.semester" +
                " from less_stud as t1 left join subjects as t2 on t1.subject_id=t2.id" +
                " left join year as t5 on t1.year_id=t5.id" +
                " left join semester as t6 on t1.sem_id=t6.id" +
                " where t1.student_id=? and t1.status<4 order by t1.year_id,t1.sem_id;";
        statement = conn.prepareStatement(query);
        statement.setString(1, sid);
        result = statement.executeQuery();
        while (result.next()) {
            stud_less_id.add(result.getString("t1.id"));
            subjectID.add(result.getString("t1.subject_id"));
            yearID.add(result.getString("t1.year_id"));
            semesterID.add(result.getString("t1.sem_id"));
            subjectCode.add(result.getString("t2.code"));
            subjectName.add(result.getString("t2.name"));
            credit.add(result.getInt("t2.credit"));
            yearName.add(result.getString("t5.year"));
            semesterName.add(result.getString("t6.semester"));
        }
    }

    public void year_count(String sid) throws SQLException {
        
        year_id = new ArrayList<Integer>();
        year_name = new ArrayList<String>();

        String query = " select distinct t1.year_id,t2.year from less_stud as t1 left join year as t2 on t1.year_id=t2.id" +
                " where student_id=? order by year_id";
        statement = conn.prepareStatement(query);
        statement.setString(1, sid);
        result = statement.executeQuery();
        while (result.next()) {
            year_id.add(result.getInt("t1.year_id"));
            year_name.add(result.getString("t2.year"));
        }
    }

    public void execute(String sid) throws SQLException {

        String query = "select concat(t1.name,'  ',t1.surname)as name, t2.name, t3.name" +
                " from student as t1" +
                " left join department as t3 on t1.dept_id=t3.id" +
                " left join faculty_dept as t4 on t4.dept_id=t1.dept_id" +
                " left join faculty as t2 on t4.faculty_id=t2.id" +
                " where t1.id=? ;";
        statement = conn.prepareStatement(query);
        statement.setString(1, sid);
        result = statement.executeQuery();

        while (result.next()) {
            nameSurname = result.getString("name");
            faculty = result.getString("t2.name");
            department = result.getString("t3.name");
        }
    }

    public double calcAverage(String s_less_id, String subj_id) throws SQLException {
        String query = "select t3.exam_name as exam,(t4.mark * t3.percentage/100) as average from less_stud " +
                " as t5 left join student as  t1 on t5.student_id=t1.id left join " +
                " subjects as t2 on t5.subject_id=t2.id left join sinif as t6 on " +
                " t1.group_id=t6.id  left join subj_exam as t4 on t5.id = t4.stud_less_id " +
                " left join exam as t3 on t4.exam_id=t3.exam_id " +
                " where t2.id=? and t4.stud_less_id =? ;";

        double average = 0;

        statement = conn.prepareStatement(query);
        statement.setString(1, subj_id);
        statement.setString(2, s_less_id);
        result = statement.executeQuery();
        int k = 0;
        double midterm = 0, fin = 0, mup = 0.0;
        while (result.next()) {
            String exam = result.getString("exam");
            if (exam.equals("Midterm")) {
                midterm = result.getDouble("average");
            } else if (exam.equals("Final")) {
                fin = result.getDouble("average");
                if (fin < 29.7) {
                    fin = 0.0;
                }
            } else {
                mup = result.getDouble("average");
                if (mup < 29.7) {
                    mup = 0.0;
                }
            }
            if ((midterm + fin) < 49.5) {
                average = midterm + mup;
            } else {
                average = midterm + fin;
            }
        }
        return average;
    }
}
