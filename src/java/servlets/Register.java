package servlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.DbStud_Less;
import iaau.DbStudent;

public class Register extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Registration was completed successfully. Thank you";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String[] ids;
        boolean status = false;
        String studID = request.getParameter("studID");
        String subjStat = request.getParameter("status");
        String year = request.getParameter("year");
        String semester = request.getParameter("sem");
        ids = request.getParameterValues("subjID");

        DbStud_Less dbStudLess = null;
        DbStudent st = null;

        try {
            if (ids != null) {
                dbStudLess = new DbStud_Less();
                dbStudLess.connect();

                st = new DbStudent();
                st.connect();

                st.updateEdu_status(studID);

                for (int i = 0; i < ids.length; i++) {
                    dbStudLess.execRegistration(studID, ids[i], subjStat, year, semester);
                }
                status = true;

            } else {
                status = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (status) {
                request.getSession().setAttribute("Message", succMesg);
            } else {
                request.getSession().setAttribute("Message", errorMesg);
            }

            response.sendRedirect("info.jsp");

            if (dbStudLess != null) {
                try {
                    dbStudLess.close();
                } catch (Exception ex1) {
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception ex2) {
                }
            }
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
// </editor-fold>
}
