package servlets;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import iaau.BaseDb;
import iaau.DbInstructor;
import iaau.DbStudYears;
import iaau.DbStud_Less;
import iaau.Instructor;
import iaau.Stud_Less;
import iaau.studYears;
import java.awt.Color;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class examResults extends HttpServlet {

    static final String errorMesg = "Check details and try again";
    private Connection conn = null;
    private PreparedStatement statment = null;
    private ResultSet result = null;
    private ResultSetMetaData rsm = null;
    private BaseDb base = null;
    private DbInstructor inst = null;
    private DbStud_Less stLess = null;
    private DbStudYears stYear = null;
    private double marks[] = new double[3];
    private int attendance;
    private String userNS = null;
    boolean status = false;

    public void getStudent(String s_id, String sem, String year, int subj_id) throws SQLException {

        String query = "select se.mark, e.exam_name from less_stud as ls "
                + "inner join subj_exam as se on se.stud_less_id=ls.id "
                + "inner join exam as e on e.exam_id=se.exam_id "
                + "where ls.subject_id=? and ls.student_id=? "
                + "and ls.year_id=? and ls.sem_id=? ";

        statment = conn.prepareStatement(query);
        statment.setInt(1, subj_id);
        statment.setString(2, s_id);
        statment.setString(3, year);
        statment.setString(4, sem);
        result = statment.executeQuery();
        rsm = result.getMetaData();

        if (rsm.getColumnCount() == 0) {
            throw new SQLException("No records found");
        }

        while (result.next()) {
            String exam = result.getString("e.exam_name");
            if (exam.equals("Midterm")) {
                marks[0] = Double.parseDouble(result.getString("se.mark"));
            } else if (exam.equals("Final")) {
                marks[1] = Double.parseDouble(result.getString("se.mark"));
            } else if (exam.equals("MakeUp")) {
                marks[2] = Double.parseDouble(result.getString("se.mark"));
            }
        }//while
    }

    public void getAttendance(String s_id, String sem, String year, int subj_id) throws SQLException {
        attendance = 0;
        String query = "select if (sum(attendance)>0,sum(attendance),0) as att "
                + "from attendance where student_id =? and subject_id =? and semester_id=? and year_id=? ";
        statment = conn.prepareStatement(query);
        statment.setString(1, s_id);
        statment.setInt(2, subj_id);
        statment.setString(3, sem);
        statment.setString(4, year);
        result = statment.executeQuery();

        while (result.next()) {
            attendance = result.getInt("att");
        }
    }

    public double calcAverage(String s_id, String sem, String year, int subj_id) throws SQLException {

        String query = "select t3.exam_name as exam,(t4.mark * t3.percentage/100) as average from less_stud "
                + "as t5 left join student as  t1 on t5.student_id=t1.id left join "
                + "subjects as t2 on t5.subject_id=t2.id left join sinif as t6 on "
                + "t1.group_id=t6.id  left join subj_exam as t4 on t5.id = t4.stud_less_id "
                + "left join exam as t3 on t4.exam_id=t3.exam_id "
                + "where  t2.id=? and t5.year_id=? and t5.sem_id=? and t5.student_id =?;";

        double average = 0;

        statment = conn.prepareStatement(query);
        statment.setInt(1, subj_id);
        statment.setString(2, year);
        statment.setString(3, sem);
        statment.setString(4, s_id);
        result = statment.executeQuery();

        int k = 0;
        double midterm = 0, fin = 0, mup = 0.0;
        while (result.next()) {
            String exam = result.getString("exam");
            if (result.wasNull()) {
                average = 0;
            } else if (exam.equals("Midterm")) {
                midterm = result.getDouble("average");
            } else if (exam.equals("Final")) {
                fin = result.getDouble("average");
                if (fin < 29.7) {
                    fin = 0.0;
                }
            } else if (exam.equals("MakeUp")) {
                mup = result.getDouble("average");
                if (mup < 29.7) {
                    mup = 0.0;
                }
            }

            if ((midterm + fin) < 49.5) {
                average = midterm + mup;
            } else {
                average = midterm + fin;
            }
        }
        return average;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        Image img;

        String stID = (String) request.getParameter("stID");
        String yearID = (String) session.getAttribute("yearID");
        String semID = (String) session.getAttribute("semID");
        String user = (String) request.getRemoteUser();


        try {
            base = new BaseDb();
            conn = base.getConnection();
            inst = new DbInstructor();
            inst.connect();
            inst.execSQLRN(user);
            ArrayList<Instructor> list = inst.getArray();
            userNS = list.get(0).getInstructorName() + " " + list.get(0).getInstructorSurname();

            stYear = new DbStudYears();
            stYear.connect();
            stYear.execSQL_currYear();
            ArrayList<studYears> stYearList = stYear.getArray();

            stLess = new DbStud_Less();
            stLess.connect();
            stLess.execSQL(stID, semID, yearID);
            ArrayList<Stud_Less> stList = stLess.getArray();


            Document document = new Document(PageSize.A4, 10, 10, 10, 10);
            PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
            response.setContentType("application/pdf");
            document.open();
            PdfContentByte punder = writer.getDirectContentUnder();
            img = Image.getInstance("/usr/local/images/iaauLogoT.png");
            img.setAbsolutePosition(document.getPageSize().getWidth() / 4, document.getPageSize().getHeight() / 3);
            img.scaleAbsolute(300, 300);
            punder.addImage(img);
            Font big_font = new Font(Font.COURIER, 19, Font.BOLD);
            big_font.setColor(new Color(0x92, 0x90, 0x83));
            Font title_font = new Font(Font.COURIER, 13, Font.BOLD);
            title_font.setColor(new Color(0x92, 0x90, 0x83));
            Font warning = new Font(Font.COURIER, 12, Font.BOLD);
            warning.setColor(new Color(0xFF, 0x00, 0x00));
            Font in_font = new Font(Font.COURIER, 12, Font.BOLD);
            Font text_font = new Font(Font.TIMES_ROMAN, 11, Font.NORMAL);
            Paragraph iaau = new Paragraph("INTERNATIONAL ATATURK ALATOO UNIVERSITY", title_font);
            iaau.setAlignment(Element.ALIGN_CENTER);
            Paragraph sif = new Paragraph("STUDENT`S SUCCESS CONTROL FORM", big_font);
            sif.setAlignment(Element.ALIGN_CENTER);
            document.add(iaau);
            document.add(sif);
            document.add(new Paragraph(10, " "));

            if (!stList.isEmpty()) {
                float[] Thead_colsWidth = {1.2f, 1.5f, 0.8f, 1.5f};
                PdfPTable Thead = new PdfPTable(4);
                Thead.setWidthPercentage(90f);
                Thead.setWidths(Thead_colsWidth);
                Thead.getDefaultCell().setBorder(0);
                Thead.addCell(new Phrase("Group:", in_font));
                Thead.addCell(new Phrase(stList.get(0).getGrpName(), text_font));
                Thead.addCell(new Phrase("Student:", in_font));
                Thead.addCell(new Phrase(stList.get(0).getStudName() + " " + stList.get(0).getStudSurname(), text_font));
                Thead.addCell(new Phrase("Academic Year:", in_font));
                Thead.addCell(new Phrase(stYearList.get(0).getYear(), text_font));
                Thead.addCell(new Phrase("Semester:", in_font));
                Thead.addCell(new Phrase(stList.get(0).getSemester(), text_font));
                document.add(Thead);
                document.add(new Paragraph(10, " "));

                float[] Tbody_colsWidth = {0.1f, 1f, 0.3f, 0.4f, 0.3f, 0.4f, 0.4f, 0.5f};
                PdfPTable Tbody = new PdfPTable(8);
                Tbody.setWidthPercentage(90f);
                Tbody.setWidths(Tbody_colsWidth);
                Tbody.getDefaultCell().setFixedHeight(16);
                Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                Tbody.addCell(new Phrase("#", in_font));
                Tbody.addCell(new Phrase("Subjects", in_font));
                Tbody.addCell(new Phrase("Hours", in_font));
                Tbody.addCell(new Phrase("Midterm", in_font));
                Tbody.addCell(new Phrase("Final", in_font));
                Tbody.addCell(new Phrase("Make Up", in_font));
                Tbody.addCell(new Phrase("Average", in_font));
                Tbody.addCell(new Phrase("Attendance", in_font));

                for (int i = 0; i < stList.size(); i++) {
                    for (int m = 0; m < marks.length; m++) {
                        marks[m] = 0;
                    }
                    getStudent(stID, semID, yearID, stList.get(i).getSubjID());
                    getAttendance(stID, semID, yearID, stList.get(i).getSubjID());

                    double av = calcAverage(stID, semID, yearID, stList.get(i).getSubjID());

                    Tbody.addCell(new Phrase(Integer.toString(i + 1), text_font));
                    Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                    Tbody.addCell(new Phrase(stList.get(i).getSubjName(), text_font));
                    Tbody.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    Tbody.addCell(new Phrase(stList.get(i).getSubHour(), text_font));
                    Tbody.addCell(new Phrase(Double.toString(marks[0]), text_font));
                    Tbody.addCell(new Phrase(Double.toString(marks[1]), text_font));
                    Tbody.addCell(new Phrase(Double.toString(marks[2]), text_font));
                    Tbody.addCell(new Phrase(Double.toString(Math.round(av)), text_font));
                    Tbody.addCell(new Phrase("" + attendance, text_font));

                }
                document.add(Tbody);
                document.add(new Paragraph(10, " "));


                PdfPTable Tfoot = new PdfPTable(1);
                Tfoot.getDefaultCell().setBorder(0);
                Tfoot.setWidthPercentage(90f);
                Tfoot.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                Tfoot.addCell(new Phrase(userNS, in_font));
                document.add(Tfoot);

            } else {
                document.add(new Phrase("no records found"));
            }

            document.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (base != null) {
                    base.close();
                    inst.close();
                    stLess.close();
                }
            } catch (SQLException sqle) {
                sqle.getErrorCode();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
