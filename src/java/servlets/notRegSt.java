package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;
import iaau.DbInstructor;

public class notRegSt extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        String user = request.getRemoteUser();
        PrintWriter out = response.getWriter();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        ResultSetMetaData rsm = null;
        BaseDb base = null;
        DbInstructor inst = null;
        String year = (String) session.getAttribute("yearID");
        String sem = (String) session.getAttribute("semID");
        String sql = null;

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Not registered student list</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        out.println("<center><table width=90% border=1 cellspacing=1 cellpadding=2 class=tealtable >");
        out.println("<tr><td><b>Student ID</td><td><b>Name Surname</td><td><b>Group</td><td><b>Email</td><td><b>Phone</td></tr>");
        try {
            base = new BaseDb();
            conn = base.getConnection();
            inst = new DbInstructor();
            inst.connect();
            inst.execSQLRN(user);

            if ((inst.q.get(0).getInstructorRole().equals("rector")) || (inst.q.get(0).getInstructorRole().equals("studSecO")) || (inst.q.get(0).getInstructorRole().equals("oidb"))) {
                sql = "SELECT "
                        + "  student.rollnum,"
                        + "  concat(student.name,' ',student.surname) as name,"
                        + "  sinif.name,"
                        + "  student.email,"
                        + "  student.phone"
                        + " FROM student"
                        + "  LEFT OUTER JOIN sinif ON (student.group_id = sinif.id)"
                        + " WHERE"
                        + "  student.id NOT IN (SELECT less_stud.student_id FROM less_stud WHERE year_id = ? AND sem_id = ? and status=1)"
                        + "and student.edu_status_id=1 order by sinif.name,name;";
                statement = conn.prepareStatement(sql);
                statement.setString(1, year);
                statement.setString(2, sem);
            } else if (inst.q.get(0).getInstructorRole().equals("dean")) {
                sql = "SELECT "
                        + "  student.rollnum,"
                        + "  concat(student.name,' ',student.surname) as name ,"
                        + "  sinif.name,"
                        + "  student.email,"
                        + "  student.phone"
                        + " FROM"
                        + "  student"
                        + "  LEFT OUTER JOIN sinif ON (student.group_id = sinif.id)"
                        + "  LEFT OUTER JOIN department ON (student.dept_id = department.id)"
                        + "  LEFT OUTER JOIN faculty ON (department.faculty_id = faculty.id)"
                        + " WHERE"
                        + "  student.id NOT IN (SELECT less_stud.student_id FROM less_stud WHERE year_id =? AND sem_id =? and status=1)"
                        + "and student.edu_status_id=1 and faculty.id=?";

                statement = conn.prepareStatement(sql);
                statement.setString(1, year);
                statement.setString(2, sem);
                statement.setInt(3, inst.q.get(0).getInstructorF_id());


            } else if (inst.q.get(0).getInstructorRole().equals("hod")) {
                sql = "SELECT "
                        + "  student.rollnum,"
                        + "  concat(student.name,' ',student.surname) as name,"
                        + "  sinif.name,  student.email,"
                        + "  student.phone"
                        + "   FROM  student"
                        + "  LEFT OUTER JOIN sinif ON (student.group_id = sinif.id)"
                        + "  LEFT OUTER JOIN department ON (student.dept_id = department.id)"
                        + " WHERE"
                        + "  student.id NOT IN (SELECT less_stud.student_id FROM less_stud WHERE year_id =? AND sem_id =? and status=1)"
                        + " and student.edu_status_id=1 and student.dept_id=?";

                statement = conn.prepareStatement(sql);
                statement.setString(1, year);
                statement.setString(2, sem);
                statement.setInt(3, inst.q.get(0).getInstructorD_id());


            }

            result = statement.executeQuery();
            rsm = result.getMetaData();
            //out.println("<tr>");
            int colCount = rsm.getColumnCount();
            if (colCount > 0) {
                while (result.next()) {
                    out.println("<tr>");
                    out.println("<td>" + result.getString("student.rollnum") + "</td>" + "<td>" + result.getString("name") + "</td>"
                            + "<td>" + result.getString("sinif.name") + "</td>"
                            + "<td>" + result.getString("student.email") + "</td>"
                            + "<td>" + result.getString("student.phone") + "</td>");
                    out.println("</tr>");
                }//while
            }//if
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (base != null) {
                try {
                    base.close();
                    inst.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            out.println("</table></center></body></html>");
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
