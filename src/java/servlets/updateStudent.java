package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;

public class updateStudent extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Student Modified Successfully . Thank you";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BaseDb base = new BaseDb();
        String studID = request.getParameter("studID");
        String rollnum = request.getParameter("rollnum");
        String studName = request.getParameter("studName");
        String studSurname = request.getParameter("studSurname");
        String acadSt = request.getParameter("acadSt");
        String educSt = request.getParameter("educSt");
        String remark = request.getParameter("remark");
        String attestat = request.getParameter("attestat");
        String ghsT = request.getParameter("ghsT");
        String ghsL = request.getParameter("ghsL");
        String enterY = request.getParameter("enterY");
        String prikaz1 = request.getParameter("prikaz1");
        String prikaz2 = request.getParameter("prikaz2");
        String prikaz3 = request.getParameter("prikaz3");
        String country = request.getParameter("country");
        String oblast = request.getParameter("oblast");
        String region = request.getParameter("region");
        String birthplace = request.getParameter("birthplace");
        String birthdate = request.getParameter("birthdate");
        String gender = request.getParameter("gender");
        String passport = request.getParameter("passport");
        String permadr = request.getParameter("permadr");
        String curradr = request.getParameter("curradr");
        String faculty = request.getParameter("faculty");
        String deprt = request.getParameter("deprt");
        String group = request.getParameter("group");
        String email = request.getParameter("email");
        String nation = request.getParameter("nation");
        String ghsSchool = request.getParameter("ghsSchool");
        String awards = request.getParameter("awards");
        String phone = request.getParameter("phone");
        String blood = request.getParameter("blood");
        String password = request.getParameter("password");
        String motherName = request.getParameter("motherName");
        String motherSurname = request.getParameter("motherSurname");
        String motherWork = request.getParameter("motherWork");
        String motherPh = request.getParameter("motherPh");
        String fatherName = request.getParameter("fatherName");
        String fatherSurname = request.getParameter("fatherSurname");
        String fatherWork = request.getParameter("fatherWork");
        String fatherPh = request.getParameter("fatherPh");
        String photo = request.getParameter("photo");
        String note1 = request.getParameter("note1");
        String note2 = request.getParameter("note2");
        String note3 = request.getParameter("note3");
        String note4 = request.getParameter("note4");
        String grad_project = request.getParameter("grad_project");
        String st = request.getParameter("account");
        boolean status = false;

        try {
            Connection conn = base.getConnection();
            String query = "update student set name = ?, surname = ?, group_id = ?, dept_id = ?, edu_status_id = ?,"
                    + "acad_status_id = ?, grad_school_type_id = ?, grad_school_language_id = ?, enter_year_id = ?, remark_id = ?, "
                    + "edu_order_number1 = ?, awards = ?, attestat = ?, school_id = ?, country_id = ?, region_id = ?, oblast_id = ?, "
                    + "permadr = ?, curradr = ?, birthpl = ?, phone = ?, blood_id = ?, nationality_id = ?, gender_id = ?, birth_date = ?, "
                    + "passport = ?, email = ?, photo = ?, mother_name = ?, mother_surname = ?, mother_workpl = ?, mother_phone = ?, "
                    + "father_name = ?, father_surname = ?, father_workpl = ?, father_phone = ?, note_1 = ?, note_2 = ?, note_3 = ?, "
                    + "note_4 = ?, grad_project = ?, edu_order_number2 = ?, edu_order_number3 = ?  where id = ?";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, studName);
            statement.setString(2, studSurname);
            statement.setString(3, group);
            statement.setString(4, deprt);
            statement.setString(5, educSt);
            statement.setString(6, acadSt);
            statement.setString(7, ghsT);
            statement.setString(8, ghsL);
            statement.setString(9, enterY);
            statement.setString(10, remark);
            statement.setString(11, prikaz1);
            statement.setString(12, awards);
            statement.setString(13, attestat);
            statement.setString(14, ghsSchool);
            statement.setString(15, country);
            statement.setString(16, region);
            statement.setString(17, oblast);
            statement.setString(18, permadr);
            statement.setString(19, curradr);
            statement.setString(20, birthplace);
            statement.setString(21, phone);
            statement.setString(22, blood);
            statement.setString(23, nation);
            statement.setString(24, gender);
            statement.setString(25, birthdate);
            statement.setString(26, passport);
            statement.setString(27, email);
            statement.setString(28, photo);
            statement.setString(29, motherName);
            statement.setString(30, motherSurname);
            statement.setString(31, motherWork);
            statement.setString(32, motherPh);
            statement.setString(33, fatherName);
            statement.setString(34, fatherSurname);
            statement.setString(35, fatherWork);
            statement.setString(36, fatherPh);
            statement.setString(37, note1);
            statement.setString(38, note2);
            statement.setString(39, note3);
            statement.setString(40, note4);
            statement.setString(41, grad_project);
            statement.setString(42, prikaz2);
            statement.setString(43, prikaz3);
            statement.setString(44, studID);
            statement.executeUpdate();


            String query1 = "update users set user_pass=?, status=? where user_name=?;";
            statement = conn.prepareStatement(query1);
            statement.setString(1, password);
            statement.setInt(2, Integer.parseInt(st));
            statement.setString(3, rollnum);
            statement.executeUpdate();

            status = true;
        } catch (Exception e) {

            status = false;
        } finally {
            if (status) {
                request.getSession().setAttribute("Message", succMesg);
            } else {
                request.getSession().setAttribute("Message", errorMesg);
            }

            response.sendRedirect("info.jsp");
            if (base != null) {
                try {
                    base.close();
                } catch (SQLException e) {
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
