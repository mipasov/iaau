package servlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.DbGroup;

public class addGroup extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Group Inserted Successfully . Thank you";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String groupName = request.getParameter("groupName");
        String deptID = request.getParameter("department");
        DbGroup dbGroup = null;
        boolean status = false;

        try {
            dbGroup = new DbGroup();
            dbGroup.connect();

            dbGroup.execAddGroup(groupName, deptID);

            status = true;
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        } finally {
            if (status) {
                request.getSession().setAttribute("Message", succMesg);
            } else {
                request.getSession().setAttribute("Message", errorMesg);
            }

            response.sendRedirect("info.jsp");

            if (dbGroup != null) {
                try {
                    dbGroup.close();
                } catch (Exception ex) {
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
