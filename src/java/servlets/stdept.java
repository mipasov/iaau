package servlets;

import iaau.BaseDb;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class stdept extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        ArrayList<String> deptCode;
        ArrayList<Integer> deptIDs;
        String[] ac_st = {"undergraduate", "master", "PhD"};
        String[] ed_st = {"active", "suspension", "not registered"};
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        ResultSetMetaData rsm = null;
        BaseDb base = null;

        String deptID = "select id,code from department";

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Number of Students by Department</title>");
        out.println("<meta http-equiv=Content-Type content=text/html; charset=UTF-8>");
        out.println("<link rel=stylesheet href=../style/sis_style.css>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        out.println("<center><table width=90% border=1 align=center class=textBold class=example table-stripeclass:alternate>");
        out.println("<thead class=label><tr height=30>"
                + "<th width=5% align=center bgcolor=#eeeeee valign=middle class=textBold"
                + "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;"
                + "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;"
                + "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>"
                + "#</th>");
        out.println("<th colspan=3 width=30% align=center bgcolor=#eeeeee valign=middle class=textBold"
                + "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;"
                + "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;"
                + "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>"
                + "Undergraduate</th>");
        out.println("<th colspan=3 width=30% align=center bgcolor=#eeeeee valign=middle class=textBold"
                + "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;"
                + "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;"
                + "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>"
                + "Master</th>");
        out.println("<th colspan=3 width=30% align=center bgcolor=#eeeeee valign=middle class=textBold"
                + "style=border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;"
                + "border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;"
                + "border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;>"
                + "PhD</th>");
        out.println("</tr></thead><tbody class=reportBody>");
        out.println("<tr><td> </td><td>Active</td><td>Suspension</td><td>Not Registered</td>"
                + "<td>Active</td><td>Suspension</td><td>Not Registered</td>"
                + "<td>Active</td><td>Suspension</td><td>Not Registered</td></tr>");

        try {
            base = new BaseDb();
            conn = base.getConnection();
            deptCode = new ArrayList<String>();
            deptIDs = new ArrayList<Integer>();

            statement = conn.prepareStatement(deptID);
            result = statement.executeQuery();
            while (result.next()) {
                deptIDs.add(result.getInt("id"));
                deptCode.add(result.getString("code"));
            }

            for (int i = 0; i < deptIDs.size(); i++) {
                String sql = null;
                out.println("<tr>");
                out.println("<td>" + deptCode.get(i) + "</td>");
                for (int j = 0; j < ac_st.length; j++) {
                    for (int k = 0; k < ed_st.length; k++) {
                        int sum = 0;

                        sql = "select a.name,e.name,count(distinct s.id) as sum from student as s "
                                + "left join education as e on s.edu_status_id=e.id "
                                + "left join academic as a on s.acad_status_id=a.id "
                                + "where s.dept_id=? and a.name=? and e.name=? group by s.edu_status_id";
                        statement = conn.prepareStatement(sql);
                        statement.setInt(1, deptIDs.get(i));
                        statement.setString(2, ac_st[j]);
                        statement.setString(3, ed_st[k]);
                        result = statement.executeQuery();

                        while (result.next()) {
                            sum = result.getInt("sum");
                        }
                        out.println("<td align=center>" + sum + "</td>");

                    } //k
                } //j
                out.println("</tr>");
            } //i

            out.println("</tbody></table>");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.println("</center></body></html>");
            out.close();
            if (base != null) {
                try {
                    base.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
