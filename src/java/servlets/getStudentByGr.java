package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;

public class getStudentByGr extends HttpServlet {

    Connection conn = null;
    PreparedStatement statement = null;
    ResultSet result = null;
    ResultSetMetaData rsm = null;
    BaseDb base = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String group_id = request.getParameter("group");
        base = new BaseDb();

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Responsible Person</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");

        out.println("<center><table width=550 height=124 align=center border=0 cellpadding=0 cellspacing=0 class=tealtable >");
        out.println("<tr>");
        out.println("<th align=center> Name </th><th align=center> Surname </th><th align=center> Group </th><th align=center>&nbsp</th>");
        out.println("</tr>");

        try {
            conn = base.getConnection();
            String query = "select t1.name,t1.surname,t2.name from student as t1 left join "
                    + "sinif as t2 on t1.group_id=t2.id where t2.id=? ;";

            statement = conn.prepareStatement(query);
            statement.setString(1, group_id);
            result = statement.executeQuery();
            rsm = result.getMetaData();
            int colCount = rsm.getColumnCount();
            if (colCount <= 0) {
                request.getSession().setAttribute("Message", "No entries found");
                response.sendRedirect("info.jsp");
            } else {
                while (result.next()) {
                    out.println("<tr>");
                    for (int i = 1; i <= colCount; ++i) {
                        out.println("<td> </td><td align=center>" + result.getString(i) + "</td>");
                    }
                    out.println("</tr>");
                }
            }
        } catch (Exception e) {
            throw new ServletException(e.getMessage());
        } finally {
            try {
                if (base != null) {
                    base.close();
                }
            } catch (SQLException sqle) {
            }
        }
        out.println("</table></center></body></html>");
        // out.println("<h1>Servlet Search at " + request.getContextPath () + "</h1>");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
