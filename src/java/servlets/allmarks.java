package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.sql.DataSource;

public class allmarks extends HttpServlet {

    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    ResultSetMetaData rsm = null;
    DataSource pool;
    String query;
    String nameSurname;
    String department;
    String[] subjects;
    String[] semester;
    String[] year;
    double marks[][];

    @Override
    public void init() throws ServletException {
        Context env = null;
        try {
            env = (Context) new InitialContext().lookup("java:comp/env");
            pool = (DataSource) env.lookup("jdbc/iaauDB");
            if (pool == null) {
                throw new ServletException(
                        "'iaauDB' is unknown DataSource");
            }
        } catch (NamingException ne) {
            throw new ServletException(ne.getMessage());
        }
    }

    public void findSize(String sid) throws SQLException {
        int size = 0;
        query = "select count(distinct subject_id) from less_stud where student_id=" + sid + ";";
        rs = stmt.executeQuery(query);
        rsm = rs.getMetaData();
        while (rs.next()) {
            size = rs.getInt("count(distinct subject_id)");
        }
        subjects = new String[size];
        semester = new String[size];
        year = new String[size];
        marks = new double[size][4];
    }

    public void execute(String sid) throws SQLException {
        query = "select t1.id,t4.name,t2.id,t2.name,t2.surname, t6.name, t7.semester, t8.year, t9.name, "
                + " t11.mark,t12.mark,t13.mark "
                + //" if(t11.mark*0.4+t12.mark*0.6<t11.mark*0.4+t13.mark*0.6,t11.mark*0.4+t13.mark*0.6,t11.mark*0.4+t12.mark*0.6)" +
                " from less_stud as t1 left join"
                + " subj_exam as t11 on t1.id=t11.stud_less_id"
                + " left join subj_exam as t12 on t12.stud_less_id=t11.stud_less_id"
                + " left join subj_exam as t13 on t13.stud_less_id=t12.stud_less_id "
                + " left join student as t2 on"
                + " t2.id=t1.student_id"
                + " left join subjects as t4 on t4.id=t1.subject_id"
                + " left join sinif as t9 on t9.id=t2.group_id"
                + " left join department as t6 on t6.id=t4.dept_id"
                + " left join semester as t7 on t7.id=t1.sem_id"
                + " left join year as t8 on t8.id=t1.year_id"
                + " where t11.exam_id=1"
                + " and t12.exam_id=2 and t13.exam_id=3"
                + " and t1.student_id=" + sid
                + " and  t8.id<=1"
                + " and t7.id<=2"
                + " and t1.id=t13.stud_less_id order by t1.subject_id";
        rs = stmt.executeQuery(query);
        rsm = rs.getMetaData();
        int index = 0;
        while (rs.next()) {
            nameSurname = rs.getString("t2.surname") + " " + rs.getString("t2.name");
            department = rs.getString("t6.name");
            subjects[index] = rs.getString("t4.name");
            semester[index] = rs.getString("t7.semester");
            year[index] = rs.getString("t8.year");
            marks[index][0] = rs.getInt("t11.mark");
            if (rs.getObject("t11.mark").equals(false)) {
                marks[index][0] = 0;
            }
            marks[index][1] = rs.getInt("t12.mark");
            if (rs.getObject("t12.mark").equals(false)) {
                marks[index][1] = 0;
            }
            marks[index][2] = rs.getInt("t13.mark");
            if (!rs.getObject("t13.mark").equals(false)) {
                marks[index][2] = 0;
            }
            //average[index] = rs.getString("if(t11.mark*0.4+t12.mark*0.6<t11.mark*0.4+t13.mark*0.6,t11.mark*0.4+t13.mark*0.6,t11.mark*0.4+t12.mark*0.6)");
            index++;
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();

        String sid = (String) request.getParameter("sid");

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Transcript Sheet</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        out.println("<center><table border=0 class=labelForm>");
        out.println("<center><table width=622 height=78 border=1"
                + " cellpadding=0 cellspacing=0 class=tealtable ><tr>");
        out.println("<td><img src=../images/Transcript.bmp width=622 height=78 alt=headline/></td></tr></table>");
        out.println("<p> </p>");


        try {
            conn = pool.getConnection();
            stmt = conn.createStatement();

            findSize(sid);
            execute(sid);
            if (subjects.length != 0) {
                out.println("<center><table width=615 align=center cellpadding=1 cellspacing=1 class=tealtable ><tr><td><b>Department : </b><td>" + department + "</td></tr>");
                out.println("<tr><td><b>Name Surname : </b><td>" + nameSurname + "</td>");
                out.println("<td><b>Student ID : </b><td>" + sid + "</td></tr></table>");
                out.println("<p>  </p>");

                out.println("<center>"
                        + "<table  width=615 align=center border=1"
                        + " cellpadding=0 cellspacing=0 class=tealtable >");
                out.println("<tr>");
                out.println("<th>  NO  </th><th>  Subject  </th>"
                        + "<th> Year </th> <th> Semester </th> <th> Mid Term </th> <th> Final </th> <th> Make Up </th> <th> Average </th>");
                out.println("</tr>");

                for (int m = 0; m < subjects.length; m++) {
                    if (((marks[m][0]) * 0.4 + (marks[m][1]) * 0.6) >= 50) {
                        marks[m][3] = ((marks[m][0]) * 0.4 + (marks[m][1]) * 0.6);
                    } else if (((marks[m][0]) * 0.4 + (marks[m][1]) * 0.6) < 50) {
                        marks[m][3] = ((marks[m][0]) * 0.4 + (marks[m][2]) * 0.6);
                    } else if (((marks[m][0]) * 0.4 + (marks[m][1]) * 0.6) < 50 && marks[m][2] == 0) {
                        marks[m][3] = ((marks[m][0]) * 0.4 + (marks[m][1]) * 0.6);
                    }
                }

                for (int i = 0; i < subjects.length; i++) {
                    out.println("<tr><td align=center>" + (i + 1) + "</td><td>" + subjects[i] + "</td><td align = center>" + year[i] + "</td><td align = center>" + semester[i] + "</td><td align = center>" + marks[i][0] + "</td></td><td align = center>" + marks[i][1] + "</td></td><td align = center>" + marks[i][2] + "</td></td><td align = center>" + marks[i][3] + "</td></tr>");
                }
                out.println("</table>");
                out.println("<p> </p>");

                float total = 0;
                for (int i = 0; i < marks.length; i++) {
                    total += marks[i][3];
                }
                out.println("Total Average : " + (total / subjects.length));
            } else {
                out.println("Check details.");
            }

            stmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
            e = e.getNextException();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        out.println("</table></center></body></html>");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
