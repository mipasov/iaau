package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;
import java.sql.ResultSet;

public class addInstructor extends HttpServlet {
    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Instructor Inserted Successfully . Thank you";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        Connection conn = null;
        BaseDb base = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        String iid = null;
        String name = request.getParameter("instName");
        String surname = request.getParameter("instSurname");
        String rollnum = request.getParameter("rollnum");        
        String country = request.getParameter("country");
        String oblast = request.getParameter("oblast");
        String region = request.getParameter("region");
        String birthPl = request.getParameter("birthplace");
        String dob = request.getParameter("birthdate");
        String gender = request.getParameter("gender");
        String passport = request.getParameter("passport");
        String permadr = request.getParameter("permadr");
        String curradr = request.getParameter("curradr");
        String email = request.getParameter("email");
        String nation = request.getParameter("nation");
        String phone = request.getParameter("phone");
        String blood = request.getParameter("blood");
        String password = request.getParameter("password");
        String faculty = request.getParameter("faculty");
        String dept = request.getParameter("dept");
        String group = request.getParameter("group");
        String sts = request.getParameter("status");
        String level= request.getParameter("level");
        String role = request.getParameter("role");
        String photo = request.getParameter("photo");
        String account = request.getParameter("account");
        boolean status = false;      
        try {
            base = new BaseDb();
            conn = base.getConnection();

            String query = "insert ignore into instructor(name,surname,level_id,status_id,rollnum,country_id,"
                    + "region_id,oblast_id,phone,permadr,curradr,birthpl,blood_id,nationality_id,gender_id,"
                    + "birth_date,passport,email,photo,faculty_id,dept_id,group_id)"
                    + " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            statement = conn.prepareStatement(query);
            statement.setString(1,name);
            statement.setString(2,surname);
            statement.setString(3,level);
            statement.setString(4,sts);
            statement.setString(5,rollnum);
            statement.setString(6,country);
            statement.setString(7,region);
            statement.setString(8,oblast);
            statement.setString(9,phone);
            statement.setString(10,permadr);
            statement.setString(11,curradr);
            statement.setString(12,birthPl);
            statement.setString(13,blood);
            statement.setString(14,nation);
            statement.setString(15,gender);
            statement.setString(16,dob);
            statement.setString(17,passport);
            statement.setString(18,email);
            statement.setString(19,photo);
            statement.setString(20,faculty);
            statement.setString(21,dept);
            statement.setString(22,group);
            statement.executeUpdate();

            query = "insert ignore into users values(?,?,?);";
            statement = conn.prepareStatement(query);
            statement.setString(1,rollnum);
            statement.setString(2,password);
            statement.setString(3,account);
            statement.executeUpdate();

            query = "insert ignore into user_roles values(?,?);";
            statement = conn.prepareStatement(query);
            statement.setString(1,rollnum);
            statement.setString(2,role);
            statement.executeUpdate();

            query = "select id from instructor where rollnum=?";
            statement = conn.prepareStatement(query);
            statement.setString(1, rollnum);
            result = statement.executeQuery();
            while(result.next()){
                iid = result.getString("id");
            }
            
            status = true;
            
        }catch (Exception e) {            
                e.printStackTrace();           
            status = false;
        }finally{
            if (status)
                request.getSession().setAttribute("Message",succMesg);
            else
                request.getSession().setAttribute("Message",errorMesg);
            
            response.sendRedirect("instructorForm?iid="+iid);
            
            if(base != null)
                try{
                    base.close();
                }catch (SQLException e){}
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}