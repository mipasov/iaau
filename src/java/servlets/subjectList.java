package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;

public class subjectList extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        String status = (String) request.getParameter("status");
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        ResultSetMetaData rsm = null;
        BaseDb base = null;

        String query = "select t2.code as department,t1.code,t1.name as subject, t1.hours,t1.credit,t1.stdyear,t3.semester "
                + "from subjects as t1 "
                + "left join department as t2 on t1.dept_id=t2.id "
                + "left join semester as t3 on t1.sem_id=t3.id "
                + "where t1.status=? order by t2.code,subject;";

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Active Subjects List</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        out.println("<center><table width=90% border=1 cellspacing=1 cellpadding=2 class=tealtable >");
        out.println("<tr><td><b>Department</td><td><b>Subject Code</td><td><b>Subject Name</td><td><b>Hours</td>"
                + "<td><b>Credit</td><td><b>StdYear</td><td><b>Semester</td></b></tr>");
        try {
            base = new BaseDb();
            conn = base.getConnection();
            statement = conn.prepareStatement(query);
            statement.setString(1, status);

            result = statement.executeQuery();
            rsm = result.getMetaData();
            int colCount = rsm.getColumnCount();

            if (colCount > 0) {
                while (result.next()) {
                    out.println("<tr>");
                    out.println("<td>" + result.getString("department") + "</td><td>" + result.getString("t1.code") + "</td>"
                            + "<td>" + result.getString("subject") + "</td><td>" + result.getString("t1.hours") + "</td>"
                            + "<td>" + result.getString("t1.credit") + "</td><td>" + result.getString("t1.stdyear") + "</td>"
                            + "<td>" + result.getString("t3.semester") + "</td>");
                    out.println("</tr>");
                }//while
            }//if
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.println("</table></center></body></html>");
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
