package servlets;

import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.DbSemester;

public class setRegStatus extends HttpServlet {

    final static String errorMesg = "Please check the details and try again";
    final static String succMesg = "Registration Successfully Set . Thank you";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String CurrSemID = request.getParameter("semester");
        String regStatus = request.getParameter("regstatus");
        
        boolean status = false;
        DbSemester dbSem = new DbSemester();

        try {
            dbSem.connect();
            if(regStatus.equals("enable"))
                dbSem.execUpdate(CurrSemID,1);
            else if(regStatus.equals("disable")){
                dbSem.execUpdate(CurrSemID,0);
            }
                
            status = true;
        } catch (Exception ex) {
            ex.printStackTrace();
            status = false;
        } finally {
            if (status) {
                session.setAttribute("Message", succMesg);
            } else {
                session.setAttribute("Message", errorMesg);
            }

            response.sendRedirect("info.jsp");

            if (dbSem != null) {
                try {
                    dbSem.close();
                } catch (SQLException e) {
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
