package servlets;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import iaau.DbStudent;
import iaau.Student;
import java.awt.Color;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author focus
 */
public class studentForm extends HttpServlet {

    Image img, pic;
    DbStudent st = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String sid = (String) request.getParameter("sid");

        st = new DbStudent();
        try {
            st.connect();
            st.execSQL(sid);

            ArrayList<Student> student = st.getArray();

            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
            Document document = new Document(PageSize.A4, 10, 10, 10, 10);
            PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
            response.setContentType("application/pdf");

            document.open();

            PdfContentByte punder = writer.getDirectContentUnder();
            img = Image.getInstance("/usr/local/images/iaauLogoT.png");
            img.setAbsolutePosition(document.getPageSize().getWidth() / 4, document.getPageSize().getHeight() / 3);
            img.scaleAbsolute(300, 300);

            punder.addImage(img);


            //PdfContentByte image = writer.getDirectContent();
            pic = Image.getInstance("/usr/local/uploads/" + student.get(0).getPhoto());
            pic.setAlignment(Image.ALIGN_RIGHT | Image.ALIGN_TOP);
            //pic.setAbsolutePosition(400, 650);
            //pic.scalePercent(15, 15);
            pic.scaleAbsoluteHeight(100);
            pic.scaleAbsoluteWidth(90);
            //image.addImage(pic);
            document.add(pic);

            Font big_font = new Font(Font.COURIER, 19, Font.BOLD);
            big_font.setColor(new Color(0x92, 0x90, 0x83));
            Font title_font = new Font(Font.COURIER, 13, Font.BOLD);
            title_font.setColor(new Color(0x92, 0x90, 0x83));
            Font warning = new Font(Font.COURIER, 10, Font.BOLD);
            warning.setColor(new Color(0xFF, 0x00, 0x00));
            Font in_font = new Font(Font.COURIER, 12, Font.BOLD);
            Font text_font = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);

            Paragraph iaau = new Paragraph("INTERNATIONAL ATATURK ALATOO UNIVERSITY", title_font);
            iaau.setAlignment(Element.ALIGN_CENTER);
            Paragraph sif = new Paragraph("STUDENT INFORMATION FORM", big_font);
            sif.setAlignment(Element.ALIGN_CENTER);
            document.add(iaau);
            document.add(sif);
            document.add(new Paragraph(15, " "));

            float[] table_colsWidth = {1f, 2f};
            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(90f);
            table.setWidths(table_colsWidth);

            if (!st.q.isEmpty()) {
                table.addCell(new Phrase("Student ID", in_font));
                table.addCell(new Phrase(student.get(0).getStRollnum(), text_font));
                table.addCell(new Phrase("Name Surname", in_font));
                table.addCell(new Phrase(student.get(0).getName() + " " + student.get(0).getSurname(), title_font));
                table.addCell(new Phrase("Academic Status", in_font));
                table.addCell(new Phrase(student.get(0).getAcademic(), text_font));
                table.addCell(new Phrase("Education Status", in_font));
                table.addCell(new Phrase(student.get(0).getEducation(), text_font));
                table.addCell(new Phrase("Remark", in_font));
                table.addCell(new Phrase(student.get(0).getRemark(), text_font));
                table.addCell(new Phrase("GHS Type", in_font));
                table.addCell(new Phrase(student.get(0).getGhsType(), text_font));
                table.addCell(new Phrase("GHS Language", in_font));
                table.addCell(new Phrase(student.get(0).getGhsLanguage(), text_font));
                table.addCell(new Phrase("Entering Academic Year", in_font));
                table.addCell(new Phrase(student.get(0).getEnterY(), text_font));
                table.addCell(new Phrase("Entering Order Number", in_font));
                table.addCell(new Phrase(student.get(0).getEnterOrderNumber(), text_font));
                table.addCell(new Phrase("Group", in_font));
                table.addCell(new Phrase(student.get(0).getGroupName(), text_font));
                table.addCell(new Phrase("Department", in_font));
                table.addCell(new Phrase(student.get(0).getDepartName(), text_font));
                table.addCell(new Phrase("Country", in_font));
                table.addCell(new Phrase(student.get(0).getCountry(), text_font));
                table.addCell(new Phrase("Birthplace", in_font));
                table.addCell(new Phrase(student.get(0).getBirthPlace(), text_font));
                table.addCell(new Phrase("Birth date", in_font));
                table.addCell(new Phrase(student.get(0).getDoB().toString(), text_font));
                table.addCell(new Phrase("Gender", in_font));
                table.addCell(new Phrase(student.get(0).getGender(), text_font));
                table.addCell(new Phrase("Passport №", in_font));
                table.addCell(new Phrase(student.get(0).getPassport(), text_font));
                table.addCell(new Phrase("Permanent Address", in_font));
                table.addCell(new Phrase(student.get(0).getPurmAddress(), text_font));
                table.addCell(new Phrase("Current Address", in_font));
                table.addCell(new Phrase(student.get(0).getCurrAddress(), text_font));
                table.addCell(new Phrase("Nationality", in_font));
                table.addCell(new Phrase(student.get(0).getNationality(), text_font));
                table.addCell(new Phrase("Graduated School", in_font));
                table.addCell(new Phrase(student.get(0).getSchool(), text_font));
                table.addCell(new Phrase("e-mail", in_font));
                table.addCell(new Phrase(student.get(0).getEmail(), text_font));
                table.addCell(new Phrase("Awards", in_font));
                table.addCell(new Phrase(student.get(0).getAwards(), text_font));
                table.addCell(new Phrase("Phone Number", in_font));
                table.addCell(new Phrase(student.get(0).getPhoneNumber(), text_font));
                table.addCell(new Phrase("Blood Type", in_font));
                table.addCell(new Phrase(student.get(0).getBlood(), text_font));
                table.addCell(new Phrase("Mother Name Surname", in_font));
                table.addCell(new Phrase(student.get(0).getMotherName() + " " + student.get(0).getMotherSurname(), text_font));
                table.addCell(new Phrase("Mother Work Place", in_font));
                table.addCell(new Phrase(student.get(0).getMotherWorkPlace(), text_font));
                table.addCell(new Phrase("Mother Phone", in_font));
                table.addCell(new Phrase(student.get(0).getMotherPh(), text_font));
                table.addCell(new Phrase("Father Name Surname", in_font));
                table.addCell(new Phrase(student.get(0).getFatherName() + " " + student.get(0).getFatherSurname(), text_font));
                table.addCell(new Phrase("Father Work Place", in_font));
                table.addCell(new Phrase(student.get(0).getFatherWorkPlace(), text_font));
                table.addCell(new Phrase("Father Phone", in_font));
                table.addCell(new Phrase(student.get(0).getFatherPh(), text_font));
                table.addCell(new Phrase("ORT", in_font));
                table.addCell(new Phrase(student.get(0).getNote1(), text_font));
                table.addCell(new Phrase("Additional ORT", in_font));
                table.addCell(new Phrase(student.get(0).getNote2(), text_font));
                table.addCell(new Phrase("Pripisnoe", in_font));
                table.addCell(new Phrase(student.get(0).getNote3(), text_font));
                table.addCell(new Phrase("Note 4", in_font));
                table.addCell(new Phrase(student.get(0).getNote4(), text_font));

                document.add(table);

                document.add(new Paragraph(25, " "));
                float[] footer_colsWidth = {2f, 1f};
                PdfPTable footer = new PdfPTable(2);
                footer.setWidthPercentage(90f);
                footer.setTotalWidth(footer_colsWidth);
                footer.getDefaultCell().setBorder(0);
                footer.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                footer.addCell(new Phrase("Student Signature __________________ ", text_font));
                footer.addCell(new Phrase("Date : " + df.format(new java.util.Date()).toString(), text_font));

                document.add(footer);

            } else {
                document.add(new Paragraph("Sorry, there is no data.", warning));
            }

            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
