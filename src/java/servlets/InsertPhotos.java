package servlets;

import iaau.BaseDb;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author everest
 */
public class InsertPhotos extends HttpServlet {

    Connection conn = null;
    PreparedStatement statement = null;
    ResultSet result = null;
    ResultSetMetaData rsm = null;
    BaseDb base = null;
    private ArrayList<String> nameSurname;
    private ArrayList<String> studentID;

    public void students() throws SQLException {
        studentID = new ArrayList<String>();
        nameSurname = new ArrayList<String>();

        String query = "select id,lower(concat(name,' ',surname)) as name from student";

        statement = conn.prepareStatement(query);
        result = statement.executeQuery();

        while (result.next()) {
            studentID.add(result.getString("id"));
            nameSurname.add(result.getString("name"));
        }

    }

    public void insertPhoto(String photo, String id) throws SQLException {

        String query = "update student set photo=? where id=?";

        statement = conn.prepareStatement(query);
        statement.setString(1, photo);
        statement.setString(2, id);
        statement.executeUpdate();

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            base = new BaseDb();
            conn = base.getConnection();

            students();

            for (int i = 0; i < nameSurname.size(); i++) {
                insertPhoto(nameSurname.get(i), studentID.get(i));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (base != null) {
                try {
                    base.close();
                } catch (SQLException ex) {
                }
            }
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
