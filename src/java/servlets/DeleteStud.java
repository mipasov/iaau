package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DeleteStud extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Record deleted successfully. Thank you";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BaseDb base = new BaseDb();
        String user = request.getRemoteUser();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
        String path = "/home/focus/Documents/delete.txt";
        String[] ids = request.getParameterValues("studID");
        String[] department = request.getParameterValues("department");
        String[] code = request.getParameterValues("code");
        String[] subject = request.getParameterValues("subject");
        String time = null;
        int type = Integer.parseInt(request.getParameter("type"));
        String query = null;
        boolean status = false;

//        File f = new File(path);
//        if(f.canWrite()) {
//                FileOutputStream fos = new FileOutputStream(f);
//                DataOutputStream dos = new DataOutputStream(fos);
//                dos.writeBytes("\ndata written by Remoteservice");
//                System.out.println("\ndata written by Remoteservice");
//                dos.flush();
//            } else {
//                System.out.println("No permissions to write to this folder");
//            }

        Date d = new Date();
        c.setTime(d);
        time = c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND);

        //FileOutputStream fos = new FileOutputStream(path, true);
        //DataOutputStream dos = new DataOutputStream(fos);

        //dos.writeBytes("\n" + df.format(new java.util.Date()) + " " + time + " " + user + ": ");

        switch (type) {
            case 1:
                query = "delete from student where ";
                break;
            case 2:
                query = "delete from subjects where ";
                break;
            case 3:
                query = "delete from less_stud where ";
                break;
            case 4:
                query = "delete from users where ";
                break;
            case 5:
                query = "delete from department where ";
                break;
            case 6:
                query = "delete from sinif where ";
                break;            
            case 8:
                query = "delete from faculty where ";
                break;
        }

        if (ids != null) {
            for (int i = 0; i < ids.length; i++) {
                if (i == 0) {
                    query = query + " id=" + ids[i];
                //dos.writeBytes(" " + query);
                } else {
                    query = query + " or " + "id=" + ids[i];
                //dos.writeBytes(" " + query);
                }
            }
            status= true;
        }
        //dos.flush();
        //dos.close();
        try {
            if (status) {
                Connection conn = base.getConnection();
                PreparedStatement statement = conn.prepareStatement(query);
                statement.executeUpdate();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (status) {
                request.getSession().setAttribute("Message", succMesg);
            } else {
                request.getSession().setAttribute("Message", errorMesg);
            }

            if (base != null) {
                try {
                    base.close();
                } catch (Exception ex) {
                }
            }
            response.sendRedirect("info.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
