package servlets;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import iaau.BaseDb;
import java.awt.Color;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.http.HttpSession;

public class newTranscript extends HttpServlet {

    Connection conn = null;
    PreparedStatement statement = null;
    ResultSet result = null;
    ResultSetMetaData rsm = null;
    BaseDb base = null;
    private String nameSurname;
    private String rollnum;
    private String department;
    private String faculty;
    private String grad_project;
    private String acadStatus;
    private Date dob;
    private String gender;
    private String mark;
    private ArrayList<String> stud_less_id;
    private ArrayList<String> subjectID;
    private ArrayList<String> subjectName;
    private ArrayList<String> subjectCode;
    private ArrayList<String> semesterID;
    private ArrayList<String> semesterName;
    private ArrayList<String> yearID;
    private ArrayList<String> yearName;
    private ArrayList<String> year_name;
    private ArrayList<Integer> credit;
    private ArrayList<Integer> year_id;
    private double total;
    private double creditSum;
    private Image img;

    public static double round(double d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Double.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
        String sid = (String) request.getParameter("sid");

        try {
            base = new BaseDb();
            conn = base.getConnection();
            subjects(sid);
            execute(sid);
            year_count(sid);

            Document document = new Document(PageSize.A4, 10, 10, 10, 10);
            PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
            response.setContentType("application/pdf");
            //response.setHeader("Content-Disposition", " attachment; filename=\"transcript.pdf\"");

            document.open();

            PdfContentByte punder = writer.getDirectContentUnder();
            img = Image.getInstance("/usr/local/images/iaauLogoT.png");
            img.setAbsolutePosition(document.getPageSize().getWidth() / 4, document.getPageSize().getHeight() / 3);
            img.scaleAbsolute(300, 300);

            punder.addImage(img);

            Font title_font = new Font(Font.COURIER, 10, Font.BOLD);
            title_font.setColor(new Color(0x92, 0x90, 0x83));
            Font text_font = new Font(Font.TIMES_ROMAN, 8, Font.NORMAL);
            //Chunk chunk1 = new Chunk("INTERNATIONAL ATATURK ALATOO UNIVERSITY UNDERGRADUATE STUDENT`S TRANSCRIPT FORM", title_font);
            //Chunk chunk2 = new Chunk("UNDERGRADUATE STUDENT`S TRANSCRIPT FORM", title_font);
            //Paragraph paragraph = new Paragraph();
            //paragraph.add(chunk1);
            //paragraph.add(Chunk.NEWLINE);
            //paragraph.add(chunk2);
            //paragraph.setAlignment(Element.ALIGN_CENTER);
            //document.add(paragraph);
            //document.add(new Paragraph(5, " "));

            Paragraph h = new Paragraph(faculty + " Faculty - Department of " + department, title_font);
            h.setAlignment(Element.ALIGN_CENTER);
            Paragraph st = new Paragraph("Transcript of " + nameSurname + "[" + rollnum + "]", title_font);
            st.setAlignment(Element.ALIGN_CENTER);
            Paragraph in = new Paragraph("Date of birth:" + dob + "  Degree conferred:" + acadStatus + "  Gender:" + gender, title_font);
            in.setAlignment(Element.ALIGN_CENTER);
            document.add(h);
            document.add(st);
            document.add(in);
            document.add(new Paragraph(5, " "));

            float[] table_colsWidth = {2f, 2f};
            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100f);
            table.setWidths(table_colsWidth);

            total = 0;
            creditSum = 0;

            for (int i = 0; i < year_id.size(); i++) {

                float[] head_table_colsWidth = {2f, 3f, 0.5f, 0.5f};
                PdfPTable head_table1 = new PdfPTable(4);
                head_table1.setWidths(head_table_colsWidth);
                head_table1.getDefaultCell().setBorder(0);

                PdfPTable head_table2 = new PdfPTable(4);
                head_table2.setWidths(head_table_colsWidth);
                head_table2.getDefaultCell().setBorder(0);

                head_table1.addCell(new Phrase("COURSE NAME", title_font));
                head_table1.addCell(new Phrase(year_name.get(i) + " FALL", title_font));
                head_table1.addCell(new Phrase("C", title_font));
                head_table1.addCell(new Phrase("G", title_font));

                head_table2.addCell(new Phrase("COURSE NAME", title_font));
                head_table2.addCell(new Phrase(year_name.get(i) + " SPRING", title_font));
                head_table2.addCell(new Phrase("C", title_font));
                head_table2.addCell(new Phrase("G", title_font));

                float[] colsWidth = {5f, 0.5f, 0.5f};
                PdfPTable tableIn1 = new PdfPTable(3);
                tableIn1.setWidths(colsWidth);
                tableIn1.getDefaultCell().setBorder(0);
                PdfPCell cell1 = new PdfPCell(head_table1);
                cell1.setColspan(4);
                tableIn1.addCell(cell1);

                PdfPTable tableIn2 = new PdfPTable(3);
                tableIn2.setWidths(colsWidth);
                tableIn2.getDefaultCell().setBorder(0);
                PdfPCell cell2 = new PdfPCell(head_table2);
                cell2.setColspan(4);
                tableIn2.addCell(cell2);

                for (int j = 0; j < subjectID.size(); j++) {
                    if (yearID.get(j).equals(Integer.toString(year_id.get(i))) && (semesterID.get(j).equals("1"))) {
                        double av = calcAverage(stud_less_id.get(j), subjectID.get(j));
                        if (av < 101) {
                            total += av * credit.get(j);
                            creditSum += credit.get(j);
                            mark = Long.toString(Math.round(av));
                        } else {
                            mark = "IP";
                        }
                        //tableIn1.addCell(new Phrase(subjectCode.get(j), text_font));
                        tableIn1.addCell(new Phrase(subjectName.get(j), text_font));
                        tableIn1.addCell(new Phrase(Integer.toString(credit.get(j)), text_font));
                        tableIn1.addCell(new Phrase(mark, text_font));
                    }
                    if (yearID.get(j).equals(Integer.toString(year_id.get(i))) && (semesterID.get(j).equals("2"))) {
                        double av = calcAverage(stud_less_id.get(j), subjectID.get(j));
                        if (av < 101) {
                            total += av * credit.get(j);
                            creditSum += credit.get(j);
                            mark = Long.toString(Math.round(av));
                        } else {
                            mark = "IP";
                        }
                        //tableIn2.addCell(new Phrase(subjectCode.get(j), text_font));
                        tableIn2.addCell(new Phrase(subjectName.get(j), text_font));
                        tableIn2.addCell(new Phrase(Integer.toString(credit.get(j)), text_font));
                        tableIn2.addCell(new Phrase(mark, text_font));
                    }
                } //for j
                table.addCell(tableIn1);
                table.addCell(tableIn2);
            } // for i

            document.add(table);

            document.add(new Paragraph(5, " "));
            float[] foot_colsWidth = {3f, 1f};
            PdfPTable foot = new PdfPTable(2);
            foot.setWidthPercentage(100f);
            foot.setTotalWidth(foot_colsWidth);
            foot.getDefaultCell().setBorder(0);
            foot.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            foot.addCell(new Phrase("GRADUATE PROJECT TITLE:" + grad_project, text_font));
            foot.addCell(new Phrase("Grade Point Average: " + Math.round((total / creditSum)), text_font));
            document.add(foot);

            document.add(new Paragraph(5, " "));
            float[] footer_colsWidth = {3f, 1f};
            PdfPTable footer = new PdfPTable(2);
            footer.setWidthPercentage(100f);
            footer.setTotalWidth(footer_colsWidth);
            footer.getDefaultCell().setBorder(0);
            footer.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            footer.addCell(new Phrase(" Askarbek Kamchiev", text_font));
            footer.addCell(new Phrase("Credit sum: " + Math.round(creditSum), text_font));
            footer.addCell(new Phrase("Head of Student Affairs", text_font));
            footer.addCell(new Phrase("", text_font));
            footer.addCell(new Phrase("Date of issue: " + df.format(new java.util.Date()) + "", text_font));
            footer.addCell(new Phrase("", text_font));
            document.add(footer);

            document.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (base != null) {
                try {
                    base.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    public String getServletInfo() {
        return "Short description";
    }

    public void subjects(String sid) throws SQLException {

        stud_less_id = new ArrayList<String>();
        subjectID = new ArrayList<String>();
        yearID = new ArrayList<String>();
        semesterID = new ArrayList<String>();
        subjectCode = new ArrayList<String>();
        subjectName = new ArrayList<String>();
        credit = new ArrayList<Integer>();
        yearName = new ArrayList<String>();
        semesterName = new ArrayList<String>();

        String query = "select t1.id, t1.subject_id, t1.year_id, t1.sem_id, t2.code, t2.name, t2.credit,"
                + " t5.year, t6.semester"
                + " from less_stud as t1 left join subjects as t2 on t1.subject_id=t2.id"
                + " left join year as t5 on t1.year_id=t5.id"
                + " left join semester as t6 on t1.sem_id=t6.id"
                + " where t1.student_id=? and t1.status<4 order by t1.year_id,t1.sem_id;";
        statement = conn.prepareStatement(query);
        statement.setString(1, sid);
        result = statement.executeQuery();
        while (result.next()) {
            stud_less_id.add(result.getString("t1.id"));
            subjectID.add(result.getString("t1.subject_id"));
            yearID.add(result.getString("t1.year_id"));
            semesterID.add(result.getString("t1.sem_id"));
            subjectCode.add(result.getString("t2.code"));
            subjectName.add(result.getString("t2.name"));
            credit.add(result.getInt("t2.credit"));
            yearName.add(result.getString("t5.year"));
            semesterName.add(result.getString("t6.semester"));
        }
    }

    public void year_count(String sid) throws SQLException {

        year_id = new ArrayList<Integer>();
        year_name = new ArrayList<String>();

        String query = " select distinct t1.year_id,t2.year from less_stud as t1 left join year as t2 on t1.year_id=t2.id"
                + " where student_id=? order by year_id";
        statement = conn.prepareStatement(query);
        statement.setString(1, sid);
        result = statement.executeQuery();
        while (result.next()) {
            year_id.add(result.getInt("t1.year_id"));
            year_name.add(result.getString("t2.year"));
        }
    }

    public void execute(String sid) throws SQLException {

        String query = "select concat(t1.name,' ',t1.surname)as name, t1.rollnum, t1.birth_date, t4.name, t3.name, t5.name,"
                + " t6.code, t1.grad_project"
                + " from student as t1"
                + " left join department as t3 on t1.dept_id=t3.id"
                + " left join faculty as t4 on t4.id=t3.faculty_id"
                + " left join academic as t5 on t1.acad_status_id=t5.id"
                + " left join gender as t6 on t1.gender_id=t6.id"
                + " where t1.id=? ;";
        statement = conn.prepareStatement(query);
        statement.setString(1, sid);
        result = statement.executeQuery();

        while (result.next()) {
            nameSurname = result.getString("name");
            rollnum = result.getString("t1.rollnum");
            faculty = result.getString("t4.name");
            department = result.getString("t3.name");
            acadStatus = result.getString("t5.name");
            dob = result.getDate("t1.birth_date");
            gender = result.getString("t6.code");
            grad_project = result.getString("t1.grad_project");
        }
    }

    public double calcAverage(String s_less_id, String subj_id) throws SQLException {
        String query = "select t3.exam_name as exam,(t4.mark * t3.percentage/100) as average from less_stud "
                + " as t5 left join student as  t1 on t5.student_id=t1.id left join "
                + " subjects as t2 on t5.subject_id=t2.id left join sinif as t6 on "
                + " t1.group_id=t6.id  left join subj_exam as t4 on t5.id = t4.stud_less_id "
                + " left join exam as t3 on t4.exam_id=t3.exam_id "
                + " where t2.id=? and t4.stud_less_id =? ;";

        double average = 0;
        double midterm = 0, fin = 0, mup = 0.0;
        int colCount = 0;

        statement = conn.prepareStatement(query);
        statement.setString(1, subj_id);
        statement.setString(2, s_less_id);
        result = statement.executeQuery();

        while (result.next()) {
            String exam = result.getString("exam");
            if (exam.equals("Midterm")) {
                midterm = result.getDouble("average");
            } else if (exam.equals("Final")) {
                fin = result.getDouble("average");
                if (fin < 29.7) {
                    fin = 0.0;
                }
            } else if (exam.equals("MakeUp")) {
                mup = result.getDouble("average");
                if (mup < 29.7) {
                    mup = 0.0;
                }
            }
            if ((midterm + fin) < 49.5) {
                average = midterm + mup;
            } else {
                average = midterm + fin;
            }
            colCount++;
        }
        if (colCount < 2) {
            average = 101;
        }
        return average;
    }
}
