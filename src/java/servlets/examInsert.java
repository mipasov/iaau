package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.DbSubjExam;

public class examInsert extends HttpServlet {
    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Record Inserted Successfully . Thank you";
    Connection conn = null;
    PreparedStatement statement = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DbSubjExam dbSubjEx = null;
        String exam_id = request.getParameter("exam");
        String subjectID = request.getParameter("subjID");
        String[] SubjLessIDs = request.getParameterValues("SubjLessID");
        String[] marks = request.getParameterValues("examMark");
        boolean status = false;

        try {
            dbSubjEx = new DbSubjExam();
            dbSubjEx.connect();

            for (int j = 0; j < marks.length; j++) {
                if (marks[j].equals("") || marks[j].equals("null")) {
                } else {
                    int mark = Integer.parseInt(marks[j]);
                    if ((mark < 0) || (mark > 100)) {
                        throw new ServletException();
                    }
                }
            }

            for (int i = 0; i < SubjLessIDs.length; i++) {
                String currentMark = marks[i];
                if (currentMark == null || currentMark.equals("")) {
                    currentMark = "0";
                }
                dbSubjEx.insertSQL(SubjLessIDs[i], exam_id, currentMark);
            }
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        } finally {
            if (status) {
                request.getSession().setAttribute("Message", succMesg);
                response.sendRedirect("midtermMarks?subjID=" + subjectID + "&exam=" + exam_id);
            } else {
                request.getSession().setAttribute("Message", errorMesg);
                response.sendRedirect("info.jsp");
            }
            try {
                dbSubjEx.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
