package servlets;

import iaau.DbSubjExam;
import java.io.*;
import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.http.*;

public class updateMarks extends HttpServlet {

    static final String SUCCESS_MESSAGE = "Record Inserted Successfully . Thank you";
    static final String FAILURE_MESSAGE = "Please check the details and try again";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");

        String[] subjExamIDs = request.getParameterValues("subjExamID");
        String[] marks = request.getParameterValues("mark");
        String subject = request.getParameter("subjID");
        String message = null;
        DbSubjExam dbSExam = null;
        try {

            for (int j = 0; j < marks.length; j++) {
                int mark = Integer.parseInt(marks[j]);
                if ((mark < 0) || (mark > 100)) {
                    throw new ServletException();
                }
            }


            if (subjExamIDs.length != marks.length) {
                throw new Exception("Length mismatch between inputs");
            }
            dbSExam = new DbSubjExam();
            dbSExam.connect();
            for (int i = 0; i < subjExamIDs.length; i++) {
                int id = Integer.parseInt(subjExamIDs[i]), mark = Integer.parseInt(marks[i]);
                dbSExam.updateSQL(id, mark);
            }
            message = SUCCESS_MESSAGE;
        } catch (Exception exc) {
            message = FAILURE_MESSAGE;
        } finally {
            request.getSession().setAttribute("Message", message);
            response.sendRedirect("info.jsp");
            if (dbSExam != null) {
                try {
                    dbSExam.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
