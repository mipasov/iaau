package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;
import java.sql.ResultSet;

public class addStudent extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String succMesg = "Student Inserted Successfully . Thank you";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BaseDb base = new BaseDb();
        ResultSet result = null;
        String sid = null;
        String name = request.getParameter("studName");
        String surname = request.getParameter("studSurname");
        String rollnum = request.getParameter("rollnum");
        //String role = request.getParameter("role");
        String group = request.getParameter("group");
        String dept = request.getParameter("deprt");
        String acadSt = request.getParameter("acadSt");
        String eduSt = request.getParameter("eduSt");
        String remark = request.getParameter("remark");
        String ghsT = request.getParameter("ghsT");
        String ghsL = request.getParameter("ghsL");
        String enterY = request.getParameter("enterY");
        String prikaz = request.getParameter("prikaz");
        String country = request.getParameter("country");
        String oblast = request.getParameter("oblast");
        String region = request.getParameter("region");
        String birthplace = request.getParameter("birthplace");
        String birthdate = request.getParameter("birthdate");
        String gender = request.getParameter("gender");
        String passport = request.getParameter("passport");
        String permadr = request.getParameter("permadr");
        String curradr = request.getParameter("curradr");
        String email = request.getParameter("email");
        String nation = request.getParameter("nation");
        String ghsSchool = request.getParameter("ghsSchool");
        String awards = request.getParameter("awards");
        String attestat = request.getParameter("attestat");
        String phone = request.getParameter("phone");
        String blood = request.getParameter("blood");
        String password = request.getParameter("password");
        String motherName = request.getParameter("motherName");
        String motherSurname = request.getParameter("motherSurname");
        String motherWork = request.getParameter("motherWork");
        String motherPh = request.getParameter("motherPh");
        String fatherName = request.getParameter("fatherName");
        String fatherSurname = request.getParameter("fatherSurname");
        String fatherWork = request.getParameter("fatherWork");
        String fatherPh = request.getParameter("fatherPh");
        String photo = request.getParameter("photo");
        String note1 = request.getParameter("note1");
        String note2 = request.getParameter("note2");
        String note3 = request.getParameter("note3");
        String note4 = request.getParameter("note4");
        String st = request.getParameter("account");

        boolean status = false;

        try {
            Connection conn = base.getConnection();
            String sql = "insert into student (name, surname, group_id, dept_id, rollnum, edu_status_id, acad_status_id, "
                    + "grad_school_type_id, grad_school_language_id, enter_year_id, remark_id, edu_order_number1, awards, attestat, "
                    + "school_id, country_id, region_id, oblast_id, permadr, curradr, birthpl, "
                    + "phone, blood_id, nationality_id, gender_id, birth_date, passport, email, "
                    + "photo, mother_name, mother_surname, mother_workpl, mother_phone, father_name, father_surname, "
                    + "father_workpl, father_phone, note_1, note_2, note_3, note_4) "
                    + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, name);
            statement.setString(2, surname);
            statement.setString(3, group);
            statement.setString(4, dept);
            statement.setString(5, rollnum);
            statement.setString(6, eduSt);
            statement.setString(7, acadSt);
            statement.setString(8, ghsT);
            statement.setString(9, ghsL);
            statement.setString(10, enterY);
            statement.setString(11, remark);
            statement.setString(12, prikaz);
            statement.setString(13, awards);
            statement.setString(14, attestat);
            statement.setString(15, ghsSchool);
            statement.setString(16, country);
            statement.setString(17, region);
            statement.setString(18, oblast);
            statement.setString(19, permadr);
            statement.setString(20, curradr);
            statement.setString(21, birthplace);
            statement.setString(22, phone);
            statement.setString(23, blood);
            statement.setString(24, nation);
            statement.setString(25, gender);
            statement.setString(26, birthdate);
            statement.setString(27, passport);
            statement.setString(28, email);
            statement.setString(29, photo);
            statement.setString(30, motherName);
            statement.setString(31, motherSurname);
            statement.setString(32, motherWork);
            statement.setString(33, motherPh);
            statement.setString(34, fatherName);
            statement.setString(35, fatherSurname);
            statement.setString(36, fatherWork);
            statement.setString(37, fatherPh);
            statement.setString(38, note1);
            statement.setString(39, note2);
            statement.setString(40, note3);
            statement.setString(41, note4);
            statement.executeUpdate();

            String query3 = "insert ignore into users values(?,?,?);";
            statement = conn.prepareStatement(query3);
            statement.setString(1, rollnum);
            statement.setString(2, password);
            statement.setInt(3, Integer.parseInt(st));
            statement.executeUpdate();

            String query4 = "insert ignore into user_roles values(?,'student');";
            statement = conn.prepareStatement(query4);
            statement.setString(1, rollnum);
            statement.executeUpdate();

            String query5 = "select id from student where rollnum=?";
            statement = conn.prepareStatement(query5);
            statement.setString(1, rollnum);
            result = statement.executeQuery();
            while (result.next()) {
                sid = result.getString("id");
            }

            status = true;
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        } finally {
            if (status) {
                request.getSession().setAttribute("Message", succMesg);
            } else {
                request.getSession().setAttribute("Message", errorMesg);
            }

            response.sendRedirect("studentForm?sid=" + sid);
            if (base != null) {
                try {
                    base.close();
                } catch (SQLException e) {
                }
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
