package servlets;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import iaau.DbInstructor;
import iaau.Instructor;
import java.awt.Color;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class instructorForm extends HttpServlet {

    Image img;
    DbInstructor inst = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String iid = (String) request.getParameter("iid");
        inst = new DbInstructor();

        try {
            inst.connect();
            inst.execSQLID(iid);
            ArrayList<Instructor> instructor = inst.getArray();

            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
            Document document = new Document(PageSize.A4, 10, 10, 10, 10);
            PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
            response.setContentType("application/pdf");

            document.open();

            PdfContentByte punder = writer.getDirectContentUnder();
            img = Image.getInstance("/usr/local/images/iaauLogoT.png");
            img.setAbsolutePosition(document.getPageSize().getWidth() / 4, document.getPageSize().getHeight() / 3);
            img.scaleAbsolute(300, 300);

            punder.addImage(img);

            Font big_font = new Font(Font.COURIER, 19, Font.BOLD);
            big_font.setColor(new Color(0x92, 0x90, 0x83));
            Font title_font = new Font(Font.COURIER, 13, Font.BOLD);
            title_font.setColor(new Color(0x92, 0x90, 0x83));
            Font warning = new Font(Font.COURIER, 12, Font.BOLD);
            warning.setColor(new Color(0xFF, 0x00, 0x00));
            Font in_font = new Font(Font.COURIER, 12, Font.BOLD);
            Font text_font = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);

            Paragraph iaau = new Paragraph("INTERNATIONAL ATATURK ALATOO UNIVERSITY", title_font);
            iaau.setAlignment(Element.ALIGN_CENTER);
            Paragraph sif = new Paragraph("INSTRUCTOR INFORMATION FORM", big_font);
            sif.setAlignment(Element.ALIGN_CENTER);
            document.add(iaau);
            document.add(sif);
            document.add(new Paragraph(15, " "));

            float[] table_colsWidth = {1f, 2f};
            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(90f);
            table.setWidths(table_colsWidth);

            if (!inst.q.isEmpty()) {
                table.addCell(new Phrase("Instructor ID", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorRollNum(), text_font));
                table.addCell(new Phrase("Name Surname", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorName() + " " + instructor.get(0).getInstructorSurname(), title_font));
                table.addCell(new Phrase("Country", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorCountry(), text_font));
                table.addCell(new Phrase("Birthplace", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorBirthPl(), text_font));
                table.addCell(new Phrase("Birth date", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorDoB().toString(), text_font));
                table.addCell(new Phrase("Gender", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorGender(), text_font));
                table.addCell(new Phrase("Passport №", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorPassport(), text_font));
                table.addCell(new Phrase("Permanent Address", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorPermAdd(), text_font));
                table.addCell(new Phrase("Current Address", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorCurrAdd(), text_font));
                table.addCell(new Phrase("e-mail", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorEmail(), text_font));
                table.addCell(new Phrase("Nationality", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorNationality(), text_font));
                table.addCell(new Phrase("Phone Number", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorPhone(), text_font));
                table.addCell(new Phrase("Blood Type", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorBlood(), text_font));
                table.addCell(new Phrase("Faculty", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorFaculty(), text_font));
                table.addCell(new Phrase("Department", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorDept(), text_font));
                table.addCell(new Phrase("Group", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorGroup(), text_font));
                table.addCell(new Phrase("Level", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorLevel(), text_font));
                table.addCell(new Phrase("Status", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorStatus(), text_font));

                document.add(table);

            } else {
                document.add(new Paragraph("Sorry, there is no data.", warning));
            }

            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                inst.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
