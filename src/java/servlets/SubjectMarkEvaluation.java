package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import iaau.DbStudent_Attendance;
import javax.servlet.*;
import javax.servlet.http.*;
import iaau.BaseDb;
import java.util.ArrayList;

public class SubjectMarkEvaluation extends HttpServlet {

    static final String errorMesg = "Please check the details and try again";
    static final String errorMesgS = "No subj Please check the details and try again";
    private String[] subjectIDs;
    private ArrayList<String> studentIDs;
    private ArrayList<String> less_studIDs;
    private ArrayList<String> stName;
    private ArrayList<String> stStatus;
    private String department = null;
    private String subjectName = null;
    private String yearName = null;
    private String semester = null;
    private Connection conn = null;
    private ResultSet result = null;
    private PreparedStatement statement = null;
    private ResultSetMetaData rsm = null;
    private BaseDb base = null;
    private DbStudent_Attendance dbStAtt = null;

    public void getInfo(String subj, String year, String sem) throws SQLException {
        less_studIDs = new ArrayList<String>();
        studentIDs = new ArrayList<String>();
        stName = new ArrayList<String>();
        stStatus = new ArrayList<String>();

        String query = "select t1.id,concat(t1.name,' ',t1.surname)as name,t5.name,t2.year,t7.name,t3.semester,t6.id,t6.status "
                + "from less_stud as t6"
                + " left join subjects as t5 on t6.subject_id=t5.id"
                + " left join year as t2 on t6.year_id=t2.id"
                + " left join semester as t3 on t6.sem_id=t3.id"
                + " left join student as t1 on t6.student_id=t1.id"
                + " left join department as t7 on t5.dept_id=t7.id"
                + " where t1.edu_status_id=1 and t5.id =? and t2.id =? and t3.id=?  order by name;";

        statement = conn.prepareStatement(query);
        statement.setString(1, subj);
        statement.setString(2, year);
        statement.setString(3, sem);
        result = statement.executeQuery();

        while (result.next()) {
            less_studIDs.add(result.getString("t6.id"));
            studentIDs.add(result.getString("t1.id"));
            stName.add(result.getString("name"));
            stStatus.add(result.getString("t6.status"));
            subjectName = result.getString("t5.name");
            yearName = result.getString("t2.year");
            semester = result.getString("t3.semester");
            department = result.getString("t7.name");
        }//while
    }

    public double calcAverage(String s_id, String sem, String year, String subj_id) throws SQLException {

        String query = "select t3.exam_name as exam,(t4.mark * t3.percentage/100) as average from less_stud "
                + "as t5 left join student as  t1 on t5.student_id=t1.id left join "
                + "subjects as t2 on t5.subject_id=t2.id left join sinif as t6 on "
                + "t1.group_id=t6.id  left join subj_exam as t4 on t5.id = t4.stud_less_id "
                + "left join exam as t3 on t4.exam_id=t3.exam_id "
                + "where  t2.id=? and t5.year_id=? and t5.sem_id=? and t5.student_id =?;";

        double average = 0;

        statement = conn.prepareStatement(query);
        statement.setString(1, subj_id);
        statement.setString(2, year);
        statement.setString(3, sem);
        statement.setString(4, s_id);
        result = statement.executeQuery();

        int k = 0;
        double midterm = 0, fin = 0, mup = 0.0;
        while (result.next()) {
            String exam = result.getString("exam");
            if (result.wasNull()) {
                average = 0;
            } else if (exam.equals("Midterm")) {
                midterm = result.getDouble("average");
            } else if (exam.equals("Final")) {
                fin = result.getDouble("average");
                if (fin < 29.7) {
                    fin = 0.0;
                }
            } else {
                mup = result.getDouble("average");
                if (mup < 29.7) {
                    mup = 0.0;
                }
            }
            if ((midterm + fin) < 49.5) {
                average = midterm + mup;
            } else {
                average = midterm + fin;
            }
        }
        return average;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        base = new BaseDb();
        dbStAtt = new DbStudent_Attendance();

        String yearID = (String) request.getParameter("year");
        String semID = (String) request.getParameter("semester");
        String dept = (String) request.getParameter("department");
        String[] subjID = request.getParameterValues("subjID");
        String exam = (String) session.getAttribute("examID");

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Evalution Report</title>");
        out.println("<link rel='stylesheet' href='style/sims_style.css'>");
        out.println("</head>");
        out.println("<body><p>&nbsp&nbsp");
        //out.println("<center><table width=622 height=78 border=1" +
        //        " cellpadding=0 cellspacing=0 class=tealtable ><tr>");
        //out.println("<td><img src=../images/MarkEvaReport.bmp width=622 height=78 alt=headline/></td></tr></table>");
        try {
            conn = base.getConnection();
            dbStAtt.connect();

            if ((exam.equals("1") || exam.equals("2"))) {
                request.getSession().setAttribute("Message", errorMesg);
                response.sendRedirect("info.jsp");
            } else if (exam.equals("3")) {
                for (int i = 0; i < subjID.length; i++) {
                    getInfo(subjID[i], yearID, semID);

                    out.println("<p> </p>");
                    out.println("<center><table width=80% align=center cellpadding=1 cellspacing=1 class=tealtable >");
                    out.println("<tr><td><b>Department : </b><td>" + department + "</td>");
                    out.println("<td><b>Subject : </b><td>" + subjectName + "</td></tr>");
                    out.println("<tr><td><b>Year : </b><td>" + yearName + "</td>");
                    out.println("<td><b>Semester : </b><td>" + semester + "</td></tr>");

                    out.println("<p> </p>");
                    out.println("<center>"
                            + "<table  width=80% align=center border=1 cellpadding=0 cellspacing=0 class=tealtable >");
                    out.println("<tr>");
                    out.println("<th> NO  </th> <th> Name Surname </th> "
                            + "<th> Average </th><th> Status </th>");
                    out.println("</tr>");
                    for (int j = 0; j < studentIDs.size(); j++) {
                        double avr = calcAverage(studentIDs.get(j), semID, yearID, subjID[i]);
                        out.println("<tr>");
                        if ((avr >= 49.5) && (Integer.parseInt(stStatus.get(j)) < 4)) {
                            out.println("<td align=center>" + (j + 1) + "</td><td>" + stName.get(j) + "</td><td align=center>" + avr + "</td><td align=center>OK(" + stStatus.get(j) + ")</tr>");
                        } else if ((avr < 49.5) && (Integer.parseInt(stStatus.get(j)) < 4)) {
                            out.println("<td align=center>" + (j + 1) + "</td><td>" + stName.get(j) + "</td><td align=center>" + avr + "</td><td align=center>F1(" + stStatus.get(j) + ")</tr>");
                            dbStAtt.changeMarkStatus(Integer.parseInt(studentIDs.get(j)), Integer.parseInt(subjID[i]), Integer.parseInt(yearID), Integer.parseInt(semID));
                        } else if ((avr < 49.5) && (Integer.parseInt(stStatus.get(j)) > 3)) {
                            out.println("<td align=center>" + (j + 1) + "</td><td>" + stName.get(j) + "</td><td align=center>" + avr + "</td><td align=center>F2(" + stStatus.get(j) + ")</tr>");
                        }
                        out.println("</tr>");
                    }//for j

                    out.println("</table></p></p>");
                } //for i

            }//else
            out.println("</center></body></html>");
        } catch (Exception e) {
            throw new ServletException(e.getMessage());
        } finally {
            if (base != null) {
                try {
                    base.close();
                } catch (SQLException ex) {
                }
            }
            if (dbStAtt != null) {
                try {
                    dbStAtt.close();
                } catch (SQLException ex) {
                }
            }
        }
        out.close();
    }

    public void getData(String query) {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
