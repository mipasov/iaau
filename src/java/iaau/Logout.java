package iaau;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Logout extends HttpServlet {
	public void doGet (HttpServletRequest request,
			   HttpServletResponse response)
		throws ServletException, IOException {

/*		response.setContentType("text/html");
		HttpSession session = request.getSession();
		String name = (String)session.getAttribute("stName");
		String msg;
		if (name == null)  msg = "Sorry this is new session";
		else msg = "Welcome Mr.";

		PrintWriter out = response.getWriter();
		String title = "Session tracking Example";

		String docType = "<!DOCTYPE HTML PUBLIC \" - //W3C//DTD HTML 4.0 Transitional//EN\">\n";
		out.println(docType + "<html><head><title> Session Tracking Example </title></head>");
		out.println("<body> <br><br> <h2>" + msg + "</h2><br><center> Your name is: " + name + "</center>");
		out.println("</body></html>");
*/
                HttpSession session = request.getSession();
		session.invalidate();
                response.sendRedirect("/IAAU");
	}
}
