package iaau;

import java.util.*;
import java.util.regex.*;

public class RegExTest {
	public static boolean tester(String patern, String value) {
		String patternString = patern;
		Pattern pattern = null;

		try {
			pattern = Pattern.compile(patternString);
		}catch (PatternSyntaxException e) {
			System.out.println("Pattern syntax error");
			System.exit(1);
		}

		String input = value;
//		System.out.println(patern + " " + value);

		Matcher matcher = pattern.matcher(input);

		if (matcher.matches()) 
			return true;
		else 
			return false;
	}
}
