/*
 * Student.java
 * Created on December 18, 2007, 1:32 PM
 */
package iaau;
/**
 * @author opensky
 */
import java.io.*;
import java.util.*;

public class Semester {
    private int id;
    private  String semester;
    private int current;
    private int regStatus;
    
    public Semester(int i,int c,String sem) {
        this.id = i;
        this.current = c;
        this.semester = sem;
    }public Semester(int i,int c,String sem,int rs) {
        this.id = i;
        this.current = c;
        this.semester = sem;
        this.regStatus=rs;
    }
    public String getSemester() {
        return semester;
    }
    
    public int getID(){
        return id;
    }
    
    public int getCurrent(){
        return current;
    }

    /**
     * @return the regStatus
     */
    public int getRegStatus() {
        return regStatus;
    }

    /**
     * @param regStatus the regStatus to set
     */
    public void setRegStatus(int regStatus) {
        this.regStatus = regStatus;
    }
}

