/*
 * DbFaculty.java
 * Created on 18  2008 �., 15:29
 */
package iaau;

/**
 * @author spider
 */
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;

public class DbFaculty extends BaseDb {

    public ArrayList<Faculty> q;

    public DbFaculty() throws ServletException {
        super();
    }

    public void execSQL() throws SQLException {
        String sql = "select * from faculty order by code";

        q = new ArrayList<Faculty>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Faculty(result.getInt("id"), result.getString("code"), result.getString("name")));
        }
    }

    public void execSQL_byID(String id) throws SQLException {

        String sql = "select * from faculty where id=? ;";

        q = new ArrayList<Faculty>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, id);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Faculty(result.getInt("id"), result.getString("code"), result.getString("name")));
        }
    }

    public void execUpdate(String facultyID, String facultyCode, String facultyName) throws SQLException {
        String sql = "update faculty set code =?, name=? where id=?;";

        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, facultyCode);
        stat.setString(2, facultyName);
        stat.setString(3, facultyID);
        stat.executeUpdate();
    }

    public void execAddFaculty(String facultyCode, String facultyName) throws SQLException {
        String sql = "insert into faculty(code,name) values(?,?);";

        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, facultyCode);
        stat.setString(2, facultyName);
        stat.executeUpdate();
    }

    public ArrayList<Faculty> getArray() {
        return q;
    }
}
