/*
 * DbUsers.java
 * Created on 29.5.2009
 */
package iaau;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
/**
 * @author focus
 */
public class DbUsers extends BaseDb{
    public ArrayList<Users> q;
    public DbUsers() throws ServletException{
        super();
    }
    public void execSQL() throws SQLException{
        String sql = "select u.user_name, u.user_name, u.status, u.user_name from users as u" +
                " left join user_roles as r on u.user_name=r.user_name";
        q = new ArrayList<Users>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Users(result.getString("u.user_name"),result.getString("u.user_name"),result.getString("u.user_name"),result.getInt("u.status")));
        }
    }

    public void execSQL_status() throws SQLException{
        String sql = "select r.role_name,users.status,count(*) as sum from users " +
                "inner join user_roles as r on users.user_name=r.user_name group by "
                + "r.role_name,users.status order by users.status,r.role_name;";
        q = new ArrayList<Users>();
        PreparedStatement statement = dbCon.prepareStatement(sql);
        ResultSet result = statement.executeQuery();
        while(result.next()){
            q.add(new Users(result.getString("r.role_name"),result.getInt("users.status"),result.getInt("sum")));
        }
    }

    public ArrayList<Users> getArray() {
        return q;
    }
}