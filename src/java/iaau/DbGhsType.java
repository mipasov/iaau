/*
 * DbGhsType.java
 * Created on 19  2008 �., 15:53
 */
package iaau;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
/**
 * @author spider
 */
public class DbGhsType extends BaseDb {
  public ArrayList<GhsType> q;
    public DbGhsType() throws ServletException{
        super();
    }
    public void execSQL() throws SQLException{
        String sql = "select * from grad_school_type";
        q = new ArrayList<GhsType>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new GhsType(result.getInt("id"),result.getString("name")));
        }
    }
    public void execSQL(String id) throws SQLException{
        String sql = "select * from grad_school_type where id=?";
        q = new ArrayList<GhsType>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1,id);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new GhsType(result.getInt("id"),result.getString("name")));
        }
    }
    
    public ArrayList<GhsType> getArray() {
        return q;
    }
}