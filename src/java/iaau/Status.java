/*
 * Status.java
 * Created on October 10, 2008, 11:22 AM
 */
package iaau;
/**
 * @author admin
 */
public class Status {
    
   private int statusID ;
   private String statusName;
   
    public Status(int statusID, String statusName) {
        this.statusID = statusID;
        this.statusName = statusName;        
    }
    
    public int getStatusID(){
        return statusID;
    }
    
    public String getStatusName(){
        return statusName;
    }    
}
