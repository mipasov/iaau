/*
 * Faculty.java
 * Created on 18  2008 �., 15:28
 */
package iaau;
/**
 * @author spider
 */
public class Faculty {
    private int id;
    private String code;
    private String name;
    
    public Faculty(int id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }
    
    public int getFacultyId(){
        return id;
    }
    
    public String getFacultyCode(){
        return code;
    }
    
    public String getFacultyName(){
        return name;
    }
}
