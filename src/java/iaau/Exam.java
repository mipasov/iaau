package iaau;
/**
 * @author opensky
 */
import java.io.*;
import java.util.*;

public class Exam {
    private int id;
    private  String exam;
    private int current;
    private int percentage;
    
    public Exam(int i,int c,String ex,int percent) {
        this.id = i;
        this.current = c;
        this.exam = ex;
        this.percentage=percent;
    }
    
    public String getExam() {
        return exam;
    }
    
    public int getID(){
        return id;
    }
    
    public int getCurrent(){
        return current;
    }
    
    public int getPercentage(){
        return percentage;
    }
}
