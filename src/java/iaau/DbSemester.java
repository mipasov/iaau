package iaau;

import java.sql.*;
import java.io.*;
import java.util.*;
import java.lang.String.*;
import javax.servlet.*;
import javax.sql.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class DbSemester extends BaseDb {
    public ArrayList<Semester> q;
    
    public DbSemester() throws ServletException{
        super();
    }
    
    public void execSQL() throws SQLException{
        
        String sql = "select * from semester ;";
        
        q = new ArrayList<Semester>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Semester(result.getInt("id"),result.getInt("curr"),result.getString("semester"),result.getInt("registration_status")));
        }
    }

    public void execSQL(String id) throws SQLException{

        String sql = "select * from semester where id=?;";

        q = new ArrayList<Semester>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1,id);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Semester(result.getInt("id"),result.getInt("curr"),result.getString("semester")));
        }
    }
    
    public void execSQL_currSem() throws SQLException{
        
        String sql = "select * from semester where curr=1;";
        
        q = new ArrayList<Semester>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Semester(result.getInt("id"),result.getInt("curr"),result.getString("semester")));
        }
    }    
    
    public void execUpdate(String currSem) throws SQLException{
        String sql = "update semester set curr = 0 ;";
        
        PreparedStatement statement = dbCon.prepareStatement(sql);
        statement.executeUpdate();       
        
        sql = "update semester set curr = 1 where id=? ;";
        
        statement = dbCon.prepareStatement(sql);
        statement.setString(1,currSem);
        statement.executeUpdate();        
    }
     public void execUpdate(String currSem,int status) throws SQLException{
        String sql = "update semester set registration_status = ? where semester=? ;";
        
        PreparedStatement statement = dbCon.prepareStatement(sql);
        statement.setInt(1, status);
        statement.setString(2, currSem);
        statement.executeUpdate();       
           
    }
    
    public ArrayList<Semester> getArray() {
        return q;
    }
}
