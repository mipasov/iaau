package iaau;

import java.sql.*;
import java.util.*;
import javax.servlet.*;

public class DbSystem_Info extends BaseDb {

    public ArrayList<System_Info> q;

    public DbSystem_Info() throws ServletException {
        super();
    }

    public void executeSQL() throws SQLException {
        String sql = "select * from system_info ";
        q = new ArrayList<System_Info>();
        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new System_Info(result.getString("info"), result.getInt("id")));
        }
    }

    public void execSQL() throws SQLException {
        String sql = "select * from system_info where curr=1";
        q = new ArrayList<System_Info>();
        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new System_Info(result.getString("info"), result.getInt("id")));
        }
    }

    public void updateSQL(String info, int id) throws SQLException {
        String sql = "update system_info set info=? where id=? ;";
        PreparedStatement statement = dbCon.prepareStatement(sql);
        statement.setString(1, info);
        statement.setInt(2, id);
        statement.executeUpdate();
    }

    public void insertSQL(String info) throws SQLException {
        String sql = "insert into system_info(info,curr) values(?,1)";
        PreparedStatement statement = dbCon.prepareStatement(sql);
        statement.setString(1, info);
        statement.executeUpdate();
    }

    public void deleteSQL(int id) throws SQLException {
        String sql = "delete from system_info where id=?;";
        PreparedStatement statement = dbCon.prepareStatement(sql);
        statement.setInt(1, id);
        statement.executeUpdate();
    }

    public void viewSQL(int id) throws SQLException {
        String sql = "update system_info set curr=1 where id=?;";
        PreparedStatement statement = dbCon.prepareStatement(sql);
        statement.setInt(1, id);
        statement.executeUpdate();
    }

    public void setNullSQL() throws SQLException {
        String sql = "update system_info set curr=0 ;";
        PreparedStatement statement = dbCon.prepareStatement(sql);
        statement.executeUpdate();
    }

    public ArrayList<System_Info> getArray() {
        return q;
    }
}
