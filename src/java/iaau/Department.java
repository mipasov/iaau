/*
 * Group.java
 * Created on December 18, 2007, 3:06 PM
 */
package iaau;
/**
 * @author opensky
 */

public class Department {
    private int id;
    private String dprt_code;
    private String dprt_name;
    private int faculty_id;
    private String faculty_code;
    
    public Department(int d_id, String d_code,String d_name, int faculty_id, String f_code ) {
        this.id = d_id;
        this.dprt_code = d_code;
        this.dprt_name = d_name;
        this.faculty_id = faculty_id;
        this.faculty_code = f_code;
    }
    
    public int getID() {
        return id;
    }
    
    public String getDprt_Code(){
        return dprt_code;
    }
    
    public String getDprt_Name() {
        return dprt_name;
    }

    public int getFaculty_id(){
        return faculty_id;
    }
    public String getFaculty_code(){
        return faculty_code;
    }
}