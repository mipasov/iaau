package iaau;

public class Blood {

    private int id;
    private String code;

    public Blood(int id, String code) {
        this.id = id;
        this.code = code;
    }

    public int getBloodID() {
        return id;
    }

    public String getBlood() {
        return code;
    }
}
