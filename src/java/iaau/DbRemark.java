/*
 * DbRemark.java
 * Created on 19  2008 �., 16:02
 */
package iaau;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;

/**
 * @author spider
 */
public class DbRemark extends BaseDb {

    public ArrayList<Remark> q;

    public DbRemark() throws ServletException {
        super();
    }

    public void execSQL() throws SQLException {
        String sql = "select * from remark order by name";
        q = new ArrayList<Remark>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Remark(result.getInt("id"), result.getString("name")));
        }
    }

    public void execSQL(String id) throws SQLException {
        String sql = "select * from remark where id=?";
        q = new ArrayList<Remark>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, id);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Remark(result.getInt("id"), result.getString("name")));
        }
    }

    public void updateRemark(String name, String id) throws SQLException {
        String sql = "update remark set name=? where id=?";
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, name);
        stat.setString(2, id);
        stat.executeUpdate();
    }

    public void addRemark(String name) throws SQLException {
        String sql = "insert into remark(name) values(?);";
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, name);
        stat.executeUpdate();
    }

    public ArrayList<Remark> getArray() {
        return q;
    }
}
