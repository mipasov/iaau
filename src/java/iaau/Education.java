/*
 * Education.java
 * Created on 19  2008, 15:49
 */
package iaau;
/**
 * @author spider
 */
public class Education {
    private int id;
    private String name;
    public Education(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public int getEducationID(){
        return id;
    }
    public String getEducationName(){
        return name;
    }
}
