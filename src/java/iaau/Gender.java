/*
 * Gender.java
 * Created on 19  2008 �., 16:08
 */
package iaau;
/**
 * @author spider
 */
public class Gender {
    private int id;
    private String code;
    public Gender(int id, String code) {
        this.id = id;
        this.code = code;
    }
    public int getGenderID(){
        return id;
    }
    public String getGender(){
        return code;
    }
}