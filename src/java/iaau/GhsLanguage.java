/*
 * GhsLanguage.java
 * Created on 19  2008 �., 15:56
 */
package iaau;
/**
 * @author spider
 */
public class GhsLanguage {
    private int id;
    private String name;
    public GhsLanguage(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public int getGhsLanguageID(){
        return id;
    }
    public String getGhsLanguage(){
        return name;
    }
}