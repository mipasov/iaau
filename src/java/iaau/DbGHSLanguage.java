package iaau;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author spider
 */
public class DbGHSLanguage extends BaseDb {

    public ArrayList<GhsLanguage> q;

    public DbGHSLanguage() throws Exception {
        super();
    }

    public void execSQL() throws SQLException {
        String sql = "select * from grad_school_language";
        q = new ArrayList<GhsLanguage>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new GhsLanguage(result.getInt("id"), result.getString("name")));
        }
    }

    public void execSQL(String id) throws SQLException {
        String sql = "select * from grad_school_language where id=?";
        q = new ArrayList<GhsLanguage>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, id);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new GhsLanguage(result.getInt("id"), result.getString("name")));
        }
    }

    public ArrayList<GhsLanguage> getArray() {
        return q;
    }
}
