/*
 * Nationality.java *
 * Created on 19  2008 �., 15:31
 */
package iaau;
/**
 * @author spider
 */
public class Nationality {
    private int id;
    private String country;
    private String nationality;
    public Nationality(int id, String country, String nationality) {
        this.id = id;
        this.country = country;
        this.nationality = nationality;
    }
    public int getNationalityID(){
        return id;
    }
    public String getCountry(){
        return country;
    }
    public String getNationality(){
        return nationality;
    }    
}