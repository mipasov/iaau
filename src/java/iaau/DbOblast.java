/*
 * DbOblast.java
 * Created on 25  2008 �., 14:30
 */
package iaau;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
/**
 * @author spider
 */
public class DbOblast extends BaseDb{
    public ArrayList<Oblast> q;
    public DbOblast() throws ServletException{
        super();
    }
    public void execSQL() throws SQLException{
        String sql = "select * from oblast";
        q = new ArrayList<Oblast>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Oblast(result.getInt("id"),result.getString("name")));
        }
    }
    public void execSQL(String id) throws SQLException{
        String sql = "select * from oblast where id=?";
        q = new ArrayList<Oblast>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1,id);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Oblast(result.getInt("id"),result.getString("name")));
        }
    }
    
    public ArrayList<Oblast> getArray() {
        return q;
    }
}