/*
 * Level.java
 * Created on October 10, 2008, 10:51 AM
 */
package iaau;
/**
 * @author admin
 */
public class Level {
    private int levelID;
    private String levelName;
    
    public Level(int levelID, String levelName) {
        this.levelID = levelID;
        this.levelName = levelName;
    }
    
    public int getLevelID(){
        return levelID;
    }
    
    public String getLevelName(){
        return levelName;
    }
    
}
