/*
 * School.java
 * Created on 16  2009 �., 10:28
 */
package iaau;
public class School {   
    private int id;
    private String name;
    public School(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public int getSchoolID(){
        return id;
    }
    public String getSchoolName(){
        return name;
    }    
}