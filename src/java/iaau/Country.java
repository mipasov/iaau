package iaau;

public class Country {

    private int id;
    private String name;
    private String code;
    private String zip;

    public Country(int id, String name, String code, String zip) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.zip = zip;
    }

    public int getCountryID() {
        return id;
    }

    public String getCountryName() {
        return name;
    }

    public String getCountryCode() {
        return code;
    }

    public String getCountryZip() {
        return zip;
    }
}
