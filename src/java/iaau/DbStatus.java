/*
 * DbStatus.java
 * Created on October 10, 2008, 11:22 AM
 */
package iaau;

/**
 * @author admin
 */
import java.sql.*;
import java.util.*;
import java.lang.String.*;
import javax.servlet.*;

public class DbStatus extends BaseDb {

    public ArrayList<Status> q;

    public DbStatus() throws ServletException {
        super();
    }

    public void execSQL() throws SQLException {

        String sql = "select * from status order by name";

        q = new ArrayList<Status>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();

        while (result.next()) {
            q.add(new Status(result.getInt("id"), result.getString("name")));
        }
    }

    public void execSQL(String id) throws SQLException {
        String sql = "select * from status where id=?;";
        q = new ArrayList<Status>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, id);
        ResultSet result = stat.executeQuery();

        while (result.next()) {
            q.add(new Status(result.getInt("id"), result.getString("name")));
        }
    }

    public void updateInstStatus(String name, String id) throws SQLException {
        String sql = "update status set name=? where id=?;";
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, name);
        stat.setString(2, id);
        stat.executeUpdate();
    }

    public void addInstStatus(String name) throws SQLException {
        String sql = "insert into status(name) values(?);";
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, name);
        stat.executeUpdate();
    }

    public ArrayList<Status> getArray() {
        return q;
    }
}
