package iaau;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;

public class DbSchool extends BaseDb {

    public ArrayList<School> q;

    public DbSchool() throws ServletException {
        super();
    }

    public void execSQL() throws SQLException {
        String sql = "select * from school order by name";
        q = new ArrayList<School>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new School(result.getInt("id"), result.getString("name")));
        }
    }

    public void execSQL_byID(String id) throws SQLException {
        String sql = "select * from school where id=?";
        q = new ArrayList<School>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, id);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new School(result.getInt("id"), result.getString("name")));
        }
    }

    public void updateSchool(String name, String id) throws SQLException {
        String sql = "update school set name=? where id=?";
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, name);
        stat.setString(2, id);
        stat.executeUpdate();
    }

    public void addSchool(String name) throws SQLException {
        String sql = "insert into school(name) values(?);";
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, name);
        stat.executeUpdate();
    }

    public ArrayList<School> getArray() {
        return q;
    }
}
