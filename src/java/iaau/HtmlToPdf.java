/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package iaau;

import com.lowagie.text.Document;
import com.lowagie.text.html.HtmlParser;
import com.lowagie.text.pdf.PdfWriter;
import java.io.FileOutputStream;

/**
 *
 * @author focus
 */
public class HtmlToPdf {

    public static void main(String args[]) {
        Document document = new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream("home/focus/Desktop/html.pdf"));
            HtmlParser.parse(document, "home/focus/Desktop/html.html");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
