package iaau;

import java.io.*;
import java.util.*;
import java.lang.String.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.sql.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Login extends HttpServlet {
    DataSource pool;
    public void init() throws ServletException{
        Context env=null;
        try{
            env = (Context) new InitialContext().lookup("java:comp/env");
            pool=(DataSource)env.lookup("jdbc/iaauDB");
            if(pool == null)
                throw new ServletException(
                        "'iaauDB' is unknown DataSource");
        }catch(NamingException ne){
            throw new ServletException(ne.getMessage());
        }
    }
    
    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        
        String module = request.getParameter("module");
        String username = request.getParameter("username");
        String pass = request.getParameter("password");
        
        String name=null;
        String surname=null;
        String userType=null;
        String yearID=null;
        String semID=null;
        String weekID=null;
        int auth=0;
        int cstate=0;
        
        HttpSession session = request.getSession();
        String userName = (String)session.getAttribute("username");
        if (userName == null) {
            userName = new String(request.getParameter("username"));
            session.setAttribute("username", username);
        }
        
        String errorMesg = new String("Incorrect Username or password");
        String namePattern = new String("[A-Za-z]+[0-9]*");
        String passPattern = new String("[0-9A-Za-z]+");
        
        boolean syntax;
        if (RegExTest.tester(namePattern, username) && RegExTest.tester(passPattern, pass)) {
            module = "order";
            syntax = true;
        }else {
            module = "cancel";
            syntax = false;
            errorMesg = new String("password or username expression is invalid");
        }
        session.setAttribute("errorMesgLogin", errorMesg);
        
        String query ="select u.name,u.surname,u.username,u.password,u.rol," +
                "u.cstate,y.id,s.id,w.id from " +
                "users as u,year as y,semester as s,weeks as w where " +
                "u.username = '" +username+
                "' and u.password = '" +pass+
                "' and y.curr=1 and s.curr=1 and w.curr=1;";
        
        
        try {
            Connection conn = pool.getConnection();
            Statement stat = conn.createStatement();
            ResultSet result = stat.executeQuery(query);
            
            if (result.next() && syntax) {
                module = "loged";
                name=result.getString("u.username");
                auth=result.getInt("u.rol");
                cstate = result.getInt("u.cstate");
                yearID=Integer.toString(result.getInt("y.id"));
                semID=Integer.toString(result.getInt("s.id"));
                weekID=Integer.toString(result.getInt("w.id"));
                result.close();
            }
            conn.close();
        }catch (SQLException e) {
            while (e !=null) {
                e.printStackTrace();
                e = e.getNextException();
            }
        }catch (Exception ex) {
            ex.printStackTrace();
        }
        
        String address;
        if (module.equals("loged")) {
            session.setAttribute("username",username);
            session.setAttribute("role",auth);
            session.setAttribute("yearID",yearID);
            session.setAttribute("semID",semID);
            session.setAttribute("weekID",weekID);
            session.setAttribute("auth",Integer.toString(auth));
            session.setAttribute("userType",userType);
            if((auth == 1) && (cstate == 1))
                response.sendRedirect("studSec/studSection.jsp");
            else if((auth == 2) && (cstate == 1))
                response.sendRedirect("secretary/secSection.jsp");
            else if((auth == 3) && (cstate == 1))
                response.sendRedirect("studSecO/studSection.jsp");
            else if((auth == 4) && (cstate == 1))
                response.sendRedirect("supervisor/supervisorSection.jsp");
            else
                response.sendRedirect("/IAAU/loginError.jsp");
        }else {
            response.sendRedirect("/IAAU");
        }
    }
    
    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        
        doGet(request, response);
    }
}
