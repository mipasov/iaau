/*
 * System_Info.java
 * Created on 24  2008 �., 15:25
 */
package iaau;
/**
 * @author spider
 */
public class System_Info {   
    private String info;
    private int id;
   
    public System_Info(String info,int id) {
        this.info = info;
        this.id = id;
    }
    
     public String getInfo(){
        return info;
    }
     
     public int getId(){
        return id;
    }
    
}
