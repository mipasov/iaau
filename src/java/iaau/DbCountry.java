/*
 * DbCountry.java
 * Created on 19  2008 �., 15:21
 */
package iaau;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
/**
 * @author spider
 */
public class DbCountry extends BaseDb{
    public ArrayList<Country> q;
    public DbCountry() throws ServletException {
        super();
    }
    public void execSQL() throws SQLException{
        String sql = "select * from country order by name";
        q = new ArrayList<Country>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);        
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Country(result.getInt("id"),result.getString("name"),result.getString("code"),result.getString("zip")));
        }
    }
    public void execSQL(String id) throws SQLException{
        String sql = "select * from country where id=?";
        q = new ArrayList<Country>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1,id);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Country(result.getInt("id"),result.getString("name"),result.getString("code"),result.getString("zip")));
        }
    }
    
    public ArrayList<Country> getArray() {
        return q;
    }
}