package iaau;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;

public class ExamAverage extends BaseDb {
    
    public ExamAverage() throws ServletException {
        super();
    }
    
    public double calculateAverage(int s_less_id, int sem, int year, int subj_id) throws SQLException {
        
        double average = 0.0;
        
        query = "select t3.exam_name as exam,(t4.mark * t3.percentage/100) as average from less_stud " +
                "as t5 left join student as  t1 on t5.student_id=t1.id left join " +
                "subjects as t2 on t5.subject_id=t2.id left join sinif as t6 on " +
                "t1.group_id=t6.id  left join subj_exam as t4 on t5.id = t4.stud_less_id " +
                "left join exam as t3 on t4.exam_id=t3.exam_id " +
                "where  t2.id=? and t5.year_id=? and " +
                "t5.sem_id=? and t4.stud_less_id =? ;";
        
        double[] marks = new double[3];
        
        PreparedStatement stmt = dbCon.prepareStatement(query);
        stmt.setInt(1,subj_id);
        stmt.setInt(2,year);
        stmt.setInt(3,sem);
        stmt.setInt(4,s_less_id);
        ResultSet rs = stmt.executeQuery();
        
        int k = 0;
        double midterm = 0,fin = 0,mup=0.0;
        while (rs.next()) {
            String exam=rs.getString("exam");
            if(exam.equals("Midterm"))
                midterm=rs.getDouble("average");
            else if(exam.equals("Final")){
                fin=rs.getDouble("average");
                if(fin<29.7)
                    fin=0.0;
            }else if (exam.equals("MakeUp")){
                mup=rs.getDouble("average");
                if(mup<29.7)
                    mup=0.0;
            }
            if((midterm+fin)<49.5) {
                average=midterm+mup;
            } else
                average=midterm+fin;
        }
        return Math.ceil(average);
    }
}
