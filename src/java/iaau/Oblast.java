/*
 * Oblast.java
 * Created on 25  2008 �., 14:29
 */
package iaau;
/**
 * @author spider
 */
public class Oblast {
   private int id;
   private String name;
    /** Creates a new instance of Oblast */
    public Oblast(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public int getOblID(){
        return id;
    }
    public String getOblName(){
        return name;
    }    
}