/*
 * DbGender.java
 * Created on 19  2008 �., 16:09
 */
package iaau;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
/**
 * @author spider
 */
public class DbGender extends BaseDb {
    public ArrayList<Gender> q;
    public DbGender() throws ServletException{
        super();
    }
    public void execSQL() throws SQLException{
        String sql = "select * from gender";
        q = new ArrayList<Gender>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Gender(result.getInt("id"),result.getString("code")));
        }
    }
    public void execSQL(String id) throws SQLException{
        String sql = "select * from gender where id=?";
        q = new ArrayList<Gender>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1,id);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Gender(result.getInt("id"),result.getString("code")));
        }
    }    
    public ArrayList<Gender> getArray() {
        return q;
    }
}