/*
 * DbRegion.java
 * Created on 19  2008 �., 15:37
 */
package iaau;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
/**
 * @author spider
 */
public class DbRegion extends BaseDb{
    public ArrayList<Region> q;
    public DbRegion() throws ServletException {
        super();
    }
    public void execSQL() throws SQLException{
        String sql = "select * from region";
        q = new ArrayList<Region>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Region(result.getInt("id"),result.getString("name"),result.getString("zip"),result.getInt("oblast_id")));
        }
    }
   public void execSQL(String id) throws SQLException{
        String sql = "select * from region where id=?";
        q = new ArrayList<Region>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1,id);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Region(result.getInt("id"),result.getString("name"),result.getString("zip"),result.getInt("oblast_id")));
        }
    }

   public void execSQLObl(String id) throws SQLException{
        String sql = "select * from region where oblast_id=?";
        q = new ArrayList<Region>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1,id);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Region(result.getInt("id"),result.getString("name"),result.getString("zip"),result.getInt("oblast_id")));
        }
    }
    
    public ArrayList<Region> getArray() {
        return q;
    }
}