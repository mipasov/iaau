package iaau;

import java.sql.*;
import java.util.*;
import java.lang.String.*;
import javax.servlet.*;

public class DbEducation extends BaseDb {

    public ArrayList<Education> q;

    public DbEducation() throws ServletException {
        super();
    }

    public void execSQL() throws SQLException {
        q = new ArrayList<Education>();

        String sql = "select id,name from education";

        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Education(result.getInt("id"), result.getString("name")));
        }
    }

    public void execSQL(String id) throws SQLException {
        q = new ArrayList<Education>();

        String sql = "select id,name from education where id=?";

        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, id);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Education(result.getInt("id"), result.getString("name")));
        }
    }

    public ArrayList<Education> getArray() {
        return q;
    }
}