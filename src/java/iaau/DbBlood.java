package iaau;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;

public class DbBlood extends BaseDb {

    public ArrayList<Blood> q;

    public DbBlood() throws ServletException {
        super();
    }

    public void execSQL() throws SQLException {
        String sql = "select * from blood";
        q = new ArrayList<Blood>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Blood(result.getInt("id"), result.getString("code")));
        }
    }

    public void execSQL(String id) throws SQLException {
        String sql = "select * from blood where id=?";
        q = new ArrayList<Blood>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, id);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Blood(result.getInt("id"), result.getString("code")));
        }
    }

    public ArrayList<Blood> getArray() {
        return q;
    }
}
