package iaau;

public class Academic {

    private int id;
    private String name;

    public Academic(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getAcademicID() {
        return id;
    }

    public String getAcademicName() {
        return name;
    }
}
