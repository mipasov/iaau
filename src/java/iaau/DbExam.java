package iaau;

import java.sql.*;
import java.io.*;
import java.util.*;
import java.lang.String.*;
import javax.servlet.*;
import javax.sql.*;

public class DbExam extends BaseDb {
    public ArrayList<Exam> q;
    
    public DbExam() throws ServletException{
        super();
    }
    
    public void execSQL() throws SQLException{
        String sql = "select * from exam ;";
        q = new ArrayList<Exam>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Exam(result.getInt("exam_id"),result.getInt("curr"),
                    result.getString("exam_name"),result.getInt("percentage")));
        }
    }
    
    public void execSQL_currExam() throws SQLException{
        
        String sql = "select * from exam where curr=1;";
        q = new ArrayList<Exam>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Exam(result.getInt("exam_id"),result.getInt("curr"),
                    result.getString("exam_name"),result.getInt("percentage")));
        }
    }
    
    public void execSQL_Exam(String ex) throws SQLException{
        
        String sql = "select * from exam where exam_id=? ;";
        q = new ArrayList<Exam>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1,ex);
        ResultSet result = stat.executeQuery();
        while(result.next()){
            q.add(new Exam(result.getInt("exam_id"),result.getInt("curr"),
                    result.getString("exam_name"),result.getInt("percentage")));
        }
    }
    
    public void execSetCurr_Exam(String currExam) throws SQLException{
        String sql = "update exam set curr = 0 ;";
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.executeUpdate();
        
        sql = "update exam set curr = 1 where exam_id =? ;";
        stat = dbCon.prepareStatement(sql);
        stat.setString(1,currExam);
        stat.executeUpdate();
    }
    
    public ArrayList<Exam> getArray() {
        return q;
    }
}
