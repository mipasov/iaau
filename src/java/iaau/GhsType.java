/*
 * GhsType.java
 * Created on 19  2008 �., 15:53
 */
package iaau;
/**
 * @author spider
 */
public class GhsType {
    private int id;
    private String name;
    public GhsType(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public int getGhsTypeID(){
        return id;
    }
    public String getGhsType(){
        return name;
    }
}