package iaau;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;

public class DbAcademic extends BaseDb {

    public ArrayList<Academic> q;

    public DbAcademic() throws ServletException {
        super();
    }

    public void execSQL() throws SQLException {
        String sql = "select * from academic";
        q = new ArrayList<Academic>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Academic(result.getInt("id"), result.getString("name")));
        }
    }

    public void execSQL(String id) throws SQLException {
        String sql = "select * from academic where id=?";
        q = new ArrayList<Academic>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, id);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Academic(result.getInt("id"), result.getString("name")));
        }
    }

    public ArrayList<Academic> getArray() {
        return q;
    }
}
