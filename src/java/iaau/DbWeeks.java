package iaau;

import java.sql.*;
import java.io.*;
import java.util.*;
import java.lang.String.*;
import javax.servlet.*;
import javax.sql.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class DbWeeks extends BaseDb{
    public ArrayList<Weeks> q;
    
    public DbWeeks() throws ServletException{
        super();
    }
    
    public void execSQL() throws SQLException{
        
        String sql = "select * from weeks ;";
        q = new ArrayList<Weeks>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Weeks(result.getInt("id"),result.getInt("curr"),result.getString("week")));
        }
    }
    
    public void execSQL_currWeek() throws SQLException{
        
        String sql = "select * from weeks where curr = 1 ;";
        q = new ArrayList<Weeks>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Weeks(result.getInt("id"),result.getInt("curr"),result.getString("week")));
        }
    }
    
    public void execSQL_underCurrWeek() throws SQLException {
        String sql = "select * from weeks where week <= (select max(week) from weeks where curr = 1) order by week asc;";
        q = new ArrayList<Weeks>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Weeks(result.getInt("id"),result.getInt("curr"),result.getString("week")));
        }
    }
    
    public void execUpdate(String currW)throws SQLException{
        String sql = "update weeks set curr = 0 ;";
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.executeUpdate();
        
        sql =  "update weeks set curr = 1 where id =? ;";
        stat = dbCon.prepareStatement(sql);
        stat.setString(1,currW);
        stat.executeUpdate();
    }
    
    public ArrayList<Weeks> getArray() {
        return q;
    }
}
