package iaau;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
/**
 * @author spider
 */
public class DbNationality extends BaseDb{
    public ArrayList<Nationality> q;
    public DbNationality() throws ServletException{
        super();
    }
    public void execSQL() throws SQLException{
        String sql = "select * from nationality order by nationality";
        q = new ArrayList<Nationality>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Nationality(result.getInt("id"),result.getString("country"),result.getString("nationality")));
        }
    }
     public void execSQL(String id) throws SQLException{
        String sql = "select * from nationality where id=?";
        q = new ArrayList<Nationality>();
        
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1,id);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Nationality(result.getInt("id"),result.getString("country"),result.getString("nationality")));
        }
    }
    
    public ArrayList<Nationality> getArray() {
        return q;
    }
}