/*
 * Student.java
 * Created on December 18, 2007, 1:32 PM
 */
package iaau;
/**
 * @author opensky
 */
import java.io.*;
import java.util.*;

public class Weeks {
    private int id;
    private  String week;
    private int current;
    
    public Weeks(int i,int c,String wk) {
        this.id = i;
        this.current = c;
        this.week = wk;
    }
    
    public String getWeek() {
        return week;
    }
    
    public int getID(){
        return id;
    }
    
    public int getCurrent(){
        return current;
    }
}