/*
 * SessionCheck.java
 * Created on October 1, 2007, 1:26 PM
 */
package iaau;

import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
/**
 * @author opensky
 * @version
 */
public class SessionCheck extends HttpServlet {
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        RequestDispatcher dispatcher=null;
        HttpSession session = request.getSession(false);
        if(session != null)
            dispatcher=request.getRequestDispatcher("/empl/EmpLogout");
        else               
            dispatcher=request.getRequestDispatcher(request.getRequestURI());
            
          out.println(dispatcher.toString());  
        if(dispatcher != null)
                dispatcher.forward(request,response);
            else 
                throw new ServletException("error null dispatcher");
      out.close();
        
        
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
