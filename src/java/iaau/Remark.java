/*
 * Remark.java
 * Created on 19  2008 �., 16:01
 */
package iaau;
/**
 * @author spider
 */
public class Remark {
    private int id;
    private String name;
    public Remark(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public int getRemarkID(){
        return id;
    }
    public String getRemark(){
        return name;
    }
}