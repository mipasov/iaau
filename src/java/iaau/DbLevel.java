/*
 * DbLevel.java
 * Created on October 10, 2008, 10:52 AM
 */
package iaau;

/**
 * @author admin
 */
import java.sql.*;
import java.util.*;
import java.lang.String.*;
import javax.servlet.*;

public class DbLevel extends BaseDb {

    public ArrayList<Level> q;

    public DbLevel() throws ServletException {
        super();
    }

    public void execSQL() throws SQLException {
        String sql = "select * from level order by name;";

        q = new ArrayList<Level>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Level(result.getInt("id"), result.getString("name")));
        }
    }

    public void execSQL(String id) throws SQLException {
        String sql = "select * from level where id=?;";
        q = new ArrayList<Level>();

        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, id);
        ResultSet result = stat.executeQuery();
        while (result.next()) {
            q.add(new Level(result.getInt("id"), result.getString("name")));
        }
    }

    public void addLevel(String level) throws SQLException {
        String sql = "insert into level(name) values(?);";
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, level);
        stat.executeUpdate();
    }

    public void updateLevel(String level, String id) throws SQLException {
        String sql = "update level set name=? where id=?;";
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, level);
        stat.setString(2, id);
        stat.executeUpdate();
    }

    public void deleteLevel(String id) throws SQLException {
        String sql = "delete from level where id=?";
        PreparedStatement stat = dbCon.prepareStatement(sql);
        stat.setString(1, id);
        stat.executeUpdate();
    }

    public ArrayList<Level> getArray() {
        return q;
    }
}
