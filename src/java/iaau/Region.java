/*
 * Region.java
 * Created on 19  2008 �., 15:37
 */
package iaau;
/**
 * @author spider
 */
public class Region {
    private int id;
    private String name;
    private String zip;
    private int oblast_id;
    public Region(int id, String name, String zip, int oblast_id) {
        this.id = id;
        this.name = name;
        this.zip = zip;
        this.oblast_id = oblast_id;
    }
    public int getRegionID(){
        return id;
    }
    public String getRegionName(){
        return name;
    }
    public String getRegionZip(){
        return zip;
    }
    public int getOblastID(){
        return oblast_id;
    }
}