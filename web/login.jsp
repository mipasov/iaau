<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<html>
    <head>
        <title>
            Log IN
        </title>
        <link type="text/css" href="../style/login.css" rel="stylesheet"/>
    </head>
    <body>
        <div id="wrapper">            
        </div>
        <div id="ctr" align="center">
        <div class="login">
            <div class="login-form">

                <img src="../images/login.gif" alt="login"/>

                <form id="loginForm" method="post" action="j_security_check" name="loginForm">
                    <div class="form-block">
                        <div class="inputlabel">Username</div>
                        <div>
                            <input class="inputbox_1" name="j_username" size="15"/>
                        </div>
                        <div class="inputlabel">Password</div>
                        <div>
                            <input class="inputbox_1" type="password" name="j_password" size="15"/>
                        </div>
                        <div align="left">
                            <input class="button_1" type="submit" name="submit" value="Login"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="login-text">
                <div class="ctr">
                    <img src="../images/security.png" width="64" height="64" alt="security"/>
                </div>
                <p>Welcome to IAAU-AMS</p>
                <p>Use a valid username and password to gain access to the system.</p>
            </div>
            <div class="clr"/>
            </div>
        </div>
        <div id="break"/>
        <noscript>!Warning! Javascript must be enabled for proper operation of the Administrator </noscript>
        <div class="footer" align="center">
            <div align="center"> </div>
            <div align="center">
                <div align="center"> </div>
                <div align="center">
                    Powered by "New Solutions"
                </div>
                <div align="center"> </div>
            </div>
        </div>
    </body>
</html>