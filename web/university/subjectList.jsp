<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Subjects"%>
<jsp:useBean id="db" scope="request" class="iaau.DbSubjects" />
<html>
    <head>
        <title>Subjects List</title>
        <link rel='stylesheet' href='../style/sis_style.css'>
<%
db.connect();

String depID = request.getParameter("depID");
try {
    db.execSQL_byDeprtActive(depID);
    ArrayList<Subjects> list = db.getArray();
%>
    </head>
<body>
<table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Subjects List</td>
    </tr>
    <% if (db.q.size() != 0) {%>
    <tr>
        <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;Department : <%=list.get(0).getDepartment()%> </td>
    </tr>
</table>
<br>
<table width="80%" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold">    
<tr height="40">
    <td width="5%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
    > #<input type="hidden" name="type" value="2" readonly="readonly" />
    </td>
    <td width="15%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
    >Code</td>
    <td align="center" width="50%" bgcolor="#eeeeee" valign="middle" class="textBold" 
        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
    >Name
    </td>
    <td align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold" 
        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
    >Hours / Week
    </td>
    <td align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold" 
        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
    >Year 
    </td>
    <td align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold" 
        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
    >Semester
    </td>
</tr>

<%
        for (int i = 0; i < db.q.size(); i++) {
%>

<tr height="30">
    <td width="5%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
    ><%=i+1%></td>
    <td width="15%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
    ><%= list.get(i).getSubjCode() %>
    </td>
    <td width="50%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
    ><%= list.get(i).getSubjectName()%> </td>
    <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
    ><%= list.get(i).getSubjHrs() %>
    </td>
    <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
    ><%= list.get(i).getStdYear() %> </td>
    <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
    ><%= list.get(i).getSemester() %> </td>
</tr>
<%}%>
<table width="80%" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="big">
<tr align="center"><td>Total : <%=db.q.size()%>  Subjects</td></tr>
</table>
<%}else {%>
<tr>
    <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
        border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
        border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9;
    border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
    Sorry there is no record for you on this time </td>
</tr>
<% } %>

</table>
</body>
</html>
<%       }catch(SQLException e) {
    throw new ServletException("Your query is not working", e);
}
db.close();
%>