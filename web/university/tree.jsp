<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<html>    
    <head>
        <title></title>
        <link rel='stylesheet' href='../style/tree.css'>
        <script language="javascript" src="../scripts/tree.js"></script>
    </head>
    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor=#FFA63A>
        <table border=0 cellpadding='10' cellspacing=0>
            <tr>
                <td>
                    <table border=0 cellpadding='1' cellspacing=1>
                        <tr>
                            <td width='16'>
                                <a id="xlists" href="javascript:Toggle('lists');">
                                <img src='images/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'></a>
                            </td>
                            <td><b>Lists</b></td>
                        </tr>
                    </table>
                    <div id="lists" style="display: none; margin-left: 2em;">
                        <table border=0 cellpadding='1' cellspacing=1>
                            <tr><td>
                                <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            <a href="faculties.jsp" target="master">Faculties</a></td></tr>
                            <tr><td>
                                <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            <a href="departments.jsp" target="master">Departments</a></td></tr>
                            <tr><td>
                                <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            <a href="groups.jsp" target="master">Groups</a></td></tr>
                            <tr><td>
                                <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            <a href="subjects.jsp" target="master">Subjects</a></td></tr>
                            <tr><td>
                                <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            <a href="instructors.jsp" target="master">Instructors</a></td></tr>
                        </table>
                    </div>

                    <table border=0 cellpadding='1' cellspacing=1>
                        <tr><td width='16'>
                                <a id="xreports" href="javascript:Toggle('reports');">
                            <img src='../images/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'></a></td>
                            <td><b>Reports</b></td>
                        </tr>
                    </table>
                    <div id="reports" style="display: none; margin-left: 2em;">
                        <table border=0 cellpadding='1' cellspacing=1>
                            <tr><td>
                                <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            <a href="searchStudent.jsp" target="master">Search Student</a></td></tr>
                            <tr><td>
                                <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            <a href="searchInstructor.jsp" target="master">Search Instructor</a></td></tr>
                            <tr><td>
                                <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            <a href="announcement.jsp" target="master">Announcement</a></td></tr>
                            <tr><td>
                                <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            <a href="stgroup" target="master">Students by group</a></td></tr>
                            <tr><td>
                                <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            <a href="stdept" target="master">Students by department</a></td></tr>
                            <tr><td>
                                <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            <a href="subjInstructor.jsp" target="master">Subject - Instructor</a></td></tr>
                            <tr><td>
                                <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            <a href="attNotEntered" target="master">Not entered attendance</a></td></tr>
                            <tr><td>
                                <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            <a href="notRegSt" target="master">Not registered students</a></td></tr>
                            <tr><td>
                                <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            <a href="examNotSubmittedInst" target="master">Not submitted exams</a></td></tr>
                            <tr><td>
                                <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            <a href="f2List.jsp" target="master">Failed students-attendance</a></td></tr>
                        </table>
                    </div>

                    <p><a href="javascript:Expand();">Expand All - </a><a href="javascript:Collapse();">Collapse All</a></p>
                </td>
            </tr>
        </table>
    </body>
</html>