<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "java.text.SimpleDateFormat" %>
<%@ page language="Java" import = "iaau.System_Info" %>

<jsp:useBean id="db" scope="request" class="iaau.DbSystem_Info"/>
<head>        
    <title>System Information</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='stylesheet' href='../style/sis_style.css'>
</head>
<body>
    <%
        db.connect();
        try {
            db.executeSQL();
            ArrayList<System_Info> info = db.getArray();
    %>
    <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Information about the System</td>
        </tr>
        <tr>
            <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
        </tr>
    </table>
    <center>
        <form name="SysInfo" method="POST">
            <table align="center" width="90%" height="133" border="0">
                <tr>
                    <%  if (db.q.size() != 0) {%>
                    <td class="big">Previous Announcements :</td>
                    <td><%for (int i = 0; i < db.q.size(); i++) {%>                        
                                <p/><%=i + 1%> <%=info.get(i).getInfo()%>
                            <%}%>
                    </td>
                </tr>
                <%  } else {%>
                <table align="center" width="90%" class="warning">
                    <tr>
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 0px;
                        border-bottom-width: 0px; border-left-width: 0px; border-right-width: 0px; border-right-style: solid;
                        border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9; border-left-color: #d9d9d9;
                        border-left-style: solid;">
                        no previous records</td>
                    </tr>
                </table>
                <%   }%>
                <table align="center" width="90%" height="133" border="0">
                    <tr>
                        <td class="big"><b>New Announcement : </b></td>
                        <td><input type="text" align="center" size="50" name="info" value=""></td>                    
                        <td><input type="submit" value="Submit" name="button" onClick="document.SysInfo.action='sysInfo';"></td>
                    </tr>
                </table>
            </table>
        </form>
    </center>
    <%   } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        db.close();
    %>
</body>
</html>