<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Remark"%>

<jsp:useBean id="db" scope="request" class="iaau.DbRemark" />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
        <title>Remark Details</title>
    </head>    
    <body>
        <% 
        String remarkID = (String) request.getParameter("remarkID");
        db.connect();
        try {
            db.execSQL(remarkID);
            ArrayList<Remark> remark = db.getArray();
        %>
        <center>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp;Remark Details</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;&nbsp;Modify Remark</td>
            </tr>
        </table>        
        <form action="updateRemark" name="updateRemark" method="POST">
        <table class="labelForm">
            <%if(db.q.size() != 0){  %>          
            <tr>
                <td>ID:</td><td><input name="remarkID" value="<%=remark.get(0).getRemarkID()%>" size="15" readonly></td>
            </tr>
            <tr>
                <td>Remark :</td><td><input name="remarkName" value="<%=remark.get(0).getRemark()%>" size="15"></td>
            </tr>
            <tr>
                <td><input type="submit" name="submit" value="Update"></td>
            </tr>
            <%}else{%>
            <tr>
                <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                    border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                    border-right-style: solid; border-bottom-style: solid; border-top-style: solid; 
                    border-top-color: #d9d9d9; border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;
                border-left-color: #d9d9d9; border-left-style: solid;">
                Sorry there is no record for you on this time </td>
            </tr>
            <%}//else%>            
        </table>
    </body>
</html>
<%       }catch(SQLException e) {
    throw new ServletException("Your query is not working", e);
}  
%>       
<%	
db.close();
%>