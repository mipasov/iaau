<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Instructor"%>
<jsp:useBean id="db" scope="request" class="iaau.DbInstructor" />
<html>
    <head>
        <title>Subject-Instructor</title>
        <link rel='stylesheet' href='../style/sis_style.css'>
<script type="text/javascript" src="../scripts/table.js"></script>
<%
db.connect();
//	String username = (String)session.getAttribute("user");
try {
    db.execSQL();

    ArrayList<Instructor> list = db.getArray();
%>
    </head>
<body>
    <center>
<table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
    <!--DWLayoutTable-->
    <tr>
        <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; Subject - Instructor</td>
    </tr>
    <tr>
        <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;&nbsp;Instructor List</td>
    </tr>
</table>
<br>
    <table width="70%" height="100" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold" class="example table-stripeclass:alternate">
        <thead>   
        <tr height="30">
                <td width="0%" align="center" bgcolor="" valign="middle" class="textBold"
                    style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 0px;
                    border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                ></td>
                
                <td align="center" width="20%" bgcolor="#eeeeee" valign="middle" class="textBold" 
                    style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                    border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                >ID
                </td>
                <td align="center" width="25%" bgcolor="#eeeeee" valign="middle" class="textBold" 
                    style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                    border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                >Name
                </td>
                <td align="center" width="30%" valign="middle" bgcolor="#eeeeee" class="textBold" 
                    style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                    border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                >Surname
                </td>
                <td align="center" width="15%" valign="middle" bgcolor="#eeeeee" class="textBold" 
                    style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                    border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                >Role
                <td align="center" width="10%" valign="middle" bgcolor="#eeeeee" class="textBold" 
                    style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                    border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                >modify
                </td>   
            </tr>
            <tr height="30">
                <td width="0%" align="center" bgcolor="" valign="middle" class="textBold"
                    style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 0px;
                    border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                </td>                
                <td align="center" width="20%" bgcolor="#eeeeee" valign="middle" class="textBold" 
                    style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                    border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">                
                </td>
                <td align="center" width="25%" bgcolor="#eeeeee" valign="middle" class="textBold" 
                    style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                    border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                <input name="filter" size="8" onkeyup="Table.filter(this,this)"/></td>
                <td align="center" width="30%" valign="middle" bgcolor="#eeeeee" class="textBold" 
                    style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                    border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                </td>
                <td align="center" width="15%" valign="middle" bgcolor="#eeeeee" class="textBold" 
                    style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                    border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                </td>
                <td align="center" width="10%" valign="middle" bgcolor="#eeeeee" class="textBold"
                    style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                    border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                </td>   
            </tr>
        </thead>
        <%
        if (db.q.size() != 0) {
        for (int i = 0; i < db.q.size(); i++) {
        %>        
        <tr height="35">
            <td width="0%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
            ><input type="hidden" name="instructorID" value="<%=list.get(i).getInstructorID()%>" /></td>
            
            <td align="center" width="20%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
            ><input type="hidden" name="instRollnum" value="<%=list.get(i).getInstructorRollNum()%>" /><%= list.get(i).getInstructorRollNum() %>
            </td>            
            <td align="center" width="25%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
            ><a href="SubjInstructor.jsp?instructorID=<%=list.get(i).getInstructorID()%>"><%= list.get(i).getInstructorName() %></a>
            </td>
            <td align="center" width="30%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
            ><%= list.get(i).getInstructorSurname()%>
            </td>
            <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
            ><%= list.get(i).getInstructorRole()%>
            </td>
            <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
            ><a href="SubjInstructorModify.jsp?instructorID=<%=list.get(i).getInstructorID()%>">modify</a>
            </td>            
        </tr>        
        <% }
        }else { %>
        <tr>
            <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;   
            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid; 
            border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9; 
            border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                Sorry there is no record for you on this time </td>
        </tr>
        <% } %>        
    </table>
</center>
<br>
<%       }catch(SQLException e) {
    throw new ServletException("Your query is not working", e);
}
db.close();
%>