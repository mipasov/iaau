<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Stud_Less" %>
<%@ page language="Java" import = "iaau.studYears" %>
<%@ page language="Java" import = "iaau.Semester" %>
<%@ page language="Java" import = "iaau.Exam" %>
<%@ page language="Java" import = "iaau.SubjExam" %>

<jsp:useBean id="db" scope="request" class="iaau.DbStud_Less"/>
<jsp:useBean id="db1" scope="request" class="iaau.DbExam"/>
<jsp:useBean id="db2" scope="request" class="iaau.DbStudYears"/>
<jsp:useBean id="db3" scope="request" class="iaau.DbSemester"/>
<jsp:useBean id="db4" scope="request" class="iaau.DbSubjExam"/>
<%
db.connect();
db1.connect();
db2.connect();
db3.connect();
db4.connect();

HttpSession sess = request.getSession();
String subject = (String) request.getParameter("subjID");
String sem = (String) sess.getAttribute("semID");
String year = (String) sess.getAttribute("yearID");
String exam_id = (String) sess.getAttribute("examID");

try {
    db.execSQL_Exam(subject, year, sem);
    db1.execSQL_currExam();
    db2.execSQL_currYear();
    db3.execSQL_currSem();
    db4.execSQL(subject,year,sem,exam_id);
    
    ArrayList<Stud_Less> list = db.getArray();
    ArrayList<Exam> examList = db1.getArray();
    ArrayList<studYears> yearList = db2.getArray();
    ArrayList<Semester> semList = db3.getArray();
    ArrayList<SubjExam> marklist = db4.getArray();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>    
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../style/sis_style.css" rel="stylesheet" type="text/css">
    </head>        
    <body>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Examination - Deleting</td>
        </tr>
        <tr>
            <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;Student List</td>
        </tr>
    </table>
    <br>
            <form name="examDelete" action="examDelete" method="POST">
                <table width="60%" class="label"><tr>
                        <td><input type="hidden" name="subjID" value="<%=subject%>" /></td>
                        
                        <td> Year : <select name="year">
                                <%for (int k = 0; k < db2.q.size(); k++) {%>        
                                <option value="<%=yearList.get(k).getID()%>">
                                    <%=yearList.get(k).getYear()%>
                                </option>
                            <%}%>                                </select>
                        </td>
                        <td> Semester : <select name="sem">
                                <%for (int k = 0; k < db3.q.size(); k++) {%>        
                                <option value="<%=semList.get(k).getID()%>">
                                    <%=semList.get(k).getSemester()%>
                                </option>
                        <%}%>                                </select> </td>
                        <td> Exam : <select name="exam">
                                <%for (int k = 0; k < db1.q.size(); k++) {%>        
                                <option value="<%=examList.get(k).getID()%>">
                                    <%=examList.get(k).getExam()%>
                                </option>
                        <%}%>                                </select></td>
                        
                    </tr>                    
                </table>
                <table width="80%" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold">
                    <% if (db4.q.size() != 0) {%>       
                    <tr height="40">                        
                        <td width="20%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        >Name <input type="hidden" name="subjID" value="<%=subject%>"/></td>
                        <td align="center" width="30%" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Surname</td>
                        <td align="center" width="15%" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Group</td>
                        <td align="center" width="15%" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Subject Status</td>
                        <td align="center" width="15%" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Mark</td>                        
                    </tr>              
                    <%  for (int i = 0; i < db4.q.size(); i++) {  %>
                    <tr height="30">
                        <td width="20%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        ><input type="hidden" name="SubjLessID" value="<%=marklist.get(i).getStud_less_id()%>"/>
                        <%=marklist.get(i).getStudentName()%></td>
                        <td align="center" width="30%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=marklist.get(i).getStudentSurname()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=marklist.get(i).getGroup()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=marklist.get(i).getSubjectStatus()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <input  type="text" name="mark" value="<%=marklist.get(i).getExamMark()%>" size="3" disabled/></td>
                        
                    </tr>
                    <% } %>                    
                    <table align="center" class="big">
                        <tr><td>Total : <input  name="total" type="hidden" value="<%=db.q.size()%>"/><%=db.q.size()%>  Students</td></tr>
                        
                        <tr> <td><input type="submit" value="Delete" name="delete" /></td></tr>
                    </table>
                    <%} else {   %>  
                    <tr>
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;   
                            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid; 
                            border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; 
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9; border-left-color: #d9d9d9;
                        border-left-style: solid;"> 
                        Sorry there is no records </td>
                    </tr>
                    <%   }   %>                    
                </table>
            </form>
        </center>
        
    </body>
</html>
<%       } catch (SQLException e) {
    throw new ServletException("Your query is not working", e);
}
%>       
<%
db.close();
db1.close();
db2.close();
db3.close();
db4.close();
%>
