<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Instructor"%>
<jsp:useBean id="db" scope="request" class="iaau.DbInstructor" />
<html>
    <head>
        <title>Instructor List</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <link href="../style/sis_style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../scripts/table.js"></script>
        <script Language="Javascript" type = "text/javascript">
            function CheckRequest()
            {
                if (confirm("Do you really want to delete instructor.")) {
                    return true
                } else {
                    return false
                }
            }
        </script>
    </head>
    <body>
        <%
            db.connect();
            //	String username = (String)session.getAttribute("user");
            try {
                db.execSQL();

                ArrayList<Instructor> list = db.getArray();
        %>
        <table width="100%" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp;Modify Instructor</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;&nbsp;Instructor List </td>
            </tr>
        </table>
        <br>
        <form name="stud" method="post" action="deleteInstructor">
            <table width="70%" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold" class="example table-stripeclass:alternate">
                <thead>
                    <tr>
                        <td width="5%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                            >&nbsp;</td>
                        <td align="center" width="20%" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                            >ID</td>
                        <td align="center" width="40%" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                            >Name <p/>(modify instructor)</td>
                        <td align="center" width="40%" valign="middle" bgcolor="#eeeeee" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                            >Surname <p/>(get instructor info form)</td>
                    </tr>
                    <tr>
                        <td width="5%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            &nbsp;</td>
                        <td align="center" width="20%" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            &nbsp;</td>
                        <td align="center" width="40%" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            <input name="filter" size="8" onkeyup="Table.filter(this,this)"/></td>
                        <td align="center" width="40%" valign="middle" bgcolor="#eeeeee" class="textBold"
                            style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            <input name="filter" size="8" onkeyup="Table.filter(this,this)"/></td>
                    </tr>
                </thead>
                <%
            if (db.q.size() != 0) {
                for (int i = 0; i < db.q.size(); i++) {
                %>
                <tbody>
                    <tr height="30">
                        <td width="5%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                            ><input type="checkbox" name="instructorID" value="<%=list.get(i).getInstructorID()%>" />
                        </td>
                        <td align="center" width="20%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                            ><input type="hidden" name="instRollnum" value="<%=list.get(i).getInstructorRollNum()%>" /><%= list.get(i).getInstructorRollNum()%>
                        </td>
                        <td align="center" width="40%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                            ><input type="hidden" name="instName" value="<%=list.get(i).getInstructorName()%>" />
                            <a href="editInstructor.jsp?instRollnum=<%=list.get(i).getInstructorRollNum()%>"><%=list.get(i).getInstructorName()%></a>
                        </td>
                        <td align="center" width="40%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                            ><input type="hidden" name="instSurname" value="<%=list.get(i).getInstructorSurname()%>" />
                            <a href="instructorForm?iid=<%= list.get(i).getInstructorID()%>"> <%= list.get(i).getInstructorSurname()%></a>
                        </td>
                    </tr>
                    <%
                }
            } else {
                    %>
                    <tr>
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                            border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9;
                            border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            Sorry there is no record for you on this time </td>
                    </tr>
                    <%            }
                    %>
                <table width="70%" align="center" border="0">
                    <tr><td><p>&nbsp;</p></td></tr>
                    <tr align="right"><td><!input type="submit" value="Delete" name="Delete" onClick = "return CheckRequest()"/></td></tr>
                </table>
                </tbody>
            </table>
        </form>
        <br>
        <%       } catch (SQLException e) {
                throw new ServletException("Your query is not working", e);
            }
            db.close();
        %>
    </body>
</html>