<%-- 
    Document   : userRole
    Created on : May 21, 2009, 4:26:06 PM
    Author     : focus
--%>
<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Instructor"%>
<jsp:useBean id="db" scope="request" class="iaau.DbInstructor" />
<html>
    <head>
        <title>User Roles</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <link href="../style/sis_style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../scripts/table.js"></script>
    </head>
    <body>
        <center>
            <%
            String iid = (String)request.getParameter("iid");
        db.connect();
        try {
            db.execSQLID(iid);
            ArrayList<Instructor> list = db.getArray();
            %>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp;User Roles</td>
                </tr>
                <%if (db.q.size() != 0){%>
                <tr>
                    <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;<%=list.get(0).getInstructorName()%> <%=list.get(0).getInstructorSurname()%></td>
                </tr>
                <%} else{%>
                <tr>
                    <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
                </tr><%} %>
            </table>
            <br>
                <table align="center" class="labelForm" cellpadding="1" cellspacing="1">
                    <thead>
                        <th>Roles Used</th>
                    </thead>
                    <tbody class="big">
                        <%for (int i = 0; i < db.q.size(); i++){%>
                        <tr><td><%=i+1%> <%=list.get(i).getInstructorRole()%></td></tr>
                        <% } %>
                    </tbody>
                </table>
                <form method="post" name="addRole">
            <table align="center" class="labelForm" width="20%">
            <tr>
                <input type="hidden" name="rollnum" value="<%=list.get(0).getInstructorRollNum()%>">
            <td width="5%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';">
                <input type="checkbox" name="ur" value="dean" />Dean  </td>
            </tr>
            <tr>
            <td width="5%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';">
                <input type="checkbox" name="ur" value="hod" />Head of Department  </td>
            </tr>
            <tr>
            <td width="5%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';">
                <input type="checkbox" name="ur" value="instructor" />Instructor</td>
            </tr>
            <tr>
            <td width="5%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';">
                <input type="checkbox" name="ur" value="rector" />Rector</td>
            </tr>
            <tr>
            <td width="5%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';">
                <input type="checkbox" name="ur" value="secretary" />Secretary</td>
            </tr>
            <tr>
            <td width="5%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';">
                <input type="checkbox" name="ur" value="studSecO" />Student Service</td>
            </tr>
            <tr>
            <td width="5%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';">
                <input type="checkbox" name="ur" value="supervisor" />Supervisor</td>
            </tr>
            <tr>
                <table align="center">
                <tr><td><input name="button" type="submit" value="Add Role" onClick="document.addRole.action='addRole';"></td>
                    <td>&nbsp;</td>
                    <td><input name="button" type="submit" value="Remove Role" onClick="document.addRole.action='addRole';"></td></tr>
                </table>
            </tr>
            </table>
                </form>
            <%       } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        db.close();
            %>
        </center>
    </body>
</html>