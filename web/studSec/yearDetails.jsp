<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.studYears"%>

<jsp:useBean id="db" scope="request" class="iaau.DbStudYears" />
<html>
    <head>
        <title>Year Details</title>
        <link rel='stylesheet' href='../style/sis_style.css'>
    </head>
    <body>
        <% 
        db.connect();
        //	String username = (String)session.getAttribute("user");
        try {
        db.execSQL();    
        ArrayList<studYears> list = db.getArray();
        %>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Year Details</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
            </tr>
        </table>
        <center>
            <h3>Select Current Year</h3>
            <form action="setCurrYear" name="currentYear" method="POST">
                <table class="labelForm">                    
                    <%
                    if (db.q.size() != 0) {
                    for (int i = 0; i < db.q.size(); i++) {
                    %>                    
                    <tr align="center">
                    <td>
                        <%if(list.get(i).getCurrent()==1){%>
                        <input type="radio" name="year" value="<%=list.get(i).getID()%>" checked="checked"/>
                        <a href ="editYear.jsp?yearID=<%=list.get(i).getID()%>">&nbsp;<%=list.get(i).getYear()%></a>
                        <%}else{%>
                        <input type="radio" name="year" value="<%=list.get(i).getID()%>"/>
                        <a href ="editYear.jsp?yearID=<%=list.get(i).getID()%>">&nbsp;<%=list.get(i).getYear()%></a>
                        <%}%>                        
                    </td>                   
                    <%
                    }
                    }else {   
                    %>
                    <tr align="center">
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                        border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                        border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; 
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      
                        border-left-color: #d9d9d9;     border-left-style: solid;">
                            Sorry there is no record for you on this time </td>
                    </tr>
                    <%
                    }
                    %>
                    <tr align="center">
                        <td>&nbsp;</td> 
                        <td><input type="submit" value="Select" name="Select" /></td></tr>
                </table>
            </form>
            <tr><td><h3>Insert New Year</h3></td></tr>
            <form name="newYear">
                <table border="1" cellpadding="0" cellspacing="0" class="warning" width="50%" bordercolor="#f9F6ee">                    
                    <tr align="center">
                        <td><b>Insert Year:</b></td>
                        <td><input type="text" name="newYear" size="20"></td>                    
                        <td><input type="submit" name="insert" value="Insert" onclick="document.newYear.action='addYear';"></td>
                    </tr>
                </table>
            </form>
        </center>
        <br>
    </body>
</html>
<%       }catch(SQLException e) {
    throw new ServletException("Your query is not working", e);
}  
%>       
<%	
db.close();
%>
