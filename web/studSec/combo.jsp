<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <h1>JSP Page</h1>
        
        <form name="doublecombo">
            <p><select name="oblast" size="1" onChange="redirect(this.options.selectedIndex)">
                    <option value="1">Batken</option>
                    <option value="2">Chui</o1ption>
                    <option value="3">Djalal-Abad</option>
                    <option value="4">Isik-Kol</option>
                    <option value="5">Naryn</option>
                    <option value="6">Osh</option>
                    <option value="7">Talas</option>
                    <option value="8">Other</option>
                </select>
                <select name="stage2" size="1">
                    <option value="11">Batken</option>
                    <option value="20">Kadamjai</option>
                    <option value="30">Lailak</option>
                    <option value="40">Suluktu</option>
                </select>
                <input type="button" name="test" value="Go!"onClick="go()">
                </p>            
            <script>
<!--
var groups=document.doublecombo.oblast.options.length
var group=new Array(groups)
for (i=0; i<groups; i++)
group[i]=new Array()
group[0][0]=new Option("Batken","11")
group[0][1]=new Option("Kadamjai","19")
group[0][2]=new Option("Lailak","29")
group[0][3]=new Option("Suluktu","39")

group[1][0]=new Option("Alamudun","6")
group[1][1]=new Option("Chui","15")
group[1][2]=new Option("Jaiyl","17")
group[1][3]=new Option("Kemin","25")
group[1][4]=new Option("Moskovsky","32")
group[1][5]=new Option("Panfilov","37")
group[1][6]=new Option("Sokuluk","38")
group[1][7]=new Option("Ysyk-Ata","50")
group[1][8]=new Option("Bishkek","52")

group[2][0]=new Option("Aksyi","2")
group[2][1]=new Option("Alabuka","4")
group[2][2]=new Option("Bazar-Korgon","12")
group[2][3]=new Option("Chatkal","13")
group[2][4]=new Option("Karakul","22")
group[2][5]=new Option("Kok-Djangak","27")
group[2][6]=new Option("Nooken","35")
group[2][7]=new Option("Suzak","40")
group[2][8]=new Option("Tash-Komur","42")
group[2][9]=new Option("Togus-Toro","44")
group[2][10]=new Option("Toktogul","46")
group[2][11]=new Option("Djalal-Abad","51")

group[3][0]=new Option("Ak-Suu","1")
group[3][1]=new Option("Balykchy","10")
group[3][2]=new Option("Djety-Oguz","16")
group[3][3]=new Option("Kara-Kol","21")
group[3][4]=new Option("Ton","47")
group[3][5]=new Option("Tup","48")
group[3][6]=new Option("Ysyk-Kol","53")

group[4][0]=new Option("Ak-Talaa","3")
group[4][1]=new Option("At-Bashy","8")
group[4][2]=new Option("Jumgal","18")
group[4][3]=new Option("Kochkor","26")
group[4][4]=new Option("Naryn","33")
group[4][5]=new Option("Tyan-Shan","43")

group[5][0]=new Option("Alai","5")
group[5][1]=new Option("Aravan","7")
group[5][2]=new Option("Chon-Alay","14")
group[5][3]=new Option("Kara-Kuldja","23")
group[5][4]=new Option("Kara-Suu","24")
group[5][5]=new Option("Kyzyl-Kyia","28")
group[5][6]=new Option("Nookat","34")
group[5][7]=new Option("Osh","36")
group[5][8]=new Option("Uzgen","49")

group[6][0]=new Option("Bakai-Ata","9")
group[6][1]=new Option("Kara-Buura","20")
group[6][2]=new Option("Manas","31")
group[6][3]=new Option("Talas","41")

group[7][0]=new Option("Other","54")

var temp=document.doublecombo.stage2

function redirect(x){
for (m=temp.options.length-1;m>0;m--)
temp.options[m]=null
for (i=0;i<group[x].length;i++){
temp.options[i]=new Option(group[x][i].text,group[x][i].value)
}
temp.options[0].selected=true
}

function go(){
location=temp.options[temp.selectedIndex].value
}
//-->
            </script>
        </form>   
    </body>
</html>
