<%-- 
    Document   : accounts
    Created on : May 12, 2009, 3:29:57 PM
    Author     : focus
--%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<html>
    <head>
        <title>Accounts</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
    <script language="javascript">
        function Check(chk)
        {
            if(document.accountStatus.Check_ctr.checked==true){
            for (i = 0; i < chk.length; i++)
            chk[i].checked = true ;
            }else{
            for (i = 0; i < chk.length; i++)
            chk[i].checked = false ;
            }
        }
    </script>
    </head>
    <body>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr><td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Account Statuses</td></tr>
                <tr><td height="20" bgcolor="#f9F6ee" valign="top">&nbsp;</td></tr>
            </table>
            <br>
            <form name="accountStatus" method="post">
                <table align="center" class="labelForm">
                    <tr><td><input type="checkbox" name="account" value="dean">Dean</td></tr>
                    <tr><td><input type="checkbox" name="account" value="hod">Head of Department</td></tr>
                    <tr><td><input type="checkbox" name="account" value="instructor">Instructor</td></tr>
                    <tr><td><input type="checkbox" name="account" value="rector">Rector</td></tr>
                    <tr><td><input type="checkbox" name="account" value="secretary">Secretary</td></tr>
                    <tr><td><input type="checkbox" name="account" value="studSecO">Student Service</td></tr>
                    <tr><td><input type="checkbox" name="account" value="supervisor">Supervisor</td></tr>
                    <tr align="center" class="warning"><td><input type="checkbox" name="Check_ctr" value="yes" onClick="Check(document.accountStatus.account)">Select all</td></tr>
                </table>
                <br>
                <table>
                    <tr>
                        <td><input type="submit" name="button" value="Activate" onClick="document.accountStatus.action='userAccounts';"></td>
                        <td>&nbsp;</td>
                        <td><input type="submit" name="button" value="Disable" onClick="document.accountStatus.action='userAccounts';"></td>
                    </tr>
                </table>
            </form>
        </center>
    </body>
</html>
