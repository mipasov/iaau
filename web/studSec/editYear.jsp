<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.studYears"%>

<jsp:useBean id="db" scope="request" class="iaau.DbStudYears" />
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <link rel='stylesheet' href='../style/sis_style.css'>
        <script type="text/javascript" src="../scripts/table.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Year Modify</title>
    </head>
    <%
    String yearID = request.getParameter("yearID");
    db.connect();
    try{
    db.execSQL(yearID);
    ArrayList<studYears> years = db.getArray();
    %>
    <body>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Year Modify</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold"></td>
            </tr>
        </table>
        <br>
        <center>
            <form name="editYear" method="POST" action="updateYear">
                <table class="labelForm">
                    <%if (db.q.size() != 0){%>
                    <tr>
                        <td>ID:</td><td><input name="yearID" value="<%=years.get(0).getID()%>" size="15" readonly></td>
                    </tr>
                    <tr>
                        <td>Year :</td><td><input name="yearName" value="<%=years.get(0).getYear()%>" size="15"></td>
                    </tr>
                    <tr>
                        <td><input type="submit" name="submit" value="Update"></td>
                    </tr>
                    <%
                    }else {   
                    %>
                    <tr>
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;   
                            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid; 
                            border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9; 
                        border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Sorry there is no record for you on this time </td>
                    </tr>
                    <%}%>
                </table>
            </form>
        </center>
    </body>
</html>
<%}catch(SQLException e) {
throw new ServletException("Your query is not working", e);
}  	
db.close();
%>