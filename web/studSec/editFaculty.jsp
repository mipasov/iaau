<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Faculty" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="db" scope="request" class="iaau.DbFaculty" />

<html>
    <link rel='stylesheet' href='../style/sis_style.css'>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Faculty</title>
    </head>
    <%
    String facultyID = (String) request.getParameter("facultyID");
    
    db.connect();
    try{
        db.execSQL_byID(facultyID);
        ArrayList<Faculty> list = db.getArray();
        
        if (db.q.size() != 0) {
            for (int i = 0; i < db.q.size(); i++) {
    %>
    <body>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Modify Faculty</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
            </tr>
        </table>
        <center>
            <form name="newFaculty" method="POST" action="updateFaculty">
                <table align="center" class="labelForm">
                    <tr>
                        <td>ID : </td>
                        <td><input type="text" name="facultyID" value="<%=list.get(i).getFacultyId()%>" size="15" readonly="readonly" /></td>
                    </tr>
                    
                    <tr>
                        <td>Code : </td>
                        <td><input type="text" name="facultyCode" value="<%=list.get(i).getFacultyCode()%>" size="15" /></td>
                    </tr>
                    <tr>
                        <td>Name : </td>
                        <td><input type="text" name="facultyName" value="<%=list.get(i).getFacultyName()%>" size="15" /></td>
                    </tr>
                    <tr><td><input type="submit" value="Update" name="updateFaculty" /></td></tr>                    
                </table>
            </form>
        </center>
        <%
            }
        }else {   
        %>
        <table align="center" class="warning">
            <tr>
                <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                    border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                    border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9;
                border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"> 
                Sorry there is no record for you on this time </td>
            </tr>
        </table>
        <%
        }
        %>
        <br>
        <%       }catch(SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        %>       
        <%
        db.close();
        %>        
    </body>
</html>