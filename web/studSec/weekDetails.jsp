<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Weeks"%>

<jsp:useBean id="db" scope="request" class="iaau.DbWeeks" />
<html>
    <head>
        <title>Week Details</title>
        <link rel='stylesheet' href='../style/sis_style.css'>
    </head>
    <body>
        <% 
        db.connect();
        
        try {
        db.execSQL();
        
        ArrayList<Weeks> list = db.getArray();
        %>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
                <!--DWLayoutTable-->
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Week Details</td>
                </tr>
                <tr>
                    <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
                </tr>
            </table>
            <h3>Select Current Week</h3>
            <form action="setCurrWeek" name="currentWeek" method="POST">
                <table class="labelForm">
                    
                    <table width="550" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold">
                        <tr height="40"  align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid; border-bottom-style: solid; border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9; border-left-color: #d9d9d9; border-left-style: solid;">                    
                        <br><br></tr>
                        <tr>
                            <%if (db.q.size() != 0) {
                            for (int j = 0; j < 8; j++){%>
                            <tr>
                                <%for (int i = 0; i < 5; i++){ %>     
                                
                                <td align="center" width="80" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                    style="border-top-width:1px; border-bottom-width:1px; border-left-width:1px; border-right-width:1px;
                                    border-right-style: solid;border-bottom-style: solid;border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;border-left-color: #d9d9d9;border-left-style: solid;">
                                    
                                    <% if(list.get((i*8)+j).getCurrent()==1){%>
                                    <input type="radio" name="week" value="<%=list.get((i*8)+j).getID()%>" checked="checked"/>
                                    <%=list.get((i*8)+j).getWeek()%>
                                    <%}else{%>
                                    <input type="radio" name="week" value="<%=list.get((i*8)+j).getID()%>" />
                                    <%=list.get((i*8)+j).getWeek()%>
                                    <%}%>
                                </td>                    
                                <%}%>
                            </tr>
                            <%}
                            }else {   
                            %>
                            <tr>
                                <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                                    border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                    border-right-style: solid; border-bottom-style: solid; 
                                    border-top-style: solid; border-top-color: #d9d9d9; 
                                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;
                                    border-left-color: #d9d9d9;     
                                border-left-style: solid;">
                                Sorry there is no record for you on this time </td>
                            </tr>
                            <%
                            }
                            %>
                        </tr>
                    </table>
                    <tr><td>&nbsp;</td> <td><input type="submit" value="Select" name="Select" /></td></tr>
                </table>
            </form>
        </center>
        <br>
    </body>
</html>
<%       }catch(SQLException e) {
    throw new ServletException("Your query is not working", e);
}  
%>       
<%	
db.close();
%>