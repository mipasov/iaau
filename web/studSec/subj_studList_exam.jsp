<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Stud_Less" %>
<%@ page language="Java" import = "iaau.studYears" %>
<%@ page language="Java" import = "iaau.Semester" %>
<%@ page language="Java" import = "iaau.Exam" %>

<jsp:useBean id="db" scope="request" class="iaau.DbStud_Less"/>
<jsp:useBean id="db1" scope="request" class="iaau.DbExam"/>
<jsp:useBean id="db2" scope="request" class="iaau.DbStudYears"/>
<jsp:useBean id="db3" scope="request" class="iaau.DbSemester"/>

<%
        db.connect();
        db1.connect();
        db2.connect();
        db3.connect();

        HttpSession sess = request.getSession();
        String subject = (String) request.getParameter("subjID");
        String sem = (String) sess.getAttribute("semID");
        String year = (String) sess.getAttribute("yearID");
        String exam = (String) sess.getAttribute("examID");

        try {
            db.execSQL_Exam(subject, year, sem);
            db1.execSQL_currExam();
            db2.execSQL_currYear();
            db3.execSQL_currSem();


            ArrayList<Stud_Less> list = db.getArray();
            ArrayList<Exam> examList = db1.getArray();
            ArrayList<studYears> yearList = db2.getArray();
            ArrayList<Semester> semList = db3.getArray();
%>
<html>
    <link href="../style/sis_style.css" rel="stylesheet" type="text/css">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    </head>

    <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
        <tr>
            <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Student List</td>
        </tr>
        <tr>
            <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp; </td>
        </tr>
    </table>
    <br>
    <body>


        <center>
            <form name="examUpdate" action="examInsert" method="POST">
                <table width="50%"><tr>
                        <td><input type="hidden" name="subjID" value="<%=subject%>" /></td>

                        <td> Year : </td><td>
                            <select name="year">
                                <%for (int k = 0; k < db2.q.size(); k++) {%>
                                <option value="<%=yearList.get(k).getID()%>">
                                    <%=yearList.get(k).getYear()%>
                                </option>
                            <%}%>                                </select>
                        </td>
                        <td> Semester : </td><td><select name="sem">
                                <%for (int k = 0; k < db3.q.size(); k++) {%>
                                <option value="<%=semList.get(k).getID()%>">
                                    <%=semList.get(k).getSemester()%>
                                </option>
                        <%}%>                                </select> </td>
                        <td> Exam : </td><td><select name="exam">
                                <%for (int k = 0; k < db1.q.size(); k++) {
        if (examList.get(k).getID() == Integer.parseInt(exam)) {%>
                                <option value="<%=examList.get(k).getID()%>" selected>
                                    <%=examList.get(k).getExam()%>
                                </option>
                                <%} else {%>
                                <option value="<%=examList.get(k).getID()%>">
                                    <%=examList.get(k).getExam()%>
                                </option>
                                <%}%>
                        <%}%>                                </select></td>

                    </tr>

                </table>
                <table width="550" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold">
                    <tr height="40">

                        <td width="130" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                            >Name </td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Surname</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Group</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Subject Status</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Mark</td>

                    </tr>
                    <%
    if (db.q.size() != 0) {
        for (int i = 0; i < db.q.size(); i++) {
                    %>
                    <tr height="40">
                        <td width="130" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                            >
                            <input type="hidden" name="SubjLessID" value="<%=list.get(i).getID()%>"/>
                        <input type="hidden" name="subjID" value="<%=subject%>"/><%=list.get(i).getStudName()%></td>
                        <td align="center" width="80" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getStudSurname()%></td>
                        <td align="center" width="80" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getGrpName()%></td>
                        <td align="center" width="80" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getStatus()%></td>
                        <td align="center" width="80" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <input type="text" name="examMark" value="" size="3" /></td>

                    </tr>
                    <%
                        }
                    } else {
                    %>
                    <tr>
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid; border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"> Sorry there is no record for you on this time </td>
                    </tr>
                    <%    }
                    %>
                    <tr><td>Total : <input  name="total" type="hidden" value="<%=db.q.size()%>"/></td><td><%=db.q.size()%>  Students</td></tr>

                    <tr> <td><input type="submit" value="Submit" name="submit" /></td></tr>
                </table>
                <table>
                    <tr><td><a href="signatureList?subjID=<%=subject%>">Signature List</a></td></tr>
                    <tr><td><a href="resultList?subjID=<%=subject%>">Result List</a></td></tr>
                    <tr><td><a href="midtermMarks?subjID=<%=subject%>&exam=1">Midterm Results</a></td></tr>
                    <tr><td><a href="finalMarks.jsp?subjID=<%=subject%>">Final Results</a></td></tr>
                    <tr><td><a href="makeUpMarks.jsp?subjID=<%=subject%>">Result List</a></td></tr>
                    <tr><td><a href="examAverage.jsp?subjID=<%=subject%>">Result List</a></td></tr>
                </table>
            </form>
        </center>
    </body>
</html>
<%       } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        db.close();
        db1.close();
        db2.close();
        db3.close();
%>
