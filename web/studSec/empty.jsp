<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" language="JavaScript">
            function nameempty()
            {
                if ( document.form.name.value == '')
                {
                    alert('No name was entered!')
                    return false;
                }
            }
        </script>
    </head>
    <body>
        <form name="form" method="post" action="empty.jsp" onsubmit="nameempty();">
            <input type="text" name="name" class="textfield">
            <input type="submit" value="Submit" name="submit">
        </form>
    </body>
</html>
