<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Student"%>
<%@page language="Java" import="iaau.studYears" %>
<%@page language="Java" import="iaau.Semester" %>
<jsp:useBean id="year" scope="request" class="iaau.DbStudYears"/>
<jsp:useBean id="semester" scope="request" class="iaau.DbSemester"/>
<jsp:useBean id="db" scope="request" class="iaau.DbStudent" />
<html>
    <head>
        <title>View Student List</title>
        <link href="../style/sis_style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../scripts/table.js"></script>
        <%
            db.connect();
            year.connect();
            semester.connect();
            //	String username = (String)session.getAttribute("user");
            String yearS = request.getParameter("year");
            String semesterS = request.getParameter("semester");

            try {
                if (request.getParameter("group") != null) {
                    db.execSQL_GR_Active(request.getParameter("group"));
                } else {
                    db.execSQL_GR_Active("0");
                }

                year.execSQL(yearS);
                semester.execSQL(semesterS);
                ArrayList<Student> list = db.getArray();
                ArrayList<studYears> yearList = year.getArray();
                ArrayList<Semester> semList = semester.getArray();
        %>
    </head>
    <body>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp;Exam Results</td>
                </tr>
                <tr>
                    <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;&nbsp;Student List </td>
                </tr>
            </table>
            <br>
            <table width="50%" class="label" align="center">
                <tr>
                        <td> Year : 
                            <select name="year">
                                <%for (int i = 0; i < year.q.size(); i++) {%>
                                <option value="<%=yearList.get(i).getID()%>">
                                    <%=yearList.get(i).getYear()%>
                                </option>
                            <%}%>                                </select>
                        </td>
                        <td> Semester :
                            <select name="sem">
                                <%for (int j = 0; j < semester.q.size(); j++) {%>
                                <option value="<%=semList.get(j).getID()%>">
                                    <%=semList.get(j).getSemester()%>
                                </option>
                        <%}%>                                </select> </td>
                    </tr>
                </table>

            <table width="70%" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold">
                <tr height="40">
                    <td width="10%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        ></td>                    
                    <td align="center" width="30%" bgcolor="#eeeeee" valign="middle" class="textBold"
                        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        >Name</td>
                    <td align="center" width="35%" valign="middle" bgcolor="#eeeeee" class="textBold"
                        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        >Surname</td>
                    <td width="15%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        >Group</td>
                </tr>

                <%
            if (db.q.size() != 0) {
                for (int i = 0; i < db.q.size(); i++) {
                %>

                <tr height="30">
                    <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        ><%=i + 1%></td>
                    <td align="center" width="30%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        ><a href="examResultsAdmin?stID=<%=list.get(i).getID()%>&yearID=<%=yearList.get(0).getID()%>&semID=<%=semList.get(0).getID()%>">
                        <%= list.get(i).getName()%></a>
                    </td>
                    <td align="center" width="35%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        ><%=list.get(i).getSurname()%>
                    </td>
                    <td width="15%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        ><%= list.get(i).getGroupName()%></td>
                </tr>
                <%
                }
            } else {
                %>
                <tr>
                    <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                        border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                        border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;
                        border-left-style: solid;"> Sorry there is no record for you on this time </td>
                </tr>
                <%  } %>
            </table>
                <table align="center" class="big">
                    <tr><td>Total : <input  name="total" type="hidden" value="<%=db.q.size()%>"/><%=db.q.size()%>  Students</td></tr>
                </table>
        </center>
    </body>
</html>
<%       } catch (SQLException e) {
                throw new ServletException("Your query is not working", e);
            }
            db.close();
%>