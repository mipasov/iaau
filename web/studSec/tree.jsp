<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<html>
    <head>
        <title>Tree Menu</title>
        <link rel='stylesheet' href='../style/tree.css'>
        <script language="javascript" src="../scripts/tree.js"></script>
    </head>
    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFA63A">



        <table border=0 cellpadding='1' cellspacing=1>
            <tr><td width='16'>
                    <a id="xregistration" href="javascript:Toggle('registration');">
                        <img src='../images/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'></a></td>
                <td><b>Registration</b></td></tr></table>
        <div id="registration" style="display: none; margin-left: 2em;">



            <table border=0 cellpadding='1' cellspacing=1>
                <tr><td width='16'>
                        <a id="xsubject" href="javascript:Toggle('subject');">
                            <img src='../images/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'></a></td>
                    <td><b>Subject</b></td></tr></table>
            <div id="subject" style="display: none; margin-left: 2em;">
                <table border=0 cellpadding='1' cellspacing=1>
                    <tr><td width='16'>
                            <img src='../images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'></td>
                        <td><a href="selectGroup.jsp" target="master">By Student</a></td></tr></table>
                <table border=0 cellpadding='1' cellspacing=1>
                    <tr><td width='16'>
                            <img src='../images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'></td>
                        <td><a href="selectRegGroup.jsp" target="master">By Group</a></td></tr></table>
            </div>

        </div> <!registration>

    <table border=0 cellpadding='1' cellspacing=1>
        <tr><td width='16'>
                <a id="xeduProcess" href="javascript:Toggle('eduProcess');">
                    <img src='../images/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                </a></td><td><b>Educational Process</b></td></tr></table>
    <div id="eduProcess" style="display: none; margin-left: 2em;">

        <table border=0 cellpadding='1' cellspacing=1>
            <tr><td width='16'>
                    <a id="xevaProcess" href="javascript:Toggle('evaProcess');">
                        <img src='../images/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'></a></td>
                <td><b>Evaluation</b></td></tr></table>
        <div id="evaProcess" style="display: none; margin-left: 2em;">
            <table border=0 cellpadding='1' cellspacing=1>
                <tr><td width='16'>
                        <a id="xevaattendance" href="javascript:Toggle('evaattendance');">
                            <img src='../images/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'></a></td>
                    <td><b>Attendance</b></td></tr></table>
            <div id="evaattendance" style="display: none; margin-left: 2em;">
                <table border=0 cellpadding='1' cellspacing=1>
                    <tr><td width='16'>
                            <img src='../images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'></td>
                        <td><a href="subjectAttEvaluation.jsp" target="master">By Subject</a></td></tr></table>
                <table border=0 cellpadding='1' cellspacing=1>
                    <tr><td width='16'>
                            <img src='../images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'></td>
                        <td><a href="depAttEvalution.jsp" target="master">By Department</a></td></tr></table>
            </div>

            <table border=0 cellpadding='1' cellspacing=1>
                <tr><td width='16'>
                        <a id="xevamarks" href="javascript:Toggle('evamarks');">
                            <img src='../images/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'></a></td>
                    <td><b>Marks</b></td></tr></table>
            <div id="evamarks" style="display: none; margin-left: 2em;">
                <table border=0 cellpadding='1' cellspacing=1>
                    <tr><td width='16'>
                            <img src='../images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'></td>
                        <td><a href="subjMarkEva.jsp" target="master">By Subject</a></td></tr></table>
                <table border=0 cellpadding='1' cellspacing=1>
                    <tr><td width='16'>
                            <img src='../images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'></td>
                        <td><a href="depMarkEva.jsp" target="master">By Department</a></td></tr></table>
            </div>

            <table border=0 cellpadding='1' cellspacing=1>
                <tr><td width='16'>
                        <a id="xsemevaluation" href="javascript:Toggle('semevaluation');">
                            <img src='../images/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'></a></td>
                    <td><b>Semester Evaluation</b></td></tr></table>
            <div id="semevaluation" style="display: none; margin-left: 2em;">
                <table border=0 cellpadding='1' cellspacing=1>
                    <tr><td width='16'>
                            <img src='../images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'></td>
                        <td><a href="depSemesterEva.jsp" target="master">Evaluation</a></td></tr></table>
            </div>

        </div> <!evaluation>
    </div> <!educational process>


</div>
<p><a href="javascript:Expand();">Expand All - </a><a href="javascript:Collapse();">Collapse All</a>
</td></tr>
</table>
</body>
</html>
