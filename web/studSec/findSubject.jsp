<%-- 
    Document   : findSubject
    Created on : May 28, 2009, 1:45:15 PM
    Author     : focus
--%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<html>
    <head>
        <link rel='stylesheet' href='../style/sis_style.css'>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Subject</title>
    </head>
    <body>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp;&nbsp;Search Subject</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
            </tr>
        </table>
        <center>
            <form name="searchInstructor" method="POST" action="findSubjectsList.jsp">
                <table class="labelForm">
                    <tr>
                        <td>Search parameter(code) : </td><td><input type="text" value="" size="20" name="code"></td>
                        <td><input type="submit" value="Find" name="find" /></td>
                    </tr>
                </table>
            </form>
        </center>
    </body>
</html>
