<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Subjects"%>
<%@ page language="Java" import="iaau.Group"%>
<%@ page language="Java" import = "iaau.studYears" %>
<%@ page language="Java" import = "iaau.Semester" %>

<jsp:useBean id="db" scope="request" class="iaau.DbSubjects" />
<jsp:useBean id="db1" scope="request" class="iaau.DbGroup" />
<jsp:useBean id="db2" scope="request" class="iaau.DbStudYears"/>
<jsp:useBean id="db3" scope="request" class="iaau.DbSemester"/>
<html>
    <head>
        <title>New Registration(Group)</title>
        <link rel='stylesheet' href='../style/sis_style.css'>
        <script type="text/javascript" src="../scripts/table.js"></script>
        <SCRIPT>
            function howMany(selectObject) {
                var numberSelected=0
                for (var i=0; i < selectObject.options.length; i++) {
                    if (selectObject.options[i].selected==true)
                        numberSelected++
                }
                if (numberSelected == 0)alert('Not selected')
            }
        </SCRIPT>
    </head>
    <body>
        <%
        db.connect();
        db1.connect();
        db2.connect();
        db3.connect();
//	String username = (String)session.getAttribute("user");
        String group = request.getParameter("group");
        String deptID = request.getParameter("department");
        String year = request.getParameter("year");
        String sem = request.getParameter("semester");
        try {
            db.execSQL_byDeprtActive(deptID, year, sem);
            db2.execSQL_currYear();
            db3.execSQL_currSem();
            db1.execSQL_byID(group);
            ArrayList<Subjects> subjList = db.getArray();
            ArrayList<Group> groupList = db1.getArray();
            ArrayList<studYears> yearList = db2.getArray();
            ArrayList<Semester> semList = db3.getArray();
            if (db.q.size() != 0) {
        %>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center"
                   border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp;Subject List</td>
                </tr>
                <tr height="20" bgcolor="#f9F6ee" valign="top" class="textBold">
                    <td>Department:<%=subjList.get(0).getDepartment()%>&nbsp;&nbsp;
                    Group:<%=groupList.get(0).getGr_Name()%></td>
                </tr>
            </table>
            <br>
            <form name="stud" method="post" action="GroupRegister">
                <table width="90%" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold"
                       class="example table-stripeclass:alternate">
                    <thead>
                        <tr height="40">
                            <th width="5%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                > <input type="hidden" name="group" value="<%=group%>" readonly="readonly" /></th>
                            <th width="10%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Code</th>
                            <th align="center" width="45%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Name</th>
                            <th align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Hours / Week</th>
                            <th align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Credit</th>
                            <th align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Year</th>
                            <th align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Semester</th>
                        </tr>
                        <tr height="40">
                            <th width="5%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                > </th>
                            <th width="10%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><input name="filter" size="8" onkeyup="Table.filter(this,this)"/></th>
                            <th align="center" width="45%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ></th>
                            <th align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ></th>
                            <th align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ></th>
                            <th align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><input name="filter" size="3" onkeyup="Table.filter(this,this)"/></th>
                            <th align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >   <input name="filter" size="5" onkeyup="Table.filter(this,this)"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                for (int i = 0; i < db.q.size(); i++) {
                        %>

                        <tr height="30">
                            <td width="5%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><input type="checkbox" name="subjID" value="<%=subjList.get(i).getID()%>" /></td>
                            <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= subjList.get(i).getSubjCode()%> </td>
                            <td width="45%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= subjList.get(i).getSubjectName()%> </td>
                            <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= subjList.get(i).getSubjHrs()%></td>
                            <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= subjList.get(i).getSubjCredit()%></td>
                            <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= subjList.get(i).getStdYear()%> </td>
                            <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= subjList.get(i).getSemester()%> </td>
                        </tr>

                        <% }%>
                        <table width="50%" class="labelForm">
                            <tr>
                                <td> Status : </td><td><select name="status">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                </select> </td>
                                <td> Year : </td><td>
                                    <select  name="year">
                                        <%for (int k = 0; k < db2.q.size(); k++) {%>
                                        <option value="<%=yearList.get(k).getID()%>">
                                            <%=yearList.get(k).getYear()%>
                                        </option>
                                    <%}%>                                </select>
                                </td>
                                <td> Semester : </td><td><select name="sem">
                                        <%for (int k = 0; k < db3.q.size(); k++) {%>
                                        <option value="<%=semList.get(k).getID()%>">
                                            <%=semList.get(k).getSemester()%>
                                        </option>
                                <%}%>                                </select> </td>

                            </tr>
                            <tr><td>&nbsp;</td> <td><input type="submit" value="Register" name="groupRegister" /></td></tr>
                        </table>
                        <%} else {
                        %>
                        <tr>
                            <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                            border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9;
                            border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            Sorry there is no record for you on this time </td>
                        </tr>
                        <%    }
                        %>
                    </tbody>
                </table>
            </form>
        </center>
        <br>
        <%       } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        db.close();
        db1.close();
        db2.close();
        db3.close();
        %>
    </body>
</html>