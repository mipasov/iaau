<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Group" %>
<jsp:useBean id="db" scope="request" class="iaau.DbGroup" />

<html>
    <link rel='stylesheet' href='../style/sis_style.css'>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Select Group</title>
    </head>
    <body>
        <%
        db.connect();
        try {
            db.execSQL();
            ArrayList<Group> list = db.getArray();
        %>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
                <!--DWLayoutTable-->
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Transcript</td>
                </tr>
                <tr>
                    <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
                </tr>
            </table>
            <br>
            <form name="selGroup" action="studentListTranscript.jsp" method="POST">
                <table class="labelForm">
                    <tr align="center">
                        <td>Select Group : <select name="group">
                                <%for (int i = 0; i < db.q.size(); i++) {%>
                                <option value="<%=list.get(i).getID()%>">
                                    <%=list.get(i).getGr_Name()%>
                                </option>
                        <%}%>                                </select></td>
                    <td><input type="submit" value="Select Group" name="grpSelect" /></td></tr>
                </table>

        </form></center>
        <%       } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        %>
        <%
        db.close();
        %>
    </body>
</html>
