<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Semester"%>

<jsp:useBean id="db" scope="request" class="iaau.DbSemester" />
<link rel='stylesheet' href='../style/sis_style.css'>
<%
    db.connect();

    try {
        db.execSQL();

        ArrayList<Semester> list = db.getArray();

%><center>    
    <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
        <tr>
            <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Registration Details</td>
        </tr>
        <tr>
            <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
        </tr>
    </table>
    <h3>Registration Status</h3>
    <form action="setRegistrationStatus" name="registrationStatus" method="POST">
        <table class="labelForm">

            <%
                if (db.q.size() != 0) {
                    for (int i = 0; i < db.q.size(); i++) {
            %>

            <tr>
                <td>

                    <% if (list.get(i).getCurrent() == 1) {%>
                    <input type="text" name="semester" value="<%=list.get(i).getSemester()%>" readonly="readonly"/>
                   

                </td> 
            </tr>
            <tr>
            <%if (list.get(i).getRegStatus() == 1) {%>
            
                <td>
                    <input type="radio" name="regstatus" value="disable" />Disable
                </td>  
              
            
            <%} else {
            %>
            <td>
                    <input type="radio" name="regstatus" value="enable" />Enable
            </td>  
          
            <%}}%>
            
            </tr>
            <%}%>


            <%
            } else {
            %>
            <tr>
                <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid; border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"> Sorry there is no record for you on this time </td>
            </tr>
            <%                }
            %>

            <tr><td>&nbsp;</td> <td><input type="submit" value="Select" name="Select" /></td></tr>
        </table>
    </form>
</center>
<br>
<%       } catch (SQLException e) {
        throw new ServletException("Your query is not working", e);
    }
%>       
<%
    db.close();
%>
