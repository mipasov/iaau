<%-- 
    Document   : examAverage
    Created on : Apr 9, 2008, 10:45:44 AM
    Author     : openkg
--%>

<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Stud_Less" %>
<%@ page language="Java" import = "iaau.studYears" %>
<%@ page language="Java" import = "iaau.Semester" %>
<%@ page language="Java" import = "iaau.SubjExam" %>

<jsp:useBean id="db" scope="request" class="iaau.DbSubjExam"/>
<jsp:useBean id="db2" scope="request" class="iaau.DbStudYears"/>
<jsp:useBean id="db3" scope="request" class="iaau.DbSemester"/>
<jsp:useBean id="aver" scope="request" class="iaau.ExamAverage"/>

<%
            db.connect();
            db2.connect();
            db3.connect();
            aver.connect();
            HttpSession sess = request.getSession();
            String subject = (String) request.getParameter("subjID");
            String exam_id =(String) request.getParameter("exam");
            String sem = (String) sess.getAttribute("semID");
            String year = (String) sess.getAttribute("yearID");

            try {
                db.execSQL_sID(subject, year, sem);
                db2.execSQL_currYear();
                db3.execSQL_currSem();

                ArrayList<SubjExam> list = db.getArray();
                ArrayList<studYears> yearList = db2.getArray();
                ArrayList<Semester> semList = db3.getArray();
%>
<html>    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../style/sis_style.css" rel="stylesheet" type="text/css">
    </head>    
    <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
        <tr>
            <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Student List</td>
        </tr>
        <tr>
            <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp; </td>
        </tr>
    </table>
    <br>
    <body>
        <center>
            <form name="markUpdate" method="POST">                
                <table width="550" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold">
                    <tr height="40">                        
                        <td width="130" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                            >Name </td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Surname</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Group</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Subject Status</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Average</td>                        
                    </tr>
                    <%
    if (db.q.size() != 0) {
        for (int i = 0; i < db.q.size(); i++) {
                    %>
                    <tr height="40">
                        <td width="130" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                            >
                        <input type="hidden" name="subjExamID" value="<%=list.get(i).getID()%>"/> 
                        <input type="hidden" name="subjID" value="<%=subject%>"/><%=list.get(i).getStudentName()%></td>
                        <td align="center" width="80" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getStudentSurname()%></td>
                        <td align="center" width="80" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getGroup()%></td>
                        <td align="center" width="80" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getSubjectStatus()%></td>
                        <td align="center" width="80" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <input disabled="true" type="text" name="mark" value="<%if(aver.calculateAverage(list.get(i).getStud_less_id(),
                                Integer.parseInt(sem),Integer.parseInt(year),Integer.parseInt(subject))<49.5)
                            out.println("F1");
                        else
                            out.println(aver.calculateAverage(list.get(i).getStud_less_id(),
                                Integer.parseInt(sem),Integer.parseInt(year),Integer.parseInt(subject)));
                            %>" size="3"/></td>
                        
                    </tr>
                    <%
                        }
                    } else {
                    %>  
                    <tr>
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid; border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"> Sorry there is no record for you on this time </td>
                    </tr>
                    <%
    }
                    %>
                    <tr><td>Total : <input  name="total" type="hidden" value="<%=db.q.size()%>"/></td><td><%=db.q.size()%>  Students</td></tr>
                                   
                </table>
                
            </form>
        </center>
        
    </body>
</html>
<%       } catch (SQLException e) {
                throw new ServletException("Your query is not working", e);
            }
%>       
<%
            db.close();
            db2.close();
            db3.close();
            aver.close();            
%>
