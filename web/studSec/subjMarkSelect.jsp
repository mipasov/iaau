<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Subjects"%>
<%@ page language="Java" import = "iaau.studYears" %>
<%@ page language="Java" import = "iaau.Semester" %>

<jsp:useBean id="db" scope="request" class="iaau.DbSubjects" />
<jsp:useBean id="db2" scope="request" class="iaau.DbStudYears"/>
<jsp:useBean id="db3" scope="request" class="iaau.DbSemester"/>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
        <script type="text/javascript" src="../scripts/table.js"></script>
        <title>Subject Select</title>        
        <script language="javascript">
function Check(chk)
        {
            if(document.subjSelect.Check_ctr.checked==true){
            for (i = 0; i < chk.length; i++)
            chk[i].checked = true ;
            }else{
            for (i = 0; i < chk.length; i++)
            chk[i].checked = false ;
            }
        }
function Checking(chk){
var count = 0;
for(i = 0; i < chk.length; i++){
if (chk[i].checked == true)
    count = 1;
}
if (count == 0)
{for (i = 0; i < chk.length; i++)
            chk[i].checked = true ;}            
}
</script>
        
    </head>
    <body>
        <% 
        db.connect();
        db2.connect();
        db3.connect();
        
        String deptID = (String)request.getParameter("department");
        String year = (String)request.getParameter("year");
        String sem = (String)request.getParameter("semester");
        try {
            db.execSQL_byDeprtActive(deptID,year,sem);
            db2.execSQL_currYear();
            db3.execSQL_currSem();
            
            
            ArrayList<Subjects> list = db.getArray();
            ArrayList<studYears> yearList = db2.getArray();
            ArrayList<Semester> semList = db3.getArray();
        %>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">  
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp;Mark Evaluation - Subject List</td>
            </tr>
             <% if (db.q.size() != 0) {%> 
            <tr height="20" bgcolor="#f9F6ee" valign="top" class="textBold">
                <td>&nbsp;Department : <%=list.get(0).getDepartment()%>
                &nbsp;&nbsp;<%=list.get(0).getStdYear()%> Course Subjects</td>
            </tr>
        </table>
        <br>
        
        <form name="subjSelect" action="SubjectMarkEvaluation" method="POST">
            <table width="50%" align="center" class="label"><tr>
                    <td><input type="hidden" name="department" value="<%=deptID%>" /></td>
                    
                    <td> Year : </td><td>
                        <select name="year">
                            <%for (int k = 0; k < db2.q.size(); k++) {%>        
                            <option value="<%=yearList.get(k).getID()%>">
                                <%=yearList.get(k).getYear()%>
                            </option>
                        <%}%>                                </select>
                    </td>
                    <td> Semester : </td><td><select name="semester">
                            <%for (int k = 0; k < db3.q.size(); k++) {%>        
                            <option value="<%=semList.get(k).getID()%>">
                                <%=semList.get(k).getSemester()%>
                            </option>
                    <%}%>                                </select> </td>                          
                </tr>         
            </table>
            <table width="80%" height="100" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold" class="example table-stripeclass:alternate">        
                <thead>
                    <tr height="30">
                        <td width="5%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        ><input type="hidden" name="type" value="3" readonly="readonly" /></td>                
                        <td align="center" width="15%" bgcolor="#eeeeee" valign="middle" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        >Code </td>
                        <td align="center" width="35%" bgcolor="#eeeeee" valign="middle" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        >Name </td>
                        <td align="center" width="10%" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        >Hours / Week </td>            
                        <td align="center" width="10%" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        >Modify </td>
                    </tr>
                    <tr height="30">
                        <td width="5%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        </td>                
                        <td align="center" width="15%" bgcolor="#eeeeee" valign="middle" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">                
                        <input name="filter" size="8" onkeyup="Table.filter(this,this)"/></td>
                        <td align="center" width="35%" bgcolor="#eeeeee" valign="middle" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">                
                        <input name="filter" size="8" onkeyup="Table.filter(this,this)"/></td>
                        <td align="center" width="10%" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">                
                        </td>            
                        <td align="center" width="10%" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">                
                        </td>            
                    </tr>
                </thead>
               <%  for (int i = 0; i < db.q.size(); i++) { %>
                <tbody>
                    <tr height="30">
                        <td width="5%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <input type="checkbox" name="subjID" value="<%=list.get(i).getID()%>" /></td>                    
                        <td width="15%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%= list.get(i).getSubjCode() %> </td>
                        <td width="35%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%= list.get(i).getSubjectName() %> </td>
                        <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            <%= list.get(i).getSubjHrs() %>
                        </td>
                        <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <a href ="modifyMarkEvaluation.jsp?subjID=<%=list.get(i).getID()%>">modify</a></td>                        
                    </tr>
                    <%}%>
                    <tr>
                            <td width="5%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            <input type="checkbox" name="Check_ctr" value="yes" onClick="Check(document.subjSelect.subjID)">                                
                            </td>
                            <td width="15%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            &nbsp;</td>
                            <td width="35%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                                <b>Check All</b>
                            </td>
                            <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            &nbsp;</td>
                            <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            &nbsp;</td>
                    </tr>
                        
                    <table align="center" class="big">                        
                        <tr>
                            <td>Total : <input  name="total" type="hidden" value="<%=db.q.size()%>"/><%=db.q.size()%>  Subjects</td>
                        </tr>
                        <tr>
                            <td><input type="submit" value="Evaluate" name="evaluate" onclick="Checking(document.subjSelect.subjID)" /></td>
                        </tr>
                    </table>
                    <%
                    }else {
                    %>
                    <tr>
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;   
                            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; 
                            border-right-style: solid; border-bottom-style: solid;  border-top-style: solid; 
                            border-top-color: #d9d9d9; border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;
                        border-left-color: #d9d9d9;     border-left-style: solid;">
                        Sorry there is no record for you on this time </td>
                    </tr>
                    <%
                    }
                    %>
                </tbody>
            </table>
        </form>
        <br>
        <%       }catch(SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        %>
        <%
        db.close();
        db2.close();
        db3.close();
        %>
    </body>
</html>