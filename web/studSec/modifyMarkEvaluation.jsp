<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Stud_Less" %>
<%@ page language="Java" import = "iaau.studYears" %>
<%@ page language="Java" import = "iaau.Semester" %>

<jsp:useBean id="db" scope="request" class="iaau.DbStud_Less"/>
<jsp:useBean id="db2" scope="request" class="iaau.DbStudYears"/>
<jsp:useBean id="db3" scope="request" class="iaau.DbSemester"/>

<%
db.connect();
db2.connect();
db3.connect();

HttpSession sess = request.getSession();
String subject = (String) request.getParameter("subjID");
String sem = (String) sess.getAttribute("semID");
String year = (String) sess.getAttribute("yearID");

try {
    db.execSQL_Status(subject, year, sem);    
    db2.execSQL_currYear();
    db3.execSQL_currSem();    
    
    ArrayList<Stud_Less> list = db.getArray();
    ArrayList<studYears> yearList = db2.getArray();
    ArrayList<Semester> semList = db3.getArray();
%>
<html>    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../style/sis_style.css" rel="stylesheet" type="text/css">
    </head>    
    <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">           
            <!--DWLayoutTable-->
        <tr>
            <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Update Mark Status</td>
        </tr>
         <% if (db.q.size() != 0) {%>
        <tr>
            <td height="20" bgcolor="#f9F6ea" valign="top" class="textBold">Subject: <%=list.get(0).getSubjName()%> </td>
        </tr>
    </table>
    <br>
    <body>
        <center>
            <form name="statusUpdate" action="updateStatus" method="POST">
                <table width="50%" class="label">                    
                        <input type="hidden" name="subjID" value="<%=subject%>" />
                        <input type="hidden" name="type" value="2" />
                    <tr>                                                
                        <td> Year : </td><td>
                            <select name="year">
                                <%for (int k = 0; k < db2.q.size(); k++) {%>        
                                <option value="<%=yearList.get(k).getID()%>">
                                    <%=yearList.get(k).getYear()%>
                                </option>
                            <%}%>                                </select>
                        </td>
                        <td> Semester : </td><td><select name="sem">
                                <%for (int k = 0; k < db3.q.size(); k++) {%>        
                                <option value="<%=semList.get(k).getID()%>">
                                    <%=semList.get(k).getSemester()%>
                                </option>
                        <%}%>                                </select> </td>                       
                    </tr>                    
                </table>
                <table width="80%" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold">                
                    <tr height="40">                        
                        <td width="130" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        >Name</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Surname</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Group</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Status</td>                        
                    </tr>
                    <%                   
                    for (int i = 0; i < db.q.size(); i++) {
                    %>
                    <tr height="30">
                        <td width="20%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            <input type="hidden" name="studID" value="<%=list.get(i).getStudID()%>"/>
                        <%=list.get(i).getStudName()%></td>
                        <td align="center" width="30%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getStudSurname()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getGrpName()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <input type="text" name="status" value="<%=list.get(i).getStatus()%>" size="3" /></td>
                    </tr>
                    <% }%>                    
                    <table align="center" class="big">
                        <tr><td>Total : <input  name="total" type="hidden" value="<%=db.q.size()%>"/><%=db.q.size()%>  Students</td></tr>                        
                        <tr> <td><input type="submit" value="Submit" name="submit" /></td></tr>
                    </table>
                    <%} else { %>  
                    <tr align="center">
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;   
                            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                            border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; 
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     
                        border-left-style: solid;"> 
                        Sorry there is no record for you on this time </td>
                    </tr>
                    <% } %>
                </table>                
            </form>
        </center>
    </body>
</html>
<%       } catch (SQLException e) {
    throw new ServletException("Your query is not working", e);
}
%>       
<%
db.close();;
db2.close();
db3.close();
%>