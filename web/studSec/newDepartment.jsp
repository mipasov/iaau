<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Faculty"%>

<jsp:useBean id="db" scope="request" class="iaau.DbFaculty"/>
<html>    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
        <title>Add Department</title>
        <%
        db.connect();        
        try{
            db.execSQL();            
            
            ArrayList<Faculty> list = db.getArray();
        %>        
    </head>
    <body>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;New Department</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
            </tr>
        </table>
        
        <center> <form name="addDepartment" action="addDepartment" method="POST">
                
                <table class="labelForm">                  
                    <tr>
                        <td>Faculty : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><select name="faculty">
                                <%for (int i = 0; i < db.q.size(); i++) {%>        
                                <option value="<%=list.get(i).getFacultyId()%>">
                                    <%=list.get(i).getFacultyName()%>
                                </option>
                        <%}%>                                </select></td>
                    </tr>                    
                    <tr>
                        <td>Department Code : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" name="depCode" value="" size="10"/></td>
                    </tr>
                    <tr>
                        <td>Department Name  : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" name="depName" value="" size="20" /></td>
                    </tr>
                    <tr><td><input type="submit" value="Add Department" name="addDep" /></td></tr>
                </table>
                
        </form></center>
    </body>
</html>
<%       }catch(SQLException e) {
    throw new ServletException("Your query is not working", e);
}  
%>       
<%	
db.close();
%>