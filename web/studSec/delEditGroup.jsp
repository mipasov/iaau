<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Group"%>
<%@ page language="Java" import = "iaau.Department"%>
<jsp:useBean id="db" scope="request" class="iaau.DbGroup" />
<jsp:useBean id="db1" scope="request" class="iaau.DbDepartment" />

<html>
    <head>
        <title>Modify Group</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
        <script Language="Javascript" type = "text/javascript">
            var newurl
            function CheckRequest()
            {
                if (confirm("\nDo you really want to delete Group?")) {
                    return true
                } else {
                    return false
                }
            }
        </script>
        <%
        db.connect();
        db1.connect();
        try {
            db.execSQL();
            db1.execSQL();

            ArrayList<Group> list = db.getArray();
            ArrayList<Department> dept = db1.getArray();
        %>

    </head>
    <body>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Modify Group</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
            </tr>
        </table>
        <center>
            <form name="deleteGroup" method="POST" action="DeleteStud">
                <table class="labelForm">
                    <thead>
                        <th><input type="hidden" name="type" value="6" /></th>
                        <th>Name</th>
                    </thead>
                    <tbody>
                        <%
            if (db.q.size() != 0) {
                for (int i = 0; i < db.q.size(); i++) {
                        %>
                        <tr>
                            <td><input type="checkbox" name="studID" value="<%=list.get(i).getID()%>" /></td>
                            <td align="left"><a href="editGroup.jsp?grID=<%=list.get(i).getID()%>"><%=list.get(i).getGr_Name()%></a></td>

                        </tr>
                        <%
                            }
                        } else {
                        %>
                        <tr>
                            <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                                border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                                border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9;
                                border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            Sorry there is no record for you on this time </td>
                        </tr>
                        <%            }
                        %>
                        <tr><td>&nbsp;</td> <td><input type="submit" value="Delete" name="Delete" onClick = "return CheckRequest()"/></td></tr>
                    </tbody>
                </table>

            </form>
        </center>
    </body>
</html>
<%       } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
%>
<%
        db.close();
        db1.close();
%>
