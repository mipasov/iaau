<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <link rel='stylesheet' href='../style/sis_style.css'>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Student</title>
    </head>
    <body>
        
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Search Student</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
            </tr>
        </table>
        
        <center>
            <form name="searchStudent" method="POST" action="findStudent">
                <table class="labelForm">                    
                    <tr>
                        <td><input type="hidden" name="type" value="1"></td>
                        <td>Search parameter : </td><td><input type="text" value="" size="20" name="name"></td>
                        <td><input type="submit" value="Find" name="find" /></td>
                    </tr>
                </table>
            </form>
        </center>
        
    </body>
</html>
