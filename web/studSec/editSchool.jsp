<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.School"%>

<jsp:useBean id="db" scope="request" class="iaau.DbSchool" />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
        <title>School Details</title>
    </head>    
    <body>
        <% 
        String schoolID = (String) request.getParameter("schoolID");
        db.connect();
        try {
            db.execSQL_byID(schoolID);
            ArrayList<School> school = db.getArray();
        %>
        <center>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp;School Details</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;&nbsp;Modify School</td>
            </tr>
        </table>        
        <form action="updateSchool" name="updateSchool" method="POST">
        <table class="labelForm">
            <%if(db.q.size() != 0){  %>          
            <tr>
                <td>ID:</td><td><input name="schoolID" value="<%=school.get(0).getSchoolID()%>" size="15" readonly></td>
            </tr>
            <tr>
                <td>School :</td><td><input name="schoolName" value="<%=school.get(0).getSchoolName()%>" size="15"></td>
            </tr>
            <tr>
                <td><input type="submit" name="submit" value="Update"></td>
            </tr>
            <%}else{%>
            <tr>
                <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                    border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                    border-right-style: solid; border-bottom-style: solid; border-top-style: solid; 
                    border-top-color: #d9d9d9; border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;
                border-left-color: #d9d9d9; border-left-style: solid;">
                Sorry there is no record for you on this time </td>
            </tr>
            <%}//else%>            
        </table>
        </form>
        </center>
    </body>
</html>
<%       }catch(SQLException e) {
    throw new ServletException("Your query is not working", e);
}  
%>       
<%	
db.close();
%>