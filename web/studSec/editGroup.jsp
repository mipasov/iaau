<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Group" %>
<%@ page language="Java" import = "iaau.Department" %>
<jsp:useBean id="db" scope="request" class="iaau.DbGroup" />
<jsp:useBean id="db1" scope="request" class="iaau.DbDepartment" />
<html>
<head>
    <link rel='stylesheet' href='../style/sis_style.css'>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Edit Group</title>
</head>
<body>
<%
String grID = (String) request.getParameter("grID");

db.connect();
db1.connect();

try{
    db.execSQL_byID(grID);
    ArrayList<Group> group = db.getArray();
    
    db1.execSQL();
    ArrayList<Department> department = db1.getArray();
%>
<table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
    <!--DWLayoutTable-->
    <tr>
        <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Modify Group</td>
    </tr>
    <tr>
        <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
    </tr>
</table>
<center>
<form name="newUser" method="POST" action="updateGroup">
<table class="labelForm">    
    <% if (db.q.size() != 0) {
        for (int i = 0; i < db.q.size(); i++) {
    %>
    <tr>
    <td>Department : </td>    
    <td><select name="department">    
            <%for (int k = 0; k < db1.q.size(); k++) {
            if(department.get(k).getID()==(group.get(0).getGroupDept_id())){%>
            <option value="<%=department.get(k).getID()%>" selected="selected">
                <%=department.get(k).getDprt_Code()%>
            </option>
            <%}else {%>
            <option value="<%=department.get(k).getID()%>">
            <%=department.get(k).getDprt_Code()%>
            <%}%>
    <%}%>                                </select></td>
    </tr>
    <tr>
        <td>ID : </td>
        <td><input type="text" name="grID" value="<%=group.get(i).getID()%>" size="15" readonly="readonly" /></td>
    </tr>
    
    <tr>
        <td>Name : </td>
        <td><input type="text" name="name" value="<%=group.get(i).getGr_Name()%>" size="15" /></td>
    </tr>
    <tr><td><input type="submit" value="Update" name="updateGroup" /></td></tr>
    
</table>
</form>
</center>
<%
        }
    }else {   
%>
<table>
    <tr>
        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;   
            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
            border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9; 
        border-bottom-color: #d9d9d9;border-left-color: #d9d9d9;border-left-style: solid;">
        Sorry there is no record for you on this time </td>
    </tr>
</table>
<%
    }
%>
<br>
<%       }catch(SQLException e) {
    throw new ServletException("Your query is not working", e);
}  
%>       
<%	
db.close();
db1.close();
%>
</body>
</html>
