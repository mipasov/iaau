<%-- 
    Document   : statuses
    Created on : May 29, 2009, 1:39:15 PM
    Author     : focus
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page language="Java" import="java.util.*" %>
<%@page language="Java" import="iaau.Users" %>
<jsp:useBean id="db" scope="request" class="iaau.DbUsers"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
        <title>Statuses</title>
        <%
        db.connect();
        try {
            db.execSQL_status();
            ArrayList<Users> list = db.getArray();
        %>
    </head>
    <body>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;User Statuses</td>
    </tr>
    <tr>
        <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
    </tr>
</table>
<br>
    <table class="labelForm" width="50%">
        <%if (db.q.size()!=0){%>
        <thead height="30">
            <th width="20%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            Users</th>
            <th width="20%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            Status</th>
            <th width="20%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            Count</th>
        </thead>
        <tbody class="textBold">
            <%for (int i = 0; i < list.size(); i++){%>
            <tr>
            <td align="center" width="25%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                        style="border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            <%=list.get(i).getRoleName()%></td>
            <td align="center" width="25%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                        style="border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 0px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            <%if (list.get(i).getUserStatus()==1){%>
            Active
            <%}else if (list.get(i).getUserStatus()==0){%>
            Inactive
            <% } %> </td>
            <td align="center" width="25%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                        style="border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            <%=list.get(i).getRoleSum()%></td>
            </tr>
            <% } %>
        </tbody>

        <% }else{ %>
        <tr>
                    <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                        border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                        border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9; border-left-color: #d9d9d9;
                    border-left-style: solid;">
                    Sorry there is no record for you on this time </td>
                </tr>
                <% } %>
    </table>
        </center>
    </body>
    <%} catch (Exception e) {
            throw new Exception("Query is not working");
        }
        db.close();
    %>
</html>
