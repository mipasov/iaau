<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Department"%>
<jsp:useBean id="db" scope="request" class="iaau.DbDepartment" />
<%
        db.connect();
//	String username = (String)session.getAttribute("user");
        try {
            db.execSQL ();

            ArrayList<Department> list = db.getArray();

            String group = (String) request.getParameter("group");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
    </head>
    <body>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Subject Registration By Group</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
            </tr>
        </table>
        <center>
            <form name="deptSelect" action="subjRegSelect.jsp" method="POST">
                <table width="70%" border="1" cellspacing="1" cellpadding="2" class="labelForm" bordercolor="#f9F6ee">
                    <tr> <input type="hidden" name="group" value="<%=group%>"> </tr>

                    <tr align="center">
                        <td>Department :<select name="department">
                                <%for (int i = 0; i < db.q.size(); i++) {%>
                                <option value="<%=list.get(i).getID()%>">
                                    <%=list.get(i).getDprt_Code()%>
                                </option>
                                <%}%>
                        </select> </td>

                        <td>Year :<select name="year">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                        </select></td>
                        <td>Semester :<select name="semester">
                                <option value="1">fall</option>
                                <option value="2">spring</option>
                        </select></td>
                        <td><input type="submit" value="Get Subject List" name="next" /></td>
                    </tr>
                </table>

        </form></center>
    </body>
</html>
<%       } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
%>       
<%
        db.close();
%>
