<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import = "java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Subjects" %>
<%@ page language="Java" import="iaau.Instructor" %>

<jsp:useBean id="db" scope="request" class="iaau.DbSubjects"/>
<jsp:useBean id="db1" scope="request" class="iaau.DbInstructor"/>
<%
HttpSession sess=request.getSession();
String instID =(String)request.getParameter("instructorID");
String sem = (String)sess.getAttribute("semID");
String year = (String)sess.getAttribute("yearID");

db.connect();
db1.connect();

try {
    db.execSQL_subj_inst(instID,year);
    db1.execSQLID(instID);
    ArrayList<Subjects> list = db.getArray();
    ArrayList<Instructor> inst = db1.getArray();
%>
<html>
<head>
    <link rel='stylesheet' href='../style/sis_style.css'>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">    
</head>
<body>

<table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" 
       align="center" border="0" cellpadding="0" cellspacing="0">
    <!--DWLayoutTable-->
    <tr>
        <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;&nbsp;Subject - Instructor Modify</td>
    </tr>
    <tr>
        <td  width="100%" height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;&nbsp;<%=inst.get(0).getInstructorName()+" "+inst.get(0).getInstructorSurname()%>
    </tr>    
</table>
<br>

<form name="subjSelect" action="SubjInstructorDel" method="POST">
    <table width="90%" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold">        
        <tr><input type="hidden" name="instructorID" value="<%=instID%>" /></tr>
        <tr height="40">            
            <td width="5%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">            
            <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            Department</td>
            <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            Study Year</td>
            <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold"
                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            Semester</td>
            <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            Code</td>
            <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            Name</td>            
        </tr>
        <%
        if(db.q.size() != 0){
        for (int i = 0; i < db.q.size(); i++) {
        %>
        <tr height="30">
            <td width="5%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            <input type="checkbox" name="subjInstID" value="<%=list.get(i).getSubjInstID()%>" /></td>
            <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            <input type="hidden" name="department" value="<%=list.get(i).getDepartment()%>" /><%=list.get(i).getDepartment()%>
            </td>
            <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            <%=list.get(i).getStdYear()%></td>
            <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            <%=list.get(i).getSemester()%></td>
            <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            <%=list.get(i).getSubjCode()%>
            <input type="hidden" name="code" value="<%=list.get(i).getSubjCode()%>">
            </td>
            <td align="center" width="60%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
            <%=list.get(i).getSubjectName()%>
            <input type="hidden" name="subject" value="<%=list.get(i).getSubjectName()%>">
            </td>
        </tr>
        <%}        
        }else {   
        %>
        
        <tr>
            <td width="100%" height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;   
                border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; 
                border-right-style: solid; border-bottom-style: solid;  border-top-style: solid; 
                border-top-color: #d9d9d9; border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      
            border-left-color: #d9d9d9;     border-left-style: solid;"> 
            Sorry there is no record for you on this time </td>
        </tr>
    </table>
    <%
        }
    %>
    <table align="center">
        <tr height="30"><td>&nbsp;</td><td><input type="submit" name="Delete Registration" value="Delete"/></td></tr>    
    </table>    
</form>
</body>
</html>
<%       }catch(SQLException e) {
    throw new ServletException("Your query is not working", e);
}  
%>       
<%	
db.close();
db1.close();
%>
