<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<html>
    <head>
        <title>Reports List</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
    </head>
    <body>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;"
               align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;&nbsp;Reports</td>
            </tr>
            <tr>
                <td  width="100%" height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;
            </tr>
        </table>
    <center>
        <table width="50%" border="0" cellspacing="1" cellpadding="1">
            <ul>
                <li><a href="f2List.jsp" target="master">F2 Subject - Student List</a></li>
                <li><a href="f1List" target="master">F1 Student List</a></li>
                <li><a href="stdept" target="master">Number of Students by Department</a></li>
                <li><a href="spaGroup.jsp" target="master">SPA List</a></li>
                <li><a href="gpaGroup.jsp" target="master">GPA List</a></li>
                <li>Grand Final English<a href="GrandFinal" target="master"> PDF</a> /
                    <a href="GrandFinalEx" target="master"> Excel</a></li>
                <li>Grand Final Turkish<a href="GrandFinalTurkish" target="master"> PDF</a> /
                    <a href="GrandFinalTurkishEx" target="master"> Excel</a></li>
            </ul>
        </table>
    </center>
</body>
</html>
