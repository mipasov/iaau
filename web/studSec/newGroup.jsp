<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Department" %>
<jsp:useBean id="db" scope="request" class="iaau.DbDepartment" />


<html>
    <link rel='stylesheet' href='../style/sis_style.css'>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Group</title>
    </head>
    <body>
        <% 
        db.connect();
        try{
            db.execSQL();
            
            ArrayList<Department> list = db.getArray();
        
        %>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;New Group</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
            </tr>
        </table>
        <center> <form name="addGroup" action="addGroup" method="POST">
                
                <table class="labelForm">
                    <tr>
                        <td>Department : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><select name="department">
                                <%for (int i = 0; i < db.q.size(); i++) {%>        
                                <option value="<%=list.get(i).getID()%>">
                                    <%=list.get(i).getDprt_Code()%>
                                </option>
                                <%}%>  
                        </select></td>
                    </tr>
                    <tr>
                        <td>Group Name  : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" name="groupName" value="" size="20" /></td>
                    </tr>
                    <tr><td><input type="submit" value="Add Group" name="addGR" /></td></tr>
                </table>
                
        </form></center>
        <%       }catch(SQLException e) {
            throw new ServletException("Your query is not working", e);
        }  
        %>       
        <%	
        db.close();
        %>
    </body>
</html>
