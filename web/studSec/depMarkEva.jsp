<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Department"%>


<jsp:useBean id="db" scope="request" class="iaau.DbDepartment" />
<link rel='stylesheet' href='../style/sis_style.css'>
<% 
db.connect();

try {
    db.execSQL();
    
    ArrayList<Department> list = db.getArray();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>        
        <center>
            <%if(db.q.size()!=0){%>
            <table width="100%" height="35" style="border-bottom-width: 0px; border-bottom-color: #000000;" 
                   align="center" border="0" cellpadding="0" cellspacing="0">    
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">
                        &nbsp; &nbsp;Select Department 
                    </td>
                </tr>
            </table>
            
            <form name="deptSelect" action="depMarkEva" method="POST">
                <table width="70%" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold">       
                    
                    <tr height="30">  
                        <td width="5%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 2px;   border-bottom-width: 1px; border-left-width: 2px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <input type="hidden" name="type" value="3" readonly="readonly" /></td>
                        <td align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid; border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9; border-left-color: #d9d9d9; border-left-style: solid;">
                        Code</td>
                        <td align="center" width="80" bgcolor="#eeeeee" valign="middle" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid; border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9; border-left-color: #d9d9d9; border-left-style: solid;">
                        Name</td> 
                    </tr>
                    <%for(int i=0; i< db.q.size(); i++){%>
                    <tr height="30">   
                        <td width="5%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <input type="checkbox" name="department" value="<%=list.get(i).getID()%>" /></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid; border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9; border-left-color: #d9d9d9; border-left-style: solid;">
                        <%= list.get(i).getDprt_Code()%> </td>
                        <td  align="center" width="80%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                             style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                             border-right-style: solid;      border-bottom-style: solid; border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9; border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%= list.get(i).getDprt_Name() %> </td>                                              
                    </tr>
                    <%}%>
                    <table align="center" class="big">
                        <tr>
                            <td>Total :<input  name="total" type="hidden" value="<%=db.q.size()%>"/><%=db.q.size()%> Departments</td>
                        </tr>
                        <tr> 
                            <td><input type="submit" value="Evaluate" name="evaluate" /></td>
                        </tr>
                    </table>                    
                    <%        
                    }else {   
                    %>
                    <tr>
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;   
                            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; 
                            border-right-style: solid; border-bottom-style: solid;  border-top-style: solid; 
                            border-top-color: #d9d9d9; border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      
                        border-left-color: #d9d9d9;     border-left-style: solid;">
                        Sorry there is no record for you on this time </td>
                    </tr>
                    <%
                    }
                    %>
                    
                </table>
            </form>
        </center>
    </body>
</html>
<%       }catch(SQLException e) {
    throw new ServletException("Your query is not working", e);
}  
%>       
<%	
db.close();
%>
