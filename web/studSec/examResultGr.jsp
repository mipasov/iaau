<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Group" %>
<%@page language="Java" import="iaau.studYears" %>
<%@page language="Java" import="iaau.Semester" %>
<jsp:useBean id="db" scope="request" class="iaau.DbGroup" />
<jsp:useBean id="year" scope="request" class="iaau.DbStudYears"/>
<jsp:useBean id="semester" scope="request" class="iaau.DbSemester"/>
<html>
    <head>
        <title>Groups</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link rel='stylesheet' href='../style/sis_style.css'>        
    </head>
    <body>
        <center>
            <%
        db.connect();
        year.connect();
        semester.connect();

        try {
            db.execSQL();
            year.execSQL();
            semester.execSQL();

            ArrayList<Group> list = db.getArray();
            ArrayList<studYears> yearList = year.getArray();
            ArrayList<Semester> semList = semester.getArray();
            %>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Exam Results</td>
                </tr>
                <tr>
                    <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
                </tr>
            </table>
            <form name="getStudent" action="examResultGrList.jsp" method="POST">
                <table width="60%" border="1" cellspacing="1" cellpadding="2" class="labelForm" bordercolor="#f9F6ee">
                    <tr align="center">
                        <td>Select Group : <select name="group">
                                <%for (int i = 0; i < db.q.size(); i++) {%>
                                <option value="<%=list.get(i).getID()%>">
                                    <%=list.get(i).getGr_Name()%>
                                </option>
                        <%}%>                                </select></td>

                        <td>Select Year : <select name="year">
                                <%for (int j = 0; j < year.q.size(); j++) {%>
                                <option value="<%=yearList.get(j).getID()%>">
                                    <%=yearList.get(j).getYear()%>
                                </option>
                        <%}%>                                </select></td>

                        <td>Select Semester : <select name="semester">
                                <%for (int k = 0; k < semester.q.size(); k++) {%>
                                <option value="<%=semList.get(k).getID()%>">
                                    <%=semList.get(k).getSemester()%>
                                </option>
                        <%}%>                                </select></td>
                    </tr>                    
                </table>
                    <br>
                    <input type="submit" value="Get List" name="getStudent" />
            </form>
        </center>
        <%       } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        db.close();
        %>
    </body>
</html>
