<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Student"%>
<jsp:useBean id="db" scope="request" class="iaau.DbStudent" />
<html>
    <head>
        <title>View Student</title>
        <link href="../style/sis_style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../scripts/table.js"></script>
        <script Language="Javascript" type = "text/javascript">
            var newurl
            function CheckRequest()
            {
                if (confirm("Do you really want to delete student .")) {
                    return true
                } else {
                    return false
                }
            }
        </script>
        <%
        db.connect();
        //	String username = (String)session.getAttribute("user");
        try {
            if (request.getParameter("group") != null) {
                db.execSQL_GR_All(request.getParameter("group"));
            } else {
                db.execSQL_GR_All("0");
            }

            ArrayList<Student> list = db.getArray();

        %>
    </head>
    <body>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp;Modify Student</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;&nbsp;Student List </td>
            </tr>
        </table>
        <br>
        <form name="stud" method="post" action="deleteStudent">
            <table width="70%" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold">
                <tr height="40">
                    <td width="10%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        ></td>
                    <td width="15%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        >Group</td>
                    <td align="center" width="30%" bgcolor="#eeeeee" valign="middle" class="textBold"
                        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        >Name <p>(modify student)</p>
                    </td>
                    <td align="center" width="35%" valign="middle" bgcolor="#eeeeee" class="textBold"
                        style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        >Surname <p>(get student info form)</p>
                    </td>
                </tr>

                <%
            if (db.q.size() != 0) {
                for (int i = 0; i < db.q.size(); i++) {
                %>

                <tr height="30">
                    <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        ><input type="checkbox" name="studID" value="<%=list.get(i).getID()%>"/></td>
                    <td width="15%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        ><%= list.get(i).getGroupName()%></td>
                    <td align="center" width="30%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        ><input type="hidden" name="stSurname" value="<%=list.get(i).getSurname()%>">
                            <a href="editStudent.jsp?studID=<%=list.get(i).getID()%>"><%= list.get(i).getName()%></a>
                    </td>
                    <td align="center" width="35%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        ><input type="hidden" name="stName" value="<%=list.get(i).getName()%>">
                            <a href="studentForm?sid=<%=list.get(i).getID()%>"><%= list.get(i).getSurname()%></a>
                    </td>
                </tr>
                <%
                    }
                } else {
                %>
                <tr>
                    <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                    border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                    border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;
                    border-left-style: solid;"> Sorry there is no record for you on this time </td>
                </tr>
                <%            }
                %>
                <tr><td>&nbsp;</td> <td><input type="submit" value="Delete" name="Delete" onClick = "return CheckRequest()"/></td></tr>
            </table>
        </form>
        <br>
    </body>
</html>
<%       } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        db.close();
%>