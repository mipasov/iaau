<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import ="java.util.*"%>
<%@ page language="Java" import ="iaau.Department"%>
<%@ page language="Java" import="iaau.Subjects" %>

<jsp:useBean id="db" scope="request" class="iaau.DbDepartment" />
<jsp:useBean id="subj" scope="request" class="iaau.DbSubjects"/>
<html>
    <head>
        <link rel='stylesheet' href='../style/sis_style.css'>
        <script type="text/javascript" src="../scripts/table.js"></script>
    </head>
    <body>
        <%
        db.connect();
        subj.connect();
        String instID = (String) request.getParameter("instructorID");
        String year = (String) session.getAttribute("yearID");
        //	String username = (String)session.getAttribute("user");
        try {
            db.execSQL();
            ArrayList<Department> deptList = db.getArray();

            subj.execSQL_subj_inst(instID, year);

            String deptID = (String) request.getParameter("department");
            String yearID = (String) request.getParameter("year");
            String semID = (String) request.getParameter("semester");

            if (deptID != null) {
                subj.execSQL_byDeprtActive(deptID, yearID, semID);
            }

            ArrayList<Subjects> subjList = subj.getArray();
        %>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Subject Instructor Registration</td>
                </tr>
                <tr>
                    <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
                </tr>
            </table>

            <form name="SubjInst" action="SubjInstructor.jsp?instructorID=<%=instID%>" method="POST">
                <table width="70%" border="1" cellspacing="1" cellpadding="2" class="labelForm" bordercolor="#f9F6ee">
                    <tr align="center">
                        <td>Department : <select name="department">
                                <%for (int i = 0; i < db.q.size(); i++) {%>
                                <option value="<%=deptList.get(i).getID()%>">
                                    <%=deptList.get(i).getDprt_Code()%>
                                </option>
                                <%}%>
                        </select></td>
                        <td>Year : <select name="year">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                        </select></td>
                        <td>Semester : <select name="semester">
                                <option value="1">fall</option>
                                <option value="2">spring</option>
                        </select></td>
                        <td><input type="submit" value="Select" name="next" /></td>
                    </tr>
                </table>
            </form>

            <form name="subjInstructor" action="SubjInstructorReg" method="POST">
                <table width="80%" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold"
                       class="example table-stripeclass:alternate">
                    <%     if (subj.q.size() != 0) {%>
                    <thead>
                        <tr height="40">
                            <th width="5%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >&nbsp;<input type="hidden" name="instructorID" value="<%=instID%>" /></th>
                            <th width="10%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Code</th>
                            <th align="center" width="35%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Name</th>
                            <th align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Hours / Week</th>
                            <th align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Credit</th>
                            <th align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Year</th>
                            <th align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Semester</th>                            
                        </tr>
                        <tr height="40">
                            <th width="5%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >&nbsp;</th>
                            <th width="10%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><input name="filter" size="8" onkeyup="Table.filter(this,this)"/></th>
                            <th align="center" width="35%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >&nbsp;</th>
                            <th align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >&nbsp;</th>
                            <th align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >&nbsp;</th>
                            <th align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><input name="filter" size="3" onkeyup="Table.filter(this,this)"/></th>
                            <th align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >   <input name="filter" size="5" onkeyup="Table.filter(this,this)"/></th>                            
                    </thead>
                    <tbody>
                        <%
         for (int i = 0; i < subj.q.size(); i++) {
                        %>
                        <tr height="30">
                            <td width="5%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><input type="checkbox" name="subjID" value="<%=subjList.get(i).getID()%>" /></td>
                            <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= subjList.get(i).getSubjCode()%> </td>
                            <td width="35%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= subjList.get(i).getSubjectName()%> </td>
                            <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= subjList.get(i).getSubjHrs()%></td>
                            <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= subjList.get(i).getSubjCredit()%></td>
                            <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= subjList.get(i).getStdYear()%> </td>
                            <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= subjList.get(i).getSemester()%> </td>                           
                        </tr>

                        <% }%>
                        <table  class="Big">
                            <tr><td>Total : <input  name="total" type="hidden" value="<%=subjList.size()%>"/></td><td><%=subjList.size()%> Subjects</td></tr>
                            <tr> <td><input type="submit" value="Register" name="Register" /></td></tr>
                        </table>


                        <%} else {%>
                        <tr>
                            <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                            border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9;
                            border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            No subjects </td>
                        </tr>
                        <%    }%>
                    </tbody>
                </table>
            </form>
        </center>
        <br>
        <%       } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        db.close();
        subj.close();
        %>
    </body>
</html>