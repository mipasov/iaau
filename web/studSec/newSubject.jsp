<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Department" %>
<jsp:useBean id="db" scope="request" class="iaau.DbDepartment" />

<html>
    <link rel='stylesheet' href='../style/sis_style.css'>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Subject</title>
    </head>
    <body>
        <% 
        db.connect();
        try{
            db.execSQL();
            
            ArrayList<Department> list = db.getArray();
        
        %>
         <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;New Subject</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
            </tr>
        </table>
        
        <center> <form name="addSubject" action="addSubject" method="POST">
                
                <table class="labelForm">
                    <tr>
                        <td>Subject Code : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" name="subjCode" value="" size="20"/></td>
                    </tr>
                    <tr>
                        <td>Subject Name  : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" name="subjName" value="" size="20" /></td>
                    </tr>
                    <tr>
                        <td>Hours / Week  : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" name="subjHrs" value="" size="20" /></td>
                    </tr>
                    <tr>
                        <td>Credit : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" name="credit" value="" size="20" /></td>
                    </tr>
                    <tr>
                        <td>Status : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><select name="status">
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                            <option value="2">Archive</option>
                        </select></td>
                    </tr>
                    <tr>
                        <td>Department : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><select name="department">
                                <%for (int i = 0; i < db.q.size(); i++) {%>        
                                <option value="<%=list.get(i).getID()%>">
                                    <%=list.get(i).getDprt_Code()%>
                                </option>
                        <%}%>                                </select></td>
                    </tr>
                    <tr>
                        <td>Year  : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><select name="year">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                        </select></td>
                    </tr>
                    <tr>
                        <td>Semester  : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><select name="semester">
                                <option value="1">fall</option>
                                <option value="2">spring</option>
                        </select></td>
                    </tr>
                    
                    <tr><td><input type="submit" value="Add Subject" name="addSubject" /></td></tr>
                </table>
                
        </form></center>
        <%       }catch(SQLException e) { 
        throw new ServletException("Your query is not working", e);        
        }  
        %>       
        <%	
        db.close();
        %>
    </body>
</html>
