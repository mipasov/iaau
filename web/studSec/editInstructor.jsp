<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Group" %>
<%@ page language="Java" import = "iaau.Department" %>
<%@ page language="Java" import = "iaau.Level" %>
<%@ page language="Java" import = "iaau.Status" %>
<%@ page language="Java" import = "iaau.Blood" %>
<%@ page language="Java" import = "iaau.Nationality" %>
<%@ page language="Java" import = "iaau.Gender" %>
<%@ page language="Java" import = "iaau.Country" %>
<%@ page language="Java" import = "iaau.Region" %>
<%@ page language="Java" import = "iaau.Instructor" %>
<%@ page language="Java" import = "iaau.Oblast" %>
<%@ page language="Java" import = "iaau.Faculty" %>

<jsp:useBean id="db" scope="request" class="iaau.DbGroup" />
<jsp:useBean id="db1" scope="request" class="iaau.DbDepartment" />
<jsp:useBean id="db2" scope="request" class="iaau.DbLevel" />
<jsp:useBean id="db3" scope="request" class="iaau.DbStatus" />
<jsp:useBean id="db4" scope="request" class="iaau.DbBlood" />
<jsp:useBean id="db5" scope="request" class="iaau.DbNationality" />
<jsp:useBean id="db6" scope="request" class="iaau.DbGender" />
<jsp:useBean id="db7" scope="request" class="iaau.DbCountry" />
<jsp:useBean id="db8" scope="request" class="iaau.DbRegion" />
<jsp:useBean id="db9" scope="request" class="iaau.DbInstructor" />
<jsp:useBean id="db11" scope="request" class="iaau.DbOblast" />
<jsp:useBean id="db10" scope="request" class="iaau.DbFaculty" />

<html>
    <head>
        <link rel='stylesheet' href='../style/sis_style.css'>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Instructor</title>
        <style type='text/css'>
            .css1 {background: #456; border: 0; font-family: Verdana; font-size: 12px;}
            .css2 {width: 35pt; font-family: Verdana; font-size: 10px}
            td.test {vertical-align: middle; padding: 2px 10px 2px 8px;}
        </style>
    </head>
    <body>
        <%
        String roles[] = {"instructor", "supervisor", "secretary", "studSecO", "rector", "dean", "hod", "oidb"};
        String roleNames[] = {"Instructor", "Supervisor", "Secretary", "Student Service", "Rector", "Dean", "HOD", "Administration"};
        String account[] = {"1", "0"};
        String accountN[] = {"Active", "Inactive"};
        String instRollnum = request.getParameter("instRollnum");
        db.connect();
        db1.connect();
        db2.connect();
        db3.connect();
        db4.connect();
        db5.connect();
        db6.connect();
        db7.connect();
        db8.connect();
        db9.connect();
        db10.connect();
        db11.connect();
        try {
            db.execSQL();
            db1.execSQL();
            db2.execSQL();
            db3.execSQL();
            db4.execSQL();
            db5.execSQL();
            db6.execSQL();
            db7.execSQL();
            db8.execSQL();
            db9.execSQLRN(instRollnum);
            db11.execSQL();
            db10.execSQL();

            ArrayList<Group> group = db.getArray();
            ArrayList<Department> depList = db1.getArray();
            ArrayList<Level> levelList = db2.getArray();
            ArrayList<Status> statusList = db3.getArray();
            ArrayList<Blood> blood = db4.getArray();
            ArrayList<Nationality> nation = db5.getArray();
            ArrayList<Gender> gender = db6.getArray();
            ArrayList<Country> country = db7.getArray();
            ArrayList<Region> region = db8.getArray();
            ArrayList<Instructor> inst = db9.getArray();
            ArrayList<Oblast> oblast = db11.getArray();
            ArrayList<Faculty> faculty = db10.getArray();
        %>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp;&nbsp;Modify Instructor</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
            </tr>
        </table>

        <center> <form name="updateInstructor" action="updateInstructor" method="POST">
                <table class="labelForm" border="2">
                    <%if (db9.q.size() != 0) {%>
                    <tr><th colspan="2"></th>
                    <td class="css1">
                        Instructor Info
                    </td>
                    <td>
                        <table class="labelForm">                            
                            <tr>
                                <td align="left">ID : </td>
                                <td><input type="hidden" name="id" value="<%=inst.get(0).getInstructorID()%>">&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="text" name="rollnum" value="<%=inst.get(0).getInstructorRollNum()%>" size="20" readonly/></td>
                            </tr>
                            <tr>
                                <td align="left">Name : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="text" name="instName" value="<%=inst.get(0).getInstructorName()%>" size="20" /></td>
                            </tr>
                            <tr>
                                <td align="left">Surname : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="text" name="instSurname" value="<%=inst.get(0).getInstructorSurname()%>" size="20" /></td>
                            </tr>
                            <tr>
                                <td align="left">Country : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><select name="country">
                                        <%for (int k = 0; k < db7.q.size(); k++) {
        if (inst.get(0).getInstructorCountry().equals(country.get(k).getCountryName())) {%>
                                        <option value="<%=country.get(k).getCountryID()%>" selected>
                                        <%=country.get(k).getCountryName()%></option>
                                        <%} else {%>
                                        <option value="<%=country.get(k).getCountryID()%>">
                                        <%=country.get(k).getCountryName()%></option>
                                        <%}%>
                                <%}%>                                </select></td>
                            </tr>
                            <tr>
                                <td align="left">Oblast : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><select name="oblast">
                                        <%for (int k = 0; k < db11.q.size(); k++) {
        if (inst.get(0).getInstructorOblast().equals(oblast.get(k).getOblName())) {%>
                                        <option value="<%=oblast.get(k).getOblID()%>" selected><%=oblast.get(k).getOblName()%></option>
                                        <%} else {%><option value="<%=oblast.get(k).getOblID()%>"><%=oblast.get(k).getOblName()%></option><%}%>
                                        <%}%>
                                </select></td>
                            </tr>
                            <tr>
                                <td align="left">Region\Town : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><select name="region">
                                        <%for (int k = 0; k < db8.q.size(); k++) {
        if (inst.get(0).getInstructorRegion().equals(region.get(k).getRegionName())) {%>
                                        <option value="<%=region.get(k).getRegionID()%>" selected><%=region.get(k).getRegionName()%></option>
                                        <%} else {%>
                                        <option value="<%=region.get(k).getRegionID()%>"><%=region.get(k).getRegionName()%></option>
                                        <%}
    }%>  </select></td>
                            </tr>
                            <tr>
                                <td align="left">Birthplace : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="text" name="birthplace" value="<%=inst.get(0).getInstructorBirthPl()%>" size="20" /></td>
                            </tr>
                            <tr>
                                <td align="left">Birth date : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="text" name="birthdate" value="<%=inst.get(0).getInstructorDoB()%>" size="20" />(yyyy/mm/dd)</td>
                            </tr>
                            <tr>
                                <td align="left">Gender : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><select name="gender">
                                        <%for (int k = 0; k < db6.q.size(); k++) {
        if (inst.get(0).getInstructorGender().equals(gender.get(k).getGender())) {%>
                                        <option value="<%=gender.get(k).getGenderID()%>" selected>
                                        <%=gender.get(k).getGender()%></option>
                                        <%} else {%>
                                        <option value="<%=gender.get(k).getGenderID()%>">
                                        <%=gender.get(k).getGender()%></option>
                                        <%}%>
                                <%}%>                                </select></td>
                            </tr>
                            <tr>
                                <td align="left">Passport № : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="text" name="passport" value="<%=inst.get(0).getInstructorPassport()%>" size="20" /></td>
                            </tr>
                            <tr>
                                <td align="left">Permanent Address: </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="text" name="permadr" value="<%=inst.get(0).getInstructorPermAdd()%>" size="20" /></td>
                            </tr>
                            <tr>
                                <td align="left">Current Address: </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="text" name="curradr" value="<%=inst.get(0).getInstructorCurrAdd()%>" size="20" /></td>
                            </tr>
                            <tr>
                                <td align="left">E-mail : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="text" name="email" value="<%=inst.get(0).getInstructorEmail()%>" size="20" /></td>
                            </tr>
                            <tr>
                                <td align="left">Nationality : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><select name="nation">
                                        <%for (int k = 0; k < db5.q.size(); k++) {
        if (inst.get(0).getInstructorNationality().equals(nation.get(k).getNationality())) {%>
                                        <option value="<%=nation.get(k).getNationalityID()%>" selected>
                                        <%=nation.get(k).getNationality()%></option>
                                        <%} else {%>
                                        <option value="<%=nation.get(k).getNationalityID()%>">
                                        <%=nation.get(k).getNationality()%></option>
                                        <%}%>
                                <%}%>                                </select></td>
                            </tr>
                            <tr>
                                <td align="left">Phome Number: </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="text" name="phone" value="<%=inst.get(0).getInstructorPhone()%>" size="20"></td>
                            </tr>
                            <tr>
                                <td align="left">Blood Group: </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><select name="blood">
                                        <%for (int k = 0; k < db4.q.size(); k++) {
        if (inst.get(0).getInstructorBlood().equals(blood.get(k).getBlood())) {%>
                                        <option value="<%=blood.get(k).getBloodID()%>" selected>
                                        <%=blood.get(k).getBlood()%></option>
                                        <%} else {%>
                                        <option value="<%=blood.get(k).getBloodID()%>">
                                        <%=blood.get(k).getBlood()%></option>
                                        <%}%>
                                <%}%>                                </select></td>
                            </tr>
                            <tr>
                                <td align="left">Password : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="text" name="password" value="<%=inst.get(0).getInstructorPassword()%>" size="20" /></td>
                            </tr>
                            <tr>
                                <td align="left">Faculty : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><select name="faculty">
                                        <%for (int k = 0; k < db10.q.size(); k++) {
        if (inst.get(0).getInstructorFaculty().equals(faculty.get(k).getFacultyCode())) {%>
                                        <option value="<%=faculty.get(k).getFacultyId()%>" selected>
                                        <%=faculty.get(k).getFacultyName()%></option>
                                        <%} else {%>
                                        <option value="<%=faculty.get(k).getFacultyId()%>">
                                        <%=faculty.get(k).getFacultyName()%></option>
                                        <%}
    }%></select></td>
                            </tr>
                            <tr>
                                <td align="left">Department : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><select name="dept">
                                        <%for (int k = 0; k < db1.q.size(); k++) {
        if (inst.get(0).getInstructorDept().equals(depList.get(k).getDprt_Code())) {%>
                                        <option value="<%=depList.get(k).getID()%>" selected>
                                        <%=depList.get(k).getDprt_Code()%></option>
                                        <%} else {%>
                                        <option value="<%=depList.get(k).getID()%>">
                                        <%=depList.get(k).getDprt_Code()%></option>
                                        <%}
    }%></select></td>
                            </tr>
                            <tr>
                                <td align="left">Group : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><select name="group">
                                        <%for (int k = 0; k < db.q.size(); k++) {
        if (inst.get(0).getInstructorGroup().equals(group.get(k).getGr_Name())) {%>
                                        <option value="<%=group.get(k).getID()%>" selected>
                                        <%=group.get(k).getGr_Name()%></option>
                                        <%} else {%>
                                        <option value="<%=group.get(k).getID()%>">
                                        <%=group.get(k).getGr_Name()%></option>
                                        <%}
    }%></select></td>
                            </tr>
                            <tr>
                                <td align="left">Level : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><select name="level">
                                        <%for (int k = 0; k < db2.q.size(); k++) {
        if (inst.get(0).getInstructorLevel().equals(levelList.get(k).getLevelName())) {%>
                                        <option value="<%=levelList.get(k).getLevelID()%>" selected>
                                        <%=levelList.get(k).getLevelName()%></option>
                                        <%} else {%>
                                        <option value="<%=levelList.get(k).getLevelID()%>">
                                            <%=levelList.get(k).getLevelName()%>
                                        </option>
                                        <%}%>
                                <%}%>                                </select></td>
                            </tr>
                            <tr>
                                <td align="left">Status : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><select name="status">
                                        <%for (int i = 0; i < db3.q.size(); i++) {
        if (inst.get(0).getInstructorStatus().equals(statusList.get(i).getStatusName())) {%>
                                        <option value="<%=statusList.get(i).getStatusID()%>" selected>
                                            <%=statusList.get(i).getStatusName()%>
                                        </option>
                                        <%} else {%>
                                        <option value="<%=statusList.get(i).getStatusID()%>">
                                            <%=statusList.get(i).getStatusName()%>
                                        </option>
                                        <%}%>
                                <%}%>                                </select></td>
                            </tr>
                            <tr>
                                <td align="left">Role : </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><select name="role">
                                        <%for (int k = 0; k < roles.length; k++) {
        if (inst.get(0).getInstructorRole().equals(roles[k])) {%>
                                        <option value="<%=roles[k]%>" selected><%=roleNames[k]%></option>
                                        <%} else {%>
                                        <option value="<%=roles[k]%>"><%=roleNames[k]%></option>
                                        <%}
    }%>         </select></td>
                            </tr>

                            <tr>
                                <td align="left">Account :</td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td><select name="account">
                                        <%for (int acc = 0; acc < account.length; acc++) {
        if (inst.get(0).getInstructorAccount().equals(account[acc])) {%>
                                        <option value="<%=account[acc]%>" selected><%=accountN[acc]%></option>
                                           <%} else {%>
                                        <option value="<%=account[acc]%>"><%=accountN[acc]%></option>
                                        <%}
    }%>
                                </select></td>
                            </tr>
                        </table>
                    </td>
                    </tr>                
                    <tr align="center"><th colspan="2"></th><td></td>
                        <td><input type="submit" value="Modify Instructor" name="modifyInstructor" />
                        <input type="button" name="pr" value="Print" onclick="window.print()"></td>
                    </tr>
                </table>
        </form></center>
        <%}%>
        <%       } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        } catch (NullPointerException ex) {
            throw new ServletException("Try again",ex);
        }
        %>
        <%
        db.close();
        db1.close();
        db2.close();
        db3.close();
        db4.close();
        db5.close();
        db6.close();
        db7.close();
        db8.close();
        db9.close();
        db10.close();
        %>
    </body>
</html>
