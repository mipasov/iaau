<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import = "java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Department"%>
<%@ page language="Java" import = "iaau.studYears"%>
<jsp:useBean id="db" scope="request" class="iaau.DbDepartment"/>
<jsp:useBean id="db1" scope="request" class="iaau.DbStudYears"/>
<%
            db.connect();
            db1.connect();
//	String username = (String)session.getAttribute("user");
            String year = (String) session.getAttribute("yearID");
            try {
                db.execSQL();
                db1.execSQL();

                ArrayList<Department> list = db.getArray();
                ArrayList<studYears> yearList = db1.getArray();
%>
<html>
    <head>
        <title>Examination</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
    </head>
    <body>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Examination</td>
                </tr>
                <tr>
                    <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
                </tr>
            </table>
            <br>
            <form name="deptSelect" action="examSubjectSelect.jsp" method="POST">
                <table align="center" width="50%" border="1" cellspacing="1" cellpadding="1" class="labelForm" bordercolor="#f9F6ee">
                    <tr align="center">
                        <td>Department : <select name="department">
                                <%for (int i = 0; i < db.q.size(); i++) {%>
                                <option value="<%=list.get(i).getID()%>">
                                    <%=list.get(i).getDprt_Code()%>
                                </option>
                                <%}%>
                            </select></td>
                        <td>Year :<select name="year">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select></td>
                        <td><input type="submit" value="Next" name="next" /></td>
                    </tr>
                </table>
            </form>
        </center>
        <%       } catch (SQLException e) {
throw new ServletException("Your query is not working", e);
}
db.close();
        %>
    </body>
</html>
