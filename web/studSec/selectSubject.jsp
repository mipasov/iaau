<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Stud_Less" %>
<jsp:useBean id="db" scope="request" class="iaau.DbStud_Less"/>
<html>
    <head>
        <title>Registration</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
        <%
        HttpSession sess = request.getSession();
        String student = (String) request.getParameter("sid");
        String sem = (String) sess.getAttribute("semID");
        String year = (String) sess.getAttribute("yearID");
        int totalHr = 0;
        int totalCr = 0;

        db.connect();
        try {
            db.execSQL(student, sem, year);

            ArrayList<Stud_Less> list = db.getArray();
        %>
        <script Language="Javascript" type = "text/javascript">
            var newurl
            function CheckRequest()
            {
                if (confirm("\nDo you really want to delete Registration?")) {
                    return true
                } else {
                    return false
                }
            }
        </script>
    </head>
    <body>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;"
                   align="center" border="0" cellpadding="0" cellspacing="0">
                <%if (db.q.size() != 0) {%>
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;&nbsp;Subject List</td>
                </tr>
                <tr>
                    <td  width="100%" height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;
                        Student : <%=list.get(0).getStudName()%>
                        <%=list.get(0).getStudSurname()%>,&nbsp; Group : <%=list.get(0).getGrpName()%> &nbsp;
                        Academic Year : <%=list.get(0).getStudYear()%>&nbsp
                    Academic Semester : <%=list.get(0).getSemester()%></td>
                </tr>
            </table>
            <br>
            <form name="deleteRegistration" action="DeleteStud" method="POST">
                <table width="90%" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold">
                    <tr height="40">
                        <td width="5%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <input type="hidden" name="type" value="3" readonly="readonly" /></td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Code</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Name</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Hours</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Credit</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Status</td>
                    </tr>
                    <%
    for (int i = 0; i < db.q.size(); i++) {
                    %>
                    <tr height="30">
                        <td width="5%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <input type="checkbox" name="studID" value="<%=list.get(i).getID()%>" /></td>
                        <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getSubjCode()%></td>
                        <td align="center" width="55%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getSubjName()%></td>
                        <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getSubHour()%></td>
                        <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getSubCredit()%></td>
                        <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getStatus()%></td>
                    </tr>
                    <%totalHr += Integer.parseInt(list.get(i).getSubHour());%>
                    <%totalCr += Integer.parseInt(list.get(i).getSubCredit());%>
                    <%  } //for %>
                    <tr height="30">
                        <td width="5%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        &nbsp;</td>
                        <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        &nbsp;</td>
                        <td align="center" width="55%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        &nbsp;</td>
                        <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=totalHr%></td>
                        <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=totalCr%></td>
                        <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        &nbsp;</td>
                    </tr>
                    <tr height="30">
                    <td><input type="submit" name="Delete Registration" value="Delete" onClick = "return CheckRequest()"/></td></tr>
                    <tr height="30">
                    <td><a href="regForm?studID=<%=student%>">Report</a></td></tr>
                    <%} else {%>
                    <tr>
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                        border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                        border-right-style: solid; border-bottom-style: solid;  border-top-style: solid;
                        border-top-color: #d9d9d9; border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;
                        border-left-color: #d9d9d9;     border-left-style: solid;">
                        Sorry there is no record for you on this time </td>
                    </tr>
                    <%  }%>

                    <tr height="30" align="center">
                        <td><a href="newReg.jsp?sid=<%=student%>" target="master">Registration</a></td>
                    </tr>
                </table>
            </form>
        </center>
    </body>
</html>
<%       } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        db.close();
%>
