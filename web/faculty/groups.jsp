<%-- 
    Document   : groups
    Created on : Apr 10, 2009, 11:19:51 AM
    Author     : focus
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page language="Java" import="java.sql.*" %>
<%@page language="Java" import = "java.util.*" %>
<%@page language="Java" import = "iaau.Group" %>
<%@page language="Java" import = "iaau.Department" %>
<%@page language="Java" import="iaau.Instructor"%>
<jsp:useBean id="db" scope="request" class="iaau.DbGroup" />
<jsp:useBean id="db2" scope="request" class="iaau.DbInstructor"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
        <title>Groups</title>
        <%
        db.connect();
        db2.connect();
        String user = (String)request.getRemoteUser();
        try {
            db2.execSQLRN(user);
            ArrayList<Instructor> inst = db2.getArray();
            db.execSQL_byFID(inst.get(0).getInstructorF_id());
            ArrayList<Group> group = db.getArray();            
        %>
    </head>
    <body>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Groups List</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
            </tr>
        </table>
        <br>
        <center>
            <table width="50%" height="100" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold" class="example table-stripeclass:alternate">
                <% if (db.q.size() != 0) {%>
                <thead class="label">
                    <tr height="30">
                        <th width="5%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        #</th>
                        <th width="40%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Department</th>
                        <th width="55%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Group</th>
                    </tr>
                </thead>
                <tbody>
                <%for (int i = 0; i < db.q.size(); i++) {%>
                <tr height="30">                    
                    <td width="5%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        ><%=i + 1%></td>
                    <td width="40%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        ><%=group.get(i).getD_Code()%></td>
                    <td width="40%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                        style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                        border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        ><%=group.get(i).getGr_Name()%></td>                  
                </tr>
                <% }%>
                <table align="center" class="big">
                    <tr><td>Total : <%=db.q.size()%>  Groups</td></tr>
                </table>
            </table>
        </center>
        <% } else {%>
        <tr>
            <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
            border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9;
            border-bottom-color: #d9d9d9;border-left-color: #d9d9d9;border-left-style: solid;">
            Sorry there is no record for you on this time </td>
        </tr>
        <% }%>
        <%       } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        db.close();       
        db2.close();
        %>
    </body>
</html>