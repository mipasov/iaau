<%-- 
    Document   : departments
    Created on : Apr 10, 2009, 11:08:45 AM
    Author     : focus
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Department"%>

<jsp:useBean id="db" scope="request" class="iaau.DbDepartment" />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
        <title>Departments</title>
        <%
        db.connect();
        try {
            db.execSQL();

            ArrayList<Department> list = db.getArray();

        %>
    </head>
    <body>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Departments List</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
            </tr>
        </table>
        <br>
        <center>
            <table width="50%" height="100" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold" class="example table-stripeclass:alternate">
                <thead class="label">
                    <tr height="30">
                <th width="5%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                    style="border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;
                    border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                #</th>
                <th width="40%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                    style="border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                    border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                Code</th>                
                <th width="55%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                    style="border-top-width: 1px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                    border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                    border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                Name</th>
                    </tr>
                </thead>
                <tbody>
                    <% if (db.q.size() != 0) {
                for (int i = 0; i < db.q.size(); i++) {%>
                    <tr height="30">
                <td width="5%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 0px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                ><%=i+1%></td>
                <td width="40%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                ><%=list.get(i).getDprt_Code()%></td>                
                <td width="55%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                ><%=list.get(i).getDprt_Name()%></td>
                    </tr>
                    <%  }
                    } else {%>
                    <tr>
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                        border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                        border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9; border-left-color: #d9d9d9;
                        border-left-style: solid;"> Sorry there is no record for you on this time </td>
                    </tr>
                    <%  }%>
                    <table align="center" class="big">
                        <tr><td>Total : <%=db.q.size()%>  Departments</td></tr>
                    </table>
                </tbody>
            </table>
        </center>
    </body>
</html>
<%       } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        db.close();
%>