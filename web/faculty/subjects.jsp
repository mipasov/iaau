<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page language="Java" import="java.sql.*" %>
<%@page language="Java" import = "java.util.*" %>
<%@page language="Java" import = "iaau.Department" %>
<%@page language="Java" import="iaau.Instructor" %>
<jsp:useBean id="db" scope="request" class="iaau.DbDepartment" />
<jsp:useBean id="db1" scope="request" class="iaau.DbInstructor"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link rel='stylesheet' href='../style/sis_style.css'>
        <title>Subject Select</title>
    </head>
    <body>
        <%
        db.connect();
        db1.connect();
        String user = (String)request.getRemoteUser();
        try{
            db1.execSQLRN(user);
            ArrayList<Instructor> inst = db1.getArray();
            db.execSQL_F_ID(inst.get(0).getInstructorF_id());
            ArrayList<Department> list = db.getArray();

        %>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Subjects List</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;&nbsp;select department</td>
            </tr>
        </table>
        <br>
        <center>
            <form name="viewSubjects" action="subjectList.jsp" method="POST">
                <table width="40%" border="1"class="labelForm" bordercolor="#f9F6ee">
                    <tr align="center">
                        <td>Department : <select name="depID">
                                <%for (int i = 0; i < db.q.size(); i++) {%>
                                <option value="<%=list.get(i).getID()%>">
                                    <%=list.get(i).getDprt_Code()%>
                                </option>
                                <%}%>
                        </select>
                        <input type="submit" value="Get List" name="getSubjectLst" /></td>
                    </tr>
                </table>

        </form></center>
        <%       }catch(SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        db.close();
        db1.close();
        %>
    </body>
</html>
