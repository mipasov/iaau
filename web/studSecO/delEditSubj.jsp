<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Department" %>
<jsp:useBean id="db" scope="request" class="iaau.DbDepartment" />
<html>
    <head>
        <title>Edit Subject</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
    </head>
    <body>
        <%
            db.connect();
            try {
                db.execSQL();

                ArrayList<Department> list = db.getArray();

        %>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
                <!--DWLayoutTable-->
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Subject Modify</td>
                </tr>
                <tr>
                    <td height="20" bgcolor="#f9F6ee" valign="top">&nbsp; </td>
                </tr>
            </table>
            <br>
            <form name="SubjectModify" action="delSubjList.jsp" method="POST">
                <table width="80%" border="1" cellspacing="1" cellpadding="2"
                       class="labelForm" bordercolor="#f9F6ee">
                    <tr align="center">
                        <td>Department : <select name="depID">
                                <%for (int i = 0; i < db.q.size(); i++) {%>
                                <option value="<%=list.get(i).getID()%>">
                                    <%=list.get(i).getDprt_Code()%>
                                </option>
                                <%}%>
                            </select>
                        </td>
                        <td>Year : <select name="yearID">
                                <option value = "1">1</option>
                                <option value = "2">2</option>
                                <option value = "3">3</option>
                                <option value = "4">4</option>
                                <option value = "5">5</option>
                            </select>
                        </td>
                        <td>Semester : <select name="semID">
                                <option value="1">fall</option>
                                <option value="2">spring</option>
                            </select>
                        </td>
                        <td><input type="submit" value="Get List" name="getSubjectLst" /></td></tr>
                </table>

            </form>
        </center>
        <%       } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        db.close();
        %>
    </body>
</html>
