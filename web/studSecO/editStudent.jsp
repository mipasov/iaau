<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Group" %>
<%@ page language="Java" import = "iaau.Department" %>
<%@ page language="Java" import = "iaau.Student" %>
<%@ page language="Java" import = "iaau.Academic" %>
<%@ page language="Java" import = "iaau.Education" %>
<%@ page language="Java" import = "iaau.Remark" %>
<%@ page language="Java" import = "iaau.GhsType" %>
<%@ page language="Java" import = "iaau.GhsLanguage" %>
<%@ page language="Java" import = "iaau.Country" %>
<%@ page language="Java" import = "iaau.Region" %>
<%@ page language="Java" import = "iaau.Oblast" %>
<%@ page language="Java" import = "iaau.Gender" %>
<%@ page language="Java" import = "iaau.Nationality" %>
<%@ page language="Java" import = "iaau.Blood" %>
<%@ page language="Java" import = "iaau.studYears"%>
<%@ page language="Java" import = "iaau.School"%>

<jsp:useBean id="db" scope="request" class="iaau.DbGroup" />
<jsp:useBean id="db1" scope="request" class="iaau.DbDepartment" />
<jsp:useBean id="db2" scope="request" class="iaau.DbAcademic" />
<jsp:useBean id="db3" scope="request" class="iaau.DbEducation" />
<jsp:useBean id="db4" scope="request" class="iaau.DbRemark" />
<jsp:useBean id="db5" scope="request" class="iaau.DbGhsType" />
<jsp:useBean id="db6" scope="request" class="iaau.DbGhsLanguage" />
<jsp:useBean id="db7" scope="request" class="iaau.DbCountry" />
<jsp:useBean id="db8" scope="request" class="iaau.DbRegion" />
<jsp:useBean id="db9" scope="request" class="iaau.DbOblast" />
<jsp:useBean id="db10" scope="request" class="iaau.DbGender" />
<jsp:useBean id="db12" scope="request" class="iaau.DbNationality" />
<jsp:useBean id="db13" scope="request" class="iaau.DbBlood" />
<jsp:useBean id="db14" scope="request" class="iaau.DbStudYears" />
<jsp:useBean id="db15" scope="request" class="iaau.DbStudent" />
<jsp:useBean id="db16" scope="request" class="iaau.DbSchool" />
<html>
    <head>
        <link rel='stylesheet' href='../style/sis_style.css'>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Student</title>
        <style type='text/css'>
            .css1 {background: #456; border: 0; font-family: Verdana; font-size: 12px;}
            .css2 {width: 35pt; font-family: Verdana; font-size: 10px}
            td.test {vertical-align: middle; padding: 2px 10px 2px 8px;}
        </style>
    </head>

    <body>
        <%
            String account[] = {"1", "0"};
            String accountN[] = {"Active", "Inactive"};
            String studID = request.getParameter("studID");
            db.connect();
            db1.connect();
            db2.connect();
            db3.connect();
            db4.connect();
            db5.connect();
            db6.connect();
            db7.connect();
            db8.connect();
            db9.connect();
            db10.connect();
            db12.connect();
            db13.connect();
            db14.connect();
            db15.connect();
            db16.connect();
            try {
                db.execSQL();
                db1.execSQL();
                db2.execSQL();
                db3.execSQL();
                db4.execSQL();
                db5.execSQL();
                db6.execSQL();
                db7.execSQL();
                db8.execSQL();
                db9.execSQL();
                db10.execSQL();
                db12.execSQL();
                db13.execSQL();
                db14.execSQL();
                db15.execSQL(studID);
                db16.execSQL();
                ArrayList<Group> list = db.getArray();
                ArrayList<Department> depList = db1.getArray();
                ArrayList<Academic> acad = db2.getArray();
                ArrayList<Education> educ = db3.getArray();
                ArrayList<Remark> remark = db4.getArray();
                ArrayList<GhsType> ghsType = db5.getArray();
                ArrayList<GhsLanguage> ghsLan = db6.getArray();
                ArrayList<Country> country = db7.getArray();
                ArrayList<Region> region = db8.getArray();
                ArrayList<Oblast> oblast = db9.getArray();
                ArrayList<Gender> gender = db10.getArray();
                ArrayList<Nationality> nation = db12.getArray();
                ArrayList<Blood> blood = db13.getArray();
                ArrayList<studYears> year = db14.getArray();
                ArrayList<Student> studList = db15.getArray();
                ArrayList<School> school = db16.getArray();
        %>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
                <!--DWLayoutTable-->
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Modify Student</td>
                </tr>
                <tr>
                    <td height="20" bgcolor="#f9F6ee" valign="top">&nbsp; </td>
                </tr>
            </table>
            <form name="updateStudent" action="updateStudent" method="POST">
                <table class="labelForm" border="2">
                    <%if (db15.q.size() != 0) {%>
                    <tr><th colspan="2"></th>
                        <td class="css1">
                            Student Info
                        </td>
                        <td colspan="2">
                            <table class="labelForm">                                
                                <tr><td><input type="hidden" name="studID" value="<%=studList.get(0).getID()%>"></td></tr>
                                <tr>
                                    <td>
                                        <input type="hidden" name="photo" value="<%=studList.get(0).getPhoto()%>">
                                        <img src="../uploads/<%=studList.get(0).getPhoto()%>"  height="100" width="90" alt="<%=studList.get(0).getPhoto()%>">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Role Number : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="rollnum" value="<%=studList.get(0).getStRollnum()%>" size="20" readonly/></td>
                                </tr>
                                <tr>
                                    <td>Name : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="studName" value="<%=studList.get(0).getName()%>" size="20" /></td>
                                </tr>
                                <tr>
                                    <td>Surname : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="studSurname" value="<%=studList.get(0).getSurname()%>" size="20" /></td>
                                </tr>
                                <tr>
                                    <td>Academic Status : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="acadSt">
                                            <%for (int k = 0; k < db2.q.size(); k++) {
        if (studList.get(0).getAcademic().equals(acad.get(k).getAcademicName())) {%>
                                            <option value="<%=acad.get(k).getAcademicID()%>" selected>
                                                <%=acad.get(k).getAcademicName()%>
                                            </option>
                                            <%} else {%>
                                            <option value="<%=acad.get(k).getAcademicID()%>">
                                                <%=acad.get(k).getAcademicName()%>
                                            </option><% }
    }%>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td>Education Status : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="educSt">
                                            <%for (int k = 0; k < db3.q.size(); k++) {
        if (studList.get(0).getEducation().equals(educ.get(k).getEducationName())) {%>
                                            <option value="<%=educ.get(k).getEducationID()%>" selected>
                                                <%=educ.get(k).getEducationName()%>
                                            </option>
                                            <%} else {%>
                                            <option value="<%=educ.get(k).getEducationID()%>">
                                                <%=educ.get(k).getEducationName()%>
                                            </option>
                                            <%}
    }%>                                </select></td>
                                </tr>
                                <tr>
                                    <td>Remark : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="remark">
                                            <%for (int k = 0; k < db4.q.size(); k++) {
        if (studList.get(0).getRemark().equals(remark.get(k).getRemark())) {%>
                                            <option value="<%=remark.get(k).getRemarkID()%>" selected>
                                                <%=remark.get(k).getRemark()%>
                                            </option>
                                            <%} else {%>
                                            <option value="<%=remark.get(k).getRemarkID()%>">
                                                <%=remark.get(k).getRemark()%>
                                            </option>
                                            <%}
    }%>                                </select></td>
                                </tr>
                                <tr>
                                    <td>GHS Type : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="ghsT">
                                            <%for (int k = 0; k < db5.q.size(); k++) {
        if (studList.get(0).getGhsType().equals(ghsType.get(k).getGhsType())) {%>
                                            <option value="<%=ghsType.get(k).getGhsTypeID()%>" selected>
                                                <%=ghsType.get(k).getGhsType()%>
                                            </option>
                                            <%} else {%>
                                            <option value="<%=ghsType.get(k).getGhsTypeID()%>">
                                                <%=ghsType.get(k).getGhsType()%>
                                            </option>
                                            <%}
    }%>                                </select></td>
                                </tr>
                                <tr>
                                    <td>GHS Language : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="ghsL">
                                            <%for (int k = 0; k < db6.q.size(); k++) {
        if (studList.get(0).getGhsLanguage().equals(ghsLan.get(k).getGhsLanguage())) {%>
                                            <option value="<%=ghsLan.get(k).getGhsLanguageID()%>" selected>
                                                <%=ghsLan.get(k).getGhsLanguage()%>
                                            </option>
                                            <%} else {%>
                                            <option value="<%=ghsLan.get(k).getGhsLanguageID()%>">
                                                <%=ghsLan.get(k).getGhsLanguage()%>
                                            </option>
                                            <%}
    }%>                                </select></td>
                                </tr>
                                <tr>
                                    <td>Entering Academic Year : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="enterY">
                                            <%for (int k = 0; k < db14.q.size(); k++) {
        if (studList.get(0).getEnterY().equals(year.get(k).getYear())) {%>
                                            <option value="<%=year.get(k).getID()%>" selected>
                                                <%=year.get(k).getYear()%>
                                            </option>
                                            <%} else {%>
                                            <option value="<%=year.get(k).getID()%>">
                                                <%=year.get(k).getYear()%>
                                            </option>
                                            <%}
    }%>                                </select></td>
                                </tr>
                                <tr>
                                    <td>Entering Order : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="prikaz1" value="<%=studList.get(0).getEnterOrderNumber()%>" size="20" /></td>
                                </tr>
                                <tr>
                                    <td>Academic Order : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="prikaz2" value="<%=studList.get(0).getAcademOrderNumber()%>" size="20" /></td>
                                </tr>
                                <tr>
                                    <td>Expulsion Order : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="prikaz3" value="<%=studList.get(0).getGraduateOrderNumber()%>" size="20" /></td>
                                </tr>
                                <tr>
                                    <td align="left">Country : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="country">
                                            <%for (int k = 0; k < db7.q.size(); k++) {
        if (studList.get(0).getCountry().equals(country.get(k).getCountryName())) {%>
                                            <option value="<%=country.get(k).getCountryID()%>" selected>
                                                <%=country.get(k).getCountryName()%>
                                            </option>
                                            <%} else {%>
                                            <option value="<%=country.get(k).getCountryID()%>">
                                                <%=country.get(k).getCountryName()%>
                                            </option>
                                            <%}
    }%>                                </select></td>
                                </tr>
                                <tr>
                                    <td align="left">Oblast : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="oblast" size="1">
                                            <%for (int k = 0; k < db9.q.size(); k++) {
        if (studList.get(0).getOblast().equals(oblast.get(k).getOblName())) {%>
                                            <option value="<%=oblast.get(k).getOblID()%>" selected><%=oblast.get(k).getOblName()%></option>
                                            <%} else {%><option value="<%=oblast.get(k).getOblID()%>"><%=oblast.get(k).getOblName()%></option><%}%>
                                            <%}%>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td align="left">Region\Town : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="region" size="1">
                                            <%for (int k = 0; k < db8.q.size(); k++) {
        if (studList.get(0).getRegion().equals(region.get(k).getRegionName())) {%>
                                            <option value="<%=region.get(k).getRegionID()%>" selected><%=region.get(k).getRegionName()%></option>
                                            <%} else {%>
                                            <option value="<%=region.get(k).getRegionID()%>"><%=region.get(k).getRegionName()%></option>
                                            <%}
    }%>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td align="left">Birthplace : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="birthplace" value="<%=studList.get(0).getBirthPlace()%>" size="20" /></td>
                                </tr>
                                <tr>
                                    <td align="left">Birth date : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="birthdate" value="<%=studList.get(0).getDoB()%>" size="20" /> (yyyy/mm/dd)</td>
                                </tr>
                                <tr>
                                    <td align="left">Gender : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="gender">
                                            <%for (int k = 0; k < db10.q.size(); k++) {
        if (studList.get(0).getGender().equals(gender.get(k).getGender())) {%>
                                            <option value="<%=gender.get(k).getGenderID()%>" selected>
                                                <%=gender.get(k).getGender()%>
                                            </option>
                                            <%} else {%>
                                            <option value="<%=gender.get(k).getGenderID()%>">
                                                <%=gender.get(k).getGender()%>
                                            </option>
                                            <%}
    }%>                                </select></td>
                                </tr>
                                <tr>
                                    <td align="left">Passport № : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="passport" value="<%=studList.get(0).getPassport()%>" size="20" /></td>
                                </tr>
                                <tr>
                                    <td align="left">Permanent Address: </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="permadr" value="<%=studList.get(0).getPurmAddress()%>" size="20" /></td>
                                </tr>
                                <tr>
                                    <td align="left">Current Address: </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="curradr" value="<%=studList.get(0).getCurrAddress()%>" size="20" /></td>
                                </tr>
                                <tr>
                                    <td>Department : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="deprt">
                                            <%for (int k = 0; k < db1.q.size(); k++) {
        if (depList.get(k).getDprt_Code().equals(studList.get(0).getDepartCode())) {%>
                                            <option value="<%=depList.get(k).getID()%>" selected="selected">
                                                <%=depList.get(k).getDprt_Code()%>
                                            </option>
                                            <%} else {%>
                                            <option value="<%=depList.get(k).getID()%>">
                                                <%=depList.get(k).getDprt_Code()%>
                                                <%}%>
                                                <%}%>                                </select></td>
                                </tr>
                                <tr>
                                    <td>Group : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="group">
                                            <%for (int j = 0; j < db.q.size(); j++) {
        if (list.get(j).getGr_Name().equals(studList.get(0).getGroupName())) {%>
                                            <option value="<%=list.get(j).getID()%>" selected="selected">
                                                <%=list.get(j).getGr_Name()%>
                                            </option>
                                            <%} else {%>
                                            <option value="<%=list.get(j).getID()%>">
                                                <%=list.get(j).getGr_Name()%>
                                            </option>

                                            <%}%>
                                            <%}%>                                </select></td>
                                </tr>
                                <tr>
                                    <td align="left">E-mail : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="email" value="<%=studList.get(0).getEmail()%>" size="20" /></td>
                                </tr>
                                <tr>
                                    <td align="left">Nationality : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="nation">
                                            <%for (int k = 0; k < db12.q.size(); k++) {
        if (studList.get(0).getNationality().equals(nation.get(k).getNationality())) {%>
                                            <option value="<%=nation.get(k).getNationalityID()%>" selected>
                                                <%=nation.get(k).getNationality()%>
                                            </option>
                                            <%} else {%>
                                            <option value="<%=nation.get(k).getNationalityID()%>">
                                                <%=nation.get(k).getNationality()%>
                                            </option>
                                            <%}
    }%>                                </select></td>
                                </tr>
                                <tr>
                                    <td align="left">Graduated School : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="ghsSchool">
                                            <%for (int k = 0; k < db16.q.size(); k++) {
        if (studList.get(0).getSchool().equals(school.get(k).getSchoolName())) {%>
                                            <option value="<%=school.get(k).getSchoolID()%>" selected>
                                                <%=school.get(k).getSchoolName()%>
                                            </option>
                                            <%} else {%>
                                            <option value="<%=school.get(k).getSchoolID()%>">
                                                <%=school.get(k).getSchoolName()%>
                                            </option>
                                            <%}
    }%>            </select></td>
                                </tr>
                                <tr>
                                    <td align="left">Awards : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="awards" value="<%=studList.get(0).getAwards()%>" size="20"></td>
                                </tr>
                                <tr>
                                    <td align="left">Phone Number : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="phone" value="<%=studList.get(0).getPhoneNumber()%>" size="20"></td>
                                </tr>
                                <tr>
                                    <td align="left">Blood Group: </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="blood">
                                            <%for (int k = 0; k < db13.q.size(); k++) {
        if (studList.get(0).getBlood().equals(blood.get(k).getBlood())) {%>
                                            <option value="<%=blood.get(k).getBloodID()%>" selected>
                                                <%=blood.get(k).getBlood()%>
                                            </option>
                                            <%} else {%>
                                            <option value="<%=blood.get(k).getBloodID()%>">
                                                <%=blood.get(k).getBlood()%>
                                            </option>
                                            <%}
    }%>                                </select></td>
                                </tr>
                                <tr>
                                    <td align="left">Account :</td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="account">
                                            <%for (int acc = 0; acc < account.length; acc++) {
        if (studList.get(0).getStatus().equals(account[acc])) {%>
                                            <option value="<%=account[acc]%>" selected><%=accountN[acc]%></option>
                                            <%} else {%>
                                            <option value="<%=account[acc]%>"><%=accountN[acc]%></option>
                                            <%}
    }%>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td align="left">Password : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="password" size="20" value="<%=studList.get(0).getPassword()%>"/></td>
                                </tr>
                                <tr>
                                    <td align="left">Graduate Project : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="grad_project" size="20" value="<%=studList.get(0).getGraduateProject()%>"/></td>
                                </tr>
                                <tr>
                                    <td align="left">Note 1 : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="note1" size="20" value="<%=studList.get(0).getNote1()%>"/></td>
                                </tr>
                                <tr>
                                    <td align="left">Note 2 : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="note2" size="20" value="<%=studList.get(0).getNote2()%>"/></td>
                                </tr>
                                <tr>
                                    <td align="left">Note 3 : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="note3" size="20" value="<%=studList.get(0).getNote3()%>"/></td>
                                </tr>
                                <tr>
                                    <td align="left">Note 4 : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="note4" size="20" value="<%=studList.get(0).getNote4()%>"/></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr><th colspan="2"></th>
                        <td class="css1">
                            Mather Info
                        </td>
                        <td>
                            <table class="labelForm">
                                <tr>
                                    <td>Name : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="motherName" value="<%=studList.get(0).getMotherName()%>" size="20" /></td>
                                </tr>
                                <tr>
                                    <td>Surname : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="motherSurname" value="<%=studList.get(0).getMotherSurname()%>" size="20" /></td>
                                </tr>
                                <tr>
                                    <td>Work Place : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="motherWork" value="<%=studList.get(0).getMotherWorkPlace()%>" size="20" /></td>
                                </tr>
                                <tr>
                                    <td align="left">Phone Number: </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="motherPh" value="<%=studList.get(0).getMotherPh()%>" size="20"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr><th colspan="2"></th>
                        <td class="css1">
                            Father Info
                        </td>
                        <td>
                            <table class="labelForm">
                                <tr>
                                    <td>Name : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="fatherName" value="<%=studList.get(0).getFatherName()%>" size="20" /></td>
                                </tr>
                                <tr>
                                    <td>Surname : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="fatherSurname" value="<%=studList.get(0).getFatherSurname()%>" size="20" /></td>
                                </tr>
                                <tr>
                                    <td>Work Place : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="fatherWork" value="<%=studList.get(0).getFatherWorkPlace()%>" size="20" /></td>
                                </tr>
                                <tr>
                                    <td align="left">Phone Phone : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="fatherPh" value="<%=studList.get(0).getFatherPh()%>" size="20"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>                  
                    <tr><th colspan="2"></th><td></td>
                            <td align="center"><input type="submit" value="Update" name="updateStudent" />
                                <input type="button" name="pr" value="Print" onclick="window.print()"></td>
                    </tr>

                    <%
                    } else {
                    %>
                    <tr>
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                            border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9;
                            border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            Sorry there is no record for you on this time </td>
                    </tr>
                </table>                
            </form>
        </center>
        <%            }
            } catch (SQLException e) {
                throw new ServletException("Your query is not working", e);
            }
            db.close();
            db1.close();
            db2.close();
            db3.close();
            db4.close();
            db5.close();
            db6.close();
            db7.close();
            db8.close();
            db9.close();
            db10.close();
            db12.close();
            db13.close();
            db14.close();
            db15.close();
        %>
    </body>
</html>