<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Department"%>
<jsp:useBean id="db" scope="request" class="iaau.DbDepartment" />

<%
            db.connect();
//	String username = (String)session.getAttribute("user");
            try {
                db.execSQL();
                ArrayList<Department> list = db.getArray();
%>
<html>
    <head>
        <title>Examination</title>
        <link rel='stylesheet' href='../style/sis_style.css'>
        <script type="text/javascript" src="../scripts/table.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Examination</td>
                </tr>
                <tr>
                    <td height="20" bgcolor="#f9F6ee" valign="top">&nbsp;</td>
                </tr>
            </table>
            <br>
            <form name="deptSelect" action="subjectSelect.jsp" method="POST">
                <table width="50%" border="1" cellspacing="1" cellpadding="2" class="labelForm" bordercolor="#f9F6ee">
                    <tr align="center">
                        <td>Department : <select name="department">
                                <%for (int i = 0; i < db.q.size(); i++) {%>        
                                <option value="<%=list.get(i).getID()%>">
                                    <%=list.get(i).getDprt_Code()%>
                                </option>
                                <%}%> 
                            </select></td>
                        <td>Year : <select name="year">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select></td>
                        <td><input type="submit" value="Next" name="next" /></td>
                    </tr>
                </table>                
            </form>
        </center>
        <%       } catch (SQLException e) {
throw new ServletException("Your query is not working", e);
}
db.close();
        %>
    </body>
</html>

