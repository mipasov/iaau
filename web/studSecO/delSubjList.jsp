<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Subjects"%>
<jsp:useBean id="db" scope="request" class="iaau.DbSubjects"/>
<html>
    <head>
        <title>Edit Subject</title>
        <link rel='stylesheet' href='../style/sis_style.css'>
        <script type="text/javascript" src="../scripts/table.js"></script>
        <%
                    db.connect();
                    String depID = request.getParameter("depID");
                    String yearID = request.getParameter("yearID");
                    String semID = request.getParameter("semID");
                    try {
                        db.execSQL_byDeprt(depID, yearID, semID);
                        ArrayList<Subjects> list = db.getArray();
        %>
        <script Language="Javascript" type = "text/javascript">
            var newurl
            function CheckRequest()
            {
                if (confirm("\nDo you really want to delete Subject?")) {
                    return true
                } else {
                    return false
                }
            }
        </script>
    </head>
    <body>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp;Modify Subject - Subject List</td>
                </tr>
                <%if (db.q.size() != 0) {%>
                <tr>
                    <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;&nbsp;Department : <%=list.get(0).getDepartment()%> </td>
                </tr>
            </table>
            <br>
            <form name="stud" method="post" action="deleteSubject">
                <table width="90%" height="100" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold" class="example table-stripeclass:alternate">
                    <thead>
                        <tr height="30">
                            <td align="center" width="5%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >&nbsp;<input type="hidden" readonly="readonly" value="2" name="type"/></td>
                            <td width="10%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Code</td>
                            <td align="center" width="35%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Name</td>
                            <td align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Hours / Week</td>
                            <td align="center" width="10%" valign="middle" bgcolor="#eeeeee" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Credit</td>
                            <td align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Year</td>
                            <td align="center" width="10%" valign="middle" bgcolor="#eeeeee" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Semester</td>
                            <td align="center" width="10%" valign="middle" bgcolor="#eeeeee" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Status</td>
                        </tr>
                        <tr height="30">
                            <td width="5%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                                &nbsp;</td>
                            <td align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                                <input name="filter" size="8" onkeyup="Table.filter(this,this)"/></td>
                            <td align="center" width="35%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                                <input name="filter" size="8" onkeyup="Table.filter(this,this)"/></td>
                            <td align="center" width="10%" valign="middle" bgcolor="#eeeeee" class="textBold"
                                style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                                &nbsp;</td>
                            <td align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                                &nbsp;</td>
                            <td align="center" width="10%" valign="middle" bgcolor="#eeeeee" class="textBold"
                                style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                                &nbsp;</td>
                            <td align="center" width="10%" valign="middle" bgcolor="#eeeeee" class="textBold"
                                style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 0px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                                &nbsp;</td>
                            <td align="center" width="10%" valign="middle" bgcolor="#eeeeee" class="textBold"
                                style="border-top-width: 0px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                                &nbsp;</td>
                        </tr>
                    </thead>
                    <%
                for (int i = 0; i < db.q.size(); i++) {
                    %>
                    <tbody>
                        <tr height="30">
                            <td width="5%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><input type="checkbox" name="s_id" value="<%=list.get(i).getID()%>" /></td>
                            <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><a href ="editSubject.jsp?subjID=<%=list.get(i).getID()%>"><%= list.get(i).getSubjCode()%> </a></td>
                            <td width="35%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            <%= list.get(i).getSubjectName()%> </td>
                            <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            <%= list.get(i).getSubjHrs()%></td>
                            <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= list.get(i).getSubjCredit()%> </td>
                            <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= list.get(i).getStdYear()%> </td>
                            <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= list.get(i).getSemester()%> </td>
                            <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><% if (list.get(i).getSubjStatus() == 1) {%>Active<%} else if (list.get(i).getSubjStatus() == 0) {%>Passive<% } else {%>Archive<%}%></td>
                        </tr>
                        <% }%>
                        <!--
                            <table align="center"><tr><td><input type="submit"
                        value="Delete" name="Delete" onClick = "return CheckRequest()" /></td></tr></table>
                        !-->
                    <% } else {%>
                    <tr>
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                            border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;
                            border-left-style: solid;"> Sorry there is no record for you on this time </td>
                    </tr>
                    <% }%>
                    </tbody>
                </table>
            </form>
        </center>
    </body>
</html>
<br>
<%       } catch (SQLException e) {
                throw new ServletException("Your query is not working", e);
            }
            db.close();
%>
