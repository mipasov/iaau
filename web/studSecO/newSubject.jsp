<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Department" %>
<jsp:useBean id="db" scope="request" class="iaau.DbDepartment" />
<html>
    <head>
        <title>Add New Subject</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
        <style type="text/css">
            .error_strings{ font-family:Verdana; font-size:10px; color:#660000;}
        </style>
        <script language="JavaScript" src="../scripts/gen_validatorv31.js" type="text/javascript"></script>
    </head>
    <body>
        <%
           db.connect();
           try {
               db.execSQL();

               ArrayList<Department> list = db.getArray();
        %>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
                <!--DWLayoutTable-->
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Add New Subject</td>
                </tr>
                <tr>
                    <td height="20" bgcolor="#f9F6ee" valign="top">&nbsp; </td>
                </tr>
            </table>
            <br>

            <form name="addSubject" action="addSubject" method="POST">
                <table class="labelForm">
                    <tr>
                        <td>Subject Code : </td>
                        <td><input type="hidden" name="status" value="1">&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" name="subjCode" value="" size="20"/></td>
                    </tr>
                    <tr>
                        <td>Subject Name  : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" name="subjName" value="" size="20" /></td>
                    </tr>
                    <tr>
                        <td>Hours / Week  : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" name="subjHrs" value="" size="20" /></td>
                    </tr>

                    <tr>
                        <td>Credit  : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" name="credit" value="" size="20" /></td>
                    </tr>
                    <tr>
                        <td>Department : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><select name="department">
                                <option>Select One</option>
                                <%for (int i = 0; i < db.q.size(); i++) {%>
                                <option value="<%=list.get(i).getID()%>">
                                    <%=list.get(i).getDprt_Code()%>
                                </option>
                            <%}%>                                </select></td>
                    </tr>
                    <tr>
                        <td>Year  : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><select name="year">
                                <option>Select One</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td>Semester  : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><select name="semester">
                                <option>Select One</option>
                                <option value="1">fall</option>
                                <option value="2">spring</option>
                            </select></td>
                    </tr>

                    <tr><td><input type="submit" value="Add Subject" name="addSubject" /></td></tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="left">
                            <div id='addSubject_errorloc' class='error_strings'></div>
                        </td>
                    </tr>
                </table>

            </form>
        </center>
                    <script language="JavaScript" type="text/javascript">
                        var frmvalidator = new Validator("addSubject");
                        frmvalidator.EnableOnPageErrorDisplaySingleBox();
                        frmvalidator.EnableMsgsTogether();

                        frmvalidator.addValidation("subjCode","req","Please enter subject Code");
                        frmvalidator.addValidation("subjName","req","Please enter subject Name");
                        frmvalidator.addValidation("subjHrs","req","Please enter subject Hours");
                        frmvalidator.addValidation("subjHrs","numeric","Hours/Week : Only digits allowed ");
                        frmvalidator.addValidation("credit","req","Please enter subject Credit");
                        frmvalidator.addValidation("credit","numeric","Credit : Only digits allowed ");
                        frmvalidator.addValidation("department","dontselect=0");
                        frmvalidator.addValidation("year","dontselect=0");
                        frmvalidator.addValidation("semester","dontselect=0");

                    </script>
            <%       } catch (SQLException e) {
                    throw new ServletException("Your query is not working", e);
                }
                        db.close();
            %>
    </body>
</html>