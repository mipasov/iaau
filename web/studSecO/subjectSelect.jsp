<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Subjects"%>

<jsp:useBean id="db" scope="request" class="iaau.DbSubjects" />
<html>
    <head>
        <title>Examination</title>
        <link rel='stylesheet' href='../style/sis_style.css'>
        <script type="text/javascript" src="../scripts/table.js"></script>
    </head>
    <body>
        <%
                    db.connect();
                    String deptID = request.getParameter("department");
                    String year = request.getParameter("year");
                    String sem = (String) session.getAttribute("semID");
                    try {
                        db.execSQL_byDeprtActive(deptID, year, sem);

                        ArrayList<Subjects> list = db.getArray();

        %>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Examination - Subject List</td>
                </tr>
                <% if (db.q.size() != 0) {%>
                <tr>
                    <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;Department : <%=list.get(0).getDepartment()%> </td>
                </tr>
            </table>
            <br>
            <form name="stud">
                <table width="90%" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold" class="example table-stripeclass:alternate">
                    <thead>
                        <tr height="40">
                            <td width="10%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Code</td>
                            <td align="center" width="70%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Name</td>
                            <td align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Hours / Week</td>
                            <td align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Delete</td>
                        </tr>
                        <tr height="30">
                            <td width="10%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                                <input name="filter" size="8" onkeyup="Table.filter(this,this)"/></td>
                            <td align="center" width="70%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            </td>
                            <td align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            </td>
                            <td align="center" width="10%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            </td>
                        </tr>
                    </thead>
                    <%  for (int i = 0; i < db.q.size(); i++) {%>
                    <tbody>
                        <tr height="35">
                            <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><a href ="subj_studList.jsp?subjID=<%=list.get(i).getID()%>"><%= list.get(i).getSubjCode()%> </a></td>
                            <td width="70%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= list.get(i).getSubjectName()%> </td>
                            <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= list.get(i).getSubjHrs()%></td>
                            <td align="center" width="10%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><a href ="subj_studListDelete.jsp?subjID=<%=list.get(i).getID()%>">delete</a></td>
                        </tr>
                        <%
                 }
             } else {
                        %>
                        <tr>
                            <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                                border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                                border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9;
                                border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                                Sorry there is no record for you on this time </td>
                        </tr>
                        <%    }
                        %>
                    </tbody>
                </table>
            </form>
        </center>
        <%       } catch (SQLException e) {
                        throw new ServletException("Your query is not working", e);
                    }
                    db.close();
        %>
    </body>
</html>