<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Group" %>
<jsp:useBean id="db" scope="request" class="iaau.DbGroup" />
<link rel='stylesheet' href='../style/sis_style.css'>
<script type="text/javascript" src="../scripts/table.js"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% 
        db.connect();
        try{
            db.execSQL();
            
            ArrayList<Group> list = db.getArray();
        
        %>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Attendance</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;&nbsp;</td>
            </tr>
        </table>
        <br>
        <center> <form name="selectGroup" action="studentListAll" method="POST">
                <table width="40%" border="1" cellspacing="1" cellpadding="2" class="labelForm" bordercolor="#f9F6ee">
                        <tr align="center">
                            <td>Select Group : <select name="group">
                                    <%for (int i = 0; i < db.q.size(); i++) {%>        
                                    <option value="<%=list.get(i).getID()%>">
                                        <%=list.get(i).getGr_Name()%>
                                    </option>
                        <%}%>                                </select></td></tr>
                        <tr align="center"><td><input type="submit" value="Select Group" name="grpSelect" /></td></tr>
                </table>
                
        </form></center>
        <%       }catch(SQLException e) {
            throw new ServletException("Your query is not working", e);
        }  
        %>       
        <%	
        db.close();
        %>
    </body>
</html>
