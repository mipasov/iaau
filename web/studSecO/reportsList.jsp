<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reports List</title>
        <link rel='stylesheet' href='../style/sis_style.css'>
    </head>
    <body>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;"
               align="center" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;&nbsp;Reports</td>
            </tr>
            <tr>
                <td  width="100%" height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;
            </tr>
        </table>
        <center>
             <table width="50%" border="0" cellspacing="1" link="990000" vlink="009900">
                <ul>
                <li><a href="attNotEntered" target="master">Not Entered Attendance List</a></li>
                <li><a href="subjInst" target="master">Subject Instructor List</a></li>
                <li><a href="notRegSt" target="master">Not Registered Students List</a></li>
                <li><a href="subjectList?status=1" target="master">Subject List(Active)</a></li>
                <li><a href="subjectList?status=0" target="master">Subject List(Inactive)</a></li>
                <li><a href="examNotSubmittedInst" target="master">Not Submitted Exam List</a></li>
                <li><a href="f2List.jsp" target="master">F2 Subject - Student List</a></li>
                <li><a href="f1List" target="master">F1 Student List</a></li>
                <li><a href="stdept" target="master">Number of Students by Department</a></li>
                <li><a href="stgroup" target="master">Number of Students by Group(active)</a></li>
                <li><a href="spaGroup.jsp" target="master">SPA List</a></li>
                <li><a href="OutOfList" target="master">Out OF List</a></li>
                <li><a href="examResultGr.jsp" target="master">Student Success Report</a></li>
                </ul>
            </table>
        </center>
    </body>
</html>
