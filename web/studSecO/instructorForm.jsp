<%-- 
    Document   : instructorForm
    Created on : Apr 29, 2009, 1:19:53 PM
    Author     : focus
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page language="Java" import ="java.sql.*" %>
<%@page language="Java" import ="java.util.*" %>
<%@page language="Java" import ="java.text.SimpleDateFormat" %>
<%@page language="Java" import ="java.awt.Color" %>
<%@page language="Java" import ="com.lowagie.text.*"%>
<%@page language="Java" import ="com.lowagie.text.pdf.PdfWriter"%>
<%@page language="Java" import ="com.lowagie.text.pdf.PdfContentByte"%>
<%@page language="Java" import ="com.lowagie.text.Image" %>
<%@page language="Java" import ="com.lowagie.text.pdf.PdfPTable" %>
<%@page language="Java" import ="com.lowagie.text.pdf.PdfPCell" %>
<%@page language="Java" import ="com.lowagie.text.HeaderFooter" %>
<%@page language="Java" import ="iaau.Student"%>
<%@page language="Java" import ="iaau.Instructor"%>
<jsp:useBean id="db1" scope="request" class="iaau.DbStudent"/>
<jsp:useBean id="db" scope="request" class="iaau.DbInstructor"/>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Student Information Form</title>
        <%
        Image img;
        String iid = (String) request.getParameter("iid");
        db.connect();
        try {
            db.execSQLID(iid);

            ArrayList<Instructor> instructor = db.getArray();
        %>
    </head>
    <body>
        <%
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
            Document document = new Document(PageSize.A4, 10, 10, 10, 10);
            PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
            response.setContentType("application/pdf");

            document.open();

            PdfContentByte punder = writer.getDirectContentUnder();
            img = Image.getInstance("/usr/local/images/iaauLogoT.png");
            img.setAbsolutePosition(document.getPageSize().getWidth() / 4, document.getPageSize().getHeight() / 3);
            img.scaleAbsolute(300, 300);

            punder.addImage(img);

            Font title_font = new Font(Font.COURIER, 16, Font.BOLD);
            title_font.setColor(new Color(0x92, 0x90, 0x83));
            Font warning = new Font(Font.COURIER, 12, Font.BOLD);
            warning.setColor(new Color(0xFF, 0x00, 0x00));
            Font in_font = new Font(Font.COURIER, 12, Font.BOLD);
            Font text_font = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);

            Paragraph iaau = new Paragraph("INTERNATIONAL ATATURK ALATOO UNIVERSITY", title_font);
            iaau.setAlignment(Element.ALIGN_CENTER);
            Paragraph sif = new Paragraph("INSTRUCTOR INFORMATION FORM", title_font);
            sif.setAlignment(Element.ALIGN_CENTER);
            document.add(iaau);
            document.add(sif);
            document.add(new Paragraph(15, " "));

            float[] table_colsWidth = {1f, 2f};
            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(90f);
            table.setWidths(table_colsWidth);

            if (db.q.size() != 0) {
                table.addCell(new Phrase("Instructor ID", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorRollNum(), text_font));
                table.addCell(new Phrase("Name Surname", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorName() + " " + instructor.get(0).getInstructorSurname(), title_font));
                table.addCell(new Phrase("Country", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorCountry(), text_font));
                table.addCell(new Phrase("Birthplace", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorBirthPl(), text_font));
                table.addCell(new Phrase("Birth date", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorDoB().toString(), text_font));
                table.addCell(new Phrase("Gender", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorGender(), text_font));
                table.addCell(new Phrase("Passport №", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorPassport(), text_font));
                table.addCell(new Phrase("Permanent Address", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorPermAdd(), text_font));
                table.addCell(new Phrase("Current Address", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorCurrAdd(), text_font));
                table.addCell(new Phrase("e-mail", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorEmail(), text_font));
                table.addCell(new Phrase("Nationality", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorNationality(), text_font));
                table.addCell(new Phrase("Phone Number", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorPhone(), text_font));
                table.addCell(new Phrase("Blood Type", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorBlood(), text_font));
                table.addCell(new Phrase("Faculty", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorFaculty(), text_font));
                table.addCell(new Phrase("Department", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorDept(), text_font));
                table.addCell(new Phrase("Group", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorGroup(), text_font));
                table.addCell(new Phrase("Level", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorLevel(), text_font));
                table.addCell(new Phrase("Status", in_font));
                table.addCell(new Phrase(instructor.get(0).getInstructorStatus(), text_font));

                document.add(table);

            } else {
                document.add(new Paragraph("Sorry, there is no data.", warning));
            }

            document.close();
        %>
    </body>
</html>
<%       } catch (Exception e) {
            throw new ServletException("Your query is not working", e);
        } finally {
            db.close();
        }
%>