<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Weeks" %>
<%@ page language="Java" import = "iaau.Stud_Attendance" %>

<jsp:useBean id="dbWeeks" scope="request" class="iaau.DbWeeks"/>
<jsp:useBean id="dbStudent_Attendance" scope="request" class="iaau.DbStudent_Attendance"/>

<% 
dbWeeks.connect();
dbStudent_Attendance.connect();

HttpSession sess=request.getSession();
String subject=(String)request.getParameter("subjID");
String semester = (String)sess.getAttribute("semID");
String year = (String)sess.getAttribute("yearID");

try {
    String editWeek = (String)request.getParameter("week"), editWeekName = null;
    dbWeeks.execSQL_underCurrWeek();
    ArrayList<Weeks> weeks = dbWeeks.getArray();
    if(editWeek == null)
        editWeek = Integer.toString(weeks.get(weeks.size()-1).getID());
    for(int i=0;i<weeks.size();i++)
        if(Integer.parseInt(editWeek)==weeks.get(i).getID()){
        editWeekName = weeks.get(i).getWeek();
        break;
        }
    
    dbStudent_Attendance.execSQL(Integer.parseInt(year),Integer.parseInt(semester),Integer.parseInt(editWeek),Integer.parseInt(subject));
    ArrayList<Stud_Attendance> stud_attendances = dbStudent_Attendance.getData();
%>
<html>
    <head>
        <title>Attendance</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
    </head>
    <body>
        <center>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;"
               align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Attendance - Modify</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top">&nbsp; </td>
            </tr>
        </table>
            <form name="subjUpdate" action="attendanceModify.jsp" method="POST">
                <table width="50%" border="1" cellspacing="1" cellpadding="2" class="labelForm" bordercolor="#f9F6ee">
                    <tr align="center">
                        <td> Month : <%=editWeekName%> <input type="hidden" name="subjID" value="<%=subject%>"/></td>
                        <td> New Month : <select name="week">
                                <%for (int k = 0; k < weeks.size(); k++) {%>        
                                <option value="<%=weeks.get(k).getID()%>">
                                    <%=weeks.get(k).getWeek()%>
                                </option>
                        <%}%>                                </select></td>
                        <td><input type="submit" name="weekUpdate" value="View"></td>
                    </tr>		    		                       
                </table>
            </form>
            <form name="subjSelect" action="updateAttendance" method="POST">
                <input type="hidden" name="subjID" value="<%=subject%>" />
                <table width="80%" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold">
                    <tr height="40">
                        
                        <td width="20%" align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Name </td>
                        <td width="35%" align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Surname</td>
                        <td width="15%" align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Group</td>
                        <td width="15%" align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Status</td>
                        <td width="15%" align="center" valign="middle" bgcolor="#eeeeee" class="textBold" 
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Attendance</td>
                        
                    </tr>
                    <%
                    if (stud_attendances.size() != 0) {
        for (int i = 0; i < stud_attendances.size(); i++) {
           // Stud_Attendance currentStudAtt = stud_attendances.get(i);
                    %>
                    <tr>
                        <td align="center" width="20%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            <input type="hidden" name="attendanceID" value="<%=stud_attendances.get(i).getId()%>"/>
                        <%=stud_attendances.get(i).getStudent_name()%></td>
                        <td align="center" width="35%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=stud_attendances.get(i).getStudent_surname()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=stud_attendances.get(i).getStudent_group()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=stud_attendances.get(i).getSubject_status()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <input type="text" name="attendence" value="<%=stud_attendances.get(i).getAttendance()%>" size="3" /></td>			
                    </tr>
                    <%
                    }
                    }else {   
                    %>  
                    <tr>
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;   
                            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid; 
                            border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9; border-left-color: #d9d9d9;
                        border-left-style: solid;"> 
                        Sorry there is no record for you on this time </td>
                    </tr>
                    <%
                    }
                    %>
                    <table align="center" class="big">
                        <tr><td>Total : <input  name="total" type="hidden" value="<%=stud_attendances.size()%>"/><%=stud_attendances.size()%>  Students</td></tr>                        
                        <tr> <td><input type="submit" value="Update" name="submit" /></td></tr>
                    </table>
                </table>
            </form>
        </center>
    </body>
</html>
<%       }catch(SQLException e) {
    throw new ServletException("Your query is not working", e);
}  
%>       
<%	
dbWeeks.close();
dbStudent_Attendance.close();
%>