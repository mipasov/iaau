<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "java.text.SimpleDateFormat" %>
<%@ page language="Java" import = "iaau.Student" %>

<jsp:useBean id="db" scope="request" class="iaau.DbStudent"/>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Student List</title>
        <link href="../style/report.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            <!--
.style1 {font-size: 12px}
            -->
        </style>
    </head>    
    <body>        
        <table align="center" width="90%" height="133" border="0">
            <tr>
                <td align="center" rowspan="3" class="topicTable"><img src="../images/sebatLogo.gif" width="80" height="81" alt="IAAU" /></td>
                <td height="59" colspan="2" class="topicTable"><h2 align="center">STUDENT LIST </h2></td>
                <td align="center" rowspan="3" class="topicTable"><img src="../images/iaauLogo.png" width="80" height="81" alt="IAAU" /></td>
            </tr>
            <tr>
                <td class="topicTable style1">Dokuman Kodu: </td>
                <td class="topicTable style1">Revizyon No/Tarihi: </td>
            </tr>
            <tr>
                <td class="topicTable style1">Yayin Tarihi: </td>
                <td class="topicTable style1">Sayfa No: </td>
            </tr>
        </table>
        <%
        String group = request.getParameter("group");
        
        db.connect();
        SimpleDateFormat df=new SimpleDateFormat("dd.MMMMMMMMMMM yyyy");
        
        try {
            if (request.getParameter("group") != null) {
                db.execSQL_GR_All(group);
            } else {
                db.execSQL_GR_All("0");
            }
            ArrayList<Student> list = db.getArray();            
        %>            
        <table align="center" width="90%" height="102" border="0">
            <%if (db.q.size() != 0) {%>
            <tr>
                <td><strong>Department: </strong></td>
                <td><%=list.get(0).getDepartName()%></td>
                <td><strong>Group :</strong></td>
                <td><%=list.get(0).getGroupName()%></td>
            </tr>            
        </table>
        
        <table align="center" width="90%" border="0">
            <tr>
                <td class="reportBody"><strong>#</strong></td>
                <td class="reportBody"><strong>Name</strong></td>
                <td class="reportBody"><strong>Surname</strong></td>
                <td class="reportBody"><strong>1</strong></td>
                <td class="reportBody"><strong>2</strong></td>
                <td class="reportBody"><strong>3</strong></td>
                <td class="reportBody"><strong>4</strong></td>
                <td class="reportBody"><strong>5</strong></td>
            </tr>
            <%    for (int i = 0; i < db.q.size(); i++) {  %>            
            <tr>
                <td class="reportBody"><%=(i+1)%></td>
                <td class="reportBody"><%=list.get(i).getName()%></td>
                <td class="reportBody"><%=list.get(i).getSurname()%></td>
                <td class="reportBody">&nbsp;</td>
                <td class="reportBody">&nbsp;</td>
                <td class="reportBody">&nbsp;</td>
                <td class="reportBody">&nbsp;</td>
                <td class="reportBody">&nbsp;</td>
            </tr>
            <%
            }
            }else {   
            %>
            <tr>
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;   
                            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid; 
                            border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; 
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9; 
                        border-left-style: solid;"> 
                        Sorry there is no record for you on this time </td>
                    </tr>
            <%
            }
            %>
            
        </table>
        <p>&nbsp;</p>
        
        <%       }catch(SQLException e) {
            throw new ServletException("Your query is not working", e);
        }  
        %>       
        <%	
        db.close();
        %>
        
    </body>
</html>
