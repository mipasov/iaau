<%--
    Document   : signatureList
    Created on : Apr 3, 2008, 9:33:35 AM
    Author     : openkg
--%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@page language="Java" import ="java.awt.Color" %>
<%@page language="Java" import ="com.lowagie.text.*"%>
<%@page language="Java" import ="com.lowagie.text.pdf.PdfWriter"%>
<%@page language="Java" import ="com.lowagie.text.pdf.PdfContentByte"%>
<%@page language="Java" import ="com.lowagie.text.Image" %>
<%@page language="Java" import ="com.lowagie.text.pdf.PdfPTable" %>
<%@page language="Java" import ="com.lowagie.text.pdf.PdfPCell" %>
<%@ page language="Java" import = "iaau.Stud_Less" %>
<%@ page language="Java" import = "iaau.studYears" %>
<%@ page language="Java" import = "iaau.Semester" %>
<%@ page language="Java" import = "iaau.Weeks" %>
<%@ page language="Java" import = "iaau.Exam" %>

<jsp:useBean id="dbase" scope="request" class="iaau.DbStud_Less"/>
<jsp:useBean id="db2" scope="request" class="iaau.DbStudYears"/>
<jsp:useBean id="db3" scope="request" class="iaau.DbSemester"/>
<jsp:useBean id="db4" scope="request" class="iaau.DbExam"/>
<jsp:useBean id="aver" scope="request" class="iaau.ExamAverage"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Signature List</title>
        <link href="../style/report.css" rel="stylesheet" type="text/css" />
    </head>
    <%
        Image img;
        dbase.connect();
        db2.connect();
        db3.connect();
        db4.connect();
        aver.connect();
        HttpSession sess = request.getSession();

        String subject = (String) request.getParameter("subjID");
        String year = (String) sess.getAttribute("yearID");
        String sem = (String) sess.getAttribute("semID");
        String exam = (String) sess.getAttribute("examID");
        try {
            dbase.execSQL_Exam(subject, year, sem);
            db2.execSQL_currYear();
            db3.execSQL_currSem();
            db4.execSQL_currExam();

            ArrayList<Stud_Less> list = dbase.getArray();
            ArrayList<studYears> yearList = db2.getArray();
            ArrayList<Semester> semList = db3.getArray();
            ArrayList<Exam> examList = db4.getArray();
    %>
    <body>
        <%
        Document document = new Document(PageSize.A4, 10, 10, 10, 10);
        PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
        response.setContentType("application/pdf");
        document.open();

        PdfContentByte punder = writer.getDirectContentUnder();
        img = Image.getInstance("/usr/local/images/iaauLogoT.png");
        img.setAbsolutePosition(document.getPageSize().getWidth() / 4, document.getPageSize().getHeight() / 3);
        img.scaleAbsolute(300, 300);

        punder.addImage(img);

        Font big_font = new Font(Font.COURIER, 19, Font.BOLD);
        big_font.setColor(new Color(0x92, 0x90, 0x83));
        Font title_font = new Font(Font.COURIER, 13, Font.BOLD);
        title_font.setColor(new Color(0x92, 0x90, 0x83));
        Font warning = new Font(Font.COURIER, 10, Font.BOLD);
        warning.setColor(new Color(0xFF, 0x00, 0x00));
        Font in_font = new Font(Font.COURIER, 12, Font.BOLD);
        Font text_font = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);

        Paragraph iaau = new Paragraph("INTERNATIONAL ATATURK ALATOO UNIVERSITY", title_font);
        iaau.setAlignment(Element.ALIGN_CENTER);
        Paragraph sif = new Paragraph("EXAMINATION SIGNATURE LIST", big_font);
        sif.setAlignment(Element.ALIGN_CENTER);
        document.add(iaau);
        document.add(sif);
        document.add(new Paragraph(10, " "));

        if (list.size() != 0) {

            float[] Thead_colsWidth = {1f, 1f};
            PdfPTable Thead = new PdfPTable(2);
            Thead.setWidthPercentage(90f);
            Thead.setWidths(Thead_colsWidth);
            Thead.getDefaultCell().setBorder(0);
            Thead.addCell(new Phrase("Department: " + list.get(0).getStudDepartment(), in_font));
            Thead.addCell(new Phrase("Subject: " + list.get(0).getSubjName(), in_font));
            Thead.addCell(new Phrase("Academic Year: " + yearList.get(0).getYear(), in_font));
            Thead.addCell(new Phrase("Semester: " + semList.get(0).getSemester(), in_font));
            Thead.addCell(new Phrase("Examination: " + examList.get(0).getExam(), in_font));
            Thead.addCell(new Phrase("Date: ", in_font));
            document.add(Thead);
            document.add(new Paragraph(10, " "));

            float[] Tbody_colsWidth = {0.1f, 1f, 0.5f, 0.5f, 0.4f, 0.6f};
            PdfPTable Tbody = new PdfPTable(6);
            Tbody.setWidthPercentage(90f);
            Tbody.setWidths(Tbody_colsWidth);
            Tbody.getDefaultCell().setFixedHeight(16);
            Tbody.addCell(new Phrase("#", in_font));
            Tbody.addCell(new Phrase("Name Surname", in_font));
            Tbody.addCell(new Phrase("Group", in_font));
            Tbody.addCell(new Phrase("Signature", in_font));
            Tbody.addCell(new Phrase("Papers", in_font));
            Tbody.addCell(new Phrase("Remark", in_font));

            int count = 0;
            for (int i = 0; i < list.size(); i++) {
                if ((exam.equals("1")) || (exam.equals("2"))) {
                    count++;
                    Tbody.addCell(new Phrase(Integer.toString(count), text_font));
                    Tbody.addCell(new Phrase(list.get(i).getStudName() + " " + list.get(i).getStudSurname(), text_font));
                    Tbody.addCell(new Phrase(list.get(i).getGrpName(), text_font));
                    Tbody.addCell(" ");
                    Tbody.addCell(" ");
                    Tbody.addCell(" ");
                } else {
                    if (aver.calculateAverage(list.get(i).getID(),
                            Integer.parseInt(sem), Integer.parseInt(year), Integer.parseInt(subject)) < 49.5) {
                        count++;
                        Tbody.addCell(new Phrase(Integer.toString(count), text_font));
                        Tbody.addCell(new Phrase(list.get(i).getStudName() + " " + list.get(i).getStudSurname(), text_font));
                        Tbody.addCell(new Phrase(list.get(i).getGrpName(), text_font));
                        Tbody.addCell(" ");
                        Tbody.addCell(" ");
                        Tbody.addCell(" ");
                    }
                }
            }
            document.add(Tbody);
            document.add(new Paragraph(10, " "));

            float[] Tfoot_colsWidth = {1.5f, 1f};
            PdfPTable Tfoot = new PdfPTable(2);
            Tfoot.setWidthPercentage(90f);
            Tfoot.setWidths(Tfoot_colsWidth);
            Tfoot.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            Tfoot.addCell(new Phrase("Name Surname :", in_font));
            Tfoot.addCell(new Phrase("Signature :", in_font));
            document.add(Tfoot);

        } else {
            document.add(new Phrase("No records found",warning));
        }

        writer.close();
        document.close();

        %>
    </body>
</html>
<%       } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        dbase.close();
        db2.close();
        db3.close();
        db4.close();
        aver.close();
%>