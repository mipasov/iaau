<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Subjects"%>
<%@ page language="Java" import = "iaau.Department"%>
<%@ page language="Java" import = "iaau.Semester"%>

<jsp:useBean id="db" scope="request" class="iaau.DbSubjects" />
<jsp:useBean id="db1" scope="request" class="iaau.DbDepartment" />
<jsp:useBean id="db2" scope="request" class="iaau.DbSemester" />

<html>
    <head>
        <title>Subject Edit</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
    </head>
    <body>
        <%
            db.connect();
            db1.connect();
            db2.connect();
            String subjID = (String) request.getParameter("subjID");
            try {
                db.execSQL_subj(subjID);
                db1.execSQL();
                db2.execSQL();
                ArrayList<Subjects> list = db.getArray();
                ArrayList<Department> deptList = db1.getArray();
                ArrayList<Semester> semlist = db2.getArray();

                if (db.q.size() != 0) {
                    for (int i = 0; i < db.q.size(); i++) {
        %>
        <center>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Modify Subject</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top">&nbsp; </td>
            </tr>
        </table>
         <form name="editSubject" action="updateSubject" method="POST">

                <table class="labelForm">
                    <tr>
                        <td>Subject ID : </td>
                        <td><input type="hidden" name="status" value="1">&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" name="subjID" value="<%=list.get(i).getID()%>" size="20" disabled/></td>
                    </tr>
                    <tr>
                        <td>Subject Code : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" name="subjCode" value="<%=list.get(i).getSubjCode()%>" size="20"/></td>
                    </tr>
                    <tr>
                        <td>Subject Name  : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" name="subjName" value="<%=list.get(i).getSubjectName()%>" size="20" /></td>
                    </tr>
                    <tr>
                        <td>Hours / Week  : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" name="subjHrs" value="<%=list.get(i).getSubjHrs()%>" size="20" /></td>
                    </tr>
                    <tr>
                        <td>Credit :</td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" name="credit" value="<%=list.get(i).getSubjCredit()%>" size="20" /></td>
                    </tr>
                    <tr>
                        <td>Department : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><select name="department">
                                <%for (int k = 0; k < db1.q.size(); k++) {
                if (deptList.get(k).getDprt_Code().equals(list.get(i).getDepartment())) {%>
                                <option value="<%=deptList.get(k).getID()%>" selected="selected" >
                                    <%=deptList.get(k).getDprt_Code()%>
                                </option>
                                <%} else {%>
                                <option value="<%=deptList.get(k).getID()%>" >
                                    <%=deptList.get(k).getDprt_Code()%>
                                    <%}%>
                                    <%}%>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Year  : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><select name="year">
                                <%for (int k = 1; k < 6; k++) {
                if (k == Integer.parseInt(list.get(i).getStdYear())) {%>
                                <option value="<%=k%>" selected="selected" >
                                    <%=k%>
                                </option>
                                <%} else {%>
                                <option value="<%=k%>" >
                                    <%=k%>
                                    <%}%>
                                    <%}%>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Semester : </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td><select name="semester">
                                <%for (int j = 0; j < db2.q.size(); j++) {
                if (semlist.get(j).getSemester().equals(list.get(i).getSemester())) {%>
                                <option value="<%=semlist.get(j).getID()%>" selected="selected" >
                                    <%=semlist.get(j).getSemester()%>
                                </option>
                                <%} else {%>
                                <option value="<%=semlist.get(j).getID()%>" >
                                    <%=semlist.get(j).getSemester()%>
                                    <%}%>
                                    <%}%>
                            </select>
                        </td>
                    </tr>

                    <tr><td><input type="submit" value="Update" name="updateSubject" /></td></tr>
                            <%
        }
    } else {
                            %>
                    <tr>
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                            border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9;
                            border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            Sorry there is no record for you on this time </td>
                    </tr>
                    <%    }
                    %>
                </table>
            </form>
        </center>
        <%       } catch (SQLException e) {
                throw new ServletException("Your query is not working", e);
            }
            db.close();
            db1.close();
            db2.close();
        %>
    </body>
</html>