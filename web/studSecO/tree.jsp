<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<html>
    
    <head>
        <title>Tree Menu</title>
        <link rel='stylesheet' href='../style/tree.css'>
        <script language="javascript" src="../scripts/tree.js"></script>
    </head>
    
    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFA63A">
        
        <table border=0 cellpadding='10' cellspacing=0><tr><td>
                    <table border=0 cellpadding='1' cellspacing=1>
                        <tr><td width='16'>
                            <a id="xeduProcess" href="javascript:Toggle('eduProcess');">
                                <img src='../images/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                    </a></td><td><b>Educational Process</b></table>
                    <div id="eduProcess" style="display: none; margin-left: 2em;">
                        <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                <a id="xinstructor" href="javascript:Toggle('instructor');">
                                    <img src='../images/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                        </a></td><td><b>Instructors</b></table>
                        <div id="instructor" style="display: none; margin-left: 2em;">
                            <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                        <img src='../images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            </td><td><a href="addInstructor.jsp" target="master">Add Instructor</a></td></tr></table>
                            <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                        <img src='../images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            </td><td><a href="DelEditInstructor.jsp" target="master">Modify Instructor</a></td></tr></table>
                            <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                        <img src='../images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            </td><td><a href="searchInstructor.jsp" target="master">Search Instructor</a></td></tr></table>
                            <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                        <img src='../images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            </td><td><a href="instructor.jsp" target="master">Subject - Instructor</a></td></tr></table>
                        </div>  
                        
                        <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                <a id="xsubject" href="javascript:Toggle('subject');">
                                    <img src='images/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                        </a></td><td><b>Subject</b></table>
                        <div id="subject" style="display: none; margin-left: 2em;">
                            <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                        <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            </td><td><a href="newSubject.jsp" target="master">Add subject</a></td></tr></table>
                            <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                        <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            </td><td><a href="delEditSubj.jsp" target="master">Modify subject</a></td></tr></table>
                            <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                        <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            </td><td><a href="selectGroup.jsp" target="master"> Subject Registration</a></td></tr></table>
                        </div>
                        <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                <a id="xstudent" href="javascript:Toggle('student');">
                                    <img src='images/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                        </a></td><td><b>Student</b></table>
                        <div id="student" style="display: none; margin-left: 2em;">
                            <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                        <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            </td><td><a href="addStudent.jsp" target="master">Add student</a></td></tr></table>
                            <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                        <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            </td><td><a href="DelEditStud.jsp" target="master">Modify student</a></td></tr></table>
                            <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                        <img src='../images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            </td><td><a href="searchStudent.jsp" target="master">Search student</a></td></tr></table>
                        </div> 
                        
                        <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                <a id="xattendance" href="javascript:Toggle('attendance');">
                                    <img src='images/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                        </a></td><td><b>Attendance</b></table>
                        <div id="attendance" style="display: none; margin-left: 2em;">
                            <table border=0 cellpadding='1' cellspacing=1>
                                <tr><td width='16'>
                                        <img src='../images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            </td><td><a href="AttdeptSelect.jsp" target="master">Make Attendance</a></td></tr></table>
                            <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                        <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            </td><td><a href="AttselectGroup.jsp" target="master">Create List</a></td></tr></table>                            
                        </div>
                        
                        <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                <a id="xexam" href="javascript:Toggle('exam');">
                                    <img src='images/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                        </a></td><td><b>Examination</b></table>
                        <div id="exam" style="display: none; margin-left: 2em;">
                            <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                        <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                            </td><td><a href="departmentSelect.jsp" target="master">Examination</a></td></tr></table>                            
                        </div>                        
                    </div>
                    
                    <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                            <a id="xextra" href="javascript:Toggle('extra');">
                                <img src='images/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                    </a></td><td><b>Additional Functions</b></table>
                    <div id="extra" style="display: none; margin-left: 2em;">
                        <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                    <img src='images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                        </td><td><a href="selGroupTranscript.jsp" target="master">transcript</a></td></tr></table>                        
                        <table border=0 cellpadding='1' cellspacing=1><tr><td width='16'>
                                    <img src='../images/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
                        </td><td><a href="reportsList.jsp" target="master">Report list</a></td></tr></table>
                    </div>                    
                    
                    <p><a href="javascript:Expand();">Expand All - </a><a href="javascript:Collapse();">Collapse All</a>
                    
        </td></tr></table>
    </body>
</html>
