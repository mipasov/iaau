<%@ page contentType="text/html"%>
<%@ page pageEncoding="UTF-8"%>
<%@ page errorPage="ExceptionHandler.jsp" %>
<%@ page language="Java" import="javazoom.upload.*,java.util.*,java.sql.*" %>
<%@ page language="Java" import = "iaau.RandPass" %>
<%@ page language="Java" import = "iaau.Group" %>
<%@ page language="Java" import = "iaau.Department" %>
<%@ page language="Java" import = "iaau.Academic" %>
<%@ page language="Java" import = "iaau.Education" %>
<%@ page language="Java" import = "iaau.Remark" %>
<%@ page language="Java" import = "iaau.GhsType" %>
<%@ page language="Java" import = "iaau.GhsLanguage" %>
<%@ page language="Java" import = "iaau.Country" %>
<%@ page language="Java" import = "iaau.Region" %>
<%@ page language="Java" import = "iaau.Oblast" %>
<%@ page language="Java" import = "iaau.Gender" %>
<%@ page language="Java" import = "iaau.Faculty" %>
<%@ page language="Java" import = "iaau.Nationality" %>
<%@ page language="Java" import = "iaau.Blood" %>
<%@ page language="Java" import = "iaau.studYears"%>
<%@ page language="Java" import = "iaau.School" %>

<jsp:useBean id="db" scope="request" class="iaau.DbGroup" />
<jsp:useBean id="db1" scope="request" class="iaau.DbDepartment" />
<jsp:useBean id="db2" scope="request" class="iaau.DbAcademic" />
<jsp:useBean id="db3" scope="request" class="iaau.DbEducation" />
<jsp:useBean id="db4" scope="request" class="iaau.DbRemark" />
<jsp:useBean id="db5" scope="request" class="iaau.DbGhsType" />
<jsp:useBean id="db6" scope="request" class="iaau.DbGhsLanguage" />
<jsp:useBean id="db7" scope="request" class="iaau.DbCountry" />
<jsp:useBean id="db8" scope="request" class="iaau.DbRegion" />
<jsp:useBean id="db9" scope="request" class="iaau.DbOblast" />
<jsp:useBean id="db10" scope="request" class="iaau.DbGender" />
<jsp:useBean id="db11" scope="request" class="iaau.DbFaculty" />
<jsp:useBean id="db12" scope="request" class="iaau.DbNationality" />
<jsp:useBean id="db13" scope="request" class="iaau.DbBlood" />
<jsp:useBean id="db14" scope="request" class="iaau.DbStudYears" />
<jsp:useBean id="db15" scope="request" class="iaau.DbSchool" />

<jsp:useBean id="upBean" scope="page" class="javazoom.upload.UploadBean" >
    <jsp:setProperty name="upBean" property="folderstore" value="/usr/local/uploads" />
    <jsp:setProperty name="upBean" property="overwrite" value="true" />
    <jsp:setProperty name="upBean" property="whitelist" value="*.jpg" />
</jsp:useBean>
<html>
    <head>
        <title>Add Student</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='../style/sis_style.css'>
        <script language="JavaScript" src="../scripts/gen_validatorv31.js" type="text/javascript"></script>
        <style type='text/css'>
            .css1 {background: #456; border: 0; font-family: Verdana; font-size: 12px;}
            .css2 {width: 35pt; font-family: Verdana; font-size: 10px}
            td.test {vertical-align: middle; padding: 2px 10px 2px 8px;}
            .style1 {font-size: 12px;font-family: Verdana;}
            .error_strings{ font-family:Verdana; font-size:10px; color:#660000;}
        </style>
    </head>
    <body>
        <%
            RandPass pass = new RandPass();
            String photoName = null;
            db.connect();
            db1.connect();
            db2.connect();
            db3.connect();
            db4.connect();
            db5.connect();
            db6.connect();
            db7.connect();
            db8.connect();
            db9.connect();
            db10.connect();
            db11.connect();
            db12.connect();
            db13.connect();
            db14.connect();
            db15.connect();
            try {
                String obl = (String) request.getParameter("oblast");

                if (obl != null) {
                    db8.execSQLObl(obl);
                } else {
                    db8.execSQLObl("1");
                }

                db.execSQL();
                db1.execSQL();
                db2.execSQL();
                db3.execSQL();
                db4.execSQL();
                db5.execSQL();
                db6.execSQL();
                db7.execSQL();
                db9.execSQL();
                db10.execSQL();
                db11.execSQL();
                db12.execSQL();
                db13.execSQL();
                db14.execSQL();
                db15.execSQL();

                ArrayList<Group> list = db.getArray();
                ArrayList<Department> depList = db1.getArray();
                ArrayList<Academic> acad = db2.getArray();
                ArrayList<Education> educ = db3.getArray();
                ArrayList<Remark> remark = db4.getArray();
                ArrayList<GhsType> ghsType = db5.getArray();
                ArrayList<GhsLanguage> ghsLan = db6.getArray();
                ArrayList<Country> country = db7.getArray();
                ArrayList<Region> region = db8.getArray();
                ArrayList<Oblast> oblast = db9.getArray();
                ArrayList<Gender> gender = db10.getArray();
                ArrayList<Faculty> faculty = db11.getArray();
                ArrayList<Nationality> nation = db12.getArray();
                ArrayList<Blood> blood = db13.getArray();
                ArrayList<studYears> year = db14.getArray();
                ArrayList<School> school = db15.getArray();
        %>
        <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;New Student</td>
            </tr>
            <tr>
                <td height="20" bgcolor="#f9F6ee" valign="top">&nbsp; </td>
            </tr>
        </table>
        <center>
            <ul class="style1">
                <%
                if (MultipartFormDataRequest.isMultipartFormData(request)) {
                    // Uses MultipartFormDataRequest to parse the HTTP request.
                    MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
                    String todo = null;
                    if (mrequest != null) {
                        todo = mrequest.getParameter("todo");
                    }
                    if ((todo != null) && (todo.equalsIgnoreCase("upload"))) {
                        Hashtable files = mrequest.getFiles();
                        if ((files != null) && (!files.isEmpty())) {
                            UploadFile file = (UploadFile) files.get("uploadfile");
                            if (file != null) {
                                out.println("<li>Form field : uploadfile" + "<BR> Uploaded file : " + file.getFileName() + " (" + file.getFileSize() + " bytes)" + "<BR> Content Type : " + file.getContentType());
                            }
                            //out.println(upBean.getFolderstore());
                            photoName = file.getFileName();
                            // Uses the bean now to store specified by jsp:setProperty at the top.
                            upBean.store(mrequest, "uploadfile");

                        } else {
                            out.println("<li>No uploaded files");
                        }
                    } else {
                        out.println("<BR> todo=" + todo);
                    }
                }
                %>
                <li>&nbsp;</li>
            </ul>
            <form method="POST" action="addStudent.jsp" name="upload" enctype="multipart/form-data">
                <table width="60%" border="0" cellspacing="1" cellpadding="1" align="center" class="style1">
                    <tr>
                        <td align="left"><b>Select a photograph to upload :</b></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <input type="file" name="uploadfile" size="50">
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <input type="hidden" name="todo" value="upload">
                            <input type="submit" name="Submit" value="Upload">
                            <input type="reset" name="Reset" value="Cancel">
                        </td>
                    </tr>
                </table>
            </form>
            <form name="studentForm" action="addStudent" method="POST">
                <table class="labelForm" border="2">
                    <tr><th colspan="2"></th>
                        <td class="css1">
                            Student Info
                        </td>
                        <td>
                            <table class="labelForm">
                                <tr>
                                    <td>
                                        <input type="hidden" name="photo" value="<%=photoName%>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>ID Number : </td>
                                    <td>&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td><input type="text" name="rollnum" value="" size="20"/></td>
                                </tr>
                                <tr>
                                    <td>Name : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="studName" value="" size="20" /></td>
                                </tr>
                                <tr>
                                    <td>Surname : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="studSurname" value="" size="20" /></td>
                                </tr>
                                <tr>
                                    <td>Academic Status : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="acadSt">
                                            <%for (int k = 0; k < db2.q.size(); k++) {%>
                                            <option value="<%=acad.get(k).getAcademicID()%>">
                                                <%=acad.get(k).getAcademicName()%>
                                            </option>
                                        <%}%>                                </select></td>
                                </tr>
                                <tr>
                                    <td>Education Status : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="eduSt">
                                            <%for (int k = 0; k < db3.q.size(); k++) {%>
                                            <option value="<%=educ.get(k).getEducationID()%>">
                                                <%=educ.get(k).getEducationName()%>
                                            </option>
                                        <%}%>                                </select></td>
                                </tr>
                                <tr>
                                    <td>Remark : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="remark">
                                            <%for (int k = 0; k < db4.q.size(); k++) {%>
                                            <option value="<%=remark.get(k).getRemarkID()%>">
                                                <%=remark.get(k).getRemark()%>
                                            </option>
                                        <%}%>                                </select></td>
                                </tr>
                                <tr>
                                    <td>GHS Type : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="ghsT">
                                            <%for (int k = 0; k < db5.q.size(); k++) {%>
                                            <option value="<%=ghsType.get(k).getGhsTypeID()%>">
                                                <%=ghsType.get(k).getGhsType()%>
                                            </option>
                                        <%}%>                                </select></td>
                                </tr>
                                <tr>
                                    <td>GHS Language : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="ghsL">
                                            <%for (int k = 0; k < db6.q.size(); k++) {%>
                                            <option value="<%=ghsLan.get(k).getGhsLanguageID()%>">
                                                <%=ghsLan.get(k).getGhsLanguage()%>
                                            </option>
                                        <%}%>                                </select></td>
                                </tr>
                                <tr>
                                    <td>Entering Academic Year : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="enterY">
                                            <option value="0">Select One</option>
                                            <%for (int k = 0; k < db14.q.size(); k++) {%>
                                            <option value="<%=year.get(k).getID()%>">
                                                <%=year.get(k).getYear()%>
                                            </option>
                                        <%}%>                                </select></td>
                                </tr>
                                <tr>
                                    <td>Entering Order Number : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="prikaz" value="" size="20" /></td>
                                </tr>
                                <tr>
                                    <td align="left">Country : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="country">
                                            <option value="0">Select One</option>
                                            <%for (int k = 0; k < db7.q.size(); k++) {%>
                                            <option value="<%=country.get(k).getCountryID()%>">
                                                <%=country.get(k).getCountryName()%>
                                            </option>
                                        <%}%>                                </select></td>
                                </tr>
                                <tr>
                                    <td align="left">Oblast : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="oblast">
                                            <option value="0">Select One</option>
                                            <%for (int ob = 0; ob < db9.q.size(); ob++) {%>
                                            <option value="<%=oblast.get(ob).getOblID()%>">
                                                <%=oblast.get(ob).getOblName()%>
                                            </option>
                                        <%}%>   </select></td>
                                </tr>
                                <tr>
                                    <td align="left">Region\Town : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="region">
                                            <option value="0">Select One</option>
                                            <%for (int reg = 0; reg < db8.q.size(); reg++) {%>
                                            <option value="<%=region.get(reg).getRegionID()%>">
                                                <%=region.get(reg).getRegionName()%>
                                            </option>
                                        <% }%></select></td>
                                </tr>
                                <tr>
                                    <td align="left">Birthplace : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="birthplace" value="" size="20" /></td>
                                </tr>
                                <tr>
                                    <td align="left">Birth date : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="birthdate" value="" size="20" /> (yyyy/mm/dd)</td>
                                </tr>
                                <tr>
                                    <td align="left">Gender : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="gender">
                                            <option value="0">Select One</option>
                                            <%for (int k = 0; k < db10.q.size(); k++) {%>
                                            <option value="<%=gender.get(k).getGenderID()%>">
                                                <%=gender.get(k).getGender()%>
                                            </option>
                                        <%}%>                                </select></td>
                                </tr>
                                <tr>
                                    <td align="left">Passport № : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="passport" value="" size="20" /></td>
                                </tr>
                                <tr>
                                    <td align="left">Permanent Address: </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="permadr" value="" size="20" /></td>
                                </tr>
                                <tr>
                                    <td align="left">Current Address: </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="curradr" value="" size="20" /></td>
                                </tr>
                                <tr>
                                    <td>Department : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="deprt">
                                            <option value="0">Select One</option>
                                            <%for (int k = 0; k < db1.q.size(); k++) {%>
                                            <option value="<%=depList.get(k).getID()%>">
                                                <%=depList.get(k).getDprt_Code()%>
                                            </option>
                                        <%}%>                                </select></td>
                                </tr>
                                <tr>
                                    <td>Group : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="group">
                                            <option value="0">Select One</option>
                                            <%for (int i = 0; i < db.q.size(); i++) {%>
                                            <option value="<%=list.get(i).getID()%>">
                                                <%=list.get(i).getGr_Name()%>
                                            </option>
                                        <%}%>                                </select></td>
                                </tr>
                                <tr>
                                    <td align="left">E-mail : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="email" value="" size="20" /></td>
                                </tr>
                                <tr>
                                    <td align="left">Nationality : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="nation">
                                            <option value="0">Select One</option>
                                            <%for (int k = 0; k < db12.q.size(); k++) {%>
                                            <option value="<%=nation.get(k).getNationalityID()%>">
                                                <%=nation.get(k).getNationality()%>
                                            </option>
                                        <%}%></select></td>
                                </tr>
                                <tr>
                                    <td align="left">Graduated School : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="ghsSchool">
                                            <option value="0">Select One</option>
                                            <%for (int s = 0; s < db15.q.size(); s++) {%>
                                            <option value="<%=school.get(s).getSchoolID()%>">
                                                <%=school.get(s).getSchoolName()%>
                                            </option>
                                        <%}%></select></td>
                                </tr>
                                <tr>
                                    <td align="left">Awards : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="awards" value="" size="20"></td>
                                </tr>
                                <tr>
                                    <td align="left">Attestat : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="attestat">
                                            <option value="0">Select One</option>
                                            <option value="1">Yes</option>
                                            <option value="2">No</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td align="left">Phone Number : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="phone" value="" size="20"></td>
                                </tr>
                                <tr>
                                    <td align="left">Blood Group: </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="blood">
                                            <option value="0">Select One</option>
                                            <%for (int k = 0; k < db13.q.size(); k++) {%>
                                            <option value="<%=blood.get(k).getBloodID()%>">
                                                <%=blood.get(k).getBlood()%>
                                            </option>
                                        <%}%>                                </select></td>
                                </tr>
                                <tr>
                                    <td align="left">Account :</td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><select name="account">
                                            <option value="0">Select One</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td align="left">Password : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="password" size="20" value="<%=pass.getNext()%>"/></td>
                                </tr>
                                <tr>
                                    <td align="left">Note 1 : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="note1" size="20"/></td>
                                </tr>
                                <tr>
                                    <td align="left">Note 2 : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="note2" size="20"/></td>
                                </tr>
                                <tr>
                                    <td align="left">Note 3 : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="note3" size="20"/></td>
                                </tr>
                                <tr>
                                    <td align="left">Note 4 : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="note4" size="20"/></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr><th colspan="2"></th>
                        <td class="css1">
                            Mother Info
                        </td>
                        <td>
                            <table class="labelForm">
                                <tr>
                                    <td>Name : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="motherName" value="" size="20" /></td>
                                </tr>
                                <tr>
                                    <td>Surname : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="motherSurname" value="" size="20" /></td>
                                </tr>
                                <tr>
                                    <td>Work Place : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="motherWork" value="" size="20" /></td>
                                </tr>
                                <tr>
                                    <td align="left">Phone Number: </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="motherPh" value="" size="20"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr><th colspan="2"></th>
                        <td class="css1">
                            Father Info
                        </td>
                        <td>
                            <table class="labelForm">
                                <tr>
                                    <td>Name : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="fatherName" value="" size="20" /></td>
                                </tr>
                                <tr>
                                    <td>Surname : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="fatherSurname" value="" size="20" /></td>
                                </tr>
                                <tr>
                                    <td>Work Place : </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="fatherWork" value="" size="20" /></td>
                                </tr>
                                <tr>
                                    <td align="left">Phone Number: </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" name="fatherPh" value="" size="20"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr align="center"><th colspan="2"></th>
                        <td><input type="submit" name="Submit" value="Submit"></td>

                        <td align="left">
                            <div id='studentForm_errorloc' class='error_strings'>
                            </div>
                        </td>
                    </tr>
                </table>
            </form>
        </center>

        <script language="JavaScript" type="text/javascript">
            //You should create the validator only after the definition of the HTML form
            var frmvalidator  = new Validator("studentForm");
            frmvalidator.EnableOnPageErrorDisplaySingleBox();
            frmvalidator.EnableMsgsTogether();

            frmvalidator.addValidation("rollnum","req","Please enter ID");
            frmvalidator.addValidation("rollnum","maxlen=11","Max length for ID is 11");
            frmvalidator.addValidation("rollnum","numeric","ID can contain only numbers");

            frmvalidator.addValidation("studName","req","Please enter First Name");
            frmvalidator.addValidation("studName","maxlen=20",	"Max length for First Name is 20");
            frmvalidator.addValidation("studName","alpha_s","Name can contain alphabetic chars only");

            frmvalidator.addValidation("studSurname","req","Please enter Last Name");
            frmvalidator.addValidation("studSurname","maxlen=25","For Last Name, Max length is 25");

            frmvalidator.addValidation("enterY","dontselect=0","Entering Academic Year: Please Select one option");
            frmvalidator.addValidation("country","dontselect=0","Country: Please Select one option");
            frmvalidator.addValidation("oblast","dontselect=0","Oblast: Please Select one option");
            frmvalidator.addValidation("region","dontselect=0","Region: Please Select one option");

            frmvalidator.addValidation("birthdate","req","Please enter Birth Date");
            frmvalidator.addValidation("birthdate","regexp=[0,9]");
            
            frmvalidator.addValidation("gender","dontselect=0","Gender: Please Select one option");
            frmvalidator.addValidation("deprt","dontselect=0","Department: Please Select one option");
            frmvalidator.addValidation("group","dontselect=0","Group: Please Select one option");

            frmvalidator.addValidation("email","maxlen=50");
            frmvalidator.addValidation("email","req");
            frmvalidator.addValidation("email","email");

            frmvalidator.addValidation("nation","dontselect=0","Nationality: Please Select one option");
            frmvalidator.addValidation("ghsSchool","dontselect=0","Graduated School: Please Select one option");
            frmvalidator.addValidation("attestat","dontselect=0","Attestat: Please Select one option");

            frmvalidator.addValidation("phone","maxlen=50");
            frmvalidator.addValidation("phone","numeric");

            frmvalidator.addValidation("blood","dontselect=0","Blood Group: Please Select one option");
            frmvalidator.addValidation("account","dontselect=0","Account: Please Select one option");
            
            function DoCustomValidation()
            {
                var frm = document.forms["studentForm"];
                if(frm.studName.value == 'Bob')
                {
                    sfm_show_error_msg("Bob, you can't submit this form. Go away! ");
                    return false;
                }
                else
                {
                    return true;
                }
            }

            frmvalidator.setAddnlValidationFunction("DoCustomValidation");
        </script>

        <%       } catch (SQLException e) {
                throw new ServletException("Your query is not working", e);
            }
            db.close();
            db1.close();
            db2.close();
            db3.close();
            db4.close();
            db5.close();
            db6.close();
            db7.close();
            db8.close();
            db9.close();
            db10.close();
            db11.close();
            db12.close();
            db13.close();
            db14.close();
            db15.close();
        %>
    </body>
</html>