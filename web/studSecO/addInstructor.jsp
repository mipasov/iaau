<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Group" %>
<%@ page language="Java" import = "iaau.Department" %>
<%@ page language="Java" import = "iaau.Level" %>
<%@ page language="Java" import = "iaau.Status" %>
<%@ page language="Java" import = "iaau.RandPass" %>
<%@ page language="Java" import = "iaau.Blood" %>
<%@ page language="Java" import = "iaau.Nationality" %>
<%@ page language="Java" import = "iaau.Gender" %>
<%@ page language="Java" import = "iaau.Country" %>
<%@ page language="Java" import = "iaau.Region" %>
<%@ page language="Java" import = "iaau.Oblast" %>
<%@ page language="Java" import="iaau.Faculty" %>

<jsp:useBean id="db" scope="request" class="iaau.DbGroup" />
<jsp:useBean id="db1" scope="request" class="iaau.DbDepartment" />
<jsp:useBean id="db2" scope="request" class="iaau.DbLevel" />
<jsp:useBean id="db3" scope="request" class="iaau.DbStatus" />
<jsp:useBean id="db4" scope="request" class="iaau.DbBlood" />
<jsp:useBean id="db5" scope="request" class="iaau.DbNationality" />
<jsp:useBean id="db6" scope="request" class="iaau.DbGender" />
<jsp:useBean id="db7" scope="request" class="iaau.DbCountry" />
<jsp:useBean id="db8" scope="request" class="iaau.DbRegion" />
<jsp:useBean id="db9" scope="request" class="iaau.DbOblast" />
<jsp:useBean id="db10" scope="request" class="iaau.DbFaculty"/>

<html>
<head>
    <link rel='stylesheet' href='../style/sis_style.css'>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Add Instructor</title>
    <style type='text/css'>
.css1 {background: #456; border: 0; font-family: Verdana; font-size: 12px;}
.css2 {width: 35pt; font-family: Verdana; font-size: 10px}
td.test {vertical-align: middle; padding: 2px 10px 2px 8px;}
</style>
</head>
<body>
<%
RandPass pass = new RandPass();
db.connect();
db1.connect();
db2.connect();
db3.connect();
db4.connect();
db5.connect();
db6.connect();
db7.connect();
db8.connect();
db9.connect();db10.connect();
try{
    db.execSQL();
    db1.execSQL();
    db2.execSQL();
    db3.execSQL();
    db4.execSQL();
    db5.execSQL();
    db6.execSQL();
    db7.execSQL();
    db8.execSQL();
    db9.execSQL();db10.execSQL();

    ArrayList<Group> group = db.getArray();
    ArrayList<Department> depList = db1.getArray();
    ArrayList<Level> levelList = db2.getArray();
    ArrayList<Status> statusList = db3.getArray();
    ArrayList<Blood> blood = db4.getArray();
    ArrayList<Nationality> nation = db5.getArray();
    ArrayList<Gender> gender = db6.getArray();
    ArrayList<Country> country = db7.getArray();
    ArrayList<Region> region = db8.getArray();
    ArrayList<Oblast> oblast = db9.getArray();
    ArrayList<Faculty> faculty = db10.getArray();
%>
<table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
    <!--DWLayoutTable-->
    <tr>
        <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;New Instructor</td>
    </tr>
    <tr>
        <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;</td>
    </tr>
</table>
<br>

<center> <form name="addInstructor" action="addInstructor" method="POST">
<table class="labelForm" border="2">
<tr><th colspan="2"></th></tr>
<td class="css1">
    Instructor Info
</td>
<td>
<table class="labelForm">
<tr>
    <td align="left">ID : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type="text" name="rollnum" value="" size="20" /></td>
</tr>
<tr>
    <td align="left">Name : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type="text" name="instName" value="" size="20" /></td>
</tr>
<tr>
    <td align="left">Surname : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type="text" name="instSurname" value="" size="20" /></td>
</tr>
<tr>
    <td align="left">Country : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><select name="country">
            <option value="">Select Country</option>
            <%for (int k = 0; k < db7.q.size(); k++) {%>
            <option value="<%=country.get(k).getCountryID()%>">
                <%=country.get(k).getCountryName()%>
            </option>
    <%}%>                                </select></td>
</tr>
<tr>
<td align="left">Oblast : </td>
<td>&nbsp;&nbsp;&nbsp;</td>
<td><select name="oblast">
    <%for (int k = 0; k < db9.q.size(); k++){%>
    <option value="<%=oblast.get(k).getOblID()%>"><%=oblast.get(k).getOblName()%></option><%}%>
</select></td>
</tr>
<tr>
    <td align="left">Region\Town : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><select name="region">
        <%for (int k = 0; k < db8.q.size(); k++){%>
            <option value="<%=region.get(k).getRegionID()%>"><%=region.get(k).getRegionName()%></option>
            <%}%>  </select></td>
</tr>
<tr>
    <td align="left">Birthplace : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type="text" name="birthplace" value="" size="20" /></td>
</tr>
<tr>
    <td align="left">Birth date : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type="text" name="birthdate" value="" size="20" />(yyyy/mm/dd)</td>
</tr>
<tr>
    <td align="left">Gender : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><select name="gender">
            <option value="">Select Gender</option>
            <%for (int k = 0; k < db6.q.size(); k++) {%>
            <option value="<%=gender.get(k).getGenderID()%>">
                <%=gender.get(k).getGender()%>
            </option>
    <%}%>                                </select></td>
</tr>
<tr>
    <td align="left">Passport № : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type="text" name="passport" value="" size="20" /></td>
</tr>
<tr>
    <td align="left">Permanent Address: </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type="text" name="permadr" value="" size="20" /></td>
</tr>
<tr>
    <td align="left">Current Address: </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type="text" name="curradr" value="" size="20" /></td>
</tr>
<tr>
    <td align="left">E-mail : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type="text" name="email" value="" size="20" /></td>
</tr>
<tr>
    <td align="left">Nationality : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><select name="nation">
            <option value="">Select Nationality</option>
            <%for (int k = 0; k < db5.q.size(); k++) {%>
            <option value="<%=nation.get(k).getNationalityID()%>">
                <%=nation.get(k).getNationality()%>
            </option>
    <%}%>                                </select></td>
</tr>
<tr>
    <td align="left">Phone Number: </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type="text" name="phone" value="" size="20"></td>
</tr>
<tr>
    <td align="left">Blood Group: </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><select name="blood">
            <option value="">Select Blood Group</option>
            <%for (int k = 0; k < db4.q.size(); k++) {%>
            <option value="<%=blood.get(k).getBloodID()%>">
                <%=blood.get(k).getBlood()%>
            </option>
    <%}%>                                </select></td>
</tr>
<tr>
    <td align="left">Password : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type="text" name="password" value="<%=pass.getNext()%>" size="20" /></td>
</tr>
<tr>
    <td align="left">Faculty : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><select name="faculty">
        <option value="1">Select Faculty</option>
        <%for (int k = 0; k< db10.q.size(); k++){%>
        <option value="<%=faculty.get(k).getFacultyId()%>">
            <%=faculty.get(k).getFacultyName()%></option><%}%>
    </select></td>
</tr>
<tr>
    <td align="left">Department : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><select name="dept">
        <option value="1">Select Department</option>
        <%for (int k = 0; k< db1.q.size(); k++){%>
        <option value="<%=depList.get(k).getID()%>">
            <%=depList.get(k).getDprt_Code()%></option><%}%>
    </select></td>
</tr>
<tr>
    <td align="left">Group : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><select name="group">        
        <%for (int k = 0; k< db.q.size(); k++){%>
        <option value="<%=group.get(k).getID()%>">
            <%=group.get(k).getGr_Name()%></option><%}%>
    </select></td>
</tr>

<tr>
    <td align="left">Level : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><select name="level">
            <%for (int k = 0; k < db2.q.size(); k++) {%>
            <option value="<%=levelList.get(k).getLevelID()%>">
                <%=levelList.get(k).getLevelName()%>
            </option>
    <%}%>                                </select></td>
</tr>
<tr>
    <td align="left">Status : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><select name="status">
            <%for (int i = 0; i < db3.q.size(); i++) {%>
            <option value="<%=statusList.get(i).getStatusID()%>">
                <%=statusList.get(i).getStatusName()%>
            </option>
    <%}%>                                </select></td>
</tr>

<tr>
    <td align="left">Role : </td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><select name="role">
            <option value="">Select One</option>
            <option value="instructor">Instructor</option>
            <option value="supervisor">Supervisor</option>
            <option value="secretary">Secretary</option>
            <option value="studSecO">Student Service</option>
            <option value="rector">Rector</option>
            <option value="dean">Dean</option>
            <option value="hod">HOD</option>            
    </select></td>
</tr>
</table>
</td>
<tr align="center"><td><input type=reset value=Reset></td>
<td><input type="submit" value="Add Instructor" name="addInst" /></td>
</tr>
</table>
</form></center>
<%       }catch(SQLException e) {
    throw new ServletException("Your query is not working", e);
}
%>
<%
db.close();
db1.close();
db2.close();
db3.close();
db4.close();
db5.close();
db6.close();
db7.close();
db8.close();db9.close();db10.close();
%>
</body>
</html>