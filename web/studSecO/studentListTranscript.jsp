<%@ page language="Java" import="java.sql.*"%>
<%@ page language="Java" import = "java.util.*"%>
<%@ page language="Java" import = "iaau.Student"%>
<%@ page language="Java" import = "iaau.Subjects"%>

<jsp:useBean id="db" scope="request" class="iaau.DbStudent" />
<html>
    <head>
        <title>Transcipt-Student List</title>
        <link rel='stylesheet' href='../style/sis_style.css'>
        <script type="text/javascript" src="../scripts/table.js"></script>
    </head>
    <body>
        <center>
            <%
        db.connect();

        //	String username = (String)session.getAttribute("user");
        try {
            if (request.getParameter("group") != null) {
                db.execSQL_GR_All(request.getParameter("group"));
            } else {
                db.execSQL_GR_All("0");
            }
            ArrayList<Student> list = db.getArray();
            %>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Transcript</td>
                </tr>
                <tr>
                    <td width="100%" class="title" height="20" bgcolor="#f9F6ee" valign="top">&nbsp;Student List</td>
                </tr>
            </table>
            <br>
            <form name="stud" method="post" action="Transcript">
                <table width="60%" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold"
                       class="example table-stripeclass:alternate">
                           <%if (db.q.size() != 0) {%>
                    <thead>
                        <tr height="40">
                            <th> <input type="hidden" name="type" value="1" readonly="readonly" /></th>
                            <th width="10%" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >#</th>
                            <th align="center" width="40%" bgcolor="#eeeeee" valign="middle" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Name
                            </th>
                            <th align="center" width="50%" valign="middle" bgcolor="#eeeeee" class="textBold"
                                style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                >Surname
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                    for (int i = 0; i < db.q.size(); i++) {
                        %>
                        <tr height="30">
                            <td></td>
                            <td width="10%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><%= i + 1%></td>
                            <td align="center" width="40%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                                ><a href="nvTranscript?sid=<%= list.get(i).getID()%>"><%= list.get(i).getName()%></a>
                            </td>
                            <td align="center" width="50%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                                style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                                border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                                border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                                <%= list.get(i).getSurname()%>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                        <table align="center" class="big">
                            <tr height="30">
                                <td>&nbsp;</td>  <td>Total :<input  name="total" type="hidden" value="<%=db.q.size()%>"/></td><td><%=list.size()%> Students </td>
                            </tr>
                        </table>
                        <%} else {%>
                        <tr>
                            <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                            border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9; border-right-color: #d9d9d9;
                            border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            Sorry there is no record for you on this time </td>
                        </tr>
                        <%  }%>
                        <tr><td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </center>
    </body>
</html>
<%       } catch (SQLException e) {
            throw new ServletException("Your query is not working", e);
        }
        db.close();
%>