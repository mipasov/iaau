<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page language="Java" import="java.sql.*" %>
<%@ page language="Java" import = "java.util.*" %>
<%@ page language="Java" import = "iaau.Stud_Less" %>
<%@ page language="Java" import = "iaau.studYears" %>
<%@ page language="Java" import = "iaau.Semester" %>
<%@ page language="Java" import = "iaau.Exam" %>

<jsp:useBean id="db" scope="request" class="iaau.DbStud_Less"/>
<jsp:useBean id="grf" scope="request" class="iaau.DbStud_Less"/>
<jsp:useBean id="db1" scope="request" class="iaau.DbExam"/>
<jsp:useBean id="db2" scope="request" class="iaau.DbStudYears"/>
<jsp:useBean id="db3" scope="request" class="iaau.DbSemester"/>
<jsp:useBean id="aver" scope="request" class="iaau.ExamAverage"/>
<html>
    <head>
        <title>Examination</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../style/sis_style.css" rel="stylesheet" type="text/css">
            <%
db.connect();
db1.connect();
db2.connect();
db3.connect();
aver.connect();
grf.connect();

HttpSession sess = request.getSession();
String subject = (String) request.getParameter("subjID");
String sem = (String) sess.getAttribute("semID");
String year = (String) sess.getAttribute("yearID");
String exam = (String) sess.getAttribute("examID");
try {
    db.execSQL_Exam(subject, year, sem);
    db1.execSQL_currExam();
    db2.execSQL_currYear();
    db3.execSQL_currSem();
    grf.execSQL_Status(subject, year, sem);

    ArrayList<Stud_Less> list = db.getArray();
    ArrayList<Stud_Less> grflist = grf.getArray();
    ArrayList<Exam> examList = db1.getArray();
    ArrayList<studYears> yearList = db2.getArray();
    ArrayList<Semester> semList = db3.getArray();
%>
    </head>
    <body>
        <center>
            <table width="100%" height="45" style="border-bottom-width: 0px; border-bottom-color: #000000;" align="center" border="0" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
        <tr>
            <td width="100%" class="title" bgcolor="#ebe1c6" height="25" valign="middle">&nbsp; &nbsp;Examination</td>
        </tr>
        <tr>
            <td height="20" bgcolor="#f9F6ee" valign="top" class="textBold">&nbsp;Student List </td>
        </tr>
    </table>
    <br>
            <form name="examUpdate" action="examInsert" method="POST">
                <table width="60%" class="label"><tr>
                        <td><input type="hidden" name="subjID" value="<%=subject%>" /></td>
                        <td> Year : <select name="year">
                                <%for (int k = 0; k < db2.q.size(); k++) {%>
                                <option value="<%=yearList.get(k).getID()%>">
                                    <%=yearList.get(k).getYear()%>
                                </option>
                            <%}%>                                </select>
                        </td>
                        <td> Semester : <select name="sem">
                                <%for (int k = 0; k < db3.q.size(); k++) {%>
                                <option value="<%=semList.get(k).getID()%>">
                                    <%=semList.get(k).getSemester()%>
                                </option>
                        <%}%>                                </select> </td>
                        <td> Exam : <select name="exam">
                                <%for (int k = 0; k < db1.q.size(); k++) {%>
                                <option value="<%=examList.get(k).getID()%>">
                                    <%=examList.get(k).getExam()%>
                                </option>
                        <%}%>                                </select></td>
                    </tr>
                </table>
                <table width="80%" height="124" align="center" border="0" cellpadding="0" cellspacing="0" class="textBold">
                    <% if (db.q.size() != 0) {%>
                    <tr height="40">
                        <td width="130" align="center" bgcolor="#eeeeee" valign="middle" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;"
                        >Name</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Surname</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Group</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Subject Status</td>
                        <td align="center" valign="middle" bgcolor="#eeeeee" class="textBold"
                            style="border-top-width: 1px;   border-bottom-width: 0px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        Mark</td>

                    </tr>
                    <%if (exam.equals("4")){
                        for (int i = 0; i < grf.q.size(); i++){ %>
                        <tr height="30">
                        <td width="20%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            <input type="hidden" name="SubjLessID" value="<%=grflist.get(i).getID()%>"/>
                        <%=grflist.get(i).getStudName()%></td>
                        <td align="center" width="30%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=grflist.get(i).getStudSurname()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=grflist.get(i).getGrpName()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=grflist.get(i).getStatus()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <input type="text" name="examMark" value="" size="3"/></td>
                    </tr>
                        <% } %>
                        <table align="center" class="big">
                        <tr><td>Total : <input  name="total" type="hidden" value="<%=grf.q.size()%>"/><%=grf.q.size()%>  Students</td></tr>

                        <tr> <td><input type="submit" value="Submit" name="submit" /></td></tr>
                        </table>
                        <% } else { %>

                    <%for (int i = 0; i < db.q.size(); i++) {
                    if ((exam.equals("1"))||(exam.equals("2"))){%>
                    <tr height="30">
                        <td width="20%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            <input type="hidden" name="SubjLessID" value="<%=list.get(i).getID()%>"/>
                        <%=list.get(i).getStudName()%></td>
                        <td align="center" width="30%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getStudSurname()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getGrpName()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getStatus()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <input type="text" name="examMark" value="" size="3" /></td>
                    </tr>
                    <% }// if
                    else if (exam.equals("3")){ if(aver.calculateAverage(list.get(i).getID(),
                    Integer.parseInt(sem),Integer.parseInt(year),Integer.parseInt(subject))<=49.5){ %>
                    <tr height="30">
                        <td width="20%" align="center" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                            <input type="hidden" name="SubjLessID" value="<%=list.get(i).getID()%>"/>
                        <%=list.get(i).getStudName()%></td>
                        <td align="center" width="30%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getStudSurname()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getGrpName()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <%=list.get(i).getStatus()%></td>
                        <td align="center" width="15%" valign="middle" onMouseOver="this.bgColor='#f9f9f9';" onMouseOut="this.bgColor='';"
                            style="border-top-width: 0px;   border-bottom-width: 1px; border-left-width: 0px; border-right-width: 1px;
                            border-right-style: solid;      border-bottom-style: solid;     border-top-style: solid; border-top-color: #d9d9d9;
                        border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;     border-left-style: solid;">
                        <input type="text" name="examMark" value="" size="3" /></td>
                    </tr>
                    <% }
                    }  }%>
                    <table align="center" class="big">
                        <tr><td>Total : <input  name="total" type="hidden" value="<%=db.q.size()%>"/><%=db.q.size()%>  Students</td></tr>

                        <tr> <td><input type="submit" value="Submit" name="submit" /></td></tr>
                    </table>
                    <% } %>

                    <% } else { %>
                    <tr>
                        <td height="60" colspan="6" class="warning" align="center" style="border-top-width: 1px;
                            border-bottom-width: 1px; border-left-width: 1px; border-right-width: 1px; border-right-style: solid;
                            border-bottom-style: solid;  border-top-style: solid; border-top-color: #d9d9d9;
                            border-right-color: #d9d9d9; border-bottom-color: #d9d9d9;      border-left-color: #d9d9d9;
                        border-left-style: solid;">
                        Sorry there is no record for you on this time </td>
                    </tr>
                    <%
                    }//else
                    %>
                </table>
                <table align="center" width="80%" border="0" cellspacing="1" cellpadding="1" class="link">
                    <tr>
                        <td><a href="signatureList?subjID=<%=subject%>">Signature List</a></td>
                        <td><a href="midtermMarks?subjID=<%=subject%>&exam=1">Midterm</a></td>
                        <td><a href="marksModify.jsp?subjID=<%=subject%>&exam=1">modify</a></td>
                    </tr>
                    <tr>
                        <td><a href="resultList?subjID=<%=subject%>">Result List</a></td>
                        <td><a href="midtermMarks?subjID=<%=subject%>&exam=2">Final</a></td>
                        <td><a href="marksModify.jsp?subjID=<%=subject%>&exam=2">modify</a></td>
                    </tr>
                    <tr>
                        <td><a href="viewAverage?subjID=<%=subject%>">Average Report</a></td>
                        <td><a href="midtermMarks?subjID=<%=subject%>&exam=3">Make-up</a></td>
                        <td><a href="marksModify.jsp?subjID=<%=subject%>&exam=3">modify</a></td>
                    </tr>
                    <% if (exam.equals("4")) {%>
                    <tr>
                        <td>&nbsp;</td>
                        <td><a href="midtermMarks?subjID=<%=subject%>&exam=4">Grand Final</a></td>
                        <td><a href="marksModify.jsp?subjID=<%=subject%>&exam=4">modify</a></td>
                    </tr>
                    <% } %>
                </table>
            </form>
        </center>
    </body>
</html>
<%       } catch (SQLException e) {
    throw new ServletException("Your query is not working", e);
}
db.close();
db1.close();
db2.close();
db3.close();
aver.close();
grf.close();
%>
